
<?php __("A new account has been created."); ?>


<?php echo __("Account type", true) . __(":", true); ?> <?php __(ucfirst($data['role'])); ?>

<?php echo __("Name", true) . __(":", true); ?> <?php echo $data['name']; ?>

<?php echo __("First name", true) . __(":", true); ?> <?php echo $data['firstname'];

if ($data['role'] == 'teacher') { ?>
<?php echo __("University", true) . __(":", true); ?> <?php echo $data['university']; ?>

<?php
} else if ($data['role'] == 'visitor') { ?>
<?php echo __("Structure", true) . __(":", true); ?> <?php echo $data['structure']; ?>

<?php echo __("Function", true) . __(":", true); ?> <?php echo $data['function']; ?>

<?php
} ?>


<?php __("This account will remain inactive until you check it and decide its activation."); ?>
