<?php

/**
 * {app}/models/diploma.php
 */
class Diploma extends AppModel {

    var $name = 'Diploma';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $hasMany = array(
        'StudentDiploma' => array(
            'className' => 'Student',
            'foreignKey' => 'diploma_id'
        )
    );


    /**
     * Liste des diplômes
     *
     * @return <array>
     */
    function getDiplomasList() {

        // Chargement de la liste
        $data = $this->find('all', array(
                    'fields' => array(
                        'id',
                        'name',
                        'level'
                    ),
                    'order' => 'id  ASC',
                    'recursive' => -1
                ));
        // Assemblage / traduction
        $diplomasList = array();
        foreach ($data as $key => $value) {
            $diplomasList[$value['Diploma']['id']] = __($value['Diploma']['name'], true)
                    . " (" . __($value['Diploma']['level'], true) . ")";
        }
        // Changement de clé (liste déroulante)
        if (isset($diplomasList[10])) {
            $diplomasList[99] = $diplomasList[10];
            unset($diplomasList[10]);
        }
        // Retour
        return $diplomasList;

    }


}

