<?php

/**
 * {app}/models/te_group.php
 */
class TeGroup extends AppModel {

    var $name = 'TeGroup';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array(
        'Containable'
    );
    // Relations
    var $belongsTo = array(
        'University' => array(
            'className' => 'University',
            'foreignKey' => 'university_id'
        ),
        'Teacher' => array(
            'className' => 'Teacher',
            'foreignKey' => 'teacher_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'group' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a group code.", true),
                    'last' => true
                ),
                'alphaNumeric' => array(
                    'rule' => array('alphaNumeric'),
                    'message' => __d('validation', "Group code must only contain letters and numbers, without accentuated characters.", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This group code already exists: you may want to use “Merge groups”.", true)
                )
            )
        );

        parent::__construct();
    }


    /**
     * Validation optionnelle : on ne fusionne pas un groupe avec lui même
     *
     * @return <bool>
     */
    function fusionCheck() {

        if ($this->data['TeGroup']['group'] == $this->data['TeGroup']['prevGroupCode'])
            return false;
        else
            return true;
    }


    /**
     * Modification des règles de validation pour la fusion de groupes
     *
     */
    function allowExistingGroup() {

        // Suppression de la règle d'unicité
        unset($this->validate['group']['isUnique']);
        // Ajout de la vérification des noms
        $rule['group']['check'] = array(
            'rule' => array('fusionCheck'),
            'message' => __d('validation', "These group codes are identical.", true)
        );
        $this->validate['group'] = $this->validate['group'] + $rule['group'];
    }


    /**
     * Modification des règles de validation pour la recherche par groupe
     *
     */
    function validateTeacherEdit() {

        // Suppression totale des règles
        unset($this->validate['group']);
    }


    /**
     * Existence / création / maj d'un groupe
     *
     * @param <string> $universityId
     * @param <string> $groupCode
     * @param <string> $teacherId (NULL -> pas de changement d'ens. resp.)
     * @param <bool> $create (false -> pas de création si le code groupe n'existe pas)
     * @return <mixed>
     */
    function checkGroup($universityId, $groupCode, $teacherId = NULL, $create = false) {

        $group = $this->find('first', array(
            'conditions' => array(
                'TeGroup.university_id' => $universityId,
                'TeGroup.group' => $groupCode
            )
        ));

        #$this->log($group, 'debug');
        if (empty($group)) {
            if ($create) {
                $data = array(
                    'university_id' => $universityId,
                    'teacher_id' => $teacherId,
                    'group' => $groupCode
                );
                $this->create($data);
                if ($this->save(array('validate' => false)))
                    return $this->id;
            }
            return false;
        } else if ($teacherId && ($teacherId != $group['TeGroup']['teacher_id'])) {
            $group['TeGroup']['teacher_id'] = $teacherId;
            $this->save($group, array(
                'validate' => false,
                'fieldList' => array('teacher_id')
            ));
        }
        return true;
    }


    /**
     * Infos spécifiques à un groupe
     *
     * @param <string> $group
     * @return <array>
     */
    function getGroupInfo($group) {

        return $this->find('first', array(
                    'conditions' => array('TeGroup.group' => $group),
                    'fields' => array(
                        'id',
                        'group',
                        'teacher_id'
                    ),
                    'contain' => array(
                        'Teacher' => array(
                            'fields' => array(
                                'id',
                                'name',
                                'firstname'
                            ),
                            'User' => array(
                                'fields' => array(
                                    'id',
                                    'photo'
                                )
                            )
                        )
                    )
                ));
    }


    /**
     * Liste des groupes par université
     *
     * @param <string> $universityId
     * @return <array>
     */
    function getGroupsList($universityId) {

        return $this->find('list', array(
                    'conditions' => array('TeGroup.university_id' => $universityId),
                    'order' => array('TeGroup.group'),
                    'recursive' => -1,
                    'fields' => array(
                        'id',
                        'group'
                    )
                ));
    }


}

