<?php

/**
 * {app}/models/portfolio.php
 */
class Portfolio extends AppModel {

    var $name = 'Portfolio';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array(
        #'Containable',
        'CleanData'
    );
    // Relations
    var $hasMany = array(
        'PoAbroad' => array(
            'className' => 'PoAbroad',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoCertificate' => array(
            'className' => 'PoCertificate',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoAssessAnswer' => array(
            'className' => 'PoAssessAnswer',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoAssessCriteria' => array(
            'className' => 'PoAssessCriteria',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoAssessUser' => array(
            'className' => 'PoAssessUser',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoContact' => array(
            'className' => 'PoContact',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoCultural' => array(
            'className' => 'PoCultural',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoDocument' => array(
            'className' => 'PoDocument',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoEmployment' => array(
            'className' => 'PoEmployment',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoObjective' => array(
            'className' => 'PoObjective',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
        'PoValuable' => array(
            'className' => 'PoValuable',
            'foreignKey' => 'portfolio_id',
            'dependent' => true
        ),
    );
    var $belongsTo = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'student_id'
        ),
        'Language' => array(
            'className' => 'Language',
            'foreignKey' => 'language_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'language_id' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'message' => __d('validation', "Please select a language.", true)
            )
        );

        parent::__construct();

    }


    /**
     * Chargement d'un profil
     *
     * @param <string> $portfolioId
     * @return <array>
     */
    function getPassport($portfolioId, $valuable = false) {

        $this->Behaviors->attach('Containable');
        $contain = array(
            'PoCertificate' => array(
                'fields' => array(
                    'id',
                    'portfolio_id',
                    'certificate_id',
                    'date',
                    'organism',
                    'certificate_info',
                    'listening',
                    'reading',
                    'speak_int',
                    'speak_prod',
                    'writing'
                ),
                'Certificate' => array(
                    'fields' => array(
                        'id',
                        'certification_id'
                    ),
                    'Certification' => array(
                        'fields' => array(
                            'id',
                            'name',
                            'description'
                        )
                    )
                ),
                'order' => 'PoCertificate.date DESC'
            )
        );
        if ($valuable) {
            $contain['PoValuable'] = array(
                'fields' => array(
                    'id',
                    'portfolio_id',
                    'name',
                    'description'
                ),
                'order' => 'PoValuable.name ASC',
            );
        }
        return $this->find('first', array(
            'conditions' => array('Portfolio.id' => $portfolioId),
            'fields' => array(
                'id',
                'mother',
                'usual',
                'listening',
                'listening_part',
                'reading',
                'reading_part',
                'speak_int',
                'speak_int_part',
                'speak_prod',
                'speak_prod_part',
                'writing',
                'writing_part',
                'assess_update',
                'study_pri',
                'study_sup',
                'study_oth',
                'study_sta',
                'activ_lis',
                'activ_spe',
                'activ_rea',
                'activ_pho',
                'activ_wri',
                'activ_oth'
            ),
            'contain' => $contain
        ));

    }


    /**
     * Chargement de la vue d'ensemble des passeports
     *
     * @param <string>  $studentId
     * @param <array>   $languages
     * @return <array>
     */
    function getPassportOverview($studentId, $languages) {

        // Chargement des passeports
        $data = $this->find('all', array(
                    'conditions' => array('Portfolio.student_id' => $studentId),
                    'fields' => array(
                        'id',
                        'language_id',
                        'mother',
                        'usual',
                        'listening',
                        'listening_part',
                        'reading',
                        'reading_part',
                        'speak_int',
                        'speak_int_part',
                        'speak_prod',
                        'speak_prod_part',
                        'writing',
                        'writing_part'
                    ),
                    'order' => 'Portfolio.id ASC',
                    'recursive' => -1
                ));

        // Mise en forme / informations complémentaires
        $passports = array();
        foreach ($data as $key => $value) {
            // Réorganisation de la matrice
            $portfolioId = $value['Portfolio']['id'];
            $langageName = $languages[$value['Portfolio']['language_id']]['name'];
            unset($value['Portfolio']['id']);
            $passports[$langageName] = $value['Portfolio'];
            // Documents attestants les niveaux
            $levelDocs = $this->countLevelDocuments($portfolioId);
            $passports[$langageName]['listening_doc'] = $levelDocs['listening']['value'];
            $passports[$langageName]['reading_doc'] = $levelDocs['reading']['value'];
            $passports[$langageName]['speak_int_doc'] = $levelDocs['speak_int']['value'];
            $passports[$langageName]['speak_prod_doc'] = $levelDocs['speak_prod']['value'];
            $passports[$langageName]['writing_doc'] = $levelDocs['writing']['value'];
            // Certificats attestants les niveaux
            $passports[$langageName]['listening_cert'] = $this->PoCertificate->find('count', array(
                        'conditions' => array(
                            'PoCertificate.portfolio_id' => $portfolioId,
                            'PoCertificate.listening >=' => $passports[$langageName]['listening']
                        )
                    ));
            $passports[$langageName]['reading_cert'] = $this->PoCertificate->find('count', array(
                        'conditions' => array(
                            'PoCertificate.portfolio_id' => $portfolioId,
                            'PoCertificate.reading >=' => $passports[$langageName]['reading']
                        )
                    ));
            $passports[$langageName]['speak_int_cert'] = $this->PoCertificate->find('count', array(
                        'conditions' => array(
                            'PoCertificate.portfolio_id' => $portfolioId,
                            'PoCertificate.speak_int >=' => $passports[$langageName]['speak_int']
                        )
                    ));
            $passports[$langageName]['speak_prod_cert'] = $this->PoCertificate->find('count', array(
                        'conditions' => array(
                            'PoCertificate.portfolio_id' => $portfolioId,
                            'PoCertificate.speak_prod >=' => $passports[$langageName]['speak_prod']
                        )
                    ));
            $passports[$langageName]['writing_cert'] = $this->PoCertificate->find('count', array(
                        'conditions' => array(
                            'PoCertificate.portfolio_id' => $portfolioId,
                            'PoCertificate.writing >=' => $passports[$langageName]['writing']
                        )
                    ));
        }

        return $passports;

    }


    /**
     * Chargement de la biographie d'un étudiant (visites)
     *
     * @param <string> $portfolioId
     * @return <array>
     */
    function getBiography($portfolioId) {

        $this->Behaviors->attach('Containable');
        return $this->find('first', array(
            'conditions' => array('Portfolio.id' => $portfolioId),
            'fields' => array('id'),
            'contain' => array(
                'PoValuable' => array(
                    'fields' => array(
                        'id',
                        'portfolio_id',
                        'name',
                        'description'
                    ),
                    'order' => 'PoValuable.name ASC'
                ),
                'PoContact' => array(
                    'fields' => array(
                        'id',
                        'portfolio_id',
                        'name',
                        'description'
                    ),
                    'order' => 'PoContact.name ASC'
                ),
                'PoAbroad' => array(
                    'fields' => array(
                        'id',
                        'portfolio_id',
                        'date',
                        'duration',
                        'unit',
                        'description'
                    ),
                    'order' => 'PoAbroad.date DESC'
                ),
                'PoCultural' => array(
                    'fields' => array(
                        'id',
                        'portfolio_id',
                        'name',
                        'description'
                    ),
                    'order' => 'PoCultural.name ASC'
                ),
                'PoEmployment' => array(
                    'fields' => array(
                        'id',
                        'portfolio_id',
                        'name',
                        'description'
                    ),
                    'order' => 'PoEmployment.name ASC'
                )
            )
        ));

    }


    /**
     * Chargement du dossier d'un étudiant (visites)
     *
     * @param <string> $portfolioId
     * @return <array>
     */
    function getDossier($portfolioId, $onlyShow = false) {

        // Initialisation
        $conditions = array();
        if ($onlyShow)
            $conditions['PoDocument.kind'] = 'show';

        // Chargement
        $this->Behaviors->attach('Containable');
        $data = $this->find('first', array(
                    'conditions' => array('Portfolio.id' => $portfolioId),
                    'fields' => array('id'),
                    'contain' => array(
                        'PoDocument' => array(
                            'conditions' => $conditions,
                            'fields' => array(
                                'id',
                                'portfolio_id',
                                'kind',
                                'competency',
                                'filename',
                                'description',
                                'type',
                                'file'
                            ),
                            'order' => 'PoDocument.filename ASC'
                        ),
                        'PoCertificate' => array(
                            'fields' => array(
                                'id',
                                'portfolio_id',
                                'certificate_id',
                                'date',
                                'organism',
                                'certificate_info',
                                'listening',
                                'reading',
                                'speak_int',
                                'speak_prod',
                                'writing'
                            ),
                            'Certificate' => array(
                                'fields' => array(
                                    'id',
                                    'certification_id'
                                ),
                                'Certification' => array(
                                    'fields' => array(
                                        'id',
                                        'name',
                                        'description'
                                    )
                                )
                            ),
                            'order' => 'PoCertificate.date DESC'
                        )
                    )
                ));

        // Séparation des types de documents
        if (!$onlyShow) {
            foreach ($data['PoDocument'] as $key => $value) {
                if ($value['kind'] == 'work') {
                    $data['PoWorkDocument'][] = $value;
                    unset($data['PoDocument'][$key]);
                }
            }
        }

        // Retour
        return $data;

    }


    /**
     * Décompte du nombre de documents (dossier) attestant un niveau de langue
     *
     * @param <string> $portfolioId
     * @param <boolean> $bubbles
     * @return <array>
     */
    function countLevelDocuments($portfolioId, $bubbles = false) {

        // Initialisation
        $levelDocs = array();
        if ($bubbles)
            $compName = array(
                1 => 'PortfolioListening',
                'PortfolioReading',
                'PortfolioSpeakInt',
                'PortfolioSpeakProd',
                'PortfolioWriting'
            );
        else
            $compName = array(
                1 => 'listening',
                'reading',
                'speak_int',
                'speak_prod',
                'writing'
            );

        // Acquisition des décomptes
        $levelDocs[$compName[1]]['value'] = $this->PoDocument->find('count', array(
                    'conditions' => array(
                        'PoDocument.portfolio_id' => $portfolioId,
                        'PoDocument.kind' => 'show',
                        'PoDocument.competency' => 1
                    )
                ));
        $levelDocs[$compName[2]]['value'] = $this->PoDocument->find('count', array(
                    'conditions' => array(
                        'PoDocument.portfolio_id' => $portfolioId,
                        'PoDocument.kind' => 'show',
                        'PoDocument.competency' => 2
                    )
                ));
        $levelDocs[$compName[3]]['value'] = $this->PoDocument->find('count', array(
                    'conditions' => array(
                        'PoDocument.portfolio_id' => $portfolioId,
                        'PoDocument.kind' => 'show',
                        'PoDocument.competency' => 3
                    )
                ));
        $levelDocs[$compName[4]]['value'] = $this->PoDocument->find('count', array(
                    'conditions' => array(
                        'PoDocument.portfolio_id' => $portfolioId,
                        'PoDocument.kind' => 'show',
                        'PoDocument.competency' => 4
                    )
                ));
        $levelDocs[$compName[5]]['value'] = $this->PoDocument->find('count', array(
                    'conditions' => array(
                        'PoDocument.portfolio_id' => $portfolioId,
                        'PoDocument.kind' => 'show',
                        'PoDocument.competency' => 5
                    )
                ));

        // Affectation des variables de vue
        if ($bubbles) {
            foreach ($levelDocs as $key => $value) {
                switch ($value['value']) {
                    case 0 :
                        $levelDocs[$key]['tooltip'] = __d('portfolio', "No material certifies this level in your Display Dossier", true);
                        $levelDocs[$key]['value'] = '0';
                        break;
                    case 1 :
                        $levelDocs[$key]['tooltip'] = "1 " . __d('portfolio', "material certifies this level in your Display Dossier", true);
                        $levelDocs[$key]['value'] = '1';
                        break;
                    case 2 :
                        $levelDocs[$key]['tooltip'] = "2 " . __d('portfolio', "materials certify this level in your Display Dossier", true);
                        $levelDocs[$key]['value'] = '2';
                        break;
                    default :
                        $levelDocs[$key]['tooltip'] = (string) $value['value'] . " " . __d('portfolio', "materials certify this level in your Display Dossier", true);
                        $levelDocs[$key]['value'] = '3';
                }
            }
        }

        // Retour
        return $levelDocs;

    }


    /**
     * Chargement de la liste de repérage "Stratégies" de la biographie
     *
     * @param <string> $id
     * @return <array>
     */
    function getStrategy($portfolioId) {

        return $this->find('first', array(
            'conditions' => array('Portfolio.id' => $portfolioId),
            'fields' => array(
                'id',
                'strategy1',
                'strategy2',
                'strategy3',
                'strategy4',
                'strategy5',
                'strategy6',
                'strategy7'
            ),
            'recursive' => -1
        ));

    }


}

