/* 
 * D'après http://css.dzone.com/news/jquery-image-cross-fade-transi
 * 
 */


// wrap as a jQuery plugin and pass jQuery in to our anoymous function
(function ($) {
    $.fn.cross = function (options) {
        return this.each(function (i) {

            // cache image id
            var image = $(this);

            // get the target from the backgroundImage + regexp
            var target = $(this).css('backgroundImage').replace(/^url|[\(\)'"]/g, '');

            // nice long chain: wrap img element in span
            $(this).wrap('<span style="float: right; position: relative;"></span>')
            // change selector to parent - i.e. newly created span
            .parent()
            // prepend a new image inside the span
            .prepend('<img>')
            // change the selector to the newly created image
            .find(':first-child')
            // set the image to the target
            .attr('src', target);

            // the CSS styling of the start image needs to be handled
            // differently for different browsers
            if ($.browser.msie || $.browser.mozilla) {
                $(this).css({
                    'position' : 'absolute',
                    'right' : 0,
                    'background' : '',
                    'top' : this.offsetTop
                });
            } else if ($.browser.opera && $.browser.version < 9.5) {
                // Browser sniffing is bad - however opera < 9.5 has a render bug
                // so this is required to get around it we can't apply the 'top' : 0
                // separately because Mozilla strips the style set originally somehow...
                $(this).css({
                    'position' : 'absolute',
                    'right' : 0,
                    'background' : '',
                    'top' : "0"
                });
            } else { // Safari
                $(this).css({
                    'position' : 'absolute',
                    'right' : 0,
                    'background' : ''
                });
            }

            // using .animate which will handle the fading
            // up from the right opacity for us
            $(this).closest('#content').find('.trigger').hover(function () {
                $(image).stop().animate({
                    opacity: 0
                }, 250);
            }, function () {
                $(image).stop().animate({
                    opacity: 1
                }, 250);
            });
        });
    };

})(jQuery);

// note that this uses the .bind('load') on the window object, rather than $(document).ready()
// because .ready() fires before the images have loaded, but we need to fire *after* because
// our code relies on the dimensions of the images already in place.
$(window).bind('load', function () {
    $('img.fade').cross();
});