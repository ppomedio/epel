<?php

/**
 * {app}/models/po_abroad.php
 */
class PoAbroad extends AppModel {

    var $name = 'PoAbroad';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'duration' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a duration.", true),
                    'last' => true
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __d('validation', "Please enter a numeric value…", true)
                )
            ),
            'unit' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please select a duration unit.", true)
            ),
            'description' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please describe experience.", true)
            )
        );

        parent::__construct();

    }


}

