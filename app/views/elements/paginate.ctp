
<?php

// Paramétrage des liens de pagination suivant la taille du conteneur
if (isset($half)) {
    $modulus = 3;
    $first = 1;
    $last = 1;
} else {
    $modulus = 9;
    $first = 3;
    $last = 3;
}

// Réécriture du nom de contrôleur dans les liens de pagination
$coolName = strtr(substr($this->Session->params['controller'], 0, -1), '_', '/');
$numbers = str_replace($this->Session->params['controller'], $coolName, $paginator->numbers(array(
            'url' => $this->params['pass'],
            'modulus' => $modulus,
            'first' => $first,
            'last' => $last,
            #'before' => '',
            #'after' => ''
        )));
if (!empty($numbers))
    $numbers .= ' - ';
$prev = str_replace($this->Session->params['controller'], $coolName, $paginator->prev('', array('url' => $this->params['pass']), null, array('class' => 'disabled')));
$next = str_replace($this->Session->params['controller'], $coolName, $paginator->next('', array('url' => $this->params['pass']), null, array('class' => 'disabled')));
$counter = str_replace($this->Session->params['controller'], $coolName, $paginator->counter(array('format' => __("Page", true) . ' %page% ' . __("of", true) . ' %pages%')));

?>
                            <div id="paginate-<?php echo $subId; ?>" class="paginate">
                                <?php echo $prev . $next . $numbers . $counter; ?>
                                <div id="data_paginate-<?php echo $subId; ?>-prev" class="tooltip-data"><?php __('Previous page'); ?></div>
                                <div id="data_paginate-<?php echo $subId; ?>-next" class="tooltip-data"><?php __('Next page'); ?></div>
                                <div id="data_paginate-<?php echo $subId; ?>-change" class="tooltip-data"><?php __('Jump to page…'); ?></div>
                            </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

// Systématiques
$this->Js->Buffer("
    // Id pour info-bulles
    $('div#paginate-" . $subId . " span a').attr('id', 'paginate-" . $subId . "-change').addClass('link');
    $('div#paginate-" . $subId . " span a.prev').attr('id', 'paginate-" . $subId . "-prev');
    $('div#paginate-" . $subId . " span a.next').attr('id', 'paginate-" . $subId . "-next');
    // Loading…
    $('#" . $subId . " a').not('.iframe, .edit, .add, .supp .people, th.controls > a').click(function() {
        $('#divToolTip').fadeOut('fast');
        $('#title-" . $subId . "').html('" . addslashes(__("Loading…", true)) . "');
        $('#" . $subId . "').fadeTo('fast', 0.6);
    });
");

// Pagination AJAX uniquement
if (isset($ajax))
    $this->Js->Buffer("
    // Tooltips
    tooltipLink('#paginate-" . $subId . "');
    // Rétablissement de l'opacité
    $('#" . $subId . "').fadeTo('slow', 1);
");
?>