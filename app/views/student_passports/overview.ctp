
                <article id="assessment">
                    <h1><?php echo $title_for_layout; ?></h1><?php
                    foreach ($user as $language => $passport) { ?>

                    <section>
                        <h2><?php
                        __d('language', $language);
                        $languageInfo = '';
                        if ($passport['mother'])
                            $languageInfo = __d('portfolio', "Mother tongue", true);
                        if ($passport['usual']) {
                            if ($languageInfo)
                                $languageInfo .= ' / ';
                            $languageInfo .= __d('portfolio', "Usual language", true);
                        }
                        if ($languageInfo)
                            echo ' <span class="smaller">(' . $languageInfo . ')</span>';
                        ?></h2>
                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Competency"); ?></th>
                                        <th><span id="<?php echo $passport['language_id']; ?>-level" class="help"><?php __d('portfolio', "Level"); ?></span></th>
                                        <th><span id="<?php echo $passport['language_id']; ?>-desc" class="help"><?php __d('portfolio', "Description"); ?></span></th>
                                        <th><span id="<?php echo $passport['language_id']; ?>-docs" class="help"><?php __d('portfolio', "Documents"); ?></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach ($competencies as $compKey => $compName) {
                                        // Bascule des couleurs de lignes
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        // Préparation
                                        if (!empty($passport[$compKey])) {
                                            // Compétences
                                            $level = $levels[$passport[$compKey]];
                                            $levelDescription = __d('levels_grid', $levelsDescriptions[$compKey][$levels[$passport[$compKey]]], true);
                                            if (!empty($passport[$compKey . '_part'])) {
                                                // Compétences partielles
                                                #$level = '<span class="alerttext">' . $level . '</span>';
                                                $levelDescription .= '<br /><em><span class="infotext bolder">' . __d('portfolio', "Also", true) . __(":", true) . '</span> ' . $passport[$compKey . '_part'] . '</em>';
                                            }
                                            // Certificats attestant cette compétence
                                            if ($passport[$compKey . '_cert']) {
                                                $levelDocuments = '<span class="infotext"><span class="bolder">' . $passport[$compKey . '_cert'] . '</span> ';
                                                if ($passport[$compKey . '_cert'] > 1)
                                                    $levelDocuments .= __d('portfolio', "certificate(s)", true) . '</span>';
                                                else
                                                    $levelDocuments .= __d('portfolio', "certificate", true) . '</span>';
                                            } else
                                                $levelDocuments = __d('portfolio', "No certificate", true);
                                            // Documents attestant cette compétence
                                            if ($passport[$compKey . '_doc']) {
                                                $levelDocuments .= '<br /><span class="infotext"><span class="bolder">' . $passport[$compKey . '_doc'] . '</span> ';
                                                if ($passport[$compKey . '_doc'] > 1)
                                                    $levelDocuments .= __d('portfolio', "document(s)", true) . '</span>';
                                                else
                                                    $levelDocuments .= __d('portfolio', "document", true) . '</span>';
                                            } else
                                                $levelDocuments .= "<br />" . __d('portfolio', "No document", true);
                                        } else {
                                            // Compétence non définie
                                            $level = '<span class="smaller">' . __("Undefined", true) . '</span>';
                                            $levelDescription = '';
                                            $levelDocuments = '';
                                        }
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $compName; ?><span class="picto-<?php echo $compKey; ?>"></span></td>
                                        <td><?php echo $level; ?></td>
                                        <td><?php echo $levelDescription; ?></td>
                                        <td><?php echo $levelDocuments; ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <div id="data_<?php echo $passport['language_id']; ?>-level" class="tooltip-data"><?php
                            __d('portfolio', "Levels displayed in orange are partially reached");
                            ?></div>
                            <div id="data_<?php echo $passport['language_id']; ?>-desc" class="tooltip-data"><?php
                            __d('portfolio', "You can define your partial competencies through the “Self-assessment” part of the Passport for this language");
                            ?></div>
                            <div id="data_<?php echo $passport['language_id']; ?>-docs" class="tooltip-data"><?php
                            __d('portfolio', "Elements from the Dossier (certificates and documents from the presentation dossier) certifying the competency");
                            ?></div>
                            </div>
                    </section>
                    <?php } ?>

                </article>
                <aside><?php
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-europass', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipHelp('#assessment');
");

?>