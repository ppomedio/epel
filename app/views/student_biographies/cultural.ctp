
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-cultural"><?php __d('portfolio', "My experience and knowledge of the culture"); ?></h2>
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "How have you been developing your knowledge of the country associated with this language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Do you belong to any cultural club associated with that language or country?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Do you read magazines or novels about the culture?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Do you regularly or occasionally visit websites related to the culture?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Have you learned any more about that country’s culture through your leisure activities?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="cultural-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new experience"); ?></h2>
                        <?php echo $this->Form->create('PoCultural', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/cultural/' . urlencode($language),
                            'id' => 'cultural-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoCultural'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __d('portfolio', "Type", true)
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <p class="info"><?php __d('portfolio', "If possible, write in the concerned language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/biography/cultural-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography'));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('student/biography/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('.instruction');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#cultural-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#cultural-edit');
    // Initialisation du formulaire
    function initForm() {
        $('#cultural-edit-form #PoCulturalId').val('');
        $('#cultural-edit-form #PoCulturalName').val('');
        $('#cultural-edit-form #PoCulturalDescription').val('');
    };
    // Soumission Ajax du formulaire
    $('#cultural-edit-form').submit(function() {
        if (!$('#cultural-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/biography/cultural/" . urlencode($language) . "',
                target: '#cultural',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#cultural-edit-form div.loading').show();
                },
                success: function() {
                    $('#cultural-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#cultural-toggle').hide();
                    $('#cultural-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#cultural-edit-form div.loading').hide();
                    $('#cultural-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>