<?php

/**
 * {app}/models/university.php
 */
class University extends AppModel {

    var $name = 'University';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $hasMany = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'university_id'
        ),
        'Teacher' => array(
            'className' => 'Teacher',
            'foreignKey' => 'university_id'
        ),
        'TeGroup' => array(
            'className' => 'TeGroup',
            'foreignKey' => 'university_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'secret' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a teacher account creation code for this university.", true),
                    'last' => true
                ),
                'between' => array(
                    'rule' => array('between', 10, 60),
                    'message' => __d('validation', "Secret phrase must be between 10 and 60 characters long.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z0-9 :;,.?!()\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^\[\]\{\}\(\)\^\+\*\$\|\/\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This teachers's account creation code is already used.", true)
                )
            ),
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter the name of this university.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z0-9 \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This university name is already used.", true)
                )
            ),
            'url' => array(
                'rule' => array('url', true),
                'allowEmpty' => true,
                'message' => __d('validation', "This Internet address is not valid.", true)
            )
        );

        parent::__construct();

    }


    /**
     *
     * @param <bool> $none  : choix "aucune" possible
     * @param <bool> $test  : choix "fictive / de test / formation" possible
     * @return <array>      : liste des universités
     */
    function getUniversityList($none = false, $test = false) {

        // Chargement
        $universities = $this->find('list', array(
            'order' => array('University.name DESC'),
            'recursive' => -1
        ));
        // Université non définie
        if ($none)
            $universities['001'] = __($universities['001'], true);
        else unset($universities['001']);
        // Université fictive / de test / formation
        if ($test || Configure::read('epel.admin.show_fict_univ'))
            $universities['002'] = __($universities['002'], true);
        else unset($universities['002']);
        // Retour
        #$this->log($universities, 'debug');
        return $universities;

    }


}

