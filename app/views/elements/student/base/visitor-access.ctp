<?php

/**
 * Pas de réglage de visibilité si le portfolio est vide
 */

if ($this->Session->read('Auth.User.portfolios')) {

?>
                    <section>
                        <h2><?php __("Visitors visibility"); ?></h2>
                        <?php echo $this->Form->create('Student', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/global_access',
                            'id' => 'visitor-access-form',
                            'class' => 'white'
                            ));
                        echo $this->Form->hidden('id'); ?>

                        <p class="info"><?php __("Your Portfolio may be seen by visitors (mainly recruiters) who have been approved by the administrator. You can choose which parts of your Portfolio visitors can access."); ?></p>
                        <div id="visitor-access-panel">
                            <fieldset>
                                <dl>
                                    <dt id="visitor-passport" class="help"><?php echo $this->Form->label('vis_passport', __("Passport", true)); ?></dt>
                                    <dd><?php echo $this->Form->checkbox('vis_passport'); ?></dd>
                                    <dt id="visitor-biography" class="help"><?php echo $this->Form->label('vis_biography', __("Biography", true)); ?></dt>
                                    <dd><?php echo $this->Form->checkbox('vis_biography'); ?></dd>
                                    <dt id="visitor-dossier" class="help"><?php echo $this->Form->label('vis_dossier', __d('portfolio', "Dossier", true)); ?></dt>
                                    <dd><?php echo $this->Form->checkbox('vis_dossier'); ?></dd>
                                </dl>
                            </fieldset>
                            <p class="info"><?php __("Visitors always see your profile, except if your Portfolio is invisible."); ?></p>
                        </div>
                        <fieldset>
                            <dl>
                                <dt id="visitor-global" class="help"><?php echo $this->Form->label('vis_hide', __("Portfolio invisible to visitors?", true)); ?></dt>
                                <dd class="master"><?php echo $this->Form->checkbox('vis_hide'); ?></dd>
                            </dl>
                        </fieldset>
                        <div class="tail"><div class="loading"></div></div>
                        <?php echo $this->Form->end(); ?>

                        <div id="data_visitor-passport" class="tooltip-data"><?php
                        echo '<span class="bolder infotext">' . __("YES", true) . __(":", true) . '</span> ' . __("you will be able to choose in your Passport which languages are visible to visitors.", true)
                                . '<br /><span class="bolder ">' . __("NO", true) . __(":", true) . '</span> ' . __("visitors have no access to your Passport.", true);
                        ?></div>
                        <div id="data_visitor-biography" class="tooltip-data"><?php
                        echo '<span class="bolder infotext">' . __("YES", true) . __(":", true) . '</span> ' . __("you will be able to choose in your Biography which languages are visible to visitors. The visibility of the “Heritage languages” part directly depends on this setting.", true)
                                . '<br /><span class="bolder">' . __("NO", true) . __(":", true) . '</span> ' . __("visitors have no access to your Biography.", true);
                        ?></div>
                        <div id="data_visitor-dossier" class="tooltip-data"><?php
                        echo '<span class="bolder infotext">' . __("YES", true) . __(":", true) . '</span> ' . __("you will be able to choose in your Dossier which languages are visible to visitors.", true)
                                . '<br /><span class="bolder">' . __("NO", true) . __(":", true) . '</span> ' . __("visitors have no access to your Dossier.", true);
                        ?></div>
                        <div id="data_visitor-global" class="tooltip-data"><?php
                        echo '<span class="bolder alerttext">' . __("YES", true) . __(":", true) . '</span> ' . __("you never appear in visitors' search results.", true)
                                . '<br /><span class="bolder">' . __("NO", true) . __(":", true) . '</span> ' . __("other settings apply.", true);
                        ?></div>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form',
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

// iButton -> buffer Js
$this->Js->Buffer("
    // Visibilités globales (iButton)
    $('#visitor-access-form fieldset dl dd input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            buttonId = $(this).attr('id');
            visButton = buttonId.substring(10, buttonId.length).toLowerCase();
            if($(this).is(':checked'))
                buttonVal = true;
            else
                buttonVal = false;
            $('#visitor-access-form').ajaxSubmit({
                data: { visButton: 'vis_' + visButton },
                beforeSubmit: function() {
                    $('#visitor-access-form div.tail div.loading').show();
                },
                success: function() {
                    $('#visitor-access-form div.tail div.loading').hide();
                }
            });
            if (visButton == 'hide') {
                if ($(this).is(':checked')) {
                    $('#visitor-access-panel').hide('fast');
                    $('#language-list p.visibility').hide('fast');
                    $('#language-list .see').hide();
                } else {
                    $('#visitor-access-panel').show('fast');
                    $('#language-list p.visibility').show('fast');
                    $('#language-list .see').show();
                }
            } else if (visButton == 'passport') {
                for (var i in visibility) {
                    if (!buttonVal)
                        visibility[i]['passport'] = 0;
                    else
                        visibility[i]['passport'] = partVisibility[i]['passport'];
                }
            } else if (visButton == 'biography') {
                for (var i in visibility) {
                    if (!buttonVal)
                        visibility[i]['biography'] = 0;
                    else
                        visibility[i]['biography'] = partVisibility[i]['biography'];
                }
            } else if (visButton == 'dossier') {
                for (var i in visibility) {
                    if (!buttonVal)
                        visibility[i]['dossier'] = 0;
                    else
                        visibility[i]['dossier'] = partVisibility[i]['dossier'];
                }
            }
            for (var i in visibility) {
                $('#data_language-info_' + i).html(partName['passport'] + visible[visibility[i]['passport']] + partName['biography'] + visible[visibility[i]['biography']] + partName['dossier'] + visible[visibility[i]['dossier']]);
            }
        });
    // Tooltips
    tooltipHelp('#visitor-access-form');
    // Tips
    addTipInfo('#visitor-access-form');
    // État initial du panneau
    if ($('#StudentVisHide').is(':checked')) {
        $('#visitor-access-panel').hide();
        $('#language-list p.visibility').hide();
        $('#language-list .see').hide();
    }
");

}
?>