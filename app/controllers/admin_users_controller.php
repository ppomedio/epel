<?php

/**
 * {app}/controllers/admin_users_controller.php
 *
 */
class AdminUsersController extends AppController {

    var $name = 'AdminUsers';
    // Modèle
    var $uses = array(
        'Admin',
        'Student',
        'Teacher',
        'Visitor',
        'University'
    );
    // Pagination
    var $paginate;
    // Composants
    var $components = array(
        'AppEmail',
        'L10nSort'
    );
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu'
    );


    function beforeFilter() {

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect(DS . $this->params['language'] . DS . 'admin' . DS . 'base');

    }


    function student() {

        // Assistant supplémentaire
        $this->helpers[] = 'Date';
        // Modification des règles de validation
        $this->Student->validateSearch();
        // Filtrage / pagination
        if (!empty($this->data)) {
            if (!empty($this->data['Student']['name'])
                    || !empty($this->data['Student']['firstname'])
                    || !empty($this->data['Student']['origin_country_id'])
                    || !empty($this->data['Student']['university_id'])
                    || !empty($this->data['Student']['group'])) {
                // Mise en forme du code groupe
                $this->data['Student']['group'] = strtoupper($this->data['Student']['group']);
                // Maj variable de session
                $search = array();
                if ($this->Session->check('Auth.User.search'))
                    $search = $this->Session->read('Auth.User.search');
                $search['Student'] = $this->data['Student'];
                $this->Session->write('Auth.User.search', $search);
            } else {
                // Raz variable de session
                $search = $this->Session->read('Auth.User.search');
                unset($search['Student']);
                $this->Session->write('Auth.User.search', $search);
            }
            $result = $this->_getUsers('Student', $this->data);
        } else {
            if ($this->Session->check('Auth.User.search.Student'))
                $result = $this->_getUsers('Student', $this->Session->read('Auth.User.search'));
            else
                $result = $this->_getUsers('Student');
        }
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Student.page', $this->params['paging']['Student']['options']['page']);
        $this->Session->write('Auth.User.pagination.Student.order', $this->params['paging']['Student']['options']['order']);
        // Données de vue communes
        $this->set('user', $result);
        $this->set('university', $this->University->getUniversityList(true));
        $this->set('country', $this->L10nSort->setList($this->Student->OriginCountry->find('list'), 'country'));
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'user' . DS . 'students-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Student accounts", true));
        }

    }


    function teacher() {

        // Modification des règles de validation
        $this->Teacher->validateSearch();
        // Filtrage / pagination
        if (!empty($this->data)) {
            if (!empty($this->data['Teacher']['name'])
                    || !empty($this->data['Teacher']['firstname'])
                    || !empty($this->data['Teacher']['university_id'])) {
                // Maj variable de session
                $search = array();
                if ($this->Session->check('Auth.User.search'))
                    $search = $this->Session->read('Auth.User.search');
                $search['Teacher'] = $this->data['Teacher'];
                $this->Session->write('Auth.User.search', $search);
            } else {
                // Raz variable de session
                $search = $this->Session->read('Auth.User.search');
                unset($search['Teacher']);
                $this->Session->write('Auth.User.search', $search);
            }
            $result = $this->_getUsers('Teacher', $this->data);
        } else {
            if ($this->Session->check('Auth.User.search.Teacher'))
                $result = $this->_getUsers('Teacher', $this->Session->read('Auth.User.search'));
            else
                $result = $this->_getUsers('Teacher');
        }
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Teacher.page', $this->params['paging']['Teacher']['options']['page']);
        $this->Session->write('Auth.User.pagination.Teacher.order', $this->params['paging']['Teacher']['options']['order']);
        // Données de vue communes
        $this->set('user', $result);
        $this->set('university', $this->University->getUniversityList());
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'user' . DS . 'teachers-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Teacher accounts", true));
        }

    }


    function visitor() {

        // Modification des règles de validation
        $this->Visitor->validateSearch();
        // Filtrage / pagination
        if (!empty($this->data)) {
            if (!empty($this->data['Visitor']['name'])
                    || !empty($this->data['Visitor']['firstname'])
                    || !empty($this->data['Visitor']['structure'])) {
                // Maj variable de session
                $search = array();
                if ($this->Session->check('Auth.User.search'))
                    $search = $this->Session->read('Auth.User.search');
                $search['Visitor'] = $this->data['Visitor'];
                $this->Session->write('Auth.User.search', $search);
            } else {
                // Raz variable de session
                $search = $this->Session->read('Auth.User.search');
                unset($search['Visitor']);
                $this->Session->write('Auth.User.search', $search);
            }
            $result = $this->_getUsers('Visitor', $this->data);
        } else {
            if ($this->Session->check('Auth.User.search.Visitor'))
                $result = $this->_getUsers('Visitor', $this->Session->read('Auth.User.search'));
            else
                $result = $this->_getUsers('Visitor');
        }
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visitor.page', $this->params['paging']['Visitor']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visitor.order', $this->params['paging']['Visitor']['options']['order']);
        // Données de vue communes
        $this->set('user', $result);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'user' . DS . 'visitors-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Visitor accounts", true));
        }

    }


    function change_student_university() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Enregistrement de la nouvelle valeur
            $this->data['Student']['group'] = '';
            $this->Student->set($this->data);
            // Sauvegarde
            $this->Student->save($this->data, array(
                'fieldList' => array(
                    'university_id',
                    'group'
                ),
                'callbacks' => false,
                'validate' => false
            ));
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    function change_teacher_university() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Enregistrement de la nouvelle valeur
            $this->Teacher->id = $this->data['Teacher']['id'];
            $this->Teacher->saveField('university_id', $this->data['Teacher']['university_id'], false);
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    function toggle_active() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $model = ucfirst($this->params['pass'][0]);
            $userId = $this->params['pass'][1];
            if ($model && $userId) {
                if (($model != 'Student') && $this->data['User']['active']) {
                    /**
                     * Si c'est un nouvel utilisateur non étudiant on lui envoie
                     * un courriel pour lui signifier l'activation de son compte
                     */
                    $date = $this->$model->find('first', array(
                                'conditions' => array('id' => $userId),
                                'fields' => array('created', 'modified'),
                                'recursive' => -1
                            ));
                    if ($date[$model]['created'] == $date[$model]['modified']) {
                        /**
                         * Envoi du courriel utilisateur
                         */
                        // Adresse cible
                        $data = $this->$model->User->find('first', array(
                                    'conditions' => array('User.id' => $userId),
                                    'fields' => array('User.id', 'User.email'),
                                    'recursive' => -1
                                ));
                        $this->AppEmail->to = $data['User']['email'];
                        // Adresse source
                        if (!isset($this->AppEmail->from))
                            $this->AppEmail->from = $this->$model->User->getEmail(1);
                        // Sujet
                        $this->AppEmail->subject .= '-> ' . __("Account activation", true);
                        // Template
                        $this->AppEmail->template = 'activation';
                        // Envoi
                        $this->AppEmail->send();
                        /**
                         * Maj de la date de modification
                         */
                        $this->$model->id = $userId;
                        $this->$model->saveField('modified', date('Y-m-d H:i:s'), false);
                    }
                }
                // Enregistrement de la nouvelle valeur
                $this->$model->User->id = $userId;
                $this->$model->User->saveField('active', $this->data['User']['active'], false);
            }
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    function init_criteria() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $model = $this->params['pass'][0];
            // Raz variable de session
            $search = $this->Session->read('Auth.User.search');
            unset($search[ucfirst($model)]);
            $this->Session->write('Auth.User.search', $search);
            // Sortie
            return $this->setAction($this->params['pass'][0]);
        }

    }


    /**
     *
     * @return <array>
     */
    private function _getUsers($model, $filter = array()) {

        // Champs communs
        $commonContain = array(
            'User' => array(
                'fields' => array(
                    'id',
                    'email',
                    'active',
                    'photo'
                )
            )
        );

        // Conditions
        $conditions = array();
        if (!empty($filter[$model]['name']))
            $conditions[$model . '.name LIKE'] = $filter[$model]['name'] . '%';
        if (!empty($filter[$model]['firstname']))
            $conditions[$model . '.firstname LIKE'] = $filter[$model]['firstname'] . '%';
        if (!empty($filter[$model]['origin_country_id']))
            $conditions[$model . '.origin_country_id'] = $filter[$model]['origin_country_id'];
        if (empty($filter[$model]['real'])) {
            if (Configure::read('epel.admin.show_fict_univ')) {
                $conditions[$model . '.real'] = 0;
            } else {
                $conditions[$model . '.real'] = 1;
            }
        }
        if (!empty($filter[$model]['university_id']))
            $conditions[$model . '.university_id'] = $filter[$model]['university_id'];
        if (!empty($filter[$model]['structure']))
            $conditions[$model . '.structure LIKE'] = $filter[$model]['structure'] . '%';
        if (!empty($filter[$model]['group']))
            $conditions[$model . '.group LIKE'] = $filter[$model]['group'] . '%';

        // Param. suivant le modèle
        switch ($model) {
            case 'Student':
                $fields = array(
                    'id',
                    'name',
                    'firstname',
                    'birth',
                    'origin_country_id',
                    'university_id',
                    'diploma_id',
                    'diploma_info',
                    'specialty',
                    'group'
                );
                $contain = array_merge($commonContain, array(
                            'OriginCountry' => array(
                                'fields' => array(
                                    'id',
                                    'name'
                                )
                            ),
                            'University' => array(
                                'fields' => array(
                                    'id',
                                    'name'
                                )
                            ),
                            'Diploma' => array(
                                'fields' => array(
                                    'id',
                                    'name',
                                    'level'
                                )
                            )
                        ));
                break;
            case 'Teacher':
                $fields = array(
                    'id',
                    'name',
                    'firstname',
                    'quality',
                    'specialty',
                    'phone',
                    'university_id'
                );
                $contain = array_merge($commonContain, array(
                            'University' => array(
                                'fields' => array(
                                    'id',
                                    'name'
                                )
                            )
                        ));
                break;
            case 'Visitor':
                $fields = array(
                    'id',
                    'name',
                    'firstname',
                    'function',
                    'structure',
                    'phone',
                    'url',
                    'address',
                    'country_id'
                );
                $contain = array_merge($commonContain, array(
                            'Country' => array(
                                'fields' => array(
                                    'id',
                                    'name'
                                )
                            )
                        ));
                break;
            default:
                return NULL;
        }

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.' . $model)) {
            $page = $this->Session->read('Auth.User.pagination.' . $model . '.page');
            $order = $this->Session->read('Auth.User.pagination.' . $model . '.order');
            $this->Session->delete('Auth.User.pagination.' . $model);
        } else {
            $page = NULL;
            $order = array($model . '.name' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'contain' => $contain,
            'page' => $page,
            'order' => $order,
            'limit' => 15
        );

        // Envoi de la pagination
        return $this->paginate($model);

    }


}

