/**
 *  jQuery Tooltip Plugin
 *  @requires jQuery v1.3 or 1.4
 *  http://intekhabrizvi.wordpress.com/
 *
 *  Copyright (c)  Intekhab A Rizvi (intekhabrizvi.wordpress.com)
 *  Licensed under GPL licenses:
 *  http://www.gnu.org/licenses/gpl.html
 * 
 *  Version: 3.2.2
 */
(function($) {
    jQuery.fn.tooltip = function(options){
        var defaults = {
            offsetX: 15,  //X Offset value
            offsetY: 10,  //Y Offset value
            fadeIn : '200', //Tooltip fadeIn speed, can use, slow, fast, number
            fadeOut : '200',//Tooltip fadeOut speed, can use, slow, fast, number
            dataAttr : 'data',	//Used when we create seprate div to hold your tooltip data, so plugin search div tage by using id 'data' and current href id on whome the mouse pointer is so if your href id is '_tooltip_1' then the div which hold that tooltips content should have id 'data_tooltip_1', if you change dataAttr from default then you need to build div tag with id 'current dataAttr _tooltip_1' without space
            bordercolor: '#6CE26C', // tooltip border color
            bgcolor: '#F8F8F8', //Tooltip background color
            fontcolor : '#006699', //Tooltip Font color
            fontsize : '15px', // Tooltip font size
            folderurl : 'NULL', // Folder url, where the tooltip's content file is placed, needed with forward slash in the last (/), or can be use as http://www.youwebsitename.com/foldername/ also.
            filetype: 'txt', // tooltip's content files type, can be use html, txt
            height: 'auto', // Tooltip's width
            width : 'auto', //Tooltip's Height
            cursor : 'help' // Mouse cursor
        };
        var options = $.extend(defaults, options);
        //Runtime div building to hold tooltip data, and make it hidden
        var $tooltip = $('<div id="divToolTip"></div>');
        return this.each(function(){
            $('body').append($tooltip);
            $tooltip.hide();
            //Runtime variable definations
            var element = this;
            var id = $(element).attr('id');
            var filename = options.folderurl + id + '.' + options.filetype;
            var dialog_id = '#divToolTip';
            //Tooltips main function
            $(this).hover(function(e){
                //var size = "Windows Width : " + $(document).width() + " Tip Width : " + e.pageX + "\n" + "Windows Height : " + $(document).height() + " Tip Height : " + e.pageY;
                //alert(size);
                //to check whether the tooltips content files folder is defined or not
                if(options.folderurl != "NULL"){
                    $(dialog_id).load(filename);

                }else
                {
                    if($('#'+options.dataAttr + '_' + id).length > 0){
                        $(dialog_id).html($('#'+ options.dataAttr + '_' + id).html());
                        //$(dialog_id).html(size);
                    }else{
                        $(dialog_id).html(id);
                        //$(dialog_id).html(size);
                    }
                }
                //assign css value to div
                $(element).css({
                    'cursor' : options.cursor
                    });
                if($(document).width() / 2 < e.pageX){
                    $(dialog_id).css({
                        'position' : 'absolute',
                        'border' : '1px solid ' + options.bordercolor,
                        'background-color' : options.bgcolor,
                        'padding' : '2px 7px 4px 8px',
                        '-moz-border-radius' : '6px 6px 6px 6px',
                        '-webkit-border-radius' : '6px 6px 6px 6px',
                        'top' : e.pageY + options.offsetY,
                        'left' :  e.pageX - $(dialog_id).width() + options.offsetX,
                        'color' : options.fontcolor,
                        'font-size' : options.fontsize,
                        'height' : options.height,
                        'width' : options.width
                    });
                //alert(size);
                }else{
                    $(dialog_id).css({
                        'position' : 'absolute',
                        'border' : '1px solid ' + options.bordercolor,
                        'background-color' : options.bgcolor,
                        'padding' : '2px 7px 4px 8px',
                        '-moz-border-radius' : '6px 6px 6px 6px',
                        '-webkit-border-radius' : '6px 6px 6px 6px',
                        'top' : e.pageY + options.offsetY,
                        'left' : e.pageX + options.offsetX,
                        'color' : options.fontcolor,
                        'font-size' : options.fontsize,
                        'cursor' : options.cursor,
                        'height' : options.height,
                        'width' : options.width
                    });
                //alert(size);
                }
                //enable div block
                $(dialog_id).stop(true, true).fadeIn(options.fadeIn);
            },function(){
                // when mouse out remove all data from div and make it hidden
                $(dialog_id).stop(true, true).fadeOut(options.fadeOut);
            }).mousemove(function(e){
                // to make tooltip moveable with mouse
                if($(document).width() / 2 < e.pageX){
                    $(dialog_id).css({
                        'top' : e.pageY + options.offsetY,
                        'left' : e.pageX - $(dialog_id).width(),
                        'height' : options.height,
                        'width' : options.width
                    });
                //$(dialog_id).html(e.pageX - $(dialog_id).width());
                }else{
                    $(dialog_id).css({
                        'top' : e.pageY + options.offsetY,
                        'left' : e.pageX + options.offsetX,
                        'height' : options.height,
                        'width' : options.width
                    });
                }
            });
        });
    };
})(jQuery);

//FINISH, simple isnt it ??
//if you like it or have any suggestions / comments , or you have some idea to make it better, 
//or you need some more fetures in it PLS PLS PLS let me know that at
//i.rizvi@hotmail.com
//Thank you for using my plugin
