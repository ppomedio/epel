<?php

$paginator->options(array(
    'update' => '#abroad'
    ));

?>

                        <div id="abroad" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th><?php __d('portfolio', "Date"); ?></th>
                                        <th><?php __d('portfolio', "Duration"); ?></th>
                                        <th class="controls">
                                            <span id="abroad-add" class="add"></span>
                                            <span id="abroad-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="4">
                                             <p class="alert"><?php
                                             __d('portfolio', "There is no stay abroad recorded in your Biography at the moment.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#abroad');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="date"><?php echo $paginator->sort(__d('portfolio', "Date", true), 'PoAbroad.date', array('url' => $this->params['pass'])); ?></th>
                                        <th class="duration"><?php echo $paginator->sort(__d('portfolio', "Duration", true), 'PoAbroad.duration', array('url' => $this->params['pass'])); ?></th>
                                        <th class="controls">
                                            <span id="abroad-add" class="add"></span>
                                            <span id="abroad-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoAbroad'] as $value) {
                                        $itemDate = $this->Date->Convert($value['date']);
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="abroad-description_<?php echo $value['id']; ?>"><?php echo nl2br($value['description']); ?></td>
                                        <td id="abroad-date_<?php echo $value['id']; ?>"><?php
                                        echo $itemDate;
                                        ?></td>
                                        <td id="abroad-duration_<?php echo $value['id']; ?>"><?php
                                            echo $value['duration'] . '&nbsp;' . $durations[$value['unit']];
                                            ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="day hidden"><?php
                                            echo $value['day'];
                                            ?></span>
                                            <span class="month hidden"><?php
                                            echo $value['month'];
                                            ?></span>
                                            <span class="year hidden"><?php
                                            echo $value['year'];
                                            ?></span>
                                            <span class="duration hidden"><?php
                                            echo $value['duration'];
                                            ?></span>
                                            <span class="unit hidden"><?php
                                            echo $value['unit'];
                                            ?></span>
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="abroad-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="abroad-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_abroad-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this experience");
                                    ?></div>
                                    <div id="data_abroad-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this experience");
                                    ?></div>
                                    <div id="abroad-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete this experience"); echo ' (' . $itemDate . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'abroad'));
                            } ?>

                            <div id="data_sort-abroad-date" class="tooltip-data"><?php __("Sort by date"); ?></div>
                            <div id="data_sort-abroad-duration" class="tooltip-data"><?php __("Sort by duration"); ?></div>
                            <div id="data_abroad-add" class="tooltip-data"><?php __d('portfolio', "Add a new experience"); ?></div>
                            <div id="data_abroad-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#abroad-edit-form #PoAbroadDescription').val() == ''
            && $('#abroad-edit-form #PoAbroadDuration').val() == ''
            && $('#abroad-edit-form #PoAbroadUnit').val() == '') {
        if ($('#abroad-edit').is(':hidden'))
            $('#abroad-toggle').hide();
        else
            $('#abroad-add').hide();
    }
    // Id, classes et cie
    $('#abroad table thead tr th.date a').attr('id', 'sort-abroad-date').addClass('sort-help');
    $('#abroad table thead tr th.duration a').attr('id', 'sort-abroad-duration').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#abroad table thead');
    tooltipAdd('#abroad table thead th.controls');
    tooltipGear('#abroad table thead th.controls');
    tooltipEdit('#abroad table tbody td.controls div.controls');
    tooltipSupp('#abroad table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#abroad-toggle').live('click', function() {
        if ($('#abroad-edit-form #PoAbroadId').val() == '')
            $('#abroad-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        if ($('#abroad-edit').is(':hidden'))
            $('#abroad-edit').slideDown('slow');
        else {
            $('#abroad-edit').slideUp('slow');
            $('#abroad-add').fadeIn('slow');
            if ($('#abroad-edit-form #PoAbroadDescription').val() == ''
                    && $('#abroad-edit-form #PoAbroadDuration').val() == ''
                    && $('#abroad-edit-form #PoAbroadUnit').val() == '')
                $('#abroad-toggle').hide();
        }
    });
    // Ajout d'une expérience
    $('#abroad-add').live('click', function() {
        $('#abroad-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#abroad-edit').is(':hidden')) {
            $('#abroad-edit').slideDown('slow');
        }
        $('#abroad-add').hide();
        $('#abroad-toggle').fadeIn('slow');
    });
    // Édition d'une expérience
    $('#abroad table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#abroad-edit > h2').text('" . addslashes(__d('portfolio', "Edit an experience", true)) . "');
        // Chargement des données dans le formulaire
        $('#abroad-edit-form #PoAbroadId').val(itemId);
        $('#abroad-edit-form #PoAbroadDuration').val($('#hidden-info_' + itemId + ' .duration').text());
        $('#abroad-edit-form #PoAbroadUnit option[value=\'' + $('#hidden-info_' + itemId + ' .unit').text() + '\']').attr('selected', 'selected');
        $('#abroad-edit-form #PoAbroadDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Date
        $('#abroad-edit-form #PoAbroadDateDay option[value=\'' + $('#hidden-info_' + itemId + ' .day').text() + '\']').attr('selected', 'selected');
        $('#abroad-edit-form #PoAbroadDateMonth option[value=\'' + $('#hidden-info_' + itemId + ' .month').text() + '\']').attr('selected', 'selected');
        $('#abroad-edit-form #PoAbroadDateYear option[value=\'' + $('#hidden-info_' + itemId + ' .year').text() + '\']').attr('selected', 'selected');
        // Affichage si masqué
        if ($('#abroad-edit').is(':hidden')) {
            $('#abroad-add').hide();
            $('#abroad-edit').slideDown('slow');
            $('#abroad-add').fadeIn('slow');
            $('#abroad-toggle').fadeIn('slow');
        } else {
            if ($('#abroad-add').is(':hidden'))
                $('#abroad-add').fadeIn('slow');
        }
    });
    // Suppression d'une expérience
    $('#abroad table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#abroad-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
					var extract = new Array();
					extract = $(this).attr('id').split('_');
					var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#abroad').load('/" . $this->params['language'] . "/student/biography/delete/" . urlencode($language) . "/abroad/' + itemId, function() {
                            if ($('#abroad-edit-form #PoAbroadId').val() == itemId) {
                                $('#abroad-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
                                initForm();
                                if ($('#abroad-edit').is(':hidden'))
                                    $('#abroad-toggle').hide();
                                else
                                    $('#abroad-add').hide();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-abroad').html('" . addslashes(__d('portfolio', "My stays abroad", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>