<?php

/**
 * {app}/controllers/accounts_controller.php
 */
class AccountsController extends AppController {

    var $name = 'Accounts';
    // Modèle
    var $uses = array('User');
    // Composants
    var $components = array(
        'AppEmail',
        'L10nSort',
        'FormatInput'
    );
    // Layout
    var $layout = 'fancy';


    function beforeFilter() {

        // Définition des autorisations
        $this->AppAuth->allow('*');
    }


    function create() {

        if (!empty($this->data)) {
            $this->User->set($this->data);
            // Validation
            if ($this->User->validates()) {
                // Stockage en variable de session
                $this->Session->write('epelAccountCreation', $this->data);
                // Suite…
                $this->setAction($this->data['User']['role']);
            }
        }
    }


    function student() {

        // Chargement du modèle
        $this->loadModel('Student');
        // Traitement
        if (!empty($this->data['Student'])) {
            // Validation
            $this->Student->set($this->data);
            if ($this->Student->validates()) {
                /**
                 * Création de l'utilisateur
                 */
                // Nouveau mot de passe
                $password = $this->User->generatePassword();
                // Assemblage des données
                $userData = $this->Session->read('epelAccountCreation');
                $userData['User']['password'] = $password['coded'];
                $userData['User']['active'] = true;
                // Raz variable de session
                $this->Session->delete('epelAccountCreation');
                // Enregistrement
                $this->User->create($userData);
                if ($this->User->save($userData, array(
                            'validate' => false,
                            'callbacks' => false,
                            'fieldList' => array(
                                'id',
                                'email',
                                'username',
                                'password',
                                'role',
                                'active'
                            )
                        ))) {
                    // ID du nouvel utilisateur
                    $userId = $this->User->getLastInsertID();
                    // Création de son dossier
                    $this->User->createUserFolder($userId);
                    /**
                     * Création de l'enregistrement étudiant
                     */
                    // ID
                    $this->data['Student']['id'] = $userId;
                    if (!empty($this->data['Student']['group']))
                        $this->data['Student']['group'] = strtoupper(trim($this->data['Student']['group']));
                    // Université + enseignant responsable
                    $data = $this->Student->getTeacherInfo();
                    $this->data['Student']['university_id'] = $data['Teacher']['university_id'];
                    $this->data['Student']['teacher_id'] = $data['Teacher']['id'];
                    // Remplacement éventuel de la clé du niveau d'études
                    // (ordre d'affichage dans la liste déroulante, voir models/diploma.php)
                    if ($this->data['Student']['diploma_id'] == '99')
                        $this->data['Student']['diploma_id'] = '10';
                    // Mise en forme du nom
                    $this->data['Student']['name'] = $this->FormatInput->formatName($this->data['Student']['name']);
                    $this->data['Student']['firstname'] = $this->FormatInput->formatName($this->data['Student']['firstname']);
                    // Champs non saisis (-> callback BeforeSave)
                    $this->data['Student']['diploma_info'] = '';
                    $this->data['Student']['domain_info'] = '';
                    // Enregistrement
                    $this->Student->create($this->data);
                    #$this->log($this->data, 'debug');
                    $this->Student->save($this->data, array(
                        'validate' => false,
                        'callbacks' => true,
                        'fieldList' => array(
                            'id',
                            'name',
                            'firstname',
                            'birth',
                            'origin_country_id',
                            'living_country_id',
                            'university_id',
                            'diploma_id',
                            'diploma_info',
                            'domain_id',
                            'domain_info',
                            'group'
                        )
                    ));
                    /**
                     * Envoi du courriel
                     */
                    // Adresse cible
                    $this->AppEmail->to = $userData['User']['email'];
                    // Adresse source
                    if (!isset($this->AppEmail->from))
                        $this->AppEmail->from = $this->User->getEmail(1);
                    // Sujet
                    $this->AppEmail->subject .= '-> ' . __("Account creation", true);
                    // Template
                    $this->AppEmail->template = 'account';
                    // Envoi des données aux éléments html/txt
                    $userData['User']['password'] = $password['clear'];
                    $this->set('data', $userData['User']);
                    // Envoi
                    $this->AppEmail->send();
                    // Sortie
                    return $this->setAction('accountReady');
                }
            }
        }
        // Contenu des listes déroulantes
        $temp = $this->L10nSort->setList($this->Student->OriginCountry->find('list'), 'country');
        $this->set('originCountry', $temp);
        $this->set('livingCountry', $temp);
        $this->set('diploma', $this->Student->Diploma->getDiplomasList());
        $this->set('domain', $this->L10nSort->setList($this->Student->Domain->find('list'), 'domain', false));
    }


    function teacher() {

        // Chargement du modèle
        $this->loadModel('Teacher');
        // Traitement
        if (!empty($this->data['Teacher'])) {
            // Validation
            $this->Teacher->set($this->data);
            if ($this->Teacher->validates()) {
                /**
                 * Création de l'utilisateur
                 */
                // Nouveau mot de passe
                $password = $this->User->generatePassword();
                // Assemblage des données
                $userData = $this->Session->read('epelAccountCreation');
                $userData['User']['password'] = $password['coded'];
                $userData['User']['active'] = false;
                // Raz variable de session
                $this->Session->delete('epelAccountCreation');
                // Enregistrement
                $this->User->create($userData);
                if ($this->User->save($userData, array(
                            'validate' => false,
                            'callbacks' => false,
                            'fieldList' => array(
                                'id',
                                'email',
                                'username',
                                'password',
                                'role',
                                'active'
                            )
                        ))) {
                    // ID du nouvel utilisateur
                    $userId = $this->User->getLastInsertID();
                    // Création de son dossier
                    $this->User->createUserFolder($userId);
                    /**
                     * Création de l'enregistrement enseignant
                     */
                    // ID
                    $this->data['Teacher']['id'] = $userId;
                    // Clé de l'université
                    $data = $this->Teacher->getUniversityInfo();
                    $this->data['Teacher']['university_id'] = $data['University']['id'];
                    // Code de création de compte étudiant par défaut
                    $this->data['Teacher']['secret'] = uniqid();
                    // Formatage
                    $this->data['Teacher']['name'] = $this->FormatInput->formatName($this->data['Teacher']['name']);
                    $this->data['Teacher']['firstname'] = $this->FormatInput->formatName($this->data['Teacher']['firstname']);
                    if (!empty($this->data['Teacher']['quality']))
                        $this->data['Teacher']['quality'] = $this->FormatInput->capFirst($this->data['Teacher']['quality']);
                    if (!empty($this->data['Teacher']['specialty']))
                        $this->data['Teacher']['specialty'] = $this->FormatInput->capFirst($this->data['Teacher']['specialty']);
                    // Préparation du courriel pour l'administrateur
                    $userData['User']['id'] = str_pad((string) $userId, 7, '0', STR_PAD_LEFT);
                    $userData['User']['name'] = $this->data['Teacher']['name'];
                    $userData['User']['firstname'] = $this->data['Teacher']['firstname'];
                    $userData['User']['university'] = $data['University']['name'];
                    // Enregistrement
                    $this->Teacher->create($this->data);
                    $this->Teacher->save($this->data, array(
                        'validate' => false,
                        'callbacks' => false,
                        'fieldList' => array(
                            'id',
                            'name',
                            'firstname',
                            'university_id',
                            'quality',
                            'specialty',
                            'secret'
                        )
                    ));
                    /**
                     * Envoi du courriel utilisateur
                     */
                    // Adresse cible
                    $this->AppEmail->to = $userData['User']['email'];
                    // Adresse source
                    if (!isset($this->AppEmail->from))
                        $this->AppEmail->from = $this->User->getEmail(1);
                    // Sujet
                    $this->AppEmail->subject .= '-> ' . __("Account creation", true);
                    // Template
                    $this->AppEmail->template = 'account';
                    // Envoi des données aux éléments html/txt
                    $userData['User']['password'] = $password['clear'];
                    $this->set('data', $userData['User']);
                    $this->set('inactive', true);
                    // Envoi
                    $this->AppEmail->send();
                    /**
                     * Envoi du courriel administrateur
                     */
                    // Adresse cible
                    $this->AppEmail->to = $this->AppEmail->from;
                    // Adresse source
                    $this->AppEmail->from = Configure::read('epel.email.from');
                    if (!isset($this->AppEmail->from))
                        $this->AppEmail->from = $userData['User']['email'];
                    // Template
                    $this->AppEmail->template = 'admin';
                    // Envoi des données aux éléments html/txt
                    $this->set('data', $userData['User']);
                    // Envoi
                    $this->AppEmail->send();
                    // Sortie
                    return $this->setAction('accountReady');
                }
            }
        }
    }


    function visitor() {

        // Chargement du modèle
        $this->loadModel('Visitor');
        // Traitement
        if (!empty($this->data['Visitor'])) {
            $this->Visitor->set($this->data);
            // Validation
            if ($this->Visitor->validates()) {
                /**
                 * Création de l'utilisateur
                 */
                // Nouveau mot de passe
                $password = $this->User->generatePassword();
                // Assemblage des données
                $userData = $this->Session->read('epelAccountCreation');
                $userData['User']['password'] = $password['coded'];
                $userData['User']['active'] = false;
                // Raz variable de session
                $this->Session->delete('epelAccountCreation');
                // Enregistrement
                $this->User->create($userData);
                if ($this->User->save($userData, array(
                            'validate' => false,
                            'callbacks' => false,
                            'fieldList' => array(
                                'id',
                                'email',
                                'username',
                                'password',
                                'role',
                                'active'
                            )
                        ))) {
                    // ID du nouvel utilisateur
                    $userId = $this->User->getLastInsertID();
                    // Création de son dossier
                    $this->User->createUserFolder($userId);
                    /**
                     * Création de l'enregistrement visiteur
                     */
                    // ID
                    $this->data['Visitor']['id'] = $userId;
                    // Formatage
                    $this->data['Visitor']['name'] = $this->FormatInput->formatName($this->data['Visitor']['name']);
                    $this->data['Visitor']['firstname'] = $this->FormatInput->formatName($this->data['Visitor']['firstname']);
                    $this->data['Visitor']['function'] = $this->FormatInput->capFirst($this->data['Visitor']['function']);
                    $this->data['Visitor']['phone'] = $this->FormatInput->formatPhone($this->data['Visitor']['phone']);
                    // Préparation du courriel pour l'administrateur
                    $userData['User']['name'] = $this->data['Visitor']['name'];
                    $userData['User']['firstname'] = $this->data['Visitor']['firstname'];
                    $userData['User']['structure'] = $this->data['Visitor']['structure'];
                    $userData['User']['function'] = $this->data['Visitor']['function'];
                    // Enregistrement
                    $this->Visitor->create($this->data);
                    $this->Visitor->save($this->data, array(
                        'validate' => false,
                        'callbacks' => false,
                        'fieldList' => array(
                            'id',
                            'name',
                            'firstname',
                            'function',
                            'structure',
                            'phone',
                            'address',
                            'country_id'
                        )
                    ));
                    /**
                     * Envoi du courriel utilisateur
                     */
                    // Adresse cible
                    $this->AppEmail->to = $userData['User']['email'];
                    // Adresse source
                    if (!isset($this->AppEmail->from))
                        $this->AppEmail->from = $this->User->getEmail(1);
                    // Sujet
                    $this->AppEmail->subject .= '-> ' . __("Account creation", true);
                    // Template
                    $this->AppEmail->template = 'account';
                    // Envoi des données aux éléments html/txt
                    $userData['User']['password'] = $password['clear'];
                    $this->set('data', $userData['User']);
                    $this->set('inactive', true);
                    // Envoi
                    $this->AppEmail->send();
                    /**
                     * Envoi du courriel administrateur
                     */
                    // Adresse cible
                    $this->AppEmail->to = $this->AppEmail->from;
                    // Adresse source
                    $this->AppEmail->from = Configure::read('epel.email.from');
                    if (!isset($this->AppEmail->from))
                        $this->AppEmail->from = $userData['User']['email'];
                    // Template
                    $this->AppEmail->template = 'admin';
                    // Envoi des données aux éléments html/txt
                    $this->set('data', $userData['User']);
                    // Envoi
                    $this->AppEmail->send();
                    // Sortie
                    return $this->setAction('accountReady');
                }
            }
        }
        // Contenu de la liste déroulante
        $this->set('country', $this->L10nSort->setList($this->Visitor->Country->find('list'), 'country'));
    }


    function forgotten() {

        // Traitement
        if (!empty($this->data)) {
            $updatedAccount = $this->User->resetPassword($this->data['User']['email']);
            if ($updatedAccount) {
                /**
                 * Envoi du courriel
                 */
                // Adresse cible
                $this->AppEmail->to = $updatedAccount['User']['email'];
                // Adresse source
                if (!isset($this->AppEmail->from))
                    $this->AppEmail->from = $this->User->getEmail(1);
                // Sujet
                $this->AppEmail->subject .= '-> ' . __("Forgotten password", true);
                // Template
                $this->AppEmail->template = 'account';
                // Envoi des données aux éléments html/txt
                $this->set('data', $updatedAccount['User']);
                $this->set('forgotten', true);
                // Envoi
                $this->AppEmail->send();
                // Sortie
                return $this->setAction('accountReady');
            } else {
                // Utilisateur non trouvé
                return $this->setAction('noUserFound');
            }
        }
    }


    function accountReady() {

    }


    function noUserFound() {

    }


}

