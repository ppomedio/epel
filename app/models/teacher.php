<?php

/**
 * {app}/models/teacher.php
 */
class Teacher extends AppModel {

    var $name = 'Teacher';
    var $useDbConfig = 'epel';
    // Init. du tableau de validation
    var $validate = array();
    // Comportements
    var $actsAs = array(
        'Containable',
        'CleanData'
    );
    // Relations
    var $hasOne = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'id',
            'conditions' => array('User.role' => 'teacher')
        )
    );
    var $hasMany = array(
        'TeGroup' => array(
            'className' => 'TeGroup',
            'foreignKey' => 'teacher_id'
        )
    );
    var $belongsTo = array(
        'University' => array(
            'className' => 'University',
            'foreignKey' => 'university_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Please enter a name.", true)
                )
            ),
            'firstname' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your first name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Please enter a first name.", true)
                )
            ),
            'university_id' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please select a university.", true)
            ),
            'quality' => array(
                'rule' => array('custom', '/^[a-zA-Z0-9 :.()\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                'allowEmpty' => true,
                'message' => __d('validation', "Some characters are not allowed…", true)
            ),
            'specialty' => array(
                'rule' => array('custom', '/^[a-zA-Z0-9 :.()\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                'allowEmpty' => true,
                'message' => __d('validation', "Some characters are not allowed…", true)
            ),
            'phone' => array(
                'rule' => array('custom', '/^[0-9-.,\s\+()]+$/'),
                #'rule' => array('custom', '/^0[1-689]([-.,\s]?\d{2}){4}$/'),
                'allowEmpty' => true,
                'message' => __d('validation', "This phone number is not valid.", true)
            ),
            'secret' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your student account creation code.", true),
                    'last' => true
                ),
                'between' => array(
                    'rule' => array('between', 10, 60),
                    'message' => __d('validation', "Secret phrase must be between 10 and 60 characters long.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z0-9 :;,.?!()\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^\[\]\{\}\(\)\^\+\*\$\|\/\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This account creation code is already used.", true)
                )
            ),
            'code' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter the current teacher account creation code for your university.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z0-9 :;,.?!()\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^\[\]\{\}\(\)\^\+\*\$\|\/\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'check' => array(
                    'rule' => array('checkCode'),
                    'message' => __d('validation', "This account creation code does not exist.", true)
                )
            )
        );

        parent::__construct();
    }


    /**
     * Création de compte : validation du code université
     *
     */
    function checkCode() {

        if (ClassRegistry::init('University')->find('count', array(
                    'conditions' => array(
                        'University.secret' => $this->data['Teacher']['code'],
                        'University.id >' => 2
                        ))))
            return true;
        else
            return false;
    }


    /**
     * Création de compte : université
     *
     * @return <array>
     */
    function getUniversityInfo() {

        $data = ClassRegistry::init('University')->find('first', array(
            'conditions' => array(
                'University.id >' => 2,
                'University.secret' => $this->data['Teacher']['code']
            ),
            'fields' => array('id', 'name'),
            'recursive' => -1
        ));

        if (!empty($data))
            return $data;
        else
            return array(
                'University.id' => 1,
                'University.name' => __("Fictitious university", true)
            );
    }


    /**
     * Profil : modification des règles de validation
     *
     */
    function validateProfile() {

        // Champ université desactivé
        unset($this->validate['university_id']);
    }


    /**
     * Recherche : modification des règles de validation
     *
     */
    function validateSearch() {

        // Suppression des règles "non-vide"
        unset($this->validate['name']['notEmpty']);
        unset($this->validate['firstname']['notEmpty']);
        unset($this->validate['university_id']);
        // Autorisation champs vides
        $this->validate['name']['format']['allowEmpty'] = true;
        $this->validate['firstname']['format']['allowEmpty'] = true;
    }


    /**
     * Nombre d'enseignants pour une université / total
     *
     * @return <int>
     */
    function count($university) {

        $count = array('total' => NULL);
        $count['university'] = $this->find('count', array(
            'conditions' => array(
                'Teacher.university_id' => $university
                )));
        if (((int) $university > 2) && ($this->University->find('count') > 3))
            $count['total'] = $this->find('count', array(
                'conditions' => array(
                    'Teacher.university_id >' => 2,
                    'Teacher.real' => 1
                    )));

        return $count;
    }


    /**
     * Chargement du profil
     *
     * @param <string> $id
     * @return <array>
     */
    function getProfile($id) {

        // Chargement du profil
        $profile = $this->find('first', array(
            'conditions' => array('Teacher.id' => $id),
            'fields' => array(
                'id',
                'name',
                'firstname',
                'university_id',
                'quality',
                'specialty',
                'phone',
                'secret'
            ),
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'id',
                        'email',
                        'username',
                        'photo'
                    )
                ),
                'University' => array(
                    'fields' => array('id', 'name')
                ),
                'TeGroup' => array(
                    'fields' => array('id', 'teacher_id', 'group')
                )
            )
                ));

        // Traduction des noms d'université statiques
        if ((int) $profile['University']['id'] < 3)
            $profile['University']['name'] = __($profile['University']['name'], true);

        #$this->log($profile, 'debug');
        return $profile;
    }


}

