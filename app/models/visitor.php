<?php

/**
 * {app}/models/visitor.php
 */
class Visitor extends AppModel {

    var $name = 'Visitor';
    var $useDbConfig = 'epel';
    // Init. du tableau de validation
    var $validate = array();
    // Comportements
    var $actsAs = array(
        'Containable',
        'CleanData'
    );
    // Relations
    var $hasOne = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'id',
            'conditions' => array('User.role' => 'visitor')
        )
    );
    var $hasMany = array(
        'Visit' => array(
            'className' => 'Visit',
            'foreignKey' => 'visitor_id',
            'dependent' => true
        )
    );
    var $belongsTo = array(
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Please enter a name.", true)
                )
            ),
            'firstname' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your first name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Please enter a first name.", true)
                )
            ),
            'function' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please enter your function.", true)
            ),
            'structure' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please enter the structure you work for.", true)
            ),
            'phone' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your phone number.", true),
                    'last' => true
                ),
                'format' => array(
                    #'rule' => array('custom', '/^0[1-689]([-.,\s]?\d{2}){4}$/'),
                    'rule' => array('custom', '/^[0-9-.,\s\+()]+$/'),
                    'message' => __d('validation', "This phone number is not valid.", true)
                )
            ),
            'url' => array(
                'rule' => array('url', true),
                'allowEmpty' => true,
                'message' => __d('validation', "This Internet address is not valid.", true)
            ),
            'address' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please enter your address.", true)
            ),
            'country_id' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please select the country you live in.", true)
            )
        );

        parent::__construct();
    }


    /**
     * Recherche : modification des règles de validation
     *
     */
    function validateSearch() {

        // Suppression des règles "non-vide"
        unset($this->validate['name']['notEmpty']);
        unset($this->validate['firstname']['notEmpty']);
        unset($this->validate['structure']);
        // Autorisation champs vides
        $this->validate['name']['format']['allowEmpty'] = true;
        $this->validate['firstname']['format']['allowEmpty'] = true;
    }


    /**
     * Nombre de visiteurs inscrits
     *
     * @return <int>
     */
    function count() {

        return $this->find('count', array(
                    'conditions' => array(
                        'Visitor.real' => 1
                        )));
    }


    /**
     * Chargement du profil
     *
     * @param <string> $id
     * @return <array>
     */
    function getProfile($id) {

        return $this->find('first', array(
                    'conditions' => array('Visitor.id' => $id),
                    'fields' => array(
                        'id',
                        'name',
                        'firstname',
                        'function',
                        'structure',
                        'phone',
                        'url',
                        'address',
                        'country_id'
                    ),
                    'contain' => array(
                        'Country' => array(
                            'fields' => array('id', 'name')
                        ),
                        'User' => array(
                            'fields' => array(
                                'id',
                                'email',
                                'username',
                                'photo'
                            )
                        )
                    )
                ));
    }


}

