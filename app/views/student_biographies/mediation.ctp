
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <section id="mediation-edit">
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "Have I ever played a mediating role between people, or institutions of different languages?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "For instance, have I ever played the role of a translator, or an interpreter, trying to adjust my words to the linguistic and cultural context?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What did I do to adjust to the context?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What knowledge of the context did I have at first, and what did I do to augment my knowledge?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "From what language did I translate, and into what language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Have I carried out this mediating task in one or several languages?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What came out of it in concrete terms?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Have I contributed to any particular achievement?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What have I derived from the experience?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "How did it increase my overal language competencies?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <section><?php
                        echo $this->element('student/biography/mediation-form'); ?>

                    </section>
                </article>
                <aside><?php
                    if ($this->Session->read('Auth.User.portfolios'))
                        echo $this->element('student/part-access', array('part' => 'biography', 'biography' => true));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#mediation-edit');
");

?>