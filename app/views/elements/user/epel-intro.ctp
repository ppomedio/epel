
                    <section>
                        <div id="old_ies_are_buggy" class="alert hidden">
                            <p class="alert">
                                <?php echo __("The eELP is not currently compatible with your version of Internet Explorer. You can use another browser such as", true) . ' '; ?><a href="http://www.mozilla-europe.org/<?php echo $this->params['language']; ?>/firefox/">Firefox</a> <?php __("or"); ?> <a href="http://www.apple.com/<?php echo $this->params['language']; ?>/safari/">Safari</a> <?php __("or, if you use Windows 7&reg; or Vista&reg;, upgrade your browser by downloading IE9."); ?>
                            </p>
                        </div>
                        <!-- Message IE < 9 -->
                        <script type="text/javascript">
                        //<![CDATA[
                        if (($.browser.msie && $.browser.version < 9) || (jQuery.support.leadingWhitespace === false)) {
                            addTipAlert('#old_ies_are_buggy');
                            $('#old_ies_are_buggy').slideDown('slow');
                        }
                        //]]>
                        </script>
                    </section>
                    <section class="half left">
                        <h2><?php __d('information', "Introduction"); ?></h2>
                        <div>
                            <p><?php __d('information', "This European Language Portfolio is a virtual space for students to keep a record of their ongoing language skills and cultural experience, and thus help them reflect on their language learning."); ?></p>
                            <p><strong><?php __d('information', "It is the property of his/her holder."); ?></strong></p>
                        </div>
                        <h2><?php __d('information', "Aims"); ?></h2>
                        <ul>
                            <li><strong><?php __d('information', "Motivate learners"); ?></strong> <?php __d('information', "by supporting their efforts to extend and diversify their language skills for different competencies at different levels."); ?></li>
                            <li><strong><?php __d('information', "Provide a statement of their language and cultural skills"); ?></strong> <?php __d('information', "they have achieved during their various experiments."); ?></li>
                        </ul>
                    </section>
                    <section class="half right">
                        <h2><?php __d('information', "Contents"); ?></h2>
                        <ul>
                            <li><strong><?php __d('information', "A language passport"); ?></strong> <?php __d('information', "based on the"); ?> <a id="<?php __("Visit the Europass website"); ?>" class="link" href="<?php echo Configure::read('epel.europass.url'); ?>">Europass</a> <?php echo __d('information', "Language Passport (&copy; European Union and Council of Europe).", true) . ' ' . __d('information', "New languages may be added on request using the", true);
                            echo ' '. $this->Html->link(__("contact form", true),
                                array(
                                    'controller' => 'contact',
                                    'action' => 'form'
                                ),
                                array(
                                    'id' => __('Send us an email', true),
                                    'class' => 'iframe contact link'
                                )
                            );
                            ?>.</li>
                            <li><strong><?php __d('information', "A detailed language biography"); ?></strong> <?php __d('information', "including the learner's experience in each language, to help improve planning and assessment and facilitate self-assessment or assessment by a teacher."); ?></li>
                            <li><strong><?php __d('information', "A dossier"); ?></strong> <?php __d('information', "containing personal work attesting the performance in each of the foreign languages mentioned as well as diplomas, certificates, CVs and learning material (recordings, texts, vocabulary, exercises, and homework of all kinds)."); ?></li>
                            <li><strong><?php __d('information', "A self-assessment system"); ?></strong> (&copy; <a id="<?php __("Visit CercleS's website"); ?>" class="link" href="<?php echo Configure::read('epel.cercles.url'); ?>">CercleS</a>) <?php __d('information', "using a set of checklists allowing to reflect on competencies at a given level and to set new objectives."); ?></li>
                        </ul>
                    </section>
                    <section>
                        <h2><?php __d('information', "Major functions"); ?></h2>
                        <dl>
                            <dt><?php __d('information', "The learning function"); ?></dt>
                            <dd><?php __d('information', "Enhance learner motivation to improve the ability to communicate in different languages, learn other languages, endeavour to go through new intercultural experiences."); ?></dd>
                            <dd><?php __d('information', "Provide learners with guidance on how to meet their objectives, improve their learning methods and success in foreign languages, plan the learning process, learn autonomously."); ?></dd>
                            <dd><?php __d('information', "Encourage learners to increase their multilingual and intercultural experience through contacts and visits, readings, using the media, or developing projects."); ?></dd>
                            <dt><?php __d('information', "The documentation and presentation functions"); ?></dt>
                            <dd><?php __d('information', "The Electronic Language Portfolio is intended to document as fully as possible its holder's multilingual language skills so as to be able to inform others in details about its holder's skills and allow for international comparisons. It is also made to enable the holder to record their experiences in other languages and cultures."); ?></dd>
                        </dl>
                    </section>
                    <section id="coe-text">
                        <h2><a id="coe-title-link" class="trigger link" href="<?php echo Configure::read('epel.coe.url'); ?>"><?php __d('information', "The Council of Europe"); ?></a></h2>
                        <div class="trigger info">
                            <img class="fade" src="/img/coe_img_01.jpg" />
                            <p><?php __d('information', "The Council of Europe is a political intergovernmental organisation founded in 1949 with its permanent headquarters in Strasbourg, France. Its mission is to guarantee democracy, human rights and justice in Europe."); ?></p>
                            <p><?php __d('information', "The Council of Europe aims to build a greater Europe based on shared values, including tolerance and respect for cultural and linguistic diversity. It promotes the use of the European Language Portfolio as a practical means of helping people of all ages and backgrounds to learn more languages and engage with other cultures. The ELP is also a means of presenting language skills, at any level and however acquired, in a clearly understandable way, using a common European system of six proficiency levels."); ?></p>
                        </div>
                        <div id="data_coe-title-link" class="tooltip-data"><?php __('Visit the website of the Council of Europe'); ?></div>
                    </section>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Message IE < 9
    if (($.browser.msie && $.browser.version < 9) || (jQuery.support.leadingWhitespace === false)) {
        addTipAlert('#old_ies_are_buggy');
        $('#old_ies_are_buggy').slideDown('slow');
    }

");
*/
?>
