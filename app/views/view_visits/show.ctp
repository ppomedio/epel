
                <article>
                    <h1 id="title-visit-page" class="spaced"><?php __("This Portfolio has been visited by…"); ?></h1>
                    <section><?php
                        echo $this->element('visits-page'); ?>

                    </section>

                </article>
                <aside><?php
                    echo $this->element('view/student'); ?>

                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="visit-info" class="information">
                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                            <p class="info"><?php __("You can send email to visitors by clicking on their name."); ?></p>
                        </div>
                    </div><?php
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#visit-info');
");

?>