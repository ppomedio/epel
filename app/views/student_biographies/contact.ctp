
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-contact"><?php __d('portfolio', "My contacts with speakers of the language"); ?></h2>
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "Outside the classroom, have you had any memorable contacts with speakers of the language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "In what context (such as with friends, in the workplace, in an everyday context, or using social networks)?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "How do you keep in contact with these speakers?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="contact-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new experience"); ?></h2>
                        <?php echo $this->Form->create('PoContact', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/contact/' . urlencode($language),
                            'id' => 'contact-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoContact'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __d('portfolio', "Context", true)
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <p class="info"><?php __d('portfolio', "If possible, write in the concerned language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/biography/contact-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography'));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('student/biography/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('.instruction');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#contact-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#contact-edit');
    // Initialisation du formulaire
    function initForm() {
        $('#contact-edit-form #PoContactId').val('');
        $('#contact-edit-form #PoContactName').val('');
        $('#contact-edit-form #PoContactDescription').val('');
    };
    // Soumission Ajax du formulaire
    $('#contact-edit-form').submit(function() {
        if (!$('#contact-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/biography/contact/" . urlencode($language) . "',
                target: '#contact',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#contact-edit-form div.loading').show();
                },
                success: function() {
                    $('#contact-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#contact-toggle').hide();
                    $('#contact-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#contact-edit-form div.loading').hide();
                    $('#contact-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>