<?php

/**
 * Pas de réglage de visibilité si le portfolio est vide
 */

if ($this->Session->read('Auth.User.portfolios')) { ?>

                    <section>
                        <h2><?php __("Teachers visibility"); ?></h2>
                        <?php echo $this->Form->create('Student', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/global_access',
                            'id' => 'teacher-access-form',
                            'class' => 'white'
                            ));
                        echo $this->Form->hidden('id'); ?>

                        <p class="info"><?php __("Every teacher in your university can see your profile but only the referring teacher of your group can see your Portfolio."); ?></p>
                        <fieldset>
                            <dl>
                                <dt id="visibility-global" class="help"><?php echo $this->Form->label('tea_hide', __("Hide your Portfolio to your referring teacher?", true)); ?></dt>
                                <dd class="master"><?php echo $this->Form->checkbox('tea_hide'); ?></dd>
                            </dl>
                        </fieldset>
                        <div class="tail"><div class="loading"></div></div>
                        <?php echo $this->Form->end(); ?>

                        <div id="data_visibility-global" class="tooltip-data"><?php
                        echo '<span class="bolder alerttext">' . __("YES", true) . __(":", true) . '</span> ' . __("no teacher can see your portfolio.", true)
                                . '<br /><span class="bolder">' . __("NO", true) . __(":", true) . '</span> ' . __("only your referring teacher can see your portfolio.", true);
                        ?></div>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form',
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

// iButton -> buffer Js
$this->Js->Buffer("
    // Visibilités globales (iButton)
    $('#teacher-access-form fieldset dl dd input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            buttonId = $(this).attr('id');
            visButton = buttonId.substring(10, buttonId.length).toLowerCase();
            if($(this).is(':checked'))
                buttonVal = true;
            else
                buttonVal = false;
            $('#teacher-access-form').ajaxSubmit({
                data: { visButton: 'tea_' + visButton },
                beforeSubmit: function() {
                    $('#teacher-access-form div.tail div.loading').show();
                },
                success: function() {
                    $('#teacher-access-form div.tail div.loading').hide();
                }
            });
        });
    // Tooltips
    tooltipHelp('#teacher-access-form');
    // Tips
    addTipInfo('#teacher-access-form');
");

}
?>