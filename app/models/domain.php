<?php

/**
 * {app}/models/domain.php
 */
class Domain extends AppModel {

    var $name = 'Domain';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $hasMany = array(
        'StudentDomain' => array(
            'className' => 'Student',
            'foreignKey' => 'domain_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a field of study name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z ()\.,\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This field of study name already exists.", true)
                )
            )
        );

        parent::__construct();

    }


}

