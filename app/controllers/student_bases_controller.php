<?php

/**
 * {app}/controllers/student_bases_controller.php
 *
 */
class StudentBasesController extends AppController {

    var $name = 'StudentBases';
    // Modèle
    var $uses = 'Student';
    // Composants
    var $components = array(
        'L10nSort',
        'FormatInput'
    );
    // Assistants
    var $helpers = array(
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Autorisation des champs "password" vides
        if ($this->params['action'] == 'connect')
            $this->Student->User->allowEmptyPasswords();

        // Retour…
        parent::beforeFilter();

    }


    /**
     * Affichage du profil
     *
     */
    function index() {

        // Chargement des données
        $this->data = $this->_getProfile($this->AppAuth->user('id'));
        $user = $this->data;
        #$this->log($user, 'debug');
        // Variables de vue
        $this->set('title_for_layout', __("Base", true));
        $this->set('user', $user);

    }


    /**
     * Mise à jour du profil
     *
     */
    function profile() {

        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->_getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Formatage (ne fonctionne pas dans beforeValidate…)
            $this->data['Student']['name'] = $this->FormatInput->formatName($this->data['Student']['name']);
            $this->data['Student']['firstname'] = $this->FormatInput->formatName($this->data['Student']['firstname']);
            if (!empty($this->data['Student']['specialty']))
                $this->data['Student']['specialty'] = $this->FormatInput->capFirst($this->data['Student']['specialty']);
            // Remplacement de clé (ordre d'affichage dans la liste déroulante, voir models/diploma.php)
            if ($this->data['Student']['diploma_id'] == '99')
                $this->data['Student']['diploma_id'] = '10';
            // Envoi des données au modèle
            $this->data['Student']['id'] = $this->AppAuth->user('id');
            $this->Student->set($this->data);
            // Si validation : sauvegarde
            if ($this->Student->validates()) {
                // Préparation maj des variables de session
                $updateSession = $this->Session->read('Auth.User.info');
                $updateSession['name'] = $this->data['Student']['name'];
                $updateSession['firstname'] = $this->data['Student']['firstname'];
                // Sauvegarde sans validation
                if ($this->Student->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array(
                                'name',
                                'firstname',
                                'birth',
                                'origin_country_id',
                                'living_country_id',
                                'diploma_id',
                                'diploma_info',
                                'domain_id',
                                'domain_info',
                                'specialty'
                            )
                        ))) {
                    // Maj des variables de session
                    $this->Session->write('Auth.User.info', $updateSession);
                    // Message
                    $this->Session->setFlash(__("Your profile has been updated.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/student/base');
                }
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->_getProfile($this->AppAuth->user('id'));
            $this->data['Student'] = $this->data['Student'] + $user['Student'];
        }
        // Contenu des listes déroulantes
        $temp = $this->L10nSort->setList($this->Student->OriginCountry->find('list'), 'country');
        $this->set('originCountry', $temp);
        $this->set('livingCountry', $temp);
        $this->set('diploma', $this->Student->Diploma->getDiplomasList());
        $this->set('domain', $this->L10nSort->setList($this->Student->Domain->find('list'), 'domain', false));
        // Variables de vue
        $this->set('title_for_layout', __("Profile edition", true));
        $this->set('user', $user);

    }


    /**
     * Édition des infos de connexion / courriel
     *
     */
    function connect() {

        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->_getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Envoi des données au modèle
            $this->data['Student']['id'] = $this->AppAuth->user('id');
            $this->data['User']['id'] = $this->AppAuth->user('id');
            $this->Student->set($this->data);
            // Préparation maj des variables de session
            $updateSession = array(
                'email' => $this->data['User']['email'],
                'username' => $this->data['User']['username']
            );
            // Sauvegarde avec validation
            if ($this->Student->User->save($this->data, array(
                        'validate' => true,
                        'fieldList' => array(
                            'email',
                            'username',
                            'password',
                            'passwd',
                            'passwd_confirm'
                        )
                    ))) {
                // Maj des variables de session
                $this->Session->write('Auth.User.email', $updateSession['email']);
                $this->Session->write('Auth.User.username', $updateSession['username']);
                // Message
                $this->Session->setFlash(__("Your information has been updated.", true));
                // Redirection
                $this->redirect('/' . $this->params['language'] . '/student/base');
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->_getProfile($this->AppAuth->user('id'));
            $this->data['Student'] = $this->data['Student'] + $user['Student'];
        }
        // Variables de vue
        $this->set('title_for_layout', __("Connection / Email data edition", true));
        $this->set('user', $user);

    }


    /**
     * Mise à jour du code groupe
     *
     */
    function group() {

        // Modification des règles de validation
        $this->Student->validateUniversity();
        // Traitement
        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->_getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Formatage (ne fonctionne pas dans beforeValidate…)
            if (!empty($this->data['Student']['group']))
                $this->data['Student']['group'] = strtoupper(trim($this->data['Student']['group']));
            // Envoi des données au modèle
            $this->data['Student']['id'] = $this->AppAuth->user('id');
            $this->data['Student']['university_id'] = $this->Session->read('Auth.User.info.university_id');
            $this->Student->set($this->data);
            // Si validation : sauvegarde
            if ($this->Student->validates()) {
                // Sauvegarde sans validation
                if ($this->Student->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array('group')
                        ))) {
                    // Message
                    $this->Session->setFlash(__("Your group code has been updated.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/student/base');
                }
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->_getProfile($this->AppAuth->user('id'));
            $this->data['Student'] = $this->data['Student'] + $user['Student'];
        }
        // Variables de vue
        $this->set('user', $user);
        $this->set('university', $this->Student->University->getUniversityList(false, true));
        $this->set('title_for_layout', __("Group code edition", true));

    }


    /**
     * Ajout d'une nouvelle langue au portfolio
     *
     */
    function language_add() {

        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->_getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Chargement variable de session
            $languages = $this->Session->read('Auth.User.languages');
            $portfolios = $this->Session->read('Auth.User.portfolios');
            $visibility = $this->Session->read('Auth.User.visibility');
            // Si validation : traitement
            if ($this->Student->Portfolio->validates()) {
                $newLangId = $this->data['Portfolio']['language_id'];
                $this->Student->Portfolio->set(array(
                    'student_id' => $this->AppAuth->user('id'),
                    'language_id' => $this->data['Portfolio']['language_id']
                ));
                if ($this->Student->Portfolio->save()) {
                    // Maj variable de session
                    $temp = $this->Student->Portfolio->Language->find('first', array(
                                'conditions' => array('Language.id' => $newLangId),
                                'fields' => array('Language.name'),
                                'recursive' => 0
                            ));
                    $languages[$newLangId]['portfolio_id'] = $this->Student->Portfolio->id;
                    $languages[$newLangId]['name'] = $temp['Language']['name'];
                    $languages[strtolower($languages[$newLangId]['name'])] = (int) $newLangId;
                    $portfolios[strtolower($languages[$newLangId]['name'])]['id'] = (int) $languages[$newLangId]['portfolio_id'];
                    $visibility[strtolower($languages[$newLangId]['name'])]['passport'] = 1;
                    $visibility[strtolower($languages[$newLangId]['name'])]['biography'] = 1;
                    $visibility[strtolower($languages[$newLangId]['name'])]['dossier'] = 1;
                    $this->Session->delete('Auth.User.languages');
                    $this->Session->delete('Auth.User.portfolios');
                    $this->Session->delete('Auth.User.visibility');
                    $this->Session->write('Auth.User.languages', $languages);
                    $this->Session->write('Auth.User.portfolios', $portfolios);
                    $this->Session->write('Auth.User.visibility', $visibility);
                    // Journalisation de l'ajout
                    $this->logThis('lang_add', $languages[$newLangId]['name']);
                    // Rechargement du menu
                    $this->Menu->setMenu();
                    // Message
                    $this->Session->setFlash(__("A new language has been added to your Portfolio.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/student/base');
                }
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->_getProfile($this->AppAuth->user('id'));
            $this->data['Student'] = $this->data['Student'] + $user['Student'];
        }
        // Contenu des listes déroulantes
        $notIn = array('0001');
        if (!empty($user['Portfolio'])) {
            foreach ($user['Portfolio'] as $key => $value) {
                $notIn[] = $value['Language']['id'];
            }
        }
        $this->set('newLanguages', $this->L10nSort->setList(
                        $this->Student->Portfolio->Language->find('list',
                                array(
                                    'conditions' => array(
                                        'NOT' => array(
                                            'Language.id' => $notIn
                                        )
                                ))), 'language'));
        // Variables de vue
        $this->set('title_for_layout', __("Language addition", true));
        $this->set('user', $user);

    }


    /**
     * Suppression d'une langue du portfolio
     *
     */
    function language_delete() {

        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->_getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Chargement variable de session
            $languages = $this->Session->read('Auth.User.languages');
            $portfolios = $this->Session->read('Auth.User.portfolios');
            $visibility = $this->Session->read('Auth.User.visibility');
            // Si validation : traitement
            if ($this->Student->Portfolio->validates()) {
                /**
                 * Suppression de la langue choisie
                 */
                if (!empty($this->data['Portfolio']['language_id'])) {
                    // Suppression des fichiers associés aux documents
                    $this->loadModel('PoDocument');
                    $this->PoDocument->deleteLanguageDocumentsFiles($this->AppAuth->user('id'), $languages[$this->data['Portfolio']['language_id']]['portfolio_id']);
                    // Suppression (enregistrements associés compris)
                    $this->Student->Portfolio->delete($languages[$this->data['Portfolio']['language_id']]['portfolio_id'], $cascade = true);
                    // Journalisation de la suppression
                    $this->logThis('lang_delete', $languages[$this->data['Portfolio']['language_id']]['name']);
                    // Maj variable de session
                    unset($visibility[strtolower($languages[$this->data['Portfolio']['language_id']]['name'])]);
                    unset($portfolios[strtolower($languages[$this->data['Portfolio']['language_id']]['name'])]);
                    unset($languages[strtolower($languages[$this->data['Portfolio']['language_id']]['name'])]);
                    unset($languages[$this->data['Portfolio']['language_id']]);
                    $this->Session->delete('Auth.User.languages');
                    $this->Session->delete('Auth.User.portfolios');
                    $this->Session->delete('Auth.User.visibility');
                    $this->Session->write('Auth.User.languages', $languages);
                    $this->Session->write('Auth.User.portfolios', $portfolios);
                    $this->Session->write('Auth.User.visibility', $visibility);
                    //Réinitialisation visibilité si portfolio vide
                    if (empty($portfolios)) {
                        $this->Student->id = $this->Session->read('Auth.User.id');
                        $this->Student->saveField('vis_hide', 1);
                    }
                    // Rechargement du menu
                    $this->Menu->setMenu();
                    // Message
                    $this->Session->setFlash(__("A language has been removed from your Portfolio.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/student/base');
                }
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->_getProfile($this->AppAuth->user('id'));
            $this->data['Student'] = $this->data['Student'] + $user['Student'];
        }
        // Contenu des listes déroulantes
        $languages = array();
        if (!empty($user['Portfolio'])) {
            foreach ($user['Portfolio'] as $key => $value) {
                $languages[$value['Language']['id']] = $value['Language']['name'];
            }
        }
        $this->set('currentLanguages', $this->L10nSort->setList($languages, 'language', true));
        // Variables de vue
        $this->set('title_for_layout', __("Language deletion", true));
        $this->set('user', $user);

    }


    /**
     * Droits d'accès globaux des enseignants et visiteurs
     *
     */
    function global_access() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Enregistrement de la nouvelle valeur
            $field = $_POST['visButton'];
            $this->Session->write('Auth.User.info.' . $field, $this->data['Student'][$field]);
            $this->Student->id = $this->data['Student']['id'];
            $this->Student->saveField($field, $this->data['Student'][$field]);
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    /**
     * Droits d'accès des visiteurs par partie / par langue
     *
     */
    function visitor_part_access() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Enregistrement de la nouvelle valeur
            $this->Session->write('Auth.User.visibility.' . $_GET['language'] . '.' . $_GET['field'], $_GET['state']);
            $portfolios = $this->Session->read('Auth.User.portfolios');
            $this->Student->Portfolio->id = $portfolios[$_GET['language']];
            $this->Student->Portfolio->saveField('vis_' . $_GET['field'], $_GET['state']);
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    /**
     * Suppression du Portfolio
     *
     */
    function delete() {

        // Vérif. portfolio vide
        $portfolio = $this->AppAuth->user('portfolios');
        if (!empty($portfolio))
            $this->redirect('/' . $this->params['language'] . '/student/base');
        // Traitement
        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->_getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Validation
            if ($this->Student->User->validates()) {
                // Vérification mdp + confirmation
                if ($this->Student->User->isMyPassword($this->data['User']['password'],
                                $this->AppAuth->user('id'))
                        && ($this->data['User']['delete_confirm']) == 1) {
                    // Journalisation de la suppresson
                    $this->logThis('acc_delete', $this->Session->read('Auth.User.info.fullname')
                            . ' (' . $this->Session->read('Auth.User.email') . ')');
                    // Suppression de l'étudiant
                    $this->Student->delete($this->AppAuth->user('id'));
                    // Suite…
                    $this->redirect('/' . $this->params['language'] . '/users/delete');
                } else {
                    // Message
                    $this->Session->setFlash(__("Deletion cancelled: password does not match.", true) . '…');
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/student/base');
                }
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->_getProfile($this->AppAuth->user('id'));
            $this->data['Student'] = $this->data['Student'] + $user['Student'];
        }
        // Variables de vue
        $this->set('title_for_layout', __("Portfolio deletion", true));
        $this->set('user', $user);

    }


    private function _getProfile($userId) {

        // Chargement des données depuis le modèle
        $profile = $this->Student->getProfile($userId);
        // Tri alphabétique des langues
        $portfolios = $profile['Portfolio'];
        $profile['Portfolio'] = $this->L10nSort->sortLocalized($portfolios);

        return $profile;

    }


}

