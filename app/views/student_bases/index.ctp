
                <article class="half"><?php
                    echo $this->element('student/base/group-show');
                    if (((int)$user['University']['id'] > 1) && !empty($user['Group']['Teacher']))
                        echo $this->element('student/base/teacher-access');
                    echo $this->element('student/base/visitor-access');
                    ?>

                </article>
                <article class="half"><?php
                    echo $this->element('connect-show');
                    echo $this->element('student/base/profile-show');
                    echo $this->element('student/base/lang-show');
                    ?>

                </article>
                <aside><?php
                    echo $this->element('photo');
                    echo $this->element('student/base/how-to-use');
                    #echo $this->element('student/base/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipEdit('article');
    tooltipAdd('article');
    tooltipSupp('article');
");

?>