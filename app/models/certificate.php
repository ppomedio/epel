<?php

/**
 * {app}/models/certificate.php
 */
class Certificate extends AppModel {

    var $name = 'Certificate';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array(
        'Containable',
        'CleanData'
    );
    // Relations
    var $hasMany = array(
        'PoCertificate' => array(
            'className' => 'PoCertificate',
            'foreignKey' => 'certificate_id'
        )
    );
    var $belongsTo = array(
        'Language' => array(
            'className' => 'Language',
            'foreignKey' => 'language_id'
        ),
        'Certification' => array(
            'className' => 'Certification',
            'foreignKey' => 'certification_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'language_id' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please select a language for this certification.", true),
                    'last' => true
                ),
                'compare' => array(
                    'rule' => array('certificateDoNotExists'),
                    'message' => __d('validation', "This certificate already exists.", true)
                )
            ),
            'certification_id' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please select a certification.", true),
                    'last' => true
                )
            )
        );

        parent::__construct();

    }


    /**
     * Fonction de validation :
     * égalité des champs "password"
     *
     * @return <bool>
     */
    function certificateDoNotExists() {

        if ($this->find('count', array(
                    'conditions' => array(
                        'language_id' => $this->data['Certificate']['language_id'],
                        'certification_id' => $this->data['Certificate']['certification_id']
                    ),
                    'recursive' => -1
                )))
            return false;
        else
            return true;

    }


    /**
     * Chargement de la liste des certificats pour une langue
     *
     * @param <string> $languageId
     * @return <array>
     */
    function getCertificatesList($languageId) {

        // Chargement
        $data = $this->find('all', array(
                    'conditions' => array('Certificate.language_id' => $languageId),
                    'fields' => array(
                        'id',
                        'language_id',
                        'certification_id'
                    ),
                    'contain' => array(
                        'Certification' => array(
                            'fields' => array(
                                'id',
                                'name'
                            ),
                            'order' => 'Certification.name ASC'
                        )
                    )
                ));
        // Mise en forme
        $certificateList = array();
        foreach ($data as $key => $value) {
            $certificateList[$value['Certificate']['id']] = $value['Certification']['name'];
        }
        // Envoi
        return $certificateList;

    }


}

