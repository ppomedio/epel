<?php

/**
 * {app}/models/language.php
 */
class Language extends AppModel {

    var $name = 'Language';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $hasMany = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'language_id'
        ),
        'Certificate' => array(
            'className' => 'Certificate',
            'foreignKey' => 'language_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a language name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z ()\.\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This language name already exists.", true)
                )
            )
        );

        parent::__construct();

    }


}

