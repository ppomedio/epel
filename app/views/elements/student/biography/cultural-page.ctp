<?php

$paginator->options(array(
    'update' => '#cultural'
    ));

?>

                        <div id="cultural" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="cultural-add" class="add"></span>
                                            <span id="cultural-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3">
                                             <p class="alert"><?php
                                             __d('portfolio', "There is no cultural experience recorded in your Biography at the moment.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#cultural');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="name"><?php echo $paginator->sort(__d('portfolio', "Type", true), 'PoCultural.name', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="cultural-add" class="add"></span>
                                            <span id="cultural-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoCultural'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="cultural-name_<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
                                        <td id="cultural-description_<?php echo $value['id']; ?>"><?php echo nl2br($value['description']); ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="cultural-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="cultural-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_cultural-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this experience");
                                    ?></div>
                                    <div id="data_cultural-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this experience");
                                    ?></div>
                                    <div id="cultural-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete this experience"); echo ' (' . $value['name'] . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'cultural'));
                            } ?>

                            <div id="data_sort-cultural-name" class="tooltip-data"><?php __("Sort by type"); ?></div>
                            <div id="data_cultural-add" class="tooltip-data"><?php __d('portfolio', "Add a new experience"); ?></div>
                            <div id="data_cultural-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#cultural-edit-form #PoCulturalDescription').val() == ''
            && $('#cultural-edit-form #PoCulturalName').val() == '') {
        if ($('#cultural-edit').is(':hidden'))
            $('#cultural-toggle').hide();
        else
            $('#cultural-add').hide();
    }
    // Id, classes et cie
    $('#cultural table thead tr th.name a').attr('id', 'sort-cultural-name').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#cultural table thead');
    tooltipAdd('#cultural table thead th.controls');
    tooltipGear('#cultural table thead th.controls');
    tooltipEdit('#cultural table tbody td.controls div.controls');
    tooltipSupp('#cultural table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#cultural-toggle').live('click', function() {
        if ($('#cultural-edit-form #PoCulturalId').val() == '')
            $('#cultural-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        if ($('#cultural-edit').is(':hidden'))
            $('#cultural-edit').slideDown('slow');
        else {
            $('#cultural-edit').slideUp('slow');
            $('#cultural-add').fadeIn('slow');
            if ($('#cultural-edit-form #PoCulturalDescription').val() == ''
                    && $('#cultural-edit-form #PoCulturalName').val() == '')
                $('#cultural-toggle').hide();
        }
    });
    // Ajout d'une expérience
    $('#cultural-add').live('click', function() {
        $('#cultural-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#cultural-edit').is(':hidden')) {
            $('#cultural-edit').slideDown('slow');
        }
        $('#cultural-add').hide();
        $('#cultural-toggle').fadeIn('slow');
    });
    // Édition d'une expérience
    $('#cultural table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#cultural-edit > h2').text('" . addslashes(__d('portfolio', "Edit an experience", true)) . "');
        // Chargement des données dans le formulaire
        $('#cultural-edit-form #PoCulturalId').val(itemId);
        $('#cultural-edit-form #PoCulturalName').val($('#cultural-name_' + itemId).text());
        $('#cultural-edit-form #PoCulturalDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Affichage si masqué
        if ($('#cultural-edit').is(':hidden')) {
            $('#cultural-add').hide();
            $('#cultural-edit').slideDown('slow');
            $('#cultural-add').fadeIn('slow');
            $('#cultural-toggle').fadeIn('slow');
        } else {
            if ($('#cultural-add').is(':hidden'))
                $('#cultural-add').fadeIn('slow');
        }
    });
    // Suppression d'une expérience
    $('#cultural table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#cultural-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
					var extract = new Array();
					extract = $(this).attr('id').split('_');
					var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#cultural').load('/" . $this->params['language'] . "/student/biography/delete/" . urlencode($language) . "/cultural/' + itemId, function() {
                            if ($('#cultural-edit-form #PoCulturalId').val() == itemId) {
                                $('#cultural-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
                                initForm();
                                if ($('#cultural-edit').is(':hidden'))
                                    $('#cultural-toggle').hide();
                                else
                                    $('#cultural-add').hide();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-cultural').html('" . addslashes(__d('portfolio', "My experience and knowledge of the culture", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>