
                    <div id="howtolearn-update">
                        <section>
                            <h2 id="howtolearn-motivation-title"><?php __d('portfolio', "Attitude and motivation"); ?></h2>
                            <?php
                            // Jeu de questions
                            $question = array(
                                101 => __d('portfolio', "analyse and discuss my motivation for learning?", true),
                                __d('portfolio', "think positively about different learning tasks?", true),
                                __d('portfolio', "use effective strategies to concentrate on learning tasks?", true),
                                __d('portfolio', "adopt a positive attitude to tasks that I do not enjoy?", true),
                                __d('portfolio', "make effective use of my weekly timetable?", true),
                            );
                            // Formulaire
                            echo $this->Form->create('PoHowtolearn', array(
                                'url' => '/' . Configure::read('Config.langCode')
                                    . '/student/biography/howtolearn',
                                'id' => 'howtolearn-motivation-form',
                                'class' => 'full table'
                                ));
                            ?>
                            <table class="howtolearn-table">
                                <thead>
                                    <tr>
                                        <?php
                                        for ($n = 1; $n < 7; $n++) {
                                            echo "<th>" . $tableHead[$n] . "</th>";
                                        } ?>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    for ($n = 101; $n < 106; $n++) {
                                        $idPart = (string)$n;
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        echo '<tr class="'. $trClass . '"><td>' . $question[$n] . '</td>';
                                        echo '<td>' . $this->Form->radio($idPart . 'answer', $options, $attributes) . '</td>';
                                        echo '<td class="master">' . $this->Form->checkbox($idPart . 'target') . '</td>';
                                        echo '<td>' . $this->Form->input($idPart . 'date', array('label' => false)) . '</td></tr>';
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->Form->hidden('part', array('default' => 1));
                            echo $this->Form->end(__("Update", true)); ?>

                        </section>
                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // iButtons
    $('#howtolearn-motivation-form table tbody input[type=checkbox]').iButton({
        labelOn: '" . __('YES', true) . "',
        labelOff: '" . __('NO', true) . "',
        resizeHandle: false,
        resizeContainer: true
        })
        .change(function() {
            // Activation du submit
            if ($('#howtolearn-motivation-form div.submit input[type=submit]').attr('disabled'))
                $('#howtolearn-motivation-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Instructions
    if ($('#howtolearn-intro').is(':hidden'))
        $('#howtolearn-intro').slideDown('slow');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#howtolearn-motivation-form div.submit input[type=submit]');
    // Activation du submit
    $('#howtolearn-motivation-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#howtolearn-motivation-form input[type=radio]').change(function() {
        if ($('#howtolearn-motivation-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-motivation-form div.submit input[type=submit]').removeAttr('disabled');
    });
    $('#howtolearn-motivation-form input[type=text]').keydown(function() {
        if ($('#howtolearn-motivation-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-motivation-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#howtolearn-motivation-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/howtolearn/1',
        target: '#howtolearn-update',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#howtolearn-motivation-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#howtolearn-motivation-form div.loading').show();
            $('#howtolearn-motivation-title').html('" . addslashes(__("Loading…", true)) . "');
            $('#howtolearn-update').fadeTo('fast', 0.6);
        },
        success: function() {
            $('#howtolearn-motivation-form div.loading').hide();
            $('#howtolearn-update').fadeTo('slow', 1);
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>