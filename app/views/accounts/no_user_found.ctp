
        <div>
            <h2><?php __("Unknown user"); ?></h2>
            <p><?php __("No user uses this email address, or corresponding account is deactivated."); ?></p>
        </div><?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Redimensionnement
    parent.$('#fancybox-inner').height(100);
    parent.$('#fancybox-wrap').height(120);
    parent.$('#fancybox-content').height(120);
    parent.$.fancybox.center();
");

?>