
                    <section id="creation-code">
                        <h2><?php __("Account creation code"); ?></h2>
                        <div>
                            <p class="info"><?php __("The “account creation code” is a phrase of your choice which you will indicate to those of your students who do not already have a Portfolio so that they can create one."); ?></p>
                            <p class="alert"><?php __("For security reasons, please renew this code regularly."); ?></p>
                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls">
                                    <?php
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/teacher/base/secret',
                                        array(
                                            'id' => 'creation-code',
                                            'class' => 'edit'
                                        )
                                    );
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div id="data_creation-code" class="tooltip-data"><?php __("Edit your account creation code"); ?></div>
                    </section>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Info tips
    addTipInfo('#creation-code');
    addTipAlert('#creation-code');
");

?>