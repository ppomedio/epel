<?php

if ($this->Session->check('Auth.User')) {
    $user = array();
    if ($this->Session->read('Auth.User.role') != 'admin') {
        $user['name'] = $this->Session->read('Auth.User.info.name');
        $user['firstname'] = $this->Session->read('Auth.User.info.firstname');
    } else {
        $user['name'] = '';
        $user['firstname'] = '';
    }
    $user['email'] = $this->Session->read('Auth.User.email');
} else {
    $user = array(
        'name' => '',
        'firstname' => '',
        'email' => ''
    );
}

echo $this->Form->create('Contact', array(
    'lang' => Configure::read('Config.langCode'),
    'action' => 'form',
    $receipt[0])
);

echo $validation->bind('Contact'); // Validation jQuery

// Titre
if ($receipt[0] == 0) {
    echo '<h2>';
    if (isset($receipt[2]))
        echo ucfirst(__("contact the administrator", true));
    else
        __("Contact us");
    echo '</h2>';
} else {
    echo '<h2>';
    __("Send an email to");
    echo '</h2>';
    echo '<h3>' . $receipt[1] . '</h3>';
}
?>

<fieldset>
<?php
if (!empty($user['name']))
    echo $this->Form->input('name',
            array(
                'label' => __("Name", true),
                'readonly'=>'readonly',
                'default' => $user['name']
            ));
else
    echo $this->Form->input('name',
            array(
                'label' => __("Name", true),
                'default' => ''
            ));

if (!empty($user['firstname']))
    echo $this->Form->input('firstname',
            array(
                'label' => __("First name", true),
                'readonly'=>'readonly',
                'default' => $user['firstname']
            ));
else
    echo $this->Form->input('firstname',
            array(
                'label' => __("First name", true),
                'default' => ''
            ));

echo $this->Form->input('email',
        array(
            'label' => __("eMail address", true),
            'default' => $user['email']
        ));

/**
 * Utilisation de Form->input au lieu de Form->textarea
 * sinon pas de validation jQuery…
 */
echo $this->Form->input('message',
        array(
            'label' => __("Message", true),
            'type' => 'textarea'
        ));
?>

</fieldset>
<?php

echo $this->Form->end(__("Send", true));

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

?>
