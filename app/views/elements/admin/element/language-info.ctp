
                        <h2><?php __("Translation"); ?></h2>
                        <div id="language-info">
                            <p class="alert"><?php __("You need to add/edit elements in english, as it is the native language of the application. Do not forget to update translation files in every interface language."); ?></p>
                            <p class="info"><?php
                            echo __("Translation files", true) . ' ('
                                    . $languageFile . ".po/.mo) "
                                    . __("are in the “locale” folder.", true);
                            ?></p>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#language-info');
    addTipAlert('#language-info');
");

?>
