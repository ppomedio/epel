<?php

/**
 * {app}/models/behaviors/data_cleaner.php
 *
 */
App::import('Core', 'Sanitize');

class CleanDataBehavior extends ModelBehavior {


    function beforeSave(&$Model) {

        /**
         * Sanitize :
         * on n'encode pas tous les caractères spéciaux (accents etc.),
         * on ne supprime pas tous les retours chariot (textareas).
         */
        $Model->data[$Model->alias] = Sanitize::clean($Model->data[$Model->alias], array(
                    'connection' => 'default',
                    'odd_spaces' => true,
                    'encode' => false,
                    'dollar' => true,
                    'carriage' => false,
                    'unicode' => true,
                    'escape' => false,
                    'backslash' => true
                ));

        /**
         * Nettoyage des champs :
         * retrait des "espaces" de début et fin,
         * retrait des tags HTML et PHP,
         * transformation des caractères spéciaux en entités HTML
         * (sauf les caractères accentués, cédilles etc.)
         */
        foreach ($Model->data[$Model->alias] as $key => $value) {
            if (!is_numeric($value) && $value !== null && !is_bool($value)) {
                $Model->data[$Model->alias][$key] = htmlspecialchars(
                                strip_tags(trim($value)),
                                ENT_NOQUOTES,
                                Configure::read('App.encoding'),
                                true
                );
            }
        }

        // Important sinon pas de sauvegarde
        return true;

    }


}

