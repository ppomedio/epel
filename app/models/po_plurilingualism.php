<?php

/**
 * {app}/models/po_plurilingualism.php
 */
class PoPlurilingualism extends AppModel {

    var $name = 'PoPlurilingualism';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'id'
        )
    );


    /**
     *
     *
     * @param <string> $studentId
     * @return <array>
     */
    function getPlurilingualism($studentId) {

        return $this->find('first', array(
            'conditions' => array('PoPlurilingualism.id' => $studentId),
            'fields' => array(
                'id',
                'description'
            ),
            'recursive' => -1
        ));

    }


}

