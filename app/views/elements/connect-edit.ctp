
                    <section>
                        <h2><?php __("Connection / Email"); echo " (";  __("editing"); echo "…)"; ?></h2>
                        <?php echo $this->Form->create('User', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/'
                                . $this->Session->read('Auth.User.role')
                                . '/base/connect',
                            'class' => ''
                            ));
                        echo $validation->bind('User'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('User.username', array(
                            'label' => __("Username", true)
                            ));
                        echo $this->Form->input('User.email', array(
                            'label' => __("eMail address", true),
                            'maxLength' => 128
                            ));
                        echo $this->Form->input('User.passwd', array(
                            'label' => __("Password", true),
                            'type' => 'password'
                            ));
                        echo $this->Form->input('User.passwd_confirm', array(
                            'label' => __("Password confirmation", true),
                            'type' => 'password'
                            ));
                        ?>
                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.dpassword'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

// Champs "password" -> buffer Js
$this->Js->Buffer("
    $('input[type=password]').dPassword({
        duration: 700,
        replacement: '%u25CF'
    });
");

?>