
                <article>
                    <section>
                        <h1 id="title-activity" class="spaced"><?php __("Activity log"); ?></h1>
                        <?php echo $this->element('admin/base/activity-page'); ?>

                    </section>
                </article>
                <aside>
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="activity-info" class="information">
                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                            <p class="info"><?php __("You can send email to users by clicking on their name."); ?></p>
                        </div>
                    </div>
                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#activity-info');
");

?>