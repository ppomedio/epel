
                        <?php echo $this->Form->create('PoHeritage', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/heritage',
                            'id' => 'heritage-edit-form',
                            'class' => 'full'
                            ));
                        ?>

                        <fieldset><?php
                        echo $this->Form->input('description', array(
                            'label' => false,
                            'rows' => '25'
                            ));
                        ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#heritage-edit-form div.submit input[type=submit]');
    // Activation du submit
    $('#heritage-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#heritage-edit-form textarea').keydown(function() {
        if ($('#heritage-edit-form div.submit input[type=submit]').attr('disabled'))
            $('#heritage-edit-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#heritage-edit-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/heritage',
        target: '#heritage-edit-form',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#heritage-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#heritage-edit-form div.loading').show();
        },
        success: function() {
            $('#heritage-edit-form div.loading').hide();
            $('#heritage-edit-form div.submit input[type=submit]').removeAttr('disabled');
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>