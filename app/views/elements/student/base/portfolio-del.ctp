
                    <section id="portfolio-delete">
                        <h2><?php __("Delete Portfolio"); ?></h2>
                        <?php echo $this->Form->create('User', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/delete',
                            'id' => 'portfolio-delete-form',
                            'class' => ''
                            ));
                        echo $validation->bind('User'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('password',
                            array(
                                'label' => __("Password", true),
                                'default'=> '',
                                'type' => 'password'
                                )
                            );
                        ?>

                        <p class="alert"><?php  __("Deleting your Porfolio is definitive and cannot be undone.");  ?></p>
                        <dl>
                            <dt><?php echo $this->Form->label('delete_confirm', __("Confirmation", true)); ?></dt>
                            <dd class="master"><?php echo $this->Form->checkbox('delete_confirm'); ?></dd>
                        </dl>
                        </fieldset>
                        <?php echo $this->Form->end(__("Delete", true)); ?>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // iButton
    $('#portfolio-delete-form fieldset dl dd input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            if (($('#UserPassword').val() != '') && $(this).is(':checked'))
                $('#portfolio-delete-form input[type=submit]').removeAttr('disabled');
            else
                $('#portfolio-delete-form input[type=submit]').attr('disabled', 'disabled');
        })
    // Alert tips
    addTipAlert('#portfolio-delete');
    // Raz des contrôles
    function resetControls(){
        $('#UserDeleteConfirm').iButton('toggle', false);
        $('#UserDeleteConfirm').iButton('disable', true);
        $('#portfolio-delete-form div.submit input[type=submit]').attr('disabled', 'disabled');
    };
    // État initial
    resetControls();
    // État submit
    $('#UserPassword').keyup(function() {
        if ($(this).val() != '')
            $('#UserDeleteConfirm').iButton('disable', false);
        else {
            $('#UserDeleteConfirm').iButton('toggle', false);
            $('#UserDeleteConfirm').iButton('disable', true);
            $('#portfolio-delete-form input[type=submit]').attr('disabled', 'disabled');
        }
    });
");

?>