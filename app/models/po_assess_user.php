<?php

/**
 * {app}/models/po_assess_user.php
 */
class PoAssessUser extends AppModel {

    var $name = 'PoAssessUser';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'description' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please define item.", true)
            )
        );

        parent::__construct();

    }


    function getAssessUserData($portfolioId, $competency, $level) {

        // Chargement des données
        $data = $this->find('all', array(
                    'conditions' => array(
                        'PoAssessUser.portfolio_id' => $portfolioId,
                        'PoAssessUser.competency' => $competency,
                        'PoAssessUser.level' => $level
                    ),
                    'fields' => array(
                        'id',
                        'description',
                        'evaluation',
                        'date1',
                        'date2',
                        'date3',
                        'target'
                    ),
                    'order' => array('PoAssessUser.id ASC'),
                    'recursive' => -1
                ));

        // Mise en forme
        $userData = array();
        foreach ($data as $key => $value) {
            $userData['PoAssessUser'][$key] = $value['PoAssessUser'];
        }

        // Retour
        return $userData;

    }


}

