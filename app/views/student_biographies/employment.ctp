
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-employment"><?php __d('portfolio', "Competencies which improve my employment prospects"); ?></h2>
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "What tasks will you need to carry out in the language when you are in employment?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "How often will that be?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What categories of people do you think you will be communicating with in that language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Will this language usually be that person’s mother tongue?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What level of competence do you think your present or future employer will be expecting from you?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Depending on your activity sector and on your qualifications, what competencies in the language could improve your chances of getting the best possible job in the shortest possible time?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="employment-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new experience"); ?></h2>
                        <?php echo $this->Form->create('PoEmployment', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/employment/' . urlencode($language),
                            'id' => 'employment-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoEmployment'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __d('portfolio', "Type", true)
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <p class="info"><?php __d('portfolio', "If possible, write in the concerned language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/biography/employment-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography'));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('student/biography/how-to-use_emp', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('.instruction');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#employment-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#employment-edit');
    // Initialisation du formulaire
    function initForm() {
        $('#employment-edit-form #PoEmploymentId').val('');
        $('#employment-edit-form #PoEmploymentName').val('');
        $('#employment-edit-form #PoEmploymentDescription').val('');
    };
    // Soumission Ajax du formulaire
    $('#employment-edit-form').submit(function() {
        if (!$('#employment-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/biography/employment/" . urlencode($language) . "',
                target: '#employment',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#employment-edit-form div.loading').show();
                },
                success: function() {
                    $('#employment-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#employment-toggle').hide();
                    $('#employment-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#employment-edit-form div.loading').hide();
                    $('#employment-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>