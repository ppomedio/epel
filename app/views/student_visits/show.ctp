
                <article>
                    <h1 id="title-visit-page" class="spaced"><?php __("This Portfolio has been visited by…"); ?></h1>
                    <section><?php
                        echo $this->element('visits-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/visit/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>