
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="element-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="add"></dt>
                                <dd><?php __("Add"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Delete"); ?></dd>
                            </dl>
                            <p class="alert"><?php __("Elements in use cannot be deleted. Be vigilant while modifying those that you can edit: you risk damaging the integrity of the database."); ?></p>
                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                        </div>
                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#element-info');
    addTipAlert('#element-info');
");

?>
