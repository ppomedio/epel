
                    <section>
                        <h2><?php __("Profile"); ?></h2>
                        <div>
                            <dl>
                                <dt><?php __("Name"); ?></dt>
                                <dd><?php echo $user['Student']['name']; ?></dd>
                                <dt><?php __("First name"); ?></dt>
                                <dd><?php echo $user['Student']['firstname']; ?></dd>
                                <dt><?php __("Date of birth"); ?></dt>
                                <dd><?php echo $this->Date->Convert($user['Student']['birth']); ?></dd>
                                <dt><?php __("Country of origin"); ?></dt>
                                <dd><?php __d('country', $user['OriginCountry']['name']); ?></dd>
                                <dt><?php __("Country of residence"); ?></dt>
                                <dd><?php __d('country', $user['LivingCountry']['name']); ?></dd>
                                <dt><?php __("Validated studies"); ?></dt>
                                <dd><?php
                                if (!empty($user['Student']['diploma_info'])) {
                                    echo $user['Student']['diploma_info'] . '<br />' . ucfirst(__($user['Diploma']['level'], true));
                                } else {
                                    __($user['Diploma']['name']);
                                } ?></dd>
                                <dt><?php __("Field of study"); ?></dt><?php
                                if (($user['Student']['domain_id'] == '0001') && !empty($user['Student']['domain_info'])) {
                                ?><dd><?php echo $user['Student']['domain_info']; ?></dd><?php } else {
                                ?><dd><?php __d('domain', $user['Domain']['name']); ?></dd><?php }
                                if ($user['Student']['specialty']) {
                                ?><dt><?php __("Specialty"); ?></dt>
                                <dd><?php echo $user['Student']['specialty']; ?></dd>
                                <?php } ?>

                            </dl>
                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls">
                                    <?php
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/student/base/profile',
                                        array(
                                            'id' => 'profile',
                                            'class' => 'edit'
                                        )
                                    );
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div id="data_profile" class="tooltip-data"><?php __('Edit profile'); ?></div>
                    </section>