
                <article><?php
                    echo $this->element('teacher/group/list');
                    ?>

                </article>
                <aside><?php
                    echo $this->element('teacher/group/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('teacher/group/edit');
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>