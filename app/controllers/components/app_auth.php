<?php

/**
 * {app}/controllers/components/app_auth.php
 * 
 */
App::import('Component', 'Auth');

class AppAuthComponent extends AuthComponent {


    /**
     * Hashage en sha1 simple
     * (voir cake/libs/controller/components/auth.php)
     *
     * @param <type> $data
     * @return <type>
     */
    function hashPasswords($data) {

        if (is_array($data)) {
            $model = & $this->getModel();
            if (isset($data[$model->alias])) {
                if (isset($data[$model->alias][$this->fields['username']]) && isset($data[$model->alias][$this->fields['password']])) {
                    $data[$model->alias][$this->fields['password']] = sha1($data[$model->alias][$this->fields['password']]);
                }
            }
        }
        return $data;

    }


}

