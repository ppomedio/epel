<?php

$paginator->options(array(
    'update' => '#certificate'
    ));

?>

                        <div id="certificate" class="table"><?php
                            if (empty($user)) { ?>

                            <table class="dossier">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Date"); ?></th>
                                        <th><?php __d('portfolio', "Delivered by"); ?></th>
                                        <th><?php __d('portfolio', "Level"); ?></th>
                                        <th class="controls">
                                            <span id="certificate-add" class="add"></span>
                                            <span id="certificate-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="5">
                                             <p class="alert"><?php
                                             __d('portfolio', "Your Dossier currently contains no certificate for this language.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#certificate');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table class="dossier">
                                <thead>
                                    <tr>
                                        <th class="type"><?php echo $paginator->sort(__d('portfolio', "Type", true), 'PoCertificate.certificate_id', array('url' => $this->params['pass'])); ?></th>
                                        <th class="date"><?php echo $paginator->sort(__d('portfolio', "Date", true), 'PoCertificate.date', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Delivered by"); ?></th>
                                        <th><span id="levels" class="help"><?php __d('portfolio', "Level(s)"); ?></span></th>
                                        <th class="controls">
                                            <span id="certificate-add" class="add"></span>
                                            <span id="certificate-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoCertificate'] as $value) {
                                        $itemDate = $this->Date->Convert($value['date']);
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php
                                        if ($value['certificate_id'] == '0001') {
                                            $itemType = $value['certificate_info'];
                                            echo '<strong>' . $value['certificate_info'] . '</strong>';
                                        } else {
                                            $itemType = $value['Certificate']['Certification']['name'];
                                            echo '<strong>' . $itemType . '</strong>';
                                            if ($value['Certificate']['Certification']['description'])
                                                echo '<br /><em class="smaller">' .  $value['Certificate']['Certification']['description'] . "</em>";
                                        }
                                        ?></td>
                                        <td id="certificate-date_<?php echo $value['id']; ?>"><?php
                                        echo $itemDate;
                                        ?></td>
                                        <td id="certificate-organism_<?php echo $value['id']; ?>"><?php
                                        echo $value['organism'];
                                        ?></td>
                                        <td><?php
                                            if (isset($value['levels']))
                                                echo $value['levels'];
                                        ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="certificate-id hidden"><?php
                                            echo $value['certificate_id'];
                                            ?></span>
                                            <span class="certificate-info hidden"><?php
                                            echo $value['certificate_info'];
                                            ?></span>
                                            <span class="description hidden"><?php
                                            echo $value['Certificate']['Certification']['description'];
                                            ?></span>
                                            <span class="listening hidden"><?php
                                            echo $value['listening'];
                                            ?></span>
                                            <span class="reading hidden"><?php
                                            echo $value['reading'];
                                            ?></span>
                                            <span class="speak_int hidden"><?php
                                            echo $value['speak_int'];
                                            ?></span>
                                            <span class="speak_prod hidden"><?php
                                            echo $value['speak_prod'];
                                            ?></span>
                                            <span class="writing hidden"><?php
                                            echo $value['writing'];
                                            ?></span>
                                            <span class="day hidden"><?php
                                            echo $value['day'];
                                            ?></span>
                                            <span class="month hidden"><?php
                                            echo $value['month'];
                                            ?></span>
                                            <span class="year hidden"><?php
                                            echo $value['year'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="certificate-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="certificate-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_certificate-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this certificate");
                                    ?></div>
                                    <div id="data_certificate-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this certificate");
                                    ?></div>
                                    <div id="certificate-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete certificate"); echo ' (' . $itemType . ' - ' . $itemDate . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'certificate'));
                            } ?>

                            <div id="data_sort-certificate-type" class="tooltip-data"><?php __("Sort by type"); ?></div>
                            <div id="data_sort-certificate-date" class="tooltip-data"><?php __("Sort by date"); ?></div>
                            <div id="data_levels" class="tooltip-data"><?php __d('portfolio', "CEFRL level, specified only if it appears on the original certificate."); ?></div>
                            <div id="data_certificate-add" class="tooltip-data"><?php __d('portfolio', "Add a new certificate"); ?></div>
                            <div id="data_certificate-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#PoCertificateCertificateInfo').val() == ''
            && $('#PoCertificateOrganism').val() == '') {
        if ($('#certificate-edit').is(':hidden'))
            $('#certificate-toggle').hide();
        else
            $('#certificate-add').hide();
    }
    // Id, classes et cie
    $('#certificate table thead tr th.type a').attr('id', 'sort-certificate-type').addClass('sort-help');
    $('#certificate table thead tr th.date a').attr('id', 'sort-certificate-date').addClass('sort-help');
    // Tooltips
    tooltipHelp('#certificate table thead');
    tooltipSortHelp('#certificate table thead');
    tooltipAdd('#certificate table thead th.controls');
    tooltipGear('#certificate table thead th.controls');
    tooltipEdit('#certificate table tbody td.controls div.controls');
    tooltipSupp('#certificate table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#certificate-toggle').live('click', function() {
        if ($('#PoCertificateId').val() == '')
            $('#certificate-edit > h2').text('" . addslashes(__d('portfolio', "Add a new certificate", true)) . "');
        if ($('#certificate-edit').is(':hidden'))
            $('#certificate-edit').slideDown('slow');
        else {
            $('#certificate-edit').slideUp('slow');
            $('#certificate-add').fadeIn('slow');
            if ($('#PoCertificateCertificateInfo').val() == ''
                    && $('#PoCertificateOrganism').val() == '')
                $('#certificate-toggle').hide();
        }
    });
    // Ajout d'un certificat
    $('#certificate-add').live('click', function() {
        $('#certificate-edit > h2').text('" . addslashes(__d('portfolio', "Add a new certificate", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#certificate-edit').is(':hidden')) {
            $('#certificate-edit').slideDown('slow');
        }
        $('#certificate-add').hide();
        $('#certificate-toggle').fadeIn('slow');
    });
    // Édition d'un certificat
    $('#certificate table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#certificate-edit > h2').text('" . addslashes(__d('portfolio', "Edit a certificate", true)) . "');
        // Chargement des données
        $('#PoCertificateId').val(itemId);
        $('#PoCertificateCertificateId option[value=\'' + $('#hidden-info_' + itemId + ' .certificate-id').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateCertificateInfo').val($('#hidden-info_' + itemId + ' .certificate-info').text());
        $('#PoCertificateOrganism').val($('#certificate-organism_' + itemId).text());
        // Chargement des données : compétences
        $('#PoCertificateListening option[value=\'' + $('#hidden-info_' + itemId + ' .listening').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateReading option[value=\'' + $('#hidden-info_' + itemId + ' .reading').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateSpeakInt option[value=\'' + $('#hidden-info_' + itemId + ' .speak_int').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateSpeakProd option[value=\'' + $('#hidden-info_' + itemId + ' .speak_prod').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateWriting option[value=\'' + $('#hidden-info_' + itemId + ' .writing').text() + '\']').attr('selected', 'selected');
        // Chargement des données : date
        $('#PoCertificateDateDay option[value=\'' + $('#hidden-info_' + itemId + ' .day').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateDateMonth option[value=\'' + $('#hidden-info_' + itemId + ' .month').text() + '\']').attr('selected', 'selected');
        $('#PoCertificateDateYear option[value=\'' + $('#hidden-info_' + itemId + ' .year').text() + '\']').attr('selected', 'selected');
        // Affichage si masqué
        if ($('#certificate-edit').is(':hidden')) {
            $('#certificate-add').hide();
            $('#certificate-edit').slideDown('slow');
            $('#certificate-add').fadeIn('slow');
            $('#certificate-toggle').fadeIn('slow');
        } else {
            if ($('#certificate-add').is(':hidden'))
                $('#certificate-add').fadeIn('slow');
        }
        // Affichage des infos complémentaires
        if ($('#hidden-info_' + itemId + ' .certificate-id').text() == '0001')
            $('#PoCertificateCertificateInfo').parent('div').show('slow');
        else
            $('#PoCertificateCertificateInfo').parent('div').hide('slow', function() {
                $('#PoCertificateCertificateInfo').val($('#PoCertificateCertificateId :selected').text());
            });
    });
    // Suppression d'un certificat
    $('#certificate table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#certificate-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
					var extract = new Array();
					extract = $(this).attr('id').split('_');
					var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#certificate').load('/" . $this->params['language'] . "/student/dossier/delete_cert/" . urlencode($language) . "/' + itemId, function() {
                        if ($('#PoCertificateId').val() == itemId) {
                            $('#certificate-edit > h2').text('" . addslashes(__d('portfolio', "Add a new certificate", true)) . "');
                            initForm();
                            if ($('#certificate-edit').is(':hidden'))
                                $('#certificate-toggle').hide();
                            else
                                $('#certificate-add').hide();
                        }
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-certificate').html('" . addslashes(__d('portfolio', "Certificates", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>