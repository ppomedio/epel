
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="dossier-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="add"></dt>
                                <dd><?php __("Add new material"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit existing document"); ?></dd>
                                <dt class="upload"></dt>
                                <dd><?php __("Replace attached file"); ?></dd>
                                <dt class="move"></dt>
                                <dd><?php __("Move document to the “Display” part of the dossier"); ?></dd>
                                <dt class="download"></dt>
                                <dd><?php __("Download attached file"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Delete document and attached file"); ?></dd>
                                <dt class="gear"></dt>
                                <dd><?php __("Shift edition panel"); ?></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#dossier-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#dossier-info');
");
*/
?>
