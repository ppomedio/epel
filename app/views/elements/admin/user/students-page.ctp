<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#adm-students',
    'before' => $this->Js->get('#adm-students')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="adm-students" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="student-id"><?php "ID"; ?></th>
                                        <th class="student-name"><?php __("Name"); ?></th>
                                        <th class="student-university"><?php __("University"); ?></th>
                                        <th class="student-group"><?php __("Group"); ?></th>
                                        <th class="user-active"><?php __("Activated"); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="6"><?php
                                        if ($this->Session->check('Auth.User.search'))
                                            __("No result matches criteria.");
                                        else
                                            __("No student account has currently been created.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="student-id"><?php echo $paginator->sort("ID", 'Student.id'); ?></th>
                                        <th class="student-name"><?php echo $paginator->sort(__("Name", true), 'Student.name'); ?></th>
                                        <th class="student-university"><?php echo $paginator->sort(__("University", true), 'Student.university_id'); ?></th>
                                        <th class="student-group"><?php echo $paginator->sort(__("Group", true), 'Student.group'); ?></th>
                                        <th class="user-active"><?php echo $paginator->sort(__("Activated", true), 'User.active'); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>
                                    <tr id="<?php echo $value['Student']['id']; ?>" class="<?php echo $trClass; ?>">
                                        <td id="edit-id_<?php echo $value['Student']['id']; ?>" class="smaller"><?php
                                        echo $value['Student']['id'];
                                        ?></td>
                                        <td id="edit-name_<?php echo $value['Student']['id']; ?>"><?php
                                        echo $this->Html->link($value['Student']['name'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['Student']['id'] . '_'
                                                        . $value['Student']['firstname'] . ' '
                                                        . $value['Student']['name'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['Student']['id'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['Student']['firstname'];
                                        ?></td>
                                        <td id="edit-university_<?php echo $value['Student']['id']; ?>" class="smaller"><?php
                                        if ((int) $value['University']['id'] < 3)
                                            echo __($value['University']['name']);
                                        else
                                            echo $value['University']['name'];
                                        ?></td>
                                        <td id="edit-group_<?php echo $value['Student']['id']; ?>" class="smaller"><?php
                                        echo $value['Student']['group'];
                                        ?></td>
                                        <td class="user-active"><?php
                                        echo $this->Form->create('User', array(
                                            'url' => '/' . Configure::read('Config.langCode')
                                                . '/admin/user/toggle_active/student/' . $value['Student']['id'],
                                            'id' => 'form_' . $value['Student']['id'],
                                            'class' => 'user-active'
                                            ));
                                        echo $this->Form->checkbox('active', array(
                                            'id' => 'active_' . $value['Student']['id'],
                                            'checked' => $value['User']['active']
                                        ));
                                        echo $this->Form->end();
                                        ?></td>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['Student']['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['Student']['id']; ?>" class="controls">
                                                <a href="#user-edit" id="edit-link_<?php echo $value['Student']['id']; ?>" class="edit"></a>
                                            </div>
                                        </td>
                                        <div id="hidden-info_<?php echo $value['Student']['id']; ?>" class="hidden">
                                            <span class="name hidden"><?php
                                            echo $value['Student']['name'];
                                            ?></span>
                                            <span class="firstname hidden"><?php
                                            echo $value['Student']['firstname'];
                                            ?></span>
                                            <span class="university-id hidden"><?php
                                            echo $value['Student']['university_id'];
                                            ?></span>
                                            <span class="email hidden"><?php
                                            echo $value['User']['email'];
                                            ?></span>
                                            <span class="birth hidden"><?php
                                            echo $this->Date->Convert($value['Student']['birth']);
                                            ?></span>
                                            <span class="country hidden"><?php
                                            __d('country', $value['OriginCountry']['name']);
                                            ?></span>
                                            <span class="diploma hidden"><?php
                                            if (!empty($value['Student']['diploma_info']))
                                                echo $value['Student']['diploma_info'];
                                            else
                                                echo __($value['Diploma']['name'], true);
                                            echo ' (' . __($value['Diploma']['level'], true) . ')';
                                            ?></span>
                                            <span class="specialty hidden"><?php
                                            echo $value['Student']['specialty'];
                                            ?></span>
                                            <span class="photo hidden"><?php
                                            echo $value['User']['photo'];
                                            ?></span>
                                        </div>
                                        <div id="data_contact-user_<?php echo $value['Student']['id']; ?>" class="tooltip-data"><?php
                                            __("Send an email to");
                                            echo ' ' . $value['Student']['firstname'] . ' ' . $value['Student']['name'];
                                        ?></div>
                                        <div id="data_edit-link_<?php echo $value['Student']['id']; ?>" class="inline tooltip-data"><?php
                                            __("Edit user"); echo ' n°' . $value['Student']['id'];
                                        ?></div>
                                    </tr><?php } ?>
                                </tbody>
                            </table><?php
                            echo $this->element('paginate', array('subId' => 'adm-students'));
                            } ?>

                            <div id="data_search-new" class="tooltip-data"><?php __("Filter criteria"); ?></div>
                            <div id="data_search-init" class="tooltip-data"><?php __("Reset filter criteria"); ?></div>
                            <div id="data_sort-student-id" class="tooltip-data"><?php __("Sort by ID"); ?></div>
                            <div id="data_sort-student-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-student-firstname" class="tooltip-data"><?php __("Sort by firstname"); ?></div>
                            <div id="data_sort-student-university" class="tooltip-data"><?php __("Sort by College / University"); ?></div>
                            <div id="data_sort-student-group" class="tooltip-data"><?php __("Sort by group code"); ?></div>
                            <div id="data_sort-user-active" class="tooltip-data"><?php __("Sort by state"); ?></div>
                            <div class="hidden">
                                <div id="user-edit" class="fancy">
                                    <h2><?php __("User's profile"); ?></h2>
                                    <?php echo $this->Form->create('Student', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/admin/user/change_student_university',
                                        'id' => 'edit-form',
                                        'class' => ''
                                        ));
                                    ?>

                                    <div id="user-photo"></div>
                                    <fieldset class="half"><?php
                                    echo $this->Form->hidden('id');
                                    echo $this->Form->input('edit_name', array(
                                        'label' => __("Name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_firstname', array(
                                        'label' => __("First name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_birth', array(
                                        'label' => __("Date of birth", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_country', array(
                                        'label' => __("Country of origin", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    </fieldset>
                                    <fieldset class="clear"><?php
                                    echo $this->Form->input('edit_diploma', array(
                                        'label' => __("Validated studies", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_specialty', array(
                                        'label' => __("Specialty", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('university_id', array(
                                        'label' => __("College / University", true),
                                        'options' => $university,
                                        #'empty' => array('0002' => "…")
                                        ));
                                    echo $this->Form->input('edit_group', array(
                                        'label' => __("Group", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_email', array(
                                        'label' => __("eMail address", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    <div id="message">
                                        <p id="info-message" class="info"></p>
                                        <p id="alert-message" class="alert"></p>
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Save", true)); ?>
                                </div>
                                <div id="user-search" class="fancy">
                                    <h2><?php __("Filter criteria"); ?></h2>
                                    <?php echo $this->Form->create('Student', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/admin/user/student',
                                        'id' => 'search-form',
                                        'class' => ''
                                        ));
                                    echo $validation->bind('Student'); // Validation jQuery
                                    if ($this->Session->check('Auth.User.search.Student'))
                                        $default = $this->Session->read('Auth.User.search.Student');
                                    else
                                        $default = array(
                                            'name' => '',
                                            'firstname' => '',
                                            'origin_country_id' => '',
                                            'university_id' => '',
                                            'group_id' => '00000',
                                            'group' => ''
                                        );
                                    ?>

                                    <fieldset><?php
                                    echo $this->Form->input('name', array(
                                        'label' => __("Name", true),
                                        'default' => $default['name']
                                        ));
                                    echo $this->Form->input('firstname', array(
                                        'label' => __("First name", true),
                                        'default' => $default['firstname']
                                        ));
                                    echo $this->Form->input('origin_country_id', array(
                                        'label' => __("Country of origin", true),
                                        'options' => $country,
                                        'empty' => "…",
                                        'default' => $default['origin_country_id']
                                        ));
                                    echo $this->Form->input('university_id', array(
                                        'label' => __("College / University", true),
                                        'options' => $university,
                                        'empty' => array(__("All universities", true) . ' + ' . __("Fictitious university", true)),
                                        'default' => $default['university_id']
                                        ));
                                    echo $this->Form->input('group', array(
                                        'label' => __("Group code", true),
                                        'default' => $default['group']
                                        )); ?>

                                    <div id="message">
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Filter", true)); ?>
                                </div>
                            </div>
                        </div>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ibutton',
        'jquery.form',
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

// État initial du picto d'initialisation
$initState = '';
if ($this->Session->check('Auth.User.search.Student')) {
    $initState = "    $('#search-init').css('display', 'inline-block');\n";
}

$this->Js->Buffer("
    // iButton
    $('form.user-active input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            var extract = new Array();
            extract = $(this).attr('id').split('_');
            var userId = extract[1];
            $(this).closest('form').ajaxSubmit({
                beforeSubmit: function() {
                    $('#control_' + userId).hide();
                    $('#loading_' + userId).show();
                },
                success: function() {
                    $('#loading_' + userId).hide();
                    $('#control_' + userId).show();
                }
            })
    });
    // Id, classes et cie
    $('#adm-students table thead tr th.student-id a').attr('id', 'sort-student-id').addClass('sort-help');
    $('#adm-students table thead tr th.student-name a').attr('id', 'sort-student-name').addClass('sort-help');
    $('#adm-students table thead tr th.student-firstname a').attr('id', 'sort-student-firstname').addClass('sort-help');
    $('#adm-students table thead tr th.student-university a').attr('id', 'sort-student-university').addClass('sort-help');
    $('#adm-students table thead tr th.student-group a').attr('id', 'sort-student-group').addClass('sort-help');
    $('#adm-students table thead tr th.user-active a').attr('id', 'sort-user-active').addClass('sort-help');
    // Init. pictos
" . $initState . "
    $('<div class=\"loading\"></div>').insertAfter('#search-form div.submit input[type=submit]');
    $('<div class=\"loading\"></div>').insertAfter('#edit-form div.submit input[type=submit]');
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Raz critères de recherche
    $('#adm-students table thead tr th.controls span#search-init').unbind('click').bind('click', function() {
        $('#divToolTip').fadeOut('fast');
        $('#title-adm-students').html('" . addslashes(__("Loading…", true)) . "');
        $('#adm-students').fadeTo('fast', 0.6);
        $('#adm-students').load('/" . $this->Session->params['language'] . "/admin/user/init_criteria/student');
    });
    // Formulaire de recherche
    $('#search-new').fancybox({
        'width'             :   450,
        'height'            :   380,
        'autoDimensions'    :   false,
        'padding'           :   0,
        'margin'            :   0,
        'speedIn'           :   900,
        'scrolling'         :   'no',
        'centerOnScroll'    :   true,
        'overlayColor'      :   '#000',
        'overlayOpacity'    :   0.3,
        'onStart'           :   function() {
                                    $('div#divToolTip').fadeOut();
                                },
        'onComplete'        :   function() {
                                    // Soumission Ajax
                                    $('#search-form').ajaxForm({
                                        target:         '#adm-students',
                                        beforeSubmit:   function() {
                                            if (!$('#search-form .error').length) {
                                                $('#title-adm-students').html('" . addslashes(__("Loading…", true)) . "');
                                                $('#adm-students').fadeTo('fast', 0.6);
                                            } else
                                                return false;
                                        },
                                        success:        function() {
                                            $.fancybox.close();
                                        }
                                    });
                                }
    });
    // Sélection d'une université
    $('#edit-form #StudentUniversityId').change(function() {
        if ($('#edit-form #StudentUniversityId option:selected').val() != $('#hidden-info_' + $('#edit-form #StudentId').val() + ' .university-id').text()) {
            $('p#info-message').attr('class', 'info')
                    .html('<span class=\"tip-info\"></span>"
                    . addslashes(__("You can change or remove this user's university only if he/she is no longer a student there and wishes to continue using his/her Portfolio.", true)) . "');
            if ($('#edit-form #StudentEditGroup').val())
                $('p#alert-message').show('slow');
            $('#edit-form input[type=submit]').removeAttr('disabled');
        } else {
            $('#edit-form p#alert-message').hide('slow');
            $('#edit-form input[type=submit]').attr('disabled', 'disabled');
        }
    });
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Formulaire d'édition
    $('a.edit').each(function(){
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var userId = extract[1];
        $(this).fancybox({
            'width'             :   450,
            'height'            :   570,
            'autoDimensions'    :   false,
            'padding'           :   0,
            'margin'            :   0,
            'speedIn'           :   900,
            'scrolling'         :   'no',
            'centerOnScroll'    :   true,
            'overlayColor'      :   '#000',
            'overlayOpacity'    :   0.3,
            'onStart'           :   function() {
                                        $('div#divToolTip').fadeOut();
                                        // Photo
                                        var userPhoto = $('#hidden-info_' + userId + ' .photo').text();
                                        if (userPhoto) {
                                            var photoUrl = 'url(/files/users/' + userId + '/photo/' + userPhoto + ')';
                                            $('#edit-form #user-photo').css('background-image', photoUrl);
                                        }
                                        // Messages
                                        $('#edit-form p#info-message').attr('class', 'info')
                                                .html('<span class=\"tip-info\"></span>" . addslashes(__("You can change or remove this user's university only if he/she is no longer a student there and wishes to continue using his/her Portfolio.", true)) . "');
                                        $('#edit-form p#alert-message').attr('class', 'alert')
                                                .html('<span class=\"tip-alert\"></span>" . addslashes(__("This will reinitialize this student's group code.", true)) . "');
                                        $('#edit-form p#alert-message').hide();
                                        // ID user
                                        $('#edit-form #StudentId').val(userId);
                                        // Initialisation des éléments du formulaire
                                        $('#edit-form #StudentEditName').val($('#hidden-info_' + userId + ' .name').text());
                                        $('#edit-form #StudentEditFirstname').val($('#hidden-info_' + userId + ' .firstname').text());
                                        $('#edit-form #StudentEditBirth').val($('#hidden-info_' + userId + ' .birth').text());
                                        $('#edit-form #StudentEditCountry').val($('#hidden-info_' + userId + ' .country').text());
                                        $('#edit-form #StudentEditDiploma').val($('#hidden-info_' + userId + ' .diploma').text());
                                        $('#edit-form #StudentEditSpecialty').val($('#hidden-info_' + userId + ' .specialty').text());
                                        $('#edit-form #StudentEditGroup').val($('#edit-group_' + userId).text());
                                        $('#edit-form #StudentEditEmail').val($('#hidden-info_' + userId + ' .email').text());
                                        $('#edit-form #StudentUniversityId option[value=\'' + $('#hidden-info_' + userId + ' .university-id').text() + '\']').attr('selected', 'selected');
                                        $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
                                    },
            'onClosed'          :   function() {
                                        // Raz des éléments du formulaire
                                        $('#edit-form #user-photo').css('background-image', 'url(/img/user.png)');
                                        $('#edit-form #StudentId').val('');
                                        $('#edit-form #StudentEditName').val('');
                                        $('#edit-form #StudentEditFirstname').val('');
                                        $('#edit-form #StudentEditBirth').val('');
                                        $('#edit-form #StudentEditCountry').val('');
                                        $('#edit-form #StudentEditDiploma').val('');
                                        $('#edit-form #StudentEditSpecialty').val('');
                                        $('#edit-form #StudentEditGroup').val('');
                                        $('#edit-form #StudentEditEmail').val('');
                                    }
        });
    });
    // Changement d'université (Ajax)
    $('#edit-form').submit(function() {
        $(this).ajaxSubmit({
            beforeSubmit: function() {
                $('#edit-form div.loading').show();
                $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
            },
            success: function() {
                var userId = $('#edit-form #StudentId').val();
                var newUniversityId = $('#edit-form #StudentUniversityId option:selected').val();
                // Maj DOM
                $('#edit-form #StudentEditGroup').val('');
                $('#adm-students #edit-group_' + userId).text('');
                //$('#hidden-info_' + userId + ' .university-id').text(newUniversityId);
                if ($('#edit-form #StudentUniversityId option:selected').val() == '001')
                    $('#adm-students #edit-university_' + userId).text('…');
                else
                    $('#adm-students #edit-university_' + userId).text($('#edit-form #StudentUniversityId option:selected').text());
                $('#hidden-info_' + userId + ' .university-id').text(newUniversityId);
                // Affichage
                $('#edit-form p#alert-message').hide('slow');
                $('#edit-form p#info-message').attr('class', 'alert')
                    .html('" . addslashes(__("This user's university has been updated.", true)) . "');
                $('#edit-form div.loading').hide();
            }
        });
        return false;
    });
    // Tooltips
    tooltipSortHelp('#adm-students table thead tr th');
    tooltipLink('#adm-students table');
    tooltipSupp('#adm-students table thead tr th.controls');
    tooltipEdit('#adm-students table tbody tr td div.controls');
    // Tips
    addTipAlert('#message');
    addTipInfo('#message');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#adm-students').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-adm-students').html('" . addslashes(__("Students' accounts", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>