<?php

$student = $this->Session->read('Auth.User.view');
if ($student['photo'])
    $photoPath = '/files/users/' . $student['id'] . '/photo/' . $student['photo'];
else
    $photoPath = '/img/user.png';

?>

                    <div id="photo">
                        <h2><?php echo $student['firstname'] . ' ' . $student['name']; ?></h2>
                        <div>
                            <table>
                                <tbody><?php
                                    if ($this->Session->read('Auth.User.role') == 'visitor' && !empty($student['university'])) { ?>

                                    <tr>
                                        <td><?php __("College / University"); ?></td>
                                        <td><?php echo $student['university']; ?></td>
                                    </tr><?php
                                    }
                                    if ($this->Session->read('Auth.User.role') == 'teacher') { ?>

                                    <tr>
                                        <td><?php __("Group code"); ?></td>
                                        <td><?php echo $student['group']; ?></td>
                                    </tr><?php
                                    } ?>

                                    <tr>
                                        <td><?php __("Validated studies"); ?></td>
                                        <td><?php
                                        if (!empty($student['diploma_info'])) {
                                            echo $student['diploma_info'] . '<br />' . __($student['diploma_level'], true);
                                        } else {
                                            __($student['diploma']);
                                        } ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __("Field of study"); ?></td>
                                        <td><?php
                                        if (($student['domain_id'] == '0001') && !empty($student['domain_info'])) {
                                            echo $student['domain_info'];
                                        } else {
                                            echo __d('domain', $student['domain']);
                                        } ?></td>
                                    </tr><?php
                                    if ($student['specialty']) { ?>

                                    <tr>
                                        <td><?php __("Specialty"); ?></td>
                                        <td><?php echo $student['specialty']; ?></td>
                                    </tr><?php
                                    } ?>

                                    <tr>
                                        <td><?php __("Date of birth"); ?></td>
                                        <td><?php echo $this->Date->Convert($student['birth']); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __("Country of origin"); ?></td>
                                        <td><?php __d('country', $student['origin']); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __("Country of residence"); ?></td>
                                        <td><?php __d('country', $student['living']); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __("Portfolio open for"); ?></td>
                                        <td><?php
                                        $symbol = false;
                                        #foreach ($student['portfolios'] as $key => $value) {
                                        foreach ($languages as $key => $value) {
                                            $add = __d('language', $key, true) . ' ';
                                            // Signalisation maternelle / usuelle
                                            if ($value['mother']) {
                                                $add .= '<span class="symbol-mother"></span>';
                                                $symbol = true;
                                            }
                                            if ($value['usual']) {
                                                $add .= '<span class="symbol-usual"></span>';
                                                $symbol = true;
                                            }
                                            echo $add . '<br />';
                                        } ?></td>

                                    </tr>
                                </tbody>
                            </table><?php
                            if ($symbol) { ?>
                            <p class="info"><?php echo '<span class="symbol-mother"></span>&nbsp;' . __d('portfolio', "Mother tongue", true) . ' / <span class="symbol-usual"></span>&nbsp;' . __d('portfolio', "Usual language", true); ?></p><?php
                            } ?>
                            <p class="info"><?php __("You can send email to the students with the menu “Contact” option."); ?></p>
                        </div>
                    </div>
<?php

$this->Js->Buffer("
    // Affichage de la photo
    var img = '" . $photoPath . "';
    if (img == '/img/user.png')
        var height = 150;
    else
        var height = parseInt(img.substring(img.lastIndexOf('-') + 1, img.lastIndexOf('.')));
    $('aside #photo > div').css('padding-top', height);
    $('aside #photo > div').css('background-image', 'url(" . $photoPath . ")');
    $('aside #photo').show();
    // Tips
    addTipInfo('#photo');
    // Fancybox (contact)
    fancyBox('#photo h2 a.iframe');
");

?>