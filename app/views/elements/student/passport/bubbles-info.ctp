
                    <div>
                        <h2><?php __("In the Dossier…"); ?></h2>
                        <div id="search-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above those gauges to see how many materials in your Display Dossier certify the level declared for this competency."); ?></p>
                            <dl class="bubbles">
                                <dt class="bubble-3"></dt>
                                <dd><?php __("Three or more"); ?></dd>
                                <dt class="bubble-2"></dt>
                                <dd><?php __("Two"); ?></dd>
                                <dt class="bubble-1"></dt>
                                <dd><?php __("One"); ?></dd>
                                <dt class="bubble-0"></dt>
                                <dd><?php __("None"); ?></dd>
                            </dl>
                        </div>
                    </div>