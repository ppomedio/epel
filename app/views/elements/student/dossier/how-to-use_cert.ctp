
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="dossier-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="add"></dt>
                                <dd><?php __("Add new certificate"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit certificate"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Delete certificate"); ?></dd>
                                <dt class="gear"></dt>
                                <dd><?php __("Shift edition panel"); ?></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#dossier-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#dossier-info');
");
*/
?>
