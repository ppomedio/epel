
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <section id="strategy-edit">
                        <h2><?php __d('portfolio', "My learning strategies"); ?></h2>
                        <div id="strategy-instr" class="instruction">
                            <p><?php __d('portfolio', "The list below is intended to help you reflect on strategies you have been using and those you might use in the future.");  ?></p>
                        </div>
                    </section>
                    <section>
                        <?php
                        echo $this->Form->create('Portfolio', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/strategy/' . urlencode($language),
                            'id' => 'strategy-form',
                            'class' => 'full table'
                            ));
                        ?>
                        <table class="strategy-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <?php
                                    for ($n = 1; $n < 5; $n++) {
                                        echo "<th>" . $tableHead[$n] . "</th>";
                                    } ?>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $trClass = 'even';
                                for ($n = 1; $n < 8; $n++) {
                                    $idPart = (string)$n;
                                    if ($trClass == 'even')
                                        $trClass = 'odd';
                                    else
                                        $trClass = 'even';
                                    echo '<tr class="'. $trClass . '"><td>' . $question[$n] . '</td>';
                                    echo '<td>' . $this->Form->radio('strategy' . $idPart, $options, $attributes) . '</td></tr>';
                                } ?>

                            </tbody>
                        </table>
                        <?php echo $this->Form->end(__("Update", true)); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography', 'never' => true));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#strategy-instr');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#strategy-form div.submit input[type=submit]');
    // Activation du submit
    $('#strategy-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#strategy-form input[type=radio]').change(function() {
        if ($('#strategy-form div.submit input[type=submit]').attr('disabled'))
            $('#strategy-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#strategy-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/strategy/" . urlencode($language) . "',
        beforeSubmit: function() {
            $('#strategy-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#strategy-form div.loading').show();
        },
        success: function() {
            $('#strategy-form div.loading').hide();
            //$('#strategy-form div.submit input[type=submit]').removeAttr('disabled');
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

?>