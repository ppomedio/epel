
                <article>
                    <h1 id="howtolearn-title"><?php echo $title_for_layout; ?></h1>
                    <section>
                        <div id="howtolearn-intro" class="instruction hidden">
                            <p><?php __d('portfolio', "The checklists which compose this part of the Biography focus on different aspects of the language learning process.");  ?></p>
                            <p><?php __d('portfolio', "They are designed to help you identify your existing strengths as a language learner and set personal targets for developing your learning skills.");  ?></p>
                            <p><?php __d('portfolio', "The checklists should be reviewed at intervals (perhaps each time you achieve a learning target; certainly once every term or semester).");  ?></p>
                            <p class="info"><?php __d('portfolio', "Use a combination of ticks and dates to plot your progress. You will probably find it helpful to discuss some elements in the checklists with your teacher and/or with other learners.");  ?></p>
                        </div>
                    </section>
                    <?php
                    $selectedOption = array(1 => '', '', '', '', '');
                    $selectedOption[$part] = ' class="current"';

                    switch($part) {

                        case 1:
                            echo $this->element('student/biography/howtolearn/motivation-form');
                            break;

                        case 2:
                            echo $this->element('student/biography/howtolearn/general-form');
                            break;

                        case 3:
                            echo $this->element('student/biography/howtolearn/formal-form');
                            break;

                        case 4:
                            echo $this->element('student/biography/howtolearn/collaborative-form');
                            break;

                        case 5:
                            echo $this->element('student/biography/howtolearn/other-form');
                            break;

                    } ?>

                </article>
                <aside><?php
                    if ($this->Session->read('Auth.User.portfolios'))
                        echo $this->element('student/part-access', array('part' => 'biography', 'never' => true));
                    ?>

                    <div>
                        <h2><?php __d('portfolio', "Chapters"); ?></h2>
                        <nav>
                            <ul>
                                <li id="nav-el_1"<?php echo $selectedOption[1]; ?>><a href="/<?php echo $this->params['language'] . '/student/biography/howtolearn/1'; ?>" id="link_1" class="jump"><?php __d('portfolio', "Attitude and motivation"); ?></a></li>
                                <li id="nav-el_2"<?php echo $selectedOption[2]; ?>><a href="/<?php echo $this->params['language'] . '/student/biography/howtolearn/2'; ?>" id="link_2" class="jump"><?php __d('portfolio', "General learning activities"); ?></a></li>
                                <li id="nav-el_3"<?php echo $selectedOption[3]; ?>><a href="/<?php echo $this->params['language'] . '/student/biography/howtolearn/3'; ?>" id="link_3" class="jump"><?php __d('portfolio', "Formal learning activities"); ?></a></li>
                                <li id="nav-el_4"<?php echo $selectedOption[4]; ?>><a href="/<?php echo $this->params['language'] . '/student/biography/howtolearn/4'; ?>" id="link_4" class="jump"><?php __d('portfolio', "Collaborative activities"); ?></a></li>
                                <li id="nav-el_5"<?php echo $selectedOption[5]; ?>><a href="/<?php echo $this->params['language'] . '/student/biography/howtolearn/5'; ?>" id="link_5" class="jump"><?php __d('portfolio', "Generally speaking…"); ?></a></li>
                            </ul>
                        </nav>
                    </div><?php
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-cercles', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Chargement des scripts utilisés
 *
 */

$this->Html->script(
    array(
        'jquery.form',
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#howtolearn-intro');
    // Saut
    $('.jump').click(function() {
        // Préparation…
        $('#howtolearn-title').html('" . addslashes(__("Loading…", true)) . "');
        $('#howtolearn-update').fadeTo('fast', 0.6);
        $('aside nav li').removeClass('current')
        // Extraction ID
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        // Maj
        $('#howtolearn-update').load('/" . $this->params['language'] . "/student/biography/howtolearn/' + itemId, function() {
            // Rétablissement opacité / titre / menu
            $('#howtolearn-update').fadeTo('slow', 1);
            $('#nav-el_' + itemId).addClass('current')
            $('#howtolearn-title').html('" . addslashes($title_for_layout) . "');
            });
        return false;
    });
");

?>