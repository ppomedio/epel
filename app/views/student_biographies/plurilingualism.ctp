
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <section id="plurilingualism-edit">
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "What can I do in the languages that I know, including my mother tongue or usual tongue?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What is my overall language competence?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "How have I learnt to do the tasks I am able to carry out?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Would I be able to carry them out in other languages?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What would I have to know then?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What is the effort required, and how much effort would it require?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What knowledge of the culture associated with the language would be implied?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <section><?php
                        echo $this->element('student/biography/plurilingualism-form'); ?>

                    </section>
                </article>
                <aside><?php
                    if ($this->Session->read('Auth.User.portfolios'))
                        echo $this->element('student/part-access', array('part' => 'biography', 'biography' => true));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#plurilingualism-edit');
");

?>