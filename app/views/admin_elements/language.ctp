
                <article class="half">
                    <section>
                        <h2 id="title-elements-list"><?php __("Languages"); ?></h2>
                        <?php echo $this->element('admin/element/languages-page'); ?>

                    </section>
                </article>
                <article class="half">
                    <section>
                        <h2 id="title-elements-form"><?php __("Add"); ?></h2>
                        <?php echo $this->Form->create('Language', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/admin/element/language',
                            'id' => 'elements-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Language'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __("Name", true)
                            ));
                        ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
                    <section><?php
                        echo $this->element('admin/element/language-info', array('languageFile' => 'languages'));
                        ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('admin/element/how-to-use');
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#elements-form div.submit input[type=submit]');
    // Soumission du formulaire
    $('#elements-form').submit(function() {
        if (!$('#elements-form .error').length)
            $('#elements-form div.loading').show();
        else
            return false;
    });
");

?>
