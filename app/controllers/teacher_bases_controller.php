<?php

/**
 * {app}/controllers/teacher_bases_controller.php
 *
 */
class TeacherBasesController extends AppController {

    var $name = 'TeacherBases';
    // Modèle
    var $uses = array(
        'Teacher',
        'Student',
        'Visitor',
    );
    // Composants
    var $components = array(
        'FormatInput'
    );
    // Pagination
    var $paginate = array('TeGroup');
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu'
    );


    function beforeFilter() {

        // Autorisation des champs "password" vides
        if ($this->params['action'] == 'connect')
            $this->Teacher->User->allowEmptyPasswords();

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // Chargement des données
        $user = $this->Teacher->getProfile($this->AppAuth->user('id'));
        // Variables de vue
        $this->set('title_for_layout', __("Teacher", true) . ' &bull; ' . __("Base", true));
        $this->set('userCount', $this->_count());
        $this->set('user', $user);

    }


    /**
     * Mise à jour du profil
     *
     */
    function profile() {

        // Modification des règles de validation
        $this->Teacher->validateProfile();
        // Traitement
        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->Teacher->getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Formatage (ne fonctionne pas dans beforeValidate…)
            $this->data['Teacher']['name'] = $this->FormatInput->formatName($this->data['Teacher']['name']);
            $this->data['Teacher']['firstname'] = $this->FormatInput->formatName($this->data['Teacher']['firstname']);
            if (!empty($this->data['Teacher']['quality']))
                $this->data['Teacher']['quality'] = $this->FormatInput->capFirst($this->data['Teacher']['quality']);
            if (!empty($this->data['Teacher']['specialty']))
                $this->data['Teacher']['specialty'] = $this->FormatInput->capFirst($this->data['Teacher']['specialty']);
            if (!empty($this->data['Teacher']['phone']))
                $this->data['Teacher']['phone'] = $this->FormatInput->formatPhone($this->data['Teacher']['phone']);
            // Envoi des données au modèle
            $this->data['Teacher']['id'] = $this->AppAuth->user('id');
            $this->Teacher->set($this->data);
            // Si validation : sauvegarde
            if ($this->Teacher->validates()) {
                // Préparation maj des variables de session
                $updateSession = $this->Session->read('Auth.User.info');
                $updateSession['name'] = $this->data['Teacher']['name'];
                $updateSession['firstname'] = $this->data['Teacher']['firstname'];
                $updateSession['quality'] = $this->data['Teacher']['quality'];
                $updateSession['specialty'] = $this->data['Teacher']['specialty'];
                // Sauvegarde
                if ($this->Teacher->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array(
                                'name',
                                'firstname',
                                'quality',
                                'specialty',
                                'phone'
                            )
                        ))) {
                    // Maj des variables de session
                    $this->Session->write('Auth.User.info', $updateSession);
                    // Message
                    $this->Session->setFlash(__("Your profile has been updated.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/teacher/base');
                }
            }
            // Rechargement des données de vues
            $user = $this->Teacher->getProfile($this->AppAuth->user('id'));
        }
        // Contenu de la liste déroulante
        $this->set('university', $this->Teacher->University->getUniversityList(false, true));
        // Nombre d'utilisateurs'
        $this->set('userCount', $this->_count());
        // Variables de vue
        $this->set('title_for_layout', __("Teacher", true) . ' &bull; ' . __("Profile edition", true));
        $this->set('user', $user);

    }


    /**
     * Édition des infos de connexion / courriel
     *
     */
    function connect() {

        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->Teacher->getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Envoi des données au modèle
            $this->data['Teacher']['id'] = $this->AppAuth->user('id');
            $this->data['User']['id'] = $this->AppAuth->user('id');
            $this->Teacher->set($this->data);
            // Préparation maj des variables de session
            $updateSession = array(
                'email' => $this->data['User']['email'],
                'username' => $this->data['User']['username']
            );
            // Sauvegarde avec validation
            if ($this->Teacher->User->save($this->data, array(
                        'validate' => true,
                        'fieldList' => array(
                            'email',
                            'username',
                            'password',
                            'passwd',
                            'passwd_confirm'
                        )
                    ))) {
                // Maj des variables de session
                $this->Session->write('Auth.User.email', $updateSession['email']);
                $this->Session->write('Auth.User.username', $updateSession['username']);
                // Message
                $this->Session->setFlash(__("Your information has been updated.", true));
                // Redirection
                $this->redirect('/' . $this->params['language'] . '/teacher/base');
            }
            // Rechargement des données pour le reste de la vue
            $user = $this->Teacher->getProfile($this->AppAuth->user('id'));
        }
        // Nombre d'utilisateurs'
        $this->set('userCount', $this->_count());
        // Variables de vue
        $this->set('title_for_layout', __("Teacher", true) . ' &bull; ' . __("Connection / Email data edition", true));
        $this->set('user', $user);

    }


    /**
     * Édition de la phrase secrète
     *
     */
    function secret() {

        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->Teacher->getProfile($this->AppAuth->user('id'));
            $user = $this->data;
        } else {
            // Envoi des données au modèle
            $this->data['Teacher']['id'] = $this->AppAuth->user('id');
            $this->Teacher->set($this->data);
            // Si validation : sauvegarde
            if ($this->Teacher->validates()) {
                // Sauvegarde
                $this->Teacher->id = $this->data['Teacher']['id'];
                if ($this->Teacher->saveField('secret', $this->data['Teacher']['secret'])) {
                    // Message
                    $this->Session->setFlash(__("Your account creation code has been updated.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/teacher/base');
                }
            }
            // Rechargement des données pour le reste de la vue
            $user = $this->Teacher->getProfile($this->AppAuth->user('id'));
        }
        // Nombre d'utilisateurs'
        $this->set('userCount', $this->_count());
        // Variables de vue
        $this->set('title_for_layout', __("Teacher", true) . ' &bull; ' . __("Account creation code edition", true));
        $this->set('user', $user);

    }


    /**
     * Nombre d'utilisateurs
     *
     * @return <array>
     */
    private function _count() {

        $userCount = array();
        $university = $this->Session->read('Auth.User.info.university_id');
        $userCount['student'] = $this->Student->count($university);
        $userCount['teacher'] = $this->Teacher->count($university);
        $userCount['visitor'] = $this->Visitor->count();

        #$this->log($userCount, 'debug');
        return $userCount;

    }


}

