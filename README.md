This is a language portfolio which follows the [COE](http://www.coe.int/) guidelines.

It allows teachers from different universities (or structures) to work with group of students, across various activities essentially based on personal work and self-assessment. External people (visitors) can, after having their account activated by the administrator, search for specific profiles among students, and contact them. Students can control what can be seen by teachers and visitors.

It was made between february and october 2010 and uses the framework [CakePHP](https://cakephp.org/), which was in version 1.3.x at this time… As a survivor of the portfolio hype, you might want to try it.

Of course, there is no documentation and it is provided “as is”, without warranty of any kind.


## Requirements

Lately, it has been seen working with:

* Apache 2.2
* PHP 5.6.10
* MySQL 5.5.42


## Installation


### 1°) CakePHP core

To install the CakePHP core, replace the “cake” folder by the one
contained in the archive file found at [https://github.com/cakephp/cakephp/releases/tag/1.3.21](https://github.com/cakephp/cakephp/releases/tag/1.3.21).


### 2°) Server settings

See “epel-httpd.conf” for an example of Apache configuration, or use
“epel-htaccess.txt” as a starting point for a “.htaccess” file (not
recommended and not tested).

>**Note:** the “rewrite” part is essential for CakePHP to work, and the PHP
“upload_max_filesize” value MUST be higher than the “epel.document.size”
value in your “app/config/config.php” file (see below).

The Apache user needs read/write access on the “webroot/files” folders (“tmp” and “users”).


### 3°) Database initialization

* Create a new MySQL database and its associated user,
* import into it the data contained in the dump file “epel-init.sql”,
* in “/app/config/” duplicate “_database.php” into “database.php”
  and fill in your database settings.


### 4°) Application settings

In “/app/config/” duplicate “_config.php” into “config.php”
and adjust settings (university footer link, CNIL number, SMTP, etc.).

For admin account credentials (and demo users ones) see the file
“epel-default_users.txt”.

Please start by opening a session as the administrator, and:

* change the admin account id and password (Base -> Connection / Email),
* give it a valid email (same page),
* create an “University” to work with (Elements -> Universities).


## Demo accounts

You'll find demo accounts credentials for the three kinds of users (students / teachers / visitors) in the file “epel-default_users.txt”.
