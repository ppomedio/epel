<!DOCTYPE html>
<!--

       ______        ______   _____  _
      |  __  |      |  __  | |  ___|| |
      | |__| |  __  | |__| | | |__  | |
      |  ____| |__| |  ____| |  __| | |
      | |____       | |      | |___ | |___
      |______|      |_|      |_____||_____| version <?php

echo Configure::read('epel.version') . "\n";
?>


    Made between february and october 2010 at the
        Université Montesquieu - Bordeaux IV
            http://www.u-bordeaux4.fr/


    inspired by the e-PEL v1 by             Dominique Vinet

    conception of "Strategy & Objectives"
    parts of the biography, rereadings      Anne-Marie Barrault-Méthy

    database & interface design,
    PHP & javascript scripting,
    french & english versions               Philippe Pomédio

    all icons and pictos                    http://icones.pro/


    Please use the “Contact” menu option for feedback & bug report.


-->
<html lang="<?php echo $this->Session->read('Config.language'); ?>">
    <head>
        <meta charset="<?php echo strtolower(Configure::read('App.encoding')); ?>">
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo strtolower(Configure::read('App.encoding')); ?>" />