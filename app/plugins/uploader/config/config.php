<?php
/**
 * Uploader Plugin Config
 *
 * A config class that holds all the settings and default mimetypes.
 *
 * @author      Miles Johnson - http://milesj.me
 * @copyright   Copyright 2006-2010, Miles Johnson, Inc.
 * @license     http://opensource.org/licenses/mit-license.php - Licensed under The MIT License
 * @link        http://milesj.me/resources/script/uploader-plugin
 */

/**
 * Current version: http://milesj.me/resources/logs/uploader-plugin
 */
$config['Uploader']['version'] = '2.6.1';

/**
 * The accepted file/mime types.
 */
$config['Uploader']['mimeTypes'] = array(
    'image' => array(
        #'bmp'	=> 'image/bmp',
        'gif'	=> 'image/gif',
        #'jpe'	=> 'image/jpeg',
        'jpg'	=> 'image/jpeg',
        'jpeg'	=> 'image/jpeg',
        #'pjpeg'	=> 'image/pjpeg',
        #'svg'	=> 'image/svg+xml',
        #'svgz'	=> 'image/svg+xml',
        #'tif'	=> 'image/tiff',
        #'tiff'	=> 'image/tiff',
        #'ico'	=> 'image/vnd.microsoft.icon',
        'png'	=> 'image/png',
        #'xpng'	=> 'image/x-png'
    ),
    'text' => array(
        'txt' 	=> 'text/plain',
        #'asc' 	=> 'text/plain',
        #'css' 	=> 'text/css',
        #'csv'	=> 'text/csv',
        'htm' 	=> 'text/html',
        'html' 	=> 'text/html',
        #'stm' 	=> 'text/html',
        'rtf' 	=> 'text/rtf',
        'rtx' 	=> 'text/richtext',
        'sgm' 	=> 'text/sgml',
        'sgml' => 'text/sgml',
        #'tsv' 	=> 'text/tab-separated-values',
        'tpl' 	=> 'text/template',
        'xml' 	=> 'text/xml',
        #'js'	=> 'text/javascript',
        'xhtml'	=> 'application/xhtml+xml',
        'xht'	=> 'application/xhtml+xml',
        #'json'	=> 'application/json'
    ),
    'archive' => array(
        'bz2'	=> 'application/x-bzip2',
        'gz'	=> 'application/x-gzip',
        'gtar'	=> 'application/x-gtar',
        'rar'	=> 'application/x-rar-compressed',
        'rev'	=> 'application/x-rar-compressed',
        'sit'	=> 'application/x-stuffit',
        'tar'	=> 'application/x-tar',
        'tgz'	=> 'application/x-compressed',
        'z'	=> 'application/x-compress',
        '7z'	=> 'application/x-7z-compressed',
        'zip'	=> 'application/zip'
    ),
    'audio' => array(
        'aif' 	=> 'audio/x-aiff',
        'aifc' 	=> 'audio/x-aiff',
        'aiff' 	=> 'audio/x-aiff',
        'au' 	=> 'audio/basic',
        'flac' 	=> 'audio/x-flac',
        #'kar' 	=> 'audio/midi',
        #'mid' 	=> 'audio/midi',
        #'midi' => 'audio/midi',
        'mp2' 	=> 'audio/mpeg',
        'mp3' 	=> 'audio/mpeg',
        'mpga' 	=> 'audio/mpeg',
        'oga' 	=> 'audio/ogg',
        'ogg' 	=> 'audio/ogg',
        'ra' 	=> 'audio/x-realaudio',
        'ram' 	=> 'audio/x-pn-realaudio',
        'rm' 	=> 'audio/x-pn-realaudio',
        'rpm' 	=> 'audio/x-pn-realaudio-plugin',
        'snd' 	=> 'audio/basic',
        #'tsi' 	=> 'audio/TSP-audio',
        'wav' 	=> 'audio/x-wav',
        'wma'	=> 'audio/x-ms-wma'
    ),
    'video' => array(
        'flv' 	=> 'video/x-flv',
        'fli' 	=> 'video/x-fli',
        'avi' 	=> 'video/x-msvideo',
        'qt' 	=> 'video/quicktime',
        'mov' 	=> 'video/quicktime',
        'movie' => 'video/x-sgi-movie',
        'mp2' 	=> 'video/mpeg',
        'mpa' 	=> 'video/mpeg',
        'mpv2' 	=> 'video/mpeg',
        'mpe' 	=> 'video/mpeg',
        'mpeg' 	=> 'video/mpeg',
        'mpg' 	=> 'video/mpeg',
        'mp4'   => 'video/mp4',
        'mkv'   => 'video/x-matroska',
        'webm'  => 'video/webm',
        #'viv' 	=> 'video/vnd.vivo',
        #'vivo' => 'video/vnd.vivo',
        '3gp'	=> 'video/3gpp',
        '3g2'	=> 'video/3gpp2',
        'wmv'	=> 'video/x-ms-wmv'
    ),
    'application' => array(
        'odm'	=> 'application/vnd.oasis.opendocument.text-master',
        'odp'	=> 'application/vnd.oasis.opendocument.presentation',
        'odt'	=> 'application/vnd.oasis.opendocument.text',
        'otp'	=> 'application/vnd.oasis.opendocument.presentation-template',
        'ott'	=> 'application/vnd.oasis.opendocument.text-template',
        #'js'	=> 'application/x-javascript',
        'xlc' 	=> 'application/vnd.ms-excel',
        #'xll' 	=> 'application/vnd.ms-excel',
        #'xlm' 	=> 'application/vnd.ms-excel',
        'xls' 	=> 'application/vnd.ms-excel',
        #'xlw' 	=> 'application/vnd.ms-excel',
        'doc'	=> 'application/msword',
        'dot'	=> 'application/msword',
        'docx'	=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'dotx'	=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
        'pdf' 	=> 'application/pdf',
        'pps'	=> 'application/vnd.ms-powerpoint',
        'ppt'	=> 'application/vnd.ms-powerpoint',
        'sxi'	=> 'application/vnd.sun.xml.impress',
        'sxw'	=> 'application/vnd.sun.xml.writer',
        #'psd' 	=> 'image/vnd.adobe.photoshop',
        #'ai' 	=> 'application/postscript',
        #'eps' 	=> 'application/postscript',
        #'ps' 	=> 'application/postscript'
    )
);
