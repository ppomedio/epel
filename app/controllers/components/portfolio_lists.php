<?php

/**
 * {app}/controllers/components/portfolio_lists.php
 *
 */
class PortfolioListsComponent extends Object {


    /**
     *
     * @return <array>
     */
    function getCompetenciesNames() {

        return array(
            1 => __d('levels_grid', "Understanding: listening", true),
            __d('levels_grid', "Understanding: reading", true),
            __d('levels_grid', "Speaking: spoken interaction", true),
            __d('levels_grid', "Speaking: spoken production", true),
            __d('levels_grid', "Writing", true)
        );

    }


    /**
     *
     * @return <array>
     */
    function getLevelsList() {

        return array(
            1 => 'A1',
            'A2',
            'B1',
            'B2',
            'C1',
            'C2'
        );

    }


    /**
     *
     * @return <array>
     */
    function getCompetenciesList() {

        return array(
            'listening' => __d('portfolio', "Listening", true),
            'reading' => __d('portfolio', "Reading", true),
            'speak_int' => __d('portfolio', "Spoken interaction", true),
            'speak_prod' => __d('portfolio', "Spoken production", true),
            'writing' => __d('portfolio', "Writing", true)
        );

    }


    /**
     *
     * @return <array>
     */
    function getLevelsDescriptions() {

        return array(
            'listening' => array(
                'A1' => __d('levels_grid', "I can understand familiar words and very basic phrases concerning myself, my family and immediate concrete surroundings when people speak slowly and clearly.", true),
                'A2' => __d('levels_grid', "I can understand phrases and the highest frequency vocabulary related to areas of most immediate personal relevance (e.g. very basic personal and family information, shopping, local area, employment). I can catch the main point in short, clear, simple messages and announcements.", true),
                'B1' => __d('levels_grid', "I can understand the main points of clear standard speech on familiar matters regularly encountered in work, school, leisure, etc. I can understand the main point of many radio or TV programmes on current affairs or topics of personal or professional interest when the delivery is relatively slow and clear.", true),
                'B2' => __d('levels_grid', "I can understand extended speech and lectures and follow even complex lines of argument provided the topic is reasonably familiar. I can understand most TV news and current affairs programmes. I can understand the majority of films in standard dialect.", true),
                'C1' => __d('levels_grid', "I can understand extended speech even when it is not clearly structured and when relationships are only implied and not signalled explicitly. I can understand television programmes and films without too much effort.", true),
                'C2' => __d('levels_grid', "I have no difficulty in understanding any kind of spoken language, whether live or broadcast, even when delivered at fast native speed, provided I have some time to get familiar with the accent.", true)
            ),
            'reading' => array(
                'A1' => __d('levels_grid', "I can understand familiar names, words and very simple sentences, for example on notices and posters or in catalogues.", true),
                'A2' => __d('levels_grid', "I can read very short, simple texts. I can find specific, predictable information in simple everyday material such as advertisements, prospectuses, menus and timetables and I can understand short simple personal letters.", true),
                'B1' => __d('levels_grid', "I can understand texts that consist mainly of high frequency everyday or job-related language. I can understand the description of events, feelings and wishes in personal letters.", true),
                'B2' => __d('levels_grid', "I can read articles and reports concerned with contemporary problems in which the writers adopt particular attitudes or viewpoints. I can understand contemporary literary prose.", true),
                'C1' => __d('levels_grid', "I can understand long and complex factual and literary texts, appreciating distinctions of style. I can understand specialised articles and longer technical instructions, even when they do not relate to my field.", true),
                'C2' => __d('levels_grid', "I can read with ease virtually all forms of the written language, including abstract, structurally or linguistically complex texts such as manuals, specialised articles and literary works.", true)
            ),
            'speak_int' => array(
                'A1' => __d('levels_grid', "I can interact in a simple way provided the other person is prepared to repeat or rephrase things at a slower rate of speech and help me formulate what I’m trying to say. I can ask and answer simple questions in areas of immediate need or on very familiar topics.", true),
                'A2' => __d('levels_grid', "I can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar topics and activities. I can handle very short social exchanges, even though I can’t usually understand enough to keep the conversation going myself.", true),
                'B1' => __d('levels_grid', "I can deal with most situations likely to arise whilst travelling in an area where the language is spoken. I can enter unprepared into conversation on topics that are familiar, of personal interest or pertinent to everyday life (e.g. family, hobbies, work, travel and current events).", true),
                'B2' => __d('levels_grid', "I can interact with a degree of fluency and spontaneity that makes regular interaction with native speakers quite possible. I can take an active part in discussion in familiar contexts, accounting for and sustaining my views.", true),
                'C1' => __d('levels_grid', "I can express myself fluently and spontaneously without much obvious searching for expressions. I can use language flexibly and effectively for social and professional purposes. I can formulate ideas and opinions with precision and relate my contribution skilfully to those of other speakers.", true),
                'C2' => __d('levels_grid', "I can take part effortlessly in any conversation or discussion and have a good familiarity with idiomatic expressions and colloquialisms. I can express myself fluently and convey finer shades of meaning precisely. If I do have a problem I can backtrack and restructure around the difficulty so smoothly that other people are hardly aware of it.", true)
            ),
            'speak_prod' => array(
                'A1' => __d('levels_grid', "I can use simple phrases and sentences to describe where I live and people I know.", true),
                'A2' => __d('levels_grid', "I can use a series of phrases and sentences to describe in simple terms my family and other people, living conditions, my educational background and my present or most recent job.", true),
                'B1' => __d('levels_grid', "I can connect phrases in a simple way in order to describe experiences and events, my dreams, hopes and ambitions. I can briefly give reasons and explanations for opinions and plans. I can narrate a story or relate the plot of a book or film and describe my reactions.", true),
                'B2' => __d('levels_grid', "I can present clear, detailed descriptions on a wide range of subjects related to my field of interest. I can explain a viewpoint on a topical issue giving the advantages and disadvantages of various options.", true),
                'C1' => __d('levels_grid', "I can present clear, detailed descriptions of complex subjects integrating sub-themes, developing particular points and rounding off with an appropriate conclusion.", true),
                'C2' => __d('levels_grid', "I can present a clear, smoothly-flowing description or argument in a style appropriate to the context and with an effective logical structure which helps the recipient to notice and remember significant points.", true)
            ),
            'writing' => array(
                'A1' => __d('levels_grid', "I can write a short, simple postcard, for example sending holiday greetings. I can fill in forms with personal details, for example entering my name, nationality and address on a hotel registration form.", true),
                'A2' => __d('levels_grid', "I can write short, simple notes and messages. I can write a very simple personal letter, for example thanking someone for something.", true),
                'B1' => __d('levels_grid', "I can write simple connected text on topics which are familiar or of personal interest. I can write personal letters describing experiences and impressions.", true),
                'B2' => __d('levels_grid', "I can write clear, detailed text on a wide range of subjects related to my interests. I can write an essay or report, passing on information or giving reasons in support of or against a particular point of view. I can write letters highlighting the personal significance of events and experiences.", true),
                'C1' => __d('levels_grid', "I can express myself in clear, well structured text, expressing points of view at some length. I can write about complex subjects in a letter, an essay or a report, underlining what I consider to be the salient issues. I can select a style appropriate to the reader in mind.", true),
                'C2' => __d('levels_grid', "I can write clear, smoothly-flowing text in an appropriate style. I can write complex letters, reports or articles which present a case with an effective logical structure which helps the recipient to notice and remember significant points. I can write summaries and reviews of professional or literary works.", true)
            )
        );

    }


    /**
     *
     * @return <array>
     */
    function getDurationsList() {

        return array(
            1 => __d('portfolio', "day(s)", true),
            __d('portfolio', "week(s)", true),
            __d('portfolio', "month(s)", true),
            __d('portfolio', "year(s)", true)
        );

    }


    /**
     *
     * @return <array>
     */
    function getSchoolStudyPeriodsList() {

        return array(
            1 => __d('portfolio', "1 year or less", true),
            __d('portfolio', "Between 1 and 2 years", true),
            __d('portfolio', "Between 2 and 3 years", true),
            __d('portfolio', "Between 3 and 4 years", true),
            __d('portfolio', "Between 4 and 5 years", true),
            __d('portfolio', "Between 5 and 6 years", true),
            __d('portfolio', "Between 6 and 7 years", true),
            __d('portfolio', "Between 7 and 8 years", true),
            __d('portfolio', "Between 8 and 9 years", true),
            __d('portfolio', "More than 9 years", true)
        );

    }


    /**
     *
     * @return <array>
     */
    function getOtherStudyPeriodsList() {

        return array(
            1 => __d('portfolio', "1 week or less", true),
            __d('portfolio', "Between 1 and 2 weeks", true),
            __d('portfolio', "Between 2 and 3 weeks", true),
            __d('portfolio', "Between 3 weeks and 1 month", true),
            __d('portfolio', "Between 1 and 2 months", true),
            __d('portfolio', "Between 2 and 3 months", true),
            __d('portfolio', "Between 3 and 4 months", true),
            __d('portfolio', "Between 4 and 5 months", true),
            __d('portfolio', "Between 5 and 6 months", true),
            __d('portfolio', "Between 6 and 9 months", true),
            __d('portfolio', "Between 9 months and 1 year", true),
            __d('portfolio', "Between 1 and 2 years", true),
            __d('portfolio', "Between 2 and 3 years", true),
            __d('portfolio', "Between 3 and 6 years", true),
            __d('portfolio', "More than 6 years", true)
        );

    }


    /**
     *
     * @return <array>
     */
    function getActivityPeriodsList() {

        return array(
            1 => __d('portfolio', "2 hours a month or less", true),
            __d('portfolio', "Between 2 and 5 hours a month", true),
            __d('portfolio', "Between 5 and 10 hours a month", true),
            __d('portfolio', "Between 10 and 15 hours a month", true),
            __d('portfolio', "Between 15 and 20 hours a month", true),
            __d('portfolio', "More than 20 hours a month", true)
        );

    }


}

