
                    <section>
                        <h2><?php __("Profile"); echo " (";  __("editing"); echo "…)"; ?></h2>
                        <?php echo $this->Form->create('Teacher', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/teacher/base/profile',
                            'id' => 'profile-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Teacher'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('name', array(
                            'label' => __("Name", true)
                            ));
                        echo $this->Form->input('firstname', array(
                            'label' => __("First name", true)
                            ));
                        echo $this->Form->input('university_id', array(
                            'label' => __("College / University", true),
                            'options' => $university,
                            'disabled' => "disabled"
                            )); ?>

                        <p class="info"><?php
                        __("In order to modify university-related information, please");
                        echo " " . $this->Html->link(__("contact the administrator", true),
                            array(
                                'controller' => 'contact',
                                'action' => 'form'
                            ),
                            array('class'=>'iframe contact')
                        );
                        ?>.</p>
                        <?php
                        echo $this->Form->input('quality', array(
                            'label' => __("Quality", true)
                            ));
                        echo $this->Form->input('specialty', array(
                            'label' => __("Specialty", true)
                            ));
                        echo $this->Form->input('phone', array(
                            'label' => __("Telephone", true)
                            ));
                        ?>
                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#profile-form');
");

?>
