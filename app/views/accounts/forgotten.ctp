
        <h2><?php __("Forgotten password"); ?></h2>
        <?php
        echo $this->Form->create('User', array(
            'url' => '/' . Configure::read('Config.langCode')
                . '/account/forgotten',
            'id' => 'forgotten-form',
            #'class' => ''
            ));
        echo $validation->bind('User'); // Validation jQuery
        ?>

        <fieldset>
            <?php
            echo $this->Form->input('email',
                    array(
                        'label' => __("eMail address", true),
                        'maxLength' => 128
                        )
                    );
            ?>

        </fieldset>
        <p class="info"><?php __("The corresponding username and a new password will be sent to this email address."); ?></p>
        <?php echo $this->Form->end(__("Send", true)); ?>

<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#forgotten-form');
");

?>
