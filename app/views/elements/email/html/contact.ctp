
        <p><strong><?php __("Send by"); ?></strong> <?php echo $data['Contact']['firstname'] . ' ' . $data['Contact']['name'] . ' '; ?>
            (<a href="mailto:<?php echo $data['Contact']['email'] ?>"><?php echo $data['Contact']['email'] ?></a>)</p>
        <hr />
        <p><strong><?php __("Message"); ?></strong><br /><?php
        $content = explode("\n", $data['Contact']['message']);
        foreach ($content as $line):
            echo $line . '<br />';
        endforeach;
        ?>

        </p>
        <hr />
        <p><em><?php echo date('Y/m/d - H:i (e)') . " - " . $ipAddress; ?></em></p>
