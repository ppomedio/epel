
                <article class="half"><?php
                    echo $this->element('connect-show');
                    echo $this->element('visitor/base/profile-show');
                    ?>

                </article>
                <article class="half">
                    <section>
                        <h2 id="title-visit-page"><?php __("Visits"); ?></h2>
                        <?php echo $this->element('visitor/base/visits-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('photo');
                    echo $this->element('visitor/base/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipEdit('article');
    tooltipSupp('article');
");

?>