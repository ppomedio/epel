<?php

/**
 * {app}/models/po_assess_criteria.php
 */
class PoAssessCriteria extends AppModel {

    var $name = 'PoAssessCriteria';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    function getAssessCriteria($portfolioId, $competency, $level) {

        // Chargement des données
        $data = $this->find('first', array(
                    'conditions' => array(
                        'PoAssessCriteria.portfolio_id' => $portfolioId,
                        'PoAssessCriteria.competency' => $competency,
                        'PoAssessCriteria.level' => $level
                    ),
                    'fields' => array(
                        'id',
                        'criteria_set',
                        'criteria1',
                        'criteria2',
                        'criteria3'
                    ),
                    'recursive' => -1
                ));

        // Mise en forme
        if (!empty($data) && ($data['PoAssessCriteria']['criteria_set'] != 3)) {
            switch ($data['PoAssessCriteria']['criteria_set']) {

                case 1:
                    $data['PoAssessCriteria']['criteria1'] = __d('assessment', "with a lot of help", true);
                    $data['PoAssessCriteria']['criteria2'] = __d('assessment', "with a little help", true);
                    $data['PoAssessCriteria']['criteria3'] = __d('assessment', "on my own", true);
                    break;

                case 2:
                    $data['PoAssessCriteria']['criteria1'] = __d('assessment', "with a lot of effort", true);
                    $data['PoAssessCriteria']['criteria2'] = __d('assessment', "under normal circumstances", true);
                    $data['PoAssessCriteria']['criteria3'] = __d('assessment', "easily in any context", true);
                    break;

                default:
                    break;

            }
        }

        //Retour
        return $data;

    }


}

