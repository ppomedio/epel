
                <article>
                    <h1><?php echo $title_for_layout; ?></h1><?php
                    echo $this->element('student/passport/mother');
                    echo $this->element('student/passport/skills');
                    echo $this->element('student/passport/exp-overview');
                    if (!empty($user['PoValuable'])) { ?>

                    <section>
                        <h2 id="title-valuable"><?php __d('portfolio', "Memorable language-learning experiences"); ?></h2>
                        <?php echo $this->element('student/passport/exp-detail-page'); ?>

                    </section><?php
                    } else { ?>

                    <section>
                        <h2><?php __d('portfolio', "Memorable language-learning experiences"); ?></h2>
                        <div>
                            <p class="alert"><?php  __d('portfolio', "There is no memorable experience currently recorded in your Portfolio.");  ?></p>
                            <p class="info"><?php  __d('portfolio', "Adding / updating / deleting these experience items takes place in the Biography part of your Portfolio.");  ?></p>
                        </div>
                    </section><?php
                    }
                    if (!empty($user['PoCertificate'])) { ?>

                    <section>
                        <h2><?php __d('portfolio', "Certificates"); ?></h2>
                        <div id="certificate" class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Date"); ?></th>
                                        <th><?php __d('portfolio', "Delivered by"); ?></th>
                                        <th><span id="certificate-level" class="help"><?php __d('portfolio', "Level(s)"); ?></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoCertificate'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>
                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php
                                        if ($value['certificate_id'] == '0001')
                                            echo '<strong>' . $value['certificate_info'] . '</strong>';
                                        else {
                                            echo '<strong>' . $value['Certificate']['Certification']['name'] . '</strong>';
                                            if ($value['Certificate']['Certification']['description'])
                                                echo "<br /><em>" .  $value['Certificate']['Certification']['description'] . "</em>";
                                        }
                                        ?></td>
                                        <td><?php echo $this->Date->Convert($value['date']); ?></td>
                                        <td><?php echo $value['organism']; ?></td>
                                        <td><?php
                                        $comp = array();
                                        foreach($competencies as $key => $competency) {
                                            if ($value[$key])
                                                $comp[] = $competency . __(":", true) . '&nbsp;<span class="bolder infotext">' . $levels[$value[$key]] . '</span>';
                                        }
                                        if (!empty($comp))
                                            echo implode("<br />", $comp);
                                        ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <p class="info"><?php  __d('portfolio', "Adding / updating / deleting certificates takes place in the Dossier part of your Portfolio.");  ?></p>
                        </div>
                        <div id="data_certificate-level" class="tooltip-data"><?php __d('portfolio', "CEFRL level, specified only if it appears on the original certificate."); ?></div>
                    </section><?php
                    } else { ?>

                    <section>
                        <h2><?php __d('portfolio', "Certificates"); ?></h2>
                        <div>
                            <p class="alert"><?php  __d('portfolio', "There is no certificate currently recorded in your Portfolio.");  ?></p>
                            <p class="info"><?php  __d('portfolio', "Adding / updating / deleting certificates takes place in the Dossier part of your Portfolio.");  ?></p>
                        </div>
                    </section><?php
                    } ?>

                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'passport'));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('student/passport/bubbles-info', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-europass', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipHelp('article');
    tooltipSortHelp('article');
    // Tips
    addTipAlert('article');
    addTipInfo('article');
    addTipInfo('#search-info');
");

?>