<?php

/**
 * {app}/models/country.php
 */
class Country extends AppModel {

    var $name = 'Country';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $hasMany = array(
        'OriginCountry' => array(
            'className' => 'Student',
            'foreignKey' => 'origin_country_id'
        ),
        'LivingCountry' => array(
            'className' => 'Student',
            'foreignKey' => 'living_country_id'
        )
    );
    var $validate = array(
        'name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => "Please enter a country name.",
                'last' => true
            ),
            'format' => array(
                'rule' => array('custom', '/^[a-zA-Z ()\.,\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                'message' => "Some characters are not allowed…",
                'last' => true
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => "This country name already exists."
            )
        )
    );


    /**
     * Traduction des messages d'erreur : pour une raison indéterminée (la
     * relation double avec 'Student' ?) définir les règles de validation dans
     * le constructeur de ce modèle entraine une erreur de dépassement
     * d'allocation mémoire sur le serveur, même avec 64Mo…
     *
     */
    function validationMessages() {

        // Traduction des messages d'erreur
        $this->validate['name']['notEmpty']['message'] = __d('validation', "Please enter a country name.", true);
        $this->validate['name']['format']['message'] = __d('validation', "Some characters are not allowed…", true);
        $this->validate['name']['isUnique']['message'] = __d('validation', "This country name already exists.", true);

    }


}

