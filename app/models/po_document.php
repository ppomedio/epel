<?php

/**
 * {app}/models/po_document.php
 */
class PoDocument extends AppModel {

    var $name = 'PoDocument';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array(
        'Uploader.FileValidation' => array(
            'filename' => array(
                'extension' => array(
                    'value' => array(
                        'gif', 'jpg', 'jpeg', 'png',
                        'txt', 'htm', 'html', 'rtf', 'rtx', 'sgm', 'sgml', 'tpl', 'xml', 'xht', 'xhtml',
                        'bz2', 'gz', 'gtar', 'rar', 'rev', 'sit', 'tar', 'tgz', 'z', '7z', 'zip',
                        'aif', 'aifc', 'aiff', 'au', 'flac', 'mp2', 'mp3', 'mpga', 'oga', 'ogg', 'ra', 'ram', 'rm', 'rpm', 'snd', 'wav', 'wma',
                        'flv', 'fli', 'avi', 'qt', 'mov', 'movie', 'mp2', 'mpa', 'mpv2', 'mpe', 'mpeg', 'mpg', 'mp4', 'wmv', '3gp', '3g2',
                        'doc', 'dot', 'docx', 'dotx', 'odm', 'odp', 'odt', 'otp', 'ott', 'pdf', 'ppt', 'pps', 'sxi', 'sxw'
                    ),
                    'error' => "Mimetype incorrect."
                ),
                'filesize' => array(
                    'value' => 4194304,
                    'error' => "Filesize incorrect."
                )
            )
        ),
        'CleanData'
    );
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'description' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please describe document.", true)
            )
        );

        parent::__construct();

    }


    /**
     * Callback pré validation
     *
     * @return <bool>
     */
    function beforeValidate() {

        // Taille maxi des images téléchargeables
        $this->validate['filename']['filesize']['rule'][1] = Configure::read('epel.document.size') * 1048576;

        // Types de fichiers téléchargeables
        $this->validate['filename']['extension']['rule'][1] = Configure::read('epel.document.type');

        // Messages d'erreur de téléchargement
        $this->validate['filename']['filesize']['message'] = __d('validation', "The filesize is too large", true) . ' (' . strval(Configure::read('epel.document.size')) . 'MB max.).';
        #$this->validate['filename']['extension']['message'] = __d('validation', "Allowed file types:", true) . ' ' . implode($this->validate['filename']['extension']['rule'][1], ', ') . '.';
        $this->validate['filename']['extension']['message'] = __d('validation', "This file type is not allowed.", true);
        $this->validate['filename']['required']['message'] = __d('validation', "Please choose a file to upload.", true);

        // Sinon la validation échoue
        return true;

    }


    /**
     * Formatage du nom d'un fichier
     *
     * @param <string> $fileName
     * @return <string>
     */
    function fileNameCleaner($fileName) {

        // Caractères accentués
        $fileName = htmlentities($fileName, ENT_NOQUOTES, 'utf-8');
        $fileName = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $fileName);
        $fileName = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $fileName);
        $fileName = preg_replace('#\&[^;]+\;#', '', $fileName);

        // Autres caractères
        $search = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', '\'');
        $replace = array('');
        $fileName = str_replace($search, $replace, $fileName);

        // Retour du nouveau nom
        return $fileName;

    }


    /**
     * Chargement d'un document
     *
     * @param <string> $docId
     * @return <array>
     */
    function getThisDocument($docId) {

        return $this->find('first', array(
            'conditions' => array('PoDocument.id' => $docId),
            'fields' => array(
                'id',
                'competency',
                'filename',
                'description'
            ),
            'recursive' => -1
        ));

    }


    /**
     * Suppression des fichiers joints aux documents pour une langue
     *
     * @param <type> $userId
     * @param <type> $portfolioId
     */
    function deleteLanguageDocumentsFiles($userId, $portfolioId) {

        // Chargement de la liste
        $documents = $this->find('all', array(
                    'conditions' => array('PoDocument.portfolio_id' => $portfolioId),
                    'fields' => array(
                        'id',
                        'portfolio_id',
                        'file'
                    ),
                    'recursive' => -1
                ));
        // Suppression des fichiers
        foreach ($documents as $key => $value) {
            unlink(WWW_ROOT . 'files' . DS . 'users' . DS . $userId . DS . $value['PoDocument']['file']);
        }

    }


}

