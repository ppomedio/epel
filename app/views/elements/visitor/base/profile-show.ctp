
                    <section>
                        <h2><?php __("Profile"); ?></h2>
                        <div>
                            <dl>
                                <dt><?php __("Name"); ?></dt>
                                <dd><?php echo $user['Visitor']['name']; ?></dd>
                                <dt><?php __("First name"); ?></dt>
                                <dd><?php echo $user['Visitor']['firstname']; ?></dd>
                                <?php
                                if ($user['Visitor']['function']) {
                                ?><dt><?php __("Function"); ?></dt>
                                <dd><?php echo $user['Visitor']['function']; ?></dd>
                                <?php
                                }
                                if ($user['Visitor']['structure']) {
                                ?><dt><?php __("Structure"); ?></dt>
                                <dd><?php echo $user['Visitor']['structure']; ?></dd>
                                <?php
                                }
                                if ($user['Visitor']['phone']) {
                                ?><dt><?php __("Telephone"); ?></dt>
                                <dd><?php echo $user['Visitor']['phone']; ?></dd>
                                <?php
                                }
                                if ($user['Visitor']['url']) {
                                ?><dt><?php __("Web site"); ?></dt>
                                <dd><a href="<?php echo $user['Visitor']['url']; ?>" id="<?php echo __("Visit", true) . ' ' . $user['Visitor']['url']; ?>" class="link"><?php echo $user['Visitor']['url']; ?></a></dd>
                                <?php
                                }
                                if ($user['Visitor']['address']) {
                                ?><dt><?php __("Address"); ?></dt>
                                <dd><?php echo nl2br($user['Visitor']['address']); ?></dd><?php
                                } ?>
                                <dt><?php __("Country"); ?></dt>
                                <dd><?php __d('country', $user['Country']['name']); ?></dd>
                            </dl>
                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls">
                                    <?php
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/visitor/base/profile',
                                        array(
                                            'id' => 'profile',
                                            'class' => 'edit'
                                        )
                                    );
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div id="data_profile" class="tooltip-data"><?php __('Edit profile'); ?></div>
                    </section>