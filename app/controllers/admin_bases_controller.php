<?php

/**
 * {app}/controllers/admin_bases_controller.php
 *
 */
class AdminBasesController extends AppController {

    var $name = 'AdminBases';
    // Modèle
    var $uses = array(
        'Admin',
        'Activity',
        'Visit'
    );
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Date',
        'Menu'
    );

    function beforeFilter() {

        // Autorisation des champs "password" vides
        if ($this->params['action'] == 'connect')
            $this->Admin->User->allowEmptyPasswords();

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // Chargement des données de pagination des visites
        $visits = $this->_getVisits();
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visit.page', $this->params['paging']['Visit']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visit.order', $this->params['paging']['Visit']['options']['order']);
        // Traitement
        if ($this->RequestHandler->isAjax()) {
            /**
             * Pagination Ajax
             */
            Configure::write('debug', 0);
            // Variables de vue
            $this->set('ajax', true);
            $this->set('user', $visits);
            // Rendu de l'élément
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'base' . DS . 'visits-page');
            return;
        } else {
            /**
             * Affichage normal
             */
            // Chargement des données
            $user = $this->AppAuth->user() + $visits;
            // Variables de vue
            $this->set('user', $user);
            $this->set('userCount', $this->Admin->User->count());
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Base", true));
        }

    }


    /**
     * Édition des infos de connexion / courriel
     *
     */
    function connect() {

        // Chargement des données de pagination des visites
        $visits = $this->_getVisits();
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visit.page', $this->params['paging']['Visit']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visit.order', $this->params['paging']['Visit']['options']['order']);
        // Traitement
        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->Admin->User->find('first', array(
                        'conditions' => array('User.id' => $this->AppAuth->user('id')),
                        'fields' => array(
                            'User.id',
                            'User.email',
                            'User.username'),
                        'recursive' => -1
                    ));
        } else {
            // Envoi des données au modèle
            $this->data['Admin']['id'] = $this->AppAuth->user('id');
            $this->data['User']['id'] = $this->AppAuth->user('id');
            $this->Admin->set($this->data);
            // Préparation maj des variables de session
            $updateSession = array(
                'email' => $this->data['User']['email'],
                'username' => $this->data['User']['username']
            );
            // Sauvegarde avec validation
            if ($this->Admin->User->save($this->data, array(
                        'validate' => true,
                        'fieldList' => array(
                            'email',
                            'username',
                            'password',
                            'passwd',
                            'passwd_confirm'
                        )
                    ))) {
                // Maj des variables de session
                $this->Session->write('Auth.User.email', $updateSession['email']);
                $this->Session->write('Auth.User.username', $updateSession['username']);
                $this->Session->write('Auth.User.info.name', '(' . $updateSession['username'] . ')');
                $this->Session->write('Auth.User.info.firstname', __("Administrator", true));
                // Message
                $this->Session->setFlash(__("Your information has been updated.", true));
                // Redirection
                $this->redirect('/' . $this->params['language'] . '/admin/base');
            }
        }
        // Affichage
        if ($this->RequestHandler->isAjax()) {
            /**
             * Pagination Ajax
             */
            Configure::write('debug', 0);
            // Variables de vue
            $this->set('ajax', true);
            $this->set('user', $visits);
            // Rendu de l'élément
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'base' . DS . 'visits-page');
            return;
        } else {
            /**
             * Affichage normal
             */
            // Chargement des données
            $user = $this->AppAuth->user() + $visits;
            // Variables de vue
            $this->set('user', $user);
            $this->set('userCount', $this->Admin->User->count());
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Connection / Email data edition", true));
        }

    }


    function activity() {

        // Chargement des données de pagination
        $this->set('activity', $this->_getActivityData());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Activity.page', $this->params['paging']['Activity']['options']['page']);
        $this->Session->write('Auth.User.pagination.Activity.order', $this->params['paging']['Activity']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'base' . DS . 'activity-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Activity log", true));
        }

    }


    /**
     * Chargement de la liste des visites
     *
     * @param <string> $languageId
     * @return <array>
     */
    private function _getVisits() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Visit')) {
            $page = $this->Session->read('Auth.User.pagination.Visit.page');
            $order = $this->Session->read('Auth.User.pagination.Visit.order');
            $this->Session->delete('Auth.User.pagination.Visit');
        } else {
            $page = NULL;
            $order = array('Visit.date' => 'DESC');
        }
        // Paramétrage
        $this->paginate = array(
            'fields' => array(
                'id',
                'student_id',
                'visitor_id',
                'date'
            ),
            'contain' => array(
                'Student' => array(
                    'conditions' => array('Student.real' => 1),
                    'fields' => array(
                        'id',
                        'name',
                        'firstname'
                    )
                ),
                'Visitor' => array(
                    'conditions' => array('Visitor.real' => 1),
                    'fields' => array(
                        'id',
                        'name',
                        'firstname'
                    )
                )
            ),
            'page' => $page,
            'order' => $order,
            'limit' => 15
        );
        // Chargement des données
        $data = $this->paginate('Visit');
        $visits = array();
        foreach ($data as $key => $value) {
            if(!empty($value['Student']['id']) && !empty($value['Visitor']['id'])) {
                $visits['Visit'][$key]['date'] = $value['Visit']['date'];
                $visits['Visit'][$key]['Student']['id'] = $value['Student']['id'];
                $visits['Visit'][$key]['Student']['name'] = $value['Student']['name'];
                $visits['Visit'][$key]['Student']['firstname'] = $value['Student']['firstname'];
                $visits['Visit'][$key]['Visitor']['id'] = $value['Visitor']['id'];
                $visits['Visit'][$key]['Visitor']['name'] = $value['Visitor']['name'];
                $visits['Visit'][$key]['Visitor']['firstname'] = $value['Visitor']['firstname'];
            }
        }
        // Envoi de la pagination
        #$this->log($visits, 'debug');
        return $visits;

    }


    /**
     *
     * @return <array>
     */
    private function _getActivityData() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Activity')) {
            $page = $this->Session->read('Auth.User.pagination.Activity.page');
            $order = $this->Session->read('Auth.User.pagination.Activity.order');
            $this->Session->delete('Auth.User.pagination.Activity');
        } else {
            $page = NULL;
            $order = array('Activity.datetime' => 'DESC');
        }

        // Paramétrage
        $this->paginate = array(
            #'conditions' => array('datetime >' => date('Y-m-d H:i:s', strtotime("-3 months"))),
            'fields' => array(
                'id',
                'datetime',
                'user_id',
                'role',
                'university_id',
                'action',
                'extra',
                'ip'
            ),
            'recursive' => -1,
            'page' => $page,
            'order' => $order,
            'limit' => 20
        );

        // Chargement
        $activity = $this->paginate('Activity');

        //Infos supplémentaires
        $this->loadModel('Student');
        $this->loadModel('Teacher');
        $this->loadModel('University');
        $this->loadModel('Visitor');
        foreach ($activity as $key => $value) {
            // Date / heure
            $date = explode(' ', $value['Activity']['datetime']);
            unset($activity[$key]['Activity']['datetime']);
            $activity[$key]['Activity']['date'] = $date[0];
            $activity[$key]['Activity']['time'] = $date[1];
            // Identité
            if ($value['Activity']['user_id'] > 1) {
                $model = ucfirst($value['Activity']['role']);
                $userName = $this->$model->find('first', array(
                            'conditions' => array(
                                'id' => $value['Activity']['user_id']
                            ),
                            'fields' => array('name', 'firstname'),
                            'recursive' => -1
                        ));
                $activity[$key]['Activity']['user_info'] = $userName[$model];
            }
            // Nom de l'université
            if ($value['Activity']['university_id']) {
                $university = $this->University->find('first', array(
                            'conditions' => array(
                                'id' => $value['Activity']['university_id']
                            ),
                            'fields' => array('name'),
                            'recursive' => -1
                        ));
                $activity[$key]['Activity']['university_info'] = $university['University'];
                if ((int)$value['Activity']['university_id'] < 3)
                    $activity[$key]['Activity']['university_info']['name'] = __($activity[$key]['Activity']['university_info']['name'], true);
            }
        }

        // Envoi de la pagination
        return $activity;

    }


}

