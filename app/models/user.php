<?php

/**
 * {app}/models/user.php
 */
class User extends AppModel {

    var $name = 'User';
    var $useDbConfig = 'epel';
    // Init. du tableau de validation
    var $validate = array();
    // Comportements
    var $actsAs = array(
        'Polymorphic',
        'Containable',
        'Uploader.FileValidation' => array(
            'photo' => array(
                'extension' => array(
                    'value' => array('gif', 'jpg', 'png', 'jpeg'),
                    'error' => "Mimetype incorrect."
                ),
                'filesize' => array(
                    'value' => 1048576,
                    'error' => "Filesize incorrect."
                )
            )
        ),
        'CleanData'
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a username.", true),
                    'last' => true
                ),
                'between' => array(
                    'rule' => array('between', 5, 20),
                    'message' => __d('validation', "Username must be between 5 and 20 characters long.", true),
                    'last' => true
                ),
                'alphaNumeric' => array(
                    'rule' => array('alphaNumeric'),
                    'message' => __d('validation', "Username must only contain letters and numbers, without accentuated characters.", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This username is already used.", true)
                )
            ),
            'password' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a password.", true),
                    'last' => true
                ),
                'minLength' => array(
                    'rule' => array('minLength', 6),
                    'message' => __d('validation', "Password must be minimum 6 characters long.", true)
                )
            ),
            'passwd' => array(
                'minLength' => array(
                    'rule' => array('minLength', 6),
                    'allowEmpty' => false,
                    'message' => __d('validation', "Password must be minimum 6 characters long.", true)
                )
            ),
            'passwd_confirm' => array(
                'minLength' => array(
                    'rule' => array('minLength', 6),
                    'allowEmpty' => false,
                    'message' => __d('validation', "Password must be minimum 6 characters long.", true),
                    'last' => true
                ),
                'compare' => array(
                    'rule' => array('comparePasswords'),
                    'message' => __d('validation', "Passwords do not match.", true)
                )
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your email address.", true),
                    'last' => true
                ),
                'email' => array(
                    'rule' => array('email'),
                    'message' => __d('validation', "This email adress is not valid.", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This email adress is already used.", true)
                )
            )
        );

        parent::__construct();

    }


    /**
     * Fonction de validation :
     * égalité des champs "password"
     *
     * @return <bool>
     */
    function comparePasswords() {

        if ($this->data['User']['passwd'] === $this->data['User']['passwd_confirm'])
            return true;
        else
            return false;

    }


    /**
     * Fonction de validation optionnelle :
     * champs "password" tous vides ou tous non vides
     *
     * @return <bool>
     */
    function checkPasswords() {

        if (!empty($this->data['User']['passwd'])) {
            if (!empty($this->data['User']['passwd_confirm']))
                return true;
            else
                return false;
        } else
            return true;

    }


    function beforeValidate() {

        // Taille maxi des images téléchargeables
        $this->validate['photo']['filesize']['rule'][1] = Configure::read('epel.photo.size') * 1048576;

        // Types de fichiers téléchargeables
        $this->validate['photo']['extension']['rule'][1] = Configure::read('epel.photo.type');

        // Messages d'erreur de téléchargement
        $this->validate['photo']['filesize']['message'] = __d('validation', "The filesize is too large", true)
                . ' (' . strval(Configure::read('epel.photo.size')) . 'MB max.).';
        $this->validate['photo']['extension']['message'] = __d('validation', "Allowed file types:", true)
                . ' ' . implode($this->validate['photo']['extension']['rule'][1], ', ') . '.';
        $this->validate['photo']['required']['message'] = __d('validation', "Please choose a file to upload.", true);

        // Sinon la validation échoue
        return true;

    }


    function beforeSave() {

        // Changement éventuel du mot de passe
        if (!empty($this->data[$this->alias]['passwd']) && !empty($this->data[$this->alias]['passwd_confirm']))
            $this->data[$this->alias]['password'] = sha1($this->data[$this->alias]['passwd']);

        // Sinon la sauvegarde n'a pas lieu
        return true;

    }


    /**
     * Ajout des règles de validation nécessaires pour la mise à jour
     * du mot de passe (appel depuis un contrôleur).
     */
    function allowEmptyPasswords() {

        // Règle de comparaison tous vides / tous non vides
        $rule['passwd']['check'] = array(
            'rule' => array('checkPasswords'),
            'message' => __d('validation', "Passwords do not match.", true)
        );
        $this->validate['passwd'] = $this->validate['passwd'] + $rule['passwd'];

        // Autorisation champs vides
        $this->validate['passwd']['minLength']['allowEmpty'] = true;
        $this->validate['passwd_confirm']['minLength']['allowEmpty'] = true;

    }


    /**
     * Nombre d'utilisateurs actifs / inactifs par role
     *
     * @return <array>
     */
    function count() {

        $userCount = array();
        $userCount['active']['student'] = $this->find('count', array(
                    'conditions' => array(
                        'User.role' => 'student',
                        'User.active' => 1,
                        'User.real' => 1
                        )));
        $userCount['inactive']['student'] = $this->find('count', array(
                    'conditions' => array(
                        'User.role' => 'student',
                        'User.active' => 0,
                        'User.real' => 1
                        )));
        $userCount['active']['teacher'] = $this->find('count', array(
                    'conditions' => array(
                        'User.role' => 'teacher',
                        'User.active' => 1,
                        'User.real' => 1
                        )));
        $userCount['inactive']['teacher'] = $this->find('count', array(
                    'conditions' => array(
                        'User.role' => 'teacher',
                        'User.active' => 0,
                        'User.real' => 1
                        )));
        $userCount['active']['visitor'] = $this->find('count', array(
                    'conditions' => array(
                        'User.role' => 'visitor',
                        'User.active' => 1,
                        'User.real' => 1
                        )));
        $userCount['inactive']['visitor'] = $this->find('count', array(
                    'conditions' => array(
                        'User.role' => 'visitor',
                        'User.active' => 0,
                        'User.real' => 1
                        )));

        return $userCount;

    }


    /**
     * Vérification de mot de passe
     *
     * @param <string> $pw  Mot de passe
     * @param <string> $id  ID utilisateur
     * @return <bool>
     */
    function isMyPassword($pw, $id) {

        $result = false;
        $data = $this->find('first', array(
                    'conditions' => array('User.id' => $id),
                    'fields' => array('password'),
                    'recursive' => -1
                ));
        if (sha1($pw) == $data['User']['password'])
            $result = true;

        return $result;

    }


    /**
     * Réinitialisation du mot de passe d'un utilisateur
     * (sauf pour l'administrateur -> sécurité)
     *
     * @param <string> $email
     * @return <mixed>
     */
    function resetPassword($email) {

        // Recherche de l'utilisateur correspondant
        $data = $this->find('first', array(
                    'conditions' => array(
                        'User.email' => $email,
                        'User.active' => 1,
                        'User.role !=' => 'admin'
                    ),
                    'fields' => array(
                        'User.id',
                        'User.username',
                        'User.email'
                    ),
                    'recursive' => -1
                ));
        // Traitement
        if (!empty($data)) {
            // Nouveau mot de passe
            $password = $this->generatePassword();
            // Enregistrement
            $this->id = $data['User']['id'];
            $this->saveField('password', $password['coded'], false);
            // Ajout du mdp à la matrice
            $data['User']['password'] = $password['clear'];
            // Retour des nouvelles infos de compte
            return $data;
        } else
            return false;

    }


    /**
     * Génération d'un mot de passe
     *
     * @return <array>
     */
    function generatePassword() {

        $password = array();
        $password['clear'] = substr(uniqid(), -8);
        $password['coded'] = sha1($password['clear']);

        return $password;

    }


    /**
     * Adresse email d'un utilisateur
     *
     * @param <string> $id
     * @return <string>
     */
    function getEmail($id) {

        $data = $this->find('first', array(
                    'conditions' => array('User.id' => $id),
                    'fields' => array('User.email'),
                    'recursive' => -1
                ));

        return $data['User']['email'];

    }


    /**
     * Création d'un dossier utilisateur
     *
     * @param <string> $userId
     * @return <bool>
     */
    function createUserFolder($userId) {

        $baseDir = WWW_ROOT . 'files' . DS . 'users'
                . DS . str_pad((string) $userId, 7, '0', STR_PAD_LEFT);
        $photoDir = $baseDir . DS . 'photo';
        if (!file_exists($photoDir)) {
            mkdir($photoDir, 0777, true);
            chmod($baseDir, 0777);
            chmod($photoDir, 0777);
        }

    }


    /**
     * Purge du dossier utilisateur complet ou d'un de ses sous-dossiers
     *
     * @param <string> $userId
     * @param <string> $subPath
     * @return <bool>
     */
    function cleanUserFiles($userId, $subPath = false) {

        $path = WWW_ROOT . 'files' . DS . 'users' . DS . $userId;
        if ($subPath) {
            $path .= DS . $subPath;
            return $this->_deleteFolder($path);
        } else
            return $this->_deleteFolder($path, true);

    }


    /**
     * Suppression récursive de dossier
     *
     * @param <string> $folder
     * @param <string> $rmdir
     * @return <bool>
     */
    private function _deleteFolder($folder, $rmdir = false) {

        if (is_dir($folder)) {
            $handle = @opendir($folder);
            if ($handle) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..') {
                        if (!is_dir($folder . DS . $file))
                            unlink($folder . DS . $file);
                        else
                            $this->_deleteFolder($folder . DS . $file, true);
                    }
                }
                closedir($handle);
                if ($rmdir)
                    rmdir($folder);
            }
            return true;
        }
        return false;

    }


}

