
        <h2><?php __("Account creation"); ?></h2>
        <?php
        echo $this->Form->create('User', array(
            'url' => '/' . Configure::read('Config.langCode')
                . '/account/create',
            'id' => 'create-form',
            #'class' => ''
            ));
        echo $validation->bind('User'); // Validation jQuery
        ?>

        <fieldset><?php
            echo $this->Form->input('User.username', array(
                'label' => __("Wished username", true)
                ));
            echo $this->Form->input('User.email', array(
                'label' => __("eMail address", true),
                'maxLength' => 128
                ));
            ?>

        </fieldset><?php
            if (Configure::read('epel.account_creation.visitor')) {
                echo $this->Form->radio('role',
                        array(
                            'student' => __("Student", true),
                            'teacher' => __("Teacher", true),
                            'visitor' => __("Visitor", true)
                        ),
                        array(
                            'legend' => __("Account type", true),
                            'value' => 'student',
                        )
                    );
            } else {
                echo $this->Form->radio('role',
                        array(
                            'student' => __("Student", true),
                            'teacher' => __("Teacher", true)
                        ),
                        array(
                            'legend' => __("Account type", true),
                            'value' => 'student',
                        )
                    );
            }
            ?>

        <?php echo $this->Form->end(__("Save", true)); ?>

<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#create-form');
");

?>
