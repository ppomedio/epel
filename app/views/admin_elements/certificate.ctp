
                <article class="half">
                    <section>
                        <h2 id="title-elements-list"><?php __("Certificates"); ?></h2>
                        <?php echo $this->element('admin/element/certificates-page'); ?>

                    </section>
                </article>
                <article class="half">
                    <section>
                        <h2 id="title-elements-form"><?php __("Add"); ?></h2>
                        <?php echo $this->Form->create('Certificate', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/admin/element/certificate',
                            'id' => 'elements-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Certificate'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('certification_id', array(
                            'label' => __("Certification", true),
                            'options' => $certifications,
                            'empty' => "…"
                            ));
                        echo $this->Form->input('certification_description', array(
                            'label' => __("Description", true),
                            'readonly'=>'readonly'
                            ));
                        echo $this->Form->input('language_id', array(
                            'label' => __("Language", true),
                            'options' => $languages,
                            'empty' => "…"
                            ));
                        ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('admin/element/how-to-use');
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

// Tableau des descriptions
$this->Js->Buffer("
    var description = new Array();
    description[''] = '';
");
foreach ($descriptions as $key => $value) {
    $this->Js->Buffer("
    description['" . $key . "'] = '" . addslashes($value) . "';");
}

$this->Js->Buffer("
    // Tips
    addTipInfo('#elements-form');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#elements-form div.submit input[type=submit]');
    // Soumission du formulaire
    $('#elements-form').submit(function() {
        if (!$('#elements-form .error').length)
            $('#elements-form div.loading').show();
        else
            return false;
    });
");

?>
