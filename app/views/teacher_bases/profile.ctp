
                <article class="half"><?php
                    echo $this->element('connect-show');
                    echo $this->element('teacher/base/secret-show');
                    ?>

                </article>
                <article class="half"><?php
                    echo $this->element('teacher/base/profile-edit');
                    echo $this->element('teacher/base/users');
                    ?>

                </article>
                <aside><?php
                    echo $this->element('photo');
                    echo $this->element('how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipEdit('article');
    tooltipSupp('article');
");

?>