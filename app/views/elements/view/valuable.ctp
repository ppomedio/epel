
                        <h2><?php __d('portfolio', "Memorable language-learning experiences"); ?></h2>
                        <div class="table">
                            <table class="typedesc">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Context"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoValuable'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo nl2br($value['description']); ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>