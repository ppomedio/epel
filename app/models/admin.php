<?php

/**
 * {app}/models/admin.php
 */
class Admin extends AppModel {

    var $name = 'Admin';
    // Ce modèle n'utilise pas de table dans la base
    #var $useDbConfig = 'epel';
    var $useTable = false;
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $hasOne = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'id',
            'conditions' => array('User.role' => 'admin'),
            'dependent' => true
        )
    );


}

