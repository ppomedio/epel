<?php

if ($user['User']['photo'])
    $photoPath = '/files/users/' . $this->Session->read('Auth.User.id') . '/photo/' . $user['User']['photo'];
else
    $photoPath = '/img/user.png';

?>

                    <div id="photo">
                        <h2><?php __("Picture"); ?></h2>
                        <div>
                            <div class="message"></div>
                            <div>
                                <div class="loading"></div>
                                <span id="photo-upload" class="upload"></span>
                                <span id="photo-delete" class="supp"></span>
                            </div>
                        </div>
                    </div>
                    <div id="data_photo-delete" class="tooltip-data"><?php __("Delete current image"); ?></div>
                    <div id="photo-delete-confirm" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php __("Do you really want to delete this image?"); ?></div>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ajaxupload',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

$this->Js->Buffer("
    // Affichage de la photo
    var img = '" . $photoPath . "';
    if (img == '/img/user.png')
        $('aside #photo > div').height(170);
    else {
        $('aside #photo > div').height(parseInt(img.substring(img.lastIndexOf('-') + 1, img.lastIndexOf('.'))) + 20);
        $('aside #photo > div > div > span#photo-delete').show();
    }
    $('aside #photo > div').css('background-image', 'url(" . $photoPath . ")');
    //$('aside #photo').show();

    // Tooltips
    tooltipSupp('#photo');

    // Téléchargement de la photo
    new AjaxUpload('#photo-upload', {
        action: '/" . $this->params['language'] . "/user/photo_upload',
        name: 'data[User][photo]',
        autoSubmit: true,
        responseType: false,
        tooltipId: '" . addslashes(__("Upload a new image", true)) . "',
        onChange: function(file, extension){},
        onSubmit: function(file, extension) {
            $('aside #photo > div > div > div.loading').show();
            $('#divToolTip').fadeOut('fast');
        },
        onComplete: function(file, response) {
            var extract = new Array();
            var resp = new Array();
            extract = response.split('|response|');
            resp = extract[1].split('=');
            $('aside #photo > div > div > div.loading').hide();
            if (resp[0] === 'uploaded') {
                $('aside #photo > div')
                    .height(parseInt(resp[1].substring(resp[1].lastIndexOf('-') + 1, resp[1].lastIndexOf('.'))) + 20)
                    .css('background-image', 'url(' + resp[1] + ')');
                $('aside #photo > div > div > span#photo-delete').show();
            } else if (resp[0] === 'error') {
                $('aside #photo > div > div.message').text(resp[1]).show().delay(3600).hide('slow');
            }
        }
        });

    // Suppression de la photo
    $('aside #photo > div div span#photo-delete').bind('click', function() {
        $('#photo-delete-confirm').dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Delete', true) . "': function() {
                    $(this).dialog('close');
                    $('aside #photo > div > div > div.loading').show();
                    $.get('/" . $this->params['language'] . "/user/photo_delete');
                    $('aside #photo > div').height(170).css('background-image', 'url(/img/user.png)');
                    $('aside #photo > div > div > div.loading').hide();
                    $('aside #photo > div div span#photo-delete').hide();
                },
                '" . __('Cancel', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });

    });
");

?>