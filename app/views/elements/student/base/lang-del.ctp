
                    <section id="language-delete">
                        <h2><?php __("Delete a language"); ?></h2>
                        <?php echo $this->Form->create('Portfolio', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/language/delete',
                            'id' => 'language-delete-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Portfolio'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('language_id', array(
                            'label' => __("Select a language", true),
                            'options' => $currentLanguages,
                            'empty' => "…"
                            ));
                        ?>

                        <p class="alert"><?php  __("Deleting a language in your Portfolio will delete any related information, including uploaded material.");  ?></p>
                        <dl>
                            <dt><?php echo $this->Form->label('delete_confirm', __("Confirmation", true)); ?></dt>
                            <dd class="master"><?php echo $this->Form->checkbox('delete_confirm'); ?></dd>
                        </dl>
                        </fieldset>
                        <?php echo $this->Form->end(__("Delete", true)); ?>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

// iButton -> buffer Js
$this->Js->Buffer("
    // iButton
    $('#language-delete-form fieldset dl dd input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            if ($(this).is(':checked')) {
                if ($('#PortfolioLanguageId option:selected').val() != '')
                    $('#language-delete-form input[type=submit]').removeAttr('disabled');
            } else
                $('#language-delete-form input[type=submit]').attr('disabled', 'disabled');
        });
    // Alert tips
    addTipAlert('#language-delete');
    // Raz des contrôles
    function resetControls(){
        $('#PortfolioDeleteConfirm').iButton('toggle', false);
        $('#PortfolioDeleteConfirm').iButton('disable', true);
        $('#PortfolioLanguageId option:selected').attr('selected', '');
        $('#language-delete-form div.submit input[type=submit]').attr('disabled', 'disabled');
    };
    // État initial
    resetControls();
    // Désactivation si modification de la langue
    $('#PortfolioLanguageId').change(function() {
        $('#PortfolioDeleteConfirm').iButton('toggle', false);
        if ($(this).val() == '') {
            $('#PortfolioDeleteConfirm').iButton('disable', true);
            $('#language-delete-form input[type=submit]').attr('disabled', 'disabled');
        } else
            $('#PortfolioDeleteConfirm').iButton('disable', false);
    });
");

?>