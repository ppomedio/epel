<?php

/**
 * {app}/models/po_objective.php
 */
class PoObjective extends AppModel {

    var $name = 'PoObjective';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'description' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please define objective.", true)
            )
        );

        parent::__construct();

    }


}

