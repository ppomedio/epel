
                <article>
                    <h1><?php
                    echo $title_for_layout;
                    $languageInfo = '';
                    if ($mother)
                        $languageInfo = __d('portfolio', "Mother tongue", true);
                    if ($usual) {
                        if ($languageInfo)
                            $languageInfo .= ' / ';
                        $languageInfo .= __d('portfolio', "Usual language", true);
                    }
                    if ($languageInfo)
                        echo ' <span class="smaller">(' . $languageInfo . ')</span>';
                    ?></h1><?php
                    // Compteur de parties affichées
                    $parts = 0;
                    /**
                     * PoDocument (show)
                     */
                    if (!empty($user['PoDocument'])) {
                        $parts .= 1;
                    ?>
                    <section>
                        <h2><?php __d('portfolio', "Display"); ?></h2>
                        <div id="show-view" class="table">
                            <table class="dossier document view">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "File name"); ?></th>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th class="competency"><span><?php __d('portfolio', "Description / Certified competency"); ?></span></th>
                                        <th class="controls"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoDocument'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td class="bolder"><?php echo $value['filename']; ?></td>
                                        <td><span class="picto-<?php echo $value['type']; ?>"></span></td>
                                        <td><?php
                                        if ($value['description'])
                                            echo nl2br($value['description']);
                                        else
                                            __d('portfolio', "Description to come…");
                                        if ($value['competency'])
                                            echo '<br /><span class="smaller infotext">' . $compNames[$value['competency']] . '</span>';
                                        ?></td>
                                        <td class="controls">
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="show-download_<?php echo $value['id']; ?>" class="download link"></span>
                                            </div>
                                        </td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="file hidden"><?php
                                            echo $value['file'];
                                            ?></span>
                                            <span class="filename hidden"><?php
                                            echo $value['filename'];
                                            ?></span>
                                        </div>
                                    </tr>
                                    <div id="data_show-download_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        echo __d('portfolio', "Download", true) . " " . $value['filename'];
                                    ?></div>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <div id="data_show-competency" class="tooltip-data"><?php __d('portfolio', "A document from the Dossier can certify a competency from the Passport: in this case the competency appears in blue, below the description."); ?></div>
                        </div>
                    </section><?php
                    }
                    /**
                     * PoCertificate
                     */
                    if (!empty($user['PoCertificate'])) {
                        $parts .= 1;
                    ?>

                    <section><?php
                        echo $this->element('view/certificate'); ?>

                    </section><?php
                    }
                    /**
                     * PoDocument (work)
                     */
                    if (!empty($user['PoWorkDocument'])) {
                        $parts .= 1;
                    ?>
                    <section>
                        <h2><?php __d('portfolio', "Process"); ?></h2>
                        <div id="show-view" class="table">
                            <table class="dossier document view">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "File name"); ?></th>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoWorkDocument'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td class="bolder"><?php echo $value['filename']; ?></td>
                                        <td><span class="picto-<?php echo $value['type']; ?>"></span></td>
                                        <td><?php
                                        if ($value['description'])
                                            echo nl2br($value['description']);
                                        else
                                            __d('portfolio', "Description to come…");
                                        ?></td>
                                        <td class="controls">
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="show-download_<?php echo $value['id']; ?>" class="download link"></span>
                                            </div>
                                        </td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="file hidden"><?php
                                            echo $value['file'];
                                            ?></span>
                                            <span class="filename hidden"><?php
                                            echo $value['filename'];
                                            ?></span>
                                        </div>
                                    </tr>
                                    <div id="data_show-download_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        echo __d('portfolio', "Download", true) . " " . $value['filename'];
                                    ?></div>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </section><?php
                    }
                    /**
                     * Vide…
                     */
                    if (!$parts) { ?>

                    <section>
                        <div id="biography-empty">
                            <p class="alert"><?php __d('portfolio', "This student's Dossier is empty."); ?></p>
                        </div>
                    </section><?php
                    } ?>

                </article>
                <aside><?php
                    echo $this->element('view/student'); ?>
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="search-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="download"></dt>
                                <dd><?php __("Download attached file"); ?></dd>
                            </dl>
                        </div>
                    </div><?php
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Id, classes et cie
    $('#show-view table thead tr th.competency span').attr('id', 'show-competency').addClass('help');
    // Tips
    addTipInfo('#search-info');
    // Tooltips
    tooltipHelp('#show-view table thead');
    // Téléchargement d'un fichier
    $('#show-view table tbody td span.download').bind('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $(location).attr('href', '/" . $this->params['language'] . "/user/download/" . $this->Session->read('Auth.User.view.id') . "/' + $('#hidden-info_' + itemId + ' .file').text() + '/' + $('#hidden-info_' + itemId + ' .filename').text());
        return false;
    });
");

?>