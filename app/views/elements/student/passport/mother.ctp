
                    <section class="half left">
                        <h2><?php __d('portfolio', "Mother tongue / Usual language"); ?></h2>
                        <?php echo $this->Form->create('Portfolio', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/passport/ajax_save/' . urlencode($language),
                            'id' => 'mother-form',
                            'class' => 'white'
                            ));
                        ?>
                        <p><?php __d('portfolio', "Is this language your mother tongue (or one of them)?"); ?></p>
                        <fieldset>
                            <?php echo $this->Form->checkbox('mother'); ?>

                        </fieldset>
                        <p><?php __d('portfolio', "Is this language your usual language (or one of them)?"); ?></p>
                        <fieldset>
                            <?php echo $this->Form->checkbox('usual'); ?>

                        </fieldset>
                        <div class="tail"><div class="loading"></div></div>
                        <?php echo $this->Form->end(); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Bascule langue maternelle
    $('#mother-form fieldset input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            $('#mother-form').ajaxSubmit({
                beforeSubmit: function() {
                    // Loading…
                    $('section form div.tail div.loading').show();
                },
                success: function() {
                    // Loading…
                    $('section form div.tail div.loading').hide();
                }
            })
        })
");

?>