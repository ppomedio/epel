<?php

/**
 * {app}/models/po_howtolearn.php
 */
class PoHowtolearn extends AppModel {

    var $name = 'PoHowtolearn';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'id'
        )
    );


    /**
     *
     *
     * @param <string> $id      ID étudiant
     * @param <string> $part    partie de la liste à charger
     * @return <array>
     */
    function getHowtolearn($id, $part = false) {

        switch ($part) {
            case 1:
                $fields = array(
                    '101answer', '101target', '101date',
                    '102answer', '102target', '102date',
                    '103answer', '103target', '103date',
                    '104answer', '104target', '104date',
                    '105answer', '105target', '105date',
                );
                break;
            case 2:
                $fields = array(
                    '201answer', '201target', '201date',
                    '202answer', '202target', '202date',
                    '203answer', '203target', '203date',
                    '204answer', '204target', '204date',
                    '205answer', '205target', '205date',
                    '206answer', '206target', '206date',
                    '207answer', '207target', '207date',
                    '208answer', '208target', '208date',
                    '209answer', '209target', '209date',
                );
                break;
            case 3:
                $fields = array(
                    '301answer', '301target', '301date',
                    '302answer', '302target', '302date',
                    '303answer', '303target', '303date',
                    '304answer', '304target', '304date',
                    '305answer', '305target', '305date',
                    '306answer', '306target', '306date',
                    '307answer', '307target', '307date',
                    '308answer', '308target', '308date',
                );
                break;
            case 4:
                $fields = array(
                    '401answer', '401target', '401date',
                    '402answer', '402target', '402date',
                    '403answer', '403target', '403date',
                    '404answer', '404target', '404date',
                );
                break;
            case 5:
                $fields = array('other');
                break;
            default:
                $fields = array();
        }

        return $this->find('first', array(
            'conditions' => array('PoHowtolearn.id' => $id),
            'fields' => $fields,
            'recursive' => -1
        ));

    }


}

