<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#elements-list',
    'before' => $this->Js->get('#elements-list')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="elements-list" class="table"><?php
                            if (empty($data)) { ?>

                            <table>
                                <thead>
                                    <tr class="smaller">
                                        <th><?php __("ID"); ?></th>
                                        <th><?php __("Certification"); ?></th>
                                        <th><?php __("Language"); ?></th>
                                        <th class="controls">
                                            <span id="element-add" class="add"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="4"><?php
                                        __("The list is empty.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr class="smaller">
                                        <th class="id"><?php echo $paginator->sort(__("ID", true), 'Certificate.id', array('url' => $this->params['pass'])); ?></th>
                                        <th class="certification"><?php echo $paginator->sort(__("Certification", true), 'Certification.name', array('url' => $this->params['pass'])); ?></th>
                                        <th class="language"><?php echo $paginator->sort(__("Language", true), 'Language.name', array('url' => $this->params['pass'])); ?></th>
                                        <th class="controls">
                                            <span id="element-add" class="add"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($data as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>

                                    <tr class="<?php echo $trClass; ?>"><?php
                                        if ($value['Certificate']['used']) { ?>

                                        <td class="alerttext"><?php
                                        } else { ?>

                                        <td><?php
                                        }
                                        echo $value['Certificate']['id']; ?></td>
                                        <td id="edit-certification_<?php echo $value['Certificate']['id']; ?>"><?php echo $value['Certification']['name']; ?></td>
                                        <td id="edit-language_<?php echo $value['Certificate']['id']; ?>"><?php
                                        #__d('language', $value['Language']['name']);
                                        echo $value['Language']['name'];
                                        ?></td>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['Certificate']['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['Certificate']['id']; ?>" class="controls"><?php
                                                if (!$value['Certificate']['used']) { ?>

                                                <span id="element-edit_<?php echo $value['Certificate']['id']; ?>" class="edit"></span>
                                                <span id="element-supp_<?php echo $value['Certificate']['id']; ?>" class="supp"></span><?php
                                                } ?>

                                            </div>
                                        </td>
                                    </tr>
                                     <div id="hidden-info_<?php echo $value['Certificate']['id']; ?>" class="hidden">
                                        <span class="description hidden"><?php
                                        echo $value['Certification']['description'];
                                        ?></span>
                                        <span class="certification_id hidden"><?php
                                        echo $value['Certification']['id'];
                                        ?></span>
                                        <span class="language_id hidden"><?php
                                        echo $value['Language']['id'];
                                        ?></span>
                                    </div>
                                    <div id="data_element-edit_<?php echo $value['Certificate']['id']; ?>" class="inline tooltip-data"><?php
                                    echo __("Edit", true) . ' “' . $value['Certification']['name'] . ' / ' . __d('language', $value['Language']['name'], true) . '”';
                                    ?></div>
                                    <div id="data_element-supp_<?php echo $value['Certificate']['id']; ?>" class="inline tooltip-data"><?php
                                    echo __("Delete", true) . ' “' . $value['Certification']['name'] . ' / ' . __d('language', $value['Language']['name'], true) . '”';
                                    ?></div>
                                    <div id="element-supp-confirm_<?php echo $value['Certificate']['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                    echo __("Please confirm delete", true) . ' “' . $value['Certification']['name'] . ' / ' . __d('language', $value['Language']['name'], true) . '”.';
                                    ?></div><?php
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'elements-list', 'half' => true));
                            } ?>

                            <div id="data_element-add" class="tooltip-data"><?php __("Erase form (add)"); ?></div>
                            <div id="data_sort-id" class="tooltip-data"><?php __("Sort by ID"); ?></div>
                            <div id="data_sort-certification" class="tooltip-data"><?php __("Sort by certification"); ?></div>
                            <div id="data_sort-language" class="tooltip-data"><?php __("Sort by language"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

// Systématiques
$this->Js->Buffer("
    // Id, classes et cie
    $('#elements-list table thead tr th.id a').attr('id', 'sort-id').addClass('sort-help');
    $('#elements-list table thead tr th.certification a').attr('id', 'sort-certification').addClass('sort-help');
    $('#elements-list table thead tr th.language a').attr('id', 'sort-language').addClass('sort-help');
    // Loading…
    $('#elements-list thead a').click(function() {
        $('#title-elements-list').html('" . __("Loading…", true) . "');
    });
    // Tooltips
    tooltipSortHelp('#elements-list table thead tr th');
    tooltipAdd('#elements-list table thead th.controls');
    tooltipEdit('#elements-list table tbody td.controls div.controls');
    tooltipSupp('#elements-list table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Initialisation du formulaire
    function initForm() {
        $('#CertificateId').val('');
        $('#CertificateCertificationId :selected').removeAttr('selected');
        $('#CertificateLanguageId :selected').removeAttr('selected');
    };
    // Ajout d'un élément
    $('#element-add').live('click', function() {
        // Titre
        $('#title-elements-form').text('" . addslashes(__("Add", true)) . "');
        // Réinitialisation du formulaire
        $('#elements-form div.error-message').remove();
        initForm();
    });
    // Édition d'un élément
    $('#elements-list table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        // Titre
        $('#title-elements-form').text('" . addslashes(__("Edit", true)) . "');
        // Initialisation du formulaire
        $('#elements-form div.error-message').remove();
        $('#CertificateId').val(itemId);
        $('#CertificateCertificationId option[value=\'' + $('#hidden-info_' + itemId + ' .certification_id').text() + '\']').attr('selected', 'selected');
        $('#CertificateCertificationDescription').val($('#hidden-info_' + itemId + ' .description').text());
        $('#CertificateLanguageId option[value=\'' + $('#hidden-info_' + itemId + ' .language_id').text() + '\']').attr('selected', 'selected');
    });
    // Maj du champ 'Description'
    $('#CertificateCertificationId').change(function() {
        $('#CertificateCertificationDescription').val(description[$(this).val()]);
    });
    // Suppression d'un élément
    $('#elements-list table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#element-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#elements-list').load('/" . $this->params['language'] . "/admin/element/delete/certificate/' + itemId, function() {
                            if ($('#elements-form #CertificationId').val() == itemId) {
                                $('#title-elements-form').text('" . addslashes(__("Add", true)) . "');
                                initForm();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__("The certificates list", true)) . " " . addslashes(__("has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
    });
");
} else {
    $this->Js->Buffer("
    // Tooltips
    tooltipSortHelp('#elements-list');
    // Rétablissement du titre
    $('#title-elements-list').html('" . addslashes(__("Certificates", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>