
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="visit-info" class="information">
                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                            <p class="info"><?php __("You can send email to visitors by clicking on their name."); ?></p>
                        </div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#visit-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#visit-info');
");
*/
?>
