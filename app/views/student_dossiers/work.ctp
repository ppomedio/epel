
                <article id="ongoing">
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-work"><?php __d('portfolio', "Process"); ?></h2>
                        <div class="instruction">
                            <p><?php echo __d('portfolio', "This part of your Dossier is where you keep", true) . ' ' . __d('portfolio', "material related to your current language learning process, e.g.:", true); ?></p>
                            <ul>
                                <li><?php __d('portfolio', "details of course requirements,"); ?></li>
                                <li><?php __d('portfolio', "lists of vocabulary you need to learn,"); ?></li>
                                <li><?php __d('portfolio', "newspaper or magazine cuttings relevant to project work in progress,"); ?></li>
                                <li><?php __d('portfolio', "drafts of various kinds."); ?></li>
                            </ul>
                            <p class="info"><?php echo __d('portfolio', "You can upload most of the common types of files (audio, video etc.) whose size does not exceed", true)
                                    . ' <span class="alerttext">' . strval(Configure::read('epel.document.size')) . '</span> ' . __d('portfolio', "MB. “Archive” file formats are allowed.", true)
                                    . ' ' . __d('portfolio', "Uploading may take time, especially for a large file and depending on the quality of your connection. Please wait until your new file appears on the list below.", true); ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="work-edit" class="hidden">
                        <h2 id="title-work-edit"><?php __d('portfolio', "Edit a document"); ?></h2>
                        <?php echo $this->Form->create('PoDocument', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/dossier/work/' . urlencode($language),
                            'id' => 'work-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoDocument'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('filename', array(
                            'label' => __d('portfolio', "File name", true),
                            'readonly'=>'readonly'
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/dossier/work-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'dossier', 'never' => true));
                    echo $this->element('student/dossier/side-menu');
                    echo $this->element('student/dossier/how-to-use_work', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ajaxupload',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#work-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#ongoing');
    // Soumission Ajax du formulaire
    $('#work-edit-form').submit(function() {
        if (!$('#work-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/dossier/update/" . urlencode($language) . "/work',
                target: '#work',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#work-edit-form div.loading').show();
                },
                success: function() {
                    $('#work-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#work-toggle').hide();
                    $('#work-add').fadeIn('slow');
                    // Initialisation du formulaire
                    $('#work-edit-form #PoDocumentId').val('');
                    $('#work-edit-form #PoDocumentFilename').val('');
                    $('#work-edit-form #PoDocumentDescription').val('');
                    $('#work-edit-form div.loading').hide();
                    $('#work-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>