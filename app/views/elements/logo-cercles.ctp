
                    <div id="logos">
                        <a href="<?php echo Configure::read('epel.cercles.url'); ?>" id="<?php __("Visit CercleS's website"); ?>" class="link">
                            <div class="logo" id="cercleslogo"></div>
                        </a>
                        <a href="<?php echo Configure::read('epel.coe.url'); ?>" id="<?php __("Visit the website of the Council of Europe"); ?>" class="link">
                            <div class="logo" id="coelogo"></div>
                        </a>
                    </div>