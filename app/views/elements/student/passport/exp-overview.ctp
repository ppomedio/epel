
                    <section class="half left">
                        <h2><?php __d('portfolio', "Experience (overview)"); ?></h2>
                        <?php echo $this->Form->create('Portfolio', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/passport/edit/' . urlencode($language),
                            'id' => 'exp-overview-form',
                            'class' => ''
                            ));
                        ?>
                        <fieldset>
                            <legend id="language-studying" class="help"><?php __d('portfolio', "Language courses"); ?></legend>
                            <?php
                            echo $this->Form->input('study_pri', array(
                                'label' => __d('portfolio', "Primary / secondary education", true),
                                'options' => $schoolStudyPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('study_sup', array(
                                'label' => __d('portfolio', "Higher education", true),
                                'options' => $schoolStudyPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('study_oth', array(
                                'label' => __d('portfolio', "Other courses", true),
                                'options' => $otherStudyPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('study_sta', array(
                                'label' => __d('portfolio', "Langue study holidays / trips", true),
                                'options' => $otherStudyPeriods,
                                'empty' => "…"
                                ));
                            ?>
                        </fieldset>
                        <fieldset>
                            <legend id="regular-activities" class="help"><?php __d('portfolio', "Regular activities"); ?></legend>
                            <?php
                            echo $this->Form->input('activ_lis', array(
                                'label' => __d('portfolio', "Listening (radio, TV…)", true),
                                'options' => $activityPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('activ_spe', array(
                                'label' => __d('portfolio', "Talking", true),
                                'options' => $activityPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('activ_rea', array(
                                'label' => __d('portfolio', "Reading (newspapers…)", true),
                                'options' => $activityPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('activ_pho', array(
                                'label' => __d('portfolio', "Telephone contacts", true),
                                'options' => $activityPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('activ_wri', array(
                                'label' => __d('portfolio', "Written messages (emails, letters, etc.)", true),
                                'options' => $activityPeriods,
                                'empty' => "…"
                                ));
                            echo $this->Form->input('activ_oth', array(
                                'label' => __d('portfolio', "Other activities (theatre, video games, etc.)", true),
                                'options' => $activityPeriods,
                                'empty' => "…"
                                ));
                            ?>
                        </fieldset>
                        <?php echo $this->Form->end(__("Update", true)); ?>

                        <div id="data_language-studying" class="tooltip-data">
                            <?php
                            __d('portfolio', "In the following lists, the higher value is included in the time period:");
                            echo ' ';
                            __d('portfolio', "if you have a three-week experience, select the “Between 2 and 3 weeks” option.");
                            ?></div>
                        <div id="data_regular-activities" class="tooltip-data">
                            <?php
                            __d('portfolio', "In the following lists, the higher value is included in the time period:");
                            echo ' ';
                            __d('portfolio', "if you practice an activity five hours a month, select the “Between 2 and 5 hours a month” option.");
                            ?></div>
                    </section>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#exp-overview-form div.submit input[type=submit]');
    // Activation du submit
    $('#exp-overview-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#exp-overview-form select').change(function() {
        if ($('#exp-overview-form div.submit input[type=submit]').attr('disabled'))
            $('#exp-overview-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#exp-overview-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/passport/ajax_save/" . urlencode($language) . "',
        beforeSubmit: function() {
            $('#exp-overview-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#exp-overview-form div.loading').show();
        },
        success: function() {
            $('#exp-overview-form div.loading').hide();
            //$('#exp-overview-form div.submit input[type=submit]').removeAttr('disabled');
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Passport has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

?>