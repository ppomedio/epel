
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="base-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="upload"></dt>
                                <dd><?php __("Upload a new image"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Delete"); ?></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#base-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#base-info');
");
*/
?>
