<?php

/**
 * {app}/controllers/components/format_input.php
 *
 */
class FormatInputComponent extends Object {


    /**
     * Première lettre du premier mot en capitale
     *
     * @param string $data
     * @return string
     */
    function capFirst($text) {

        return ucfirst(strtolower(trim($text)));

    }


    /**
     * Formatage d'un nom
     *
     * @param string $name
     * @return string
     */
    function formatName($name) {

        $particules = array(
            'De', 'Du',
            'Of',
            'An', 'Auf', 'Von', 'Der', 'Zu',
            'Del', 'Las', 'Les', 'Los', 'Y',
            'A', 'Da', 'Das', 'Dos',
            'Af', 'Av',
        );

        $name = preg_replace('/\s+/', ' ', $name);

        if (strpos($name, '-')) {
            $data = explode('-', $name);
            $step1 = ucfirst(strtolower(trim($data[0])));
            unset($data[0]);
            foreach ($data as $key => $value) {
                $step1 .= '-' . ucfirst(strtolower($value));
            }
        } else
            $step1 = ucwords(strtolower($name));

        if (strpos($step1, ' ')) {
            $data = explode(' ', $step1);
            $step2 = '';
            foreach ($data as $key => $value) {
                if ((strlen($value) < 4) && in_array($value, $particules))
                    $step2 .= ' ' . strtolower($value);
                else
                    $step2 .= ' ' . $value;
            }
            $formatName = trim($step2);
        } else
            $formatName = $step1;

        return $formatName;

    }


    /**
     * Formatage d'un n° de téléphone
     *
     * @param string $phoneNumber
     * @return string
     */
    function formatPhone($phoneNumber) {

        $tel = preg_replace('/[-.,\s]/', ' ', $phoneNumber);
        #if(strlen($tel) == 10)
        #    $tel = substr($tel, 0, 2)
        #        . ' ' . substr($tel, 2, 2)
        #        . ' ' . substr($tel, 4, 2)
        #        . ' ' . substr($tel, 6, 2)
        #        . ' ' . substr($tel, 8, 2);

        return $tel;

    }


}

