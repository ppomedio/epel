
                <article>
                    <h1><?php
                    echo $title_for_layout;
                    $languageInfo = '';
                    if ($mother)
                        $languageInfo = __d('portfolio', "Mother tongue", true);
                    if ($usual) {
                        if ($languageInfo)
                            $languageInfo .= ' / ';
                        $languageInfo .= __d('portfolio', "Usual language", true);
                    }
                    if ($languageInfo)
                        echo ' <span class="smaller">(' . $languageInfo . ')</span>';
                    ?></h1><?php
                    // Compteur de parties affichées
                    $parts = 0;
                    /**
                     * PoValuable
                     */
                    if (!empty($user['PoValuable'])) {
                        $parts .= 1;
                    ?>

                    <section><?php
                        echo $this->element('view/valuable'); ?>

                    </section><?php
                    }
                    /**
                     * PoContact
                     */
                    if (!empty($user['PoContact'])) {
                        $parts .= 1;
                    ?>

                    <section>
                        <h2><?php __d('portfolio', "My contacts with speakers of the language"); ?></h2>
                        <div class="table">
                            <table class="typedesc">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Context"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoContact'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo nl2br($value['description']); ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </section><?php
                    }
                    /**
                     * PoAbroad
                     */
                    if (!empty($user['PoAbroad'])) {
                        $parts .= 1;
                    ?>

                    <section>
                        <h2><?php __d('portfolio', "My stays abroad"); ?></h2>
                        <div class="table">
                            <table class="descdatelength">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th><?php __d('portfolio', "Date"); ?></th>
                                        <th><?php __d('portfolio', "Duration"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoAbroad'] as $value) {
                                        $itemDate = $this->Date->Convert($value['date']);
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo nl2br($value['description']); ?></td>
                                        <td><?php echo $this->Date->Convert($value['date']); ?></td>
                                        <td><?php echo $value['duration'] . '&nbsp;' . $durations[$value['unit']]; ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </section><?php
                    }
                    /**
                     * PoCultural
                     */
                    if (!empty($user['PoCultural'])) {
                        $parts .= 1;
                    ?>

                    <section>
                        <h2><?php __d('portfolio', "My experience and knowledge of the culture"); ?></h2>
                        <div class="table">
                            <table class="typedesc">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoCultural'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo nl2br($value['description']); ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </section><?php
                    }
                    /**
                     * PoEmployment
                     */
                    if (!empty($user['PoEmployment'])) {
                        $parts .= 1;
                    ?>

                    <section>
                        <h2><?php __d('portfolio', "Competencies which improve my employment prospects"); ?></h2>
                        <div class="table">
                            <table class="typedesc">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoEmployment'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo nl2br($value['description']); ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </section><?php
                    }
                    /**
                     * Vide…
                     */
                    if (!$parts) { ?>

                    <section>
                        <div id="biography-empty">
                            <p class="alert"><?php __d('portfolio', "This student's Biography is empty."); ?></p>
                        </div>
                    </section><?php
                    } ?>

                </article>
                <aside><?php
                    echo $this->element('view/student');
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>