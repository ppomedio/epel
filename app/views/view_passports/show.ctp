
                <article>
                    <h1><?php
                    echo $title_for_layout;
                    $languageInfo = '';
                    if ($user['Portfolio']['mother'])
                        $languageInfo = __d('portfolio', "Mother tongue", true);
                    if ($user['Portfolio']['usual']) {
                        if ($languageInfo)
                            $languageInfo .= ' / ';
                        $languageInfo .= __d('portfolio', "Usual language", true);
                    }
                    if ($languageInfo)
                        echo ' <span class="smaller">(' . $languageInfo . ')</span>';
                    ?></h1>
                    <section id="experience">
                        <h2><?php __d('portfolio', "Experience (overview)"); ?></h2>
                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="2"><?php __d('portfolio', "Language courses"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php __d('portfolio', "Primary / secondary education"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['study_pri'])
                                               echo $schoolStudyPeriods[$user['Portfolio']['study_pri']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Higher education"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['study_sup'])
                                               echo $schoolStudyPeriods[$user['Portfolio']['study_sup']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Other courses"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['study_oth'])
                                               echo $otherStudyPeriods[$user['Portfolio']['study_oth']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Langue study holidays / trips"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['study_sta'])
                                               echo $otherStudyPeriods[$user['Portfolio']['study_sta']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="2"><?php __d('portfolio', "Regular activities"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php __d('portfolio', "Listening (radio, TV…)"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['activ_lis'])
                                               echo $activityPeriods[$user['Portfolio']['activ_lis']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Talking"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['activ_spe'])
                                               echo $activityPeriods[$user['Portfolio']['activ_spe']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Reading (newspapers…)"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['activ_rea'])
                                               echo $activityPeriods[$user['Portfolio']['activ_rea']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Telephone contacts"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['activ_pho'])
                                               echo $activityPeriods[$user['Portfolio']['activ_pho']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Written messages (emails, letters, etc.)"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['activ_wri'])
                                               echo $activityPeriods[$user['Portfolio']['activ_wri']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php __d('portfolio', "Other activities (theatre, video games, etc.)"); ?></td>
                                        <td><?php
                                           if ($user['Portfolio']['activ_oth'])
                                               echo $activityPeriods[$user['Portfolio']['activ_oth']];
                                           else
                                               echo '…';
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="assessment">
                        <h2><?php __d('portfolio', "Self-assessment"); ?></h2>
                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Competency"); ?></th>
                                        <th><span id="<?php __d('portfolio', "Levels displayed in orange are partially reached"); ?>" class="help"><?php __d('portfolio', "Level"); ?></span></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <?php
                                        if (!isset($noDossier)) { ?>

                                        <th><span id="<?php __d('portfolio', "Dossier documents certifying the competencies"); ?>" class="help"><?php __d('portfolio', "Documents"); ?></span></th>
                                        <?php } ?>

                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($competencies as $compKey => $compName) {
                                        // Bascule des couleurs de lignes
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        // Préparation
                                        if (!empty($user['Portfolio'][$compKey])) {
                                            // Compétences standard
                                            $level = $levels[$user['Portfolio'][$compKey]];
                                            $levelDescription = __d('levels_grid', $levelsDescriptions[$compKey][$levels[$user['Portfolio'][$compKey]]], true);
                                            if (!empty($user['Portfolio'][$compKey . '_part'])) {
                                                // Compétences partielles
                                                $levelDescription .= '<br /><em><span class="infotext bolder">' . __d('portfolio', "Also", true) . __(":", true) . "</span>" . $user['Portfolio'][$compKey . '_part'] . '</em>';
                                            }
                                            // Documents attestant cette compétence
                                            if (!isset($noDossier)) {
                                                if ($levelDocs[$compKey]['value']) {
                                                    $levelDocuments = '<br /><span class="infotext"><span class="bolder">' . $levelDocs[$compKey]['value'] . '</span> ';
                                                    if ($levelDocs[$compKey]['value'] > 1)
                                                        $levelDocuments .= __d('portfolio', "document(s)", true) . '</span>';
                                                    else
                                                        $levelDocuments .= __d('portfolio', "document", true) . '</span>';
                                                } else
                                                    $levelDocuments = "<br />" . __d('portfolio', "No document", true);
                                            }
                                        } else {
                                            // Compétence non définie
                                            $level = '<span class="smaller">' . __("Undefined", true) . '</span>';
                                            $levelDescription = '';
                                            $levelDocuments = '';
                                        } ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $compName; ?><span class="picto-<?php echo $compKey; ?>"></span></td>
                                        <td><?php echo $level; ?></td>
                                        <td><?php echo $levelDescription; ?></td>
                                        <?php
                                        if (!isset($noDossier)) { ?>

                                        <td><?php echo $levelDocuments; ?></td><?php
                                        } ?>

                                    </tr><?php
                                    } ?>

                                </tbody>
                            </table>
                        </div>
                    </section><?php
                    if (!isset($noDossier) && !empty($user['PoCertificate'])) { ?>

                    <section><?php
                        echo $this->element('view/certificate'); ?>
                    </section><?php
                    }
                    if (!isset($noBiography) && !empty($user['PoValuable'])) { ?>

                    <section><?php
                        echo $this->element('view/valuable'); ?>

                    </section><?php
                    } ?>

                </article>
                <aside><?php
                    echo $this->element('view/student');
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipHelp('#assessment thead');
");

?>