<?php

$paginator->options(array(
    'update' => '#show',
    ));

?>

                        <div id="show" class="table"><?php
                            if (empty($user)) { ?>

                            <table class="dossier document">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "File name"); ?></th>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description / Certified competency"); ?></th>
                                        <th class="controls">
                                            <div id="show-loading" class="loading"></div>
                                            <div id="head-control">
                                                <span id="show-add" class="add"></span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="5">
                                             <p class="alert"><?php
                                             __d('portfolio', "Your Dossier currently contains no display material for this language.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#show');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table class="dossier document">
                                <thead>
                                    <tr>
                                        <th class="filename"><?php echo $paginator->sort(__d('portfolio', "File name", true), 'PoDocument.filename', array('url' => $this->params['pass'])); ?></th>
                                        <th class="type"><?php echo $paginator->sort(__d('portfolio', "Type", true), 'PoDocument.type', array('url' => $this->params['pass'])); ?></th>
                                        <th class="competency"><?php echo $paginator->sort(__d('portfolio', "Description / Certified competency", true), 'PoDocument.competency', array('url' => $this->params['pass'])); ?></th>
                                        <th class="controls">
                                            <div id="show-loading" class="loading"></div>
                                            <div id="head-control">
                                                <span id="show-add" class="add"></span>
                                                <span id="show-toggle" class="gear"></span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoDocument'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="show-filename_<?php echo $value['id']; ?>"><?php
                                        echo $value['filename'] . '<br /><p class="xsmaller">' . __d('portfolio', "Last update", true) . __(":", true) . '<br /><span class="infotext">' . $this->Date->Convert($value['modified']) . '</span></p>';
                                        ?></td>
                                        <td id="show-type_<?php echo $value['id']; ?>"><span class="picto-<?php echo $value['type']; ?>"></span></td>
                                        <td id="show-description_<?php echo $value['id']; ?>"><?php
                                        if ($value['description'])
                                            echo nl2br($value['description']);
                                        else
                                            __d('portfolio', "Description to come…");
                                        if ($value['competency'])
                                            echo '<br /><span class="smaller infotext">' . $competencies[$value['competency']] . '</span>';
                                        ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="file hidden"><?php
                                            echo $value['file'];
                                            ?></span>
                                            <span class="filename hidden"><?php
                                            echo $value['filename'];
                                            ?></span>
                                            <span class="competency hidden"><?php
                                            echo $value['competency'];
                                            ?></span>
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="show-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="show-replace_<?php echo $value['id']; ?>" class="upload"></span>
                                                <span id="show-move_<?php echo $value['id']; ?>" class="move link"></span>
                                                <span id="show-download_<?php echo $value['id']; ?>" class="download link"></span>
                                                <span id="show-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_show-download_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        echo __d('portfolio', "Download", true) . " " . $value['filename'];
                                    ?></div>
                                    <div id="data_show-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this document");
                                    ?></div>
                                    <div id="data_show-move_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        echo __d('portfolio', "Move this document to the “Process” part.", true);
                                    ?></div>
                                    <div id="data_show-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this document");
                                    ?></div>
                                    <div id="show-move-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        echo __d('portfolio', "Please confirm move document to the “Process” part", true) . ' (' . $value['filename'] . ').';
                                    ?></div>
                                    <div id="show-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete document"); echo ' (' . $value['filename'] . ').';
                                    ?></div>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'show'));
                            } ?>

                            <div id="data_sort-show-filename" class="tooltip-data"><?php __("Sort by file name"); ?></div>
                            <div id="data_sort-show-type" class="tooltip-data"><?php __("Sort by type"); ?></div>
                            <div id="data_sort-show-competency" class="tooltip-data"><?php __d('portfolio', "Sort by certified competency"); ?></div>
                            <div id="data_show-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial panneau d'édition & bascule
    if ($('#show-edit-form #PoDocumentId').val() == '') {
        $('#show-toggle').hide();
        $('#show-edit').slideUp('slow');
    } else if ($('#show-edit').is(':hidden'))
        $('#show-toggle').hide();
    // Id, classes et cie
    $('#show table thead tr th.filename a').attr('id', 'sort-show-filename').addClass('sort-help');
    $('#show table thead tr th.type a').attr('id', 'sort-show-type').addClass('sort-help');
    $('#show table thead tr th.competency a').attr('id', 'sort-show-competency').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#show table thead');
    tooltipGear('#show table thead tr th.controls');
    tooltipLink('#show table tbody tr td');
    tooltipEdit('#show table tbody tr td.controls div.controls');
    tooltipSupp('#show table tbody tr td.controls div.controls');
    // Déplacement d'un document
    $('#show table tbody td.controls span.move').unbind('click').bind('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#show-move-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    $(this).dialog('close');
                    $('#control_' + itemId).hide();
                    $('#loading_' + itemId).show();
                    if ($('#show-edit-form #PoDocumentId').val() == itemId) {
                        // Initialisation du formulaire
                        $('#show-edit-form #PoDocumentId').val('');
                        $('#show-edit-form #PoDocumentFilename').val('');
                        $('#show-edit-form #PoDocumentCompetency :selected').removeAttr('selected');
                        $('#show-edit-form #PoDocumentDescription').val('');
                    }
                    $('#show').load('/" . $this->params['language'] . "/student/dossier/move/" . urlencode($language) . "/show/work/' + itemId, function() {
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
        return false;
    });
    // Remplacement de document
    $('#show table tbody td.controls span.upload').each(function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        new AjaxUpload($(this), {
            action: '/" . $this->params['language'] . "/student/dossier/upload/" . urlencode($language) . "',
            name: 'data[PoDocument][filename]',
            autoSubmit: true,
            responseType: false,
            tooltipId: '" . addslashes(__d('portfolio', "Replace file", true)) . ' ' ."' + $('#hidden-info_' + itemId + ' .filename').text(),
            onChange: function(file, extension){},
            onSubmit: function(file, extension) {
                this.setData({kind : 'show', id : itemId, file : $('#hidden-info_' + itemId + ' .file').text()});
                $('#divToolTip').fadeOut('fast');
                $('#control_' + itemId).hide();
                $('#loading_' + itemId).show();
                $('#title-show').html('" . addslashes(__("Uploading…", true)) . "');
            },
            onComplete: function(file, response) {
                var extract = new Array();
                var resp = new Array();
                extract = response.split('|response|');
                resp = extract[1].split('=');
                if (resp[0] === 'error') {
                    $('#title-show').html('" . addslashes(__d('portfolio', "Display", true)) . "');
                    $('#loading_' + itemId).hide();
                    $('#control_' + itemId).show();
                    $('#content div#ajaxMessage')
                        .text(resp[1])
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                } else {
                    $('#show').load('/" . $this->params['language'] . "/student/dossier/show/" . urlencode($language) . "', function() {
                        if ($('#show-edit-form #PoDocumentId').val() == itemId)
                            $('#show-edit-form #PoDocumentFilename').val(resp[1]);
                        $('#loading_' + itemId).hide();
                        $('#control_' + itemId).show();
                        $('#content div#ajaxMessage')
                            .text('" . __d('portfolio', "Your Dossier has been updated.", true) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                }
            }
        });
    });
    // Téléchargement d'un fichier
    $('#show table tbody td span.download').unbind('click').bind('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $(location).attr('href', '/" . $this->params['language'] . "/user/download/" . $this->Session->read('Auth.User.id') . "/' + $('#hidden-info_' + itemId + ' .file').text() + '/' + $('#hidden-info_' + itemId + ' .filename').text());
        return false;
    });
    // Téléversement d'un fichier
    new AjaxUpload('#show-add', {
        action: '/" . $this->params['language'] . "/student/dossier/upload/" . urlencode($language) . "',
        name: 'data[PoDocument][filename]',
        autoSubmit: true,
        responseType: false,
        tooltipId: '" . addslashes(__d('portfolio', "Add a new document", true)) . "',
        onChange: function(file, extension){},
        onSubmit: function(file, extension) {
            this.setData({kind : 'show'});
            $('#divToolTip').fadeOut('fast');
            $('#head-control').hide();
            $('#show-loading').show();
            $('#title-show').html('" . addslashes(__("Uploading…", true)) . "');
        },
        onComplete: function(file, response) {
            var extract = new Array();
            var resp = new Array();
            extract = response.split('|response|');
            resp = extract[1].split('=');
            if (resp[0] === 'error') {
                $('#title-show').html('" . addslashes(__d('portfolio', "Display", true)) . "');
                $('#show-loading').hide();
                $('#head-control').show();
                $('#content div#ajaxMessage')
                    .text(resp[1])
                    .show(function(){
                        $(this).delay(3600).hide('slow');
                });
            } else {
                $('#show').load('/" . $this->params['language'] . "/student/dossier/show/" . urlencode($language) . "', {newDocId : resp[1]}, function() {
                    $('#show-loading').hide();
                    $('#head-control').show();
                    $('#content div#ajaxMessage')
                        .text('" . __d('portfolio', "Your Dossier has been updated.", true) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                    });
                });
            }
        }
    });
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#show-toggle').live('click', function() {
        if ($('#show-edit').is(':hidden'))
            $('#show-edit').slideDown('slow');
        else {
            $('#show-edit').slideUp('slow');
            if ($('#show-edit-form #PoDocumentId').val() == '')
                $('#show-toggle').fadeOut('slow');
        }
    });
    // Édition d'un document
    $('#show table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        // Chargement des données dans le formulaire
        $('#show-edit-form #PoDocumentId').val(itemId);
        $('#show-edit-form #PoDocumentFilename').val($('#hidden-info_' + itemId + ' .filename').text());
        $('#show-edit-form #PoDocumentCompetency option[value=\'' + $('#hidden-info_' + itemId + ' .competency').text() + '\']').attr('selected', 'selected');
        $('#show-edit-form #PoDocumentDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Titre
        $('#title-show-edit').html('" . addslashes(__d('portfolio', "Edit a document", true)) . "');
        // Affichage si masqué
        if ($('#show-edit').is(':hidden')) {
            $('#divToolTip').fadeOut('fast');
            $('#show-add').hide();
            $('#show-edit').slideDown('slow');
            $('#show-add').fadeIn('slow');
            $('#show-toggle').fadeIn('slow');
        };
        return false;
    });
    // Suppression d'un document
    $('#show table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#show-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    var file = $('#hidden-info_' + itemId + ' .file').text();
                    $(this).dialog('close');
                    $('#control_' + itemId).hide();
                    $('#loading_' + itemId).show();
                    if ($('#show-edit-form #PoDocumentId').val() == itemId) {
                        // Initialisation du formulaire
                        $('#show-edit-form #PoDocumentId').val('');
                        $('#show-edit-form #PoDocumentFilename').val('');
                        $('#show-edit-form #PoDocumentCompetency :selected').removeAttr('selected');
                        $('#show-edit-form #PoDocumentDescription').val('');
                    }
                    $('#show').load('/" . $this->params['language'] . "/student/dossier/delete_doc/" . urlencode($language) . "/show/' + itemId + '/' + file, function() {
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
        return false;
    });
");
} else {
    if (isset($newDoc))
        $initEdit = "
    // État initial formulaire
    $('#show-edit-form #PoDocumentId').val('" . $newDoc['PoDocument']['id'] . "');
    $('#show-edit-form #PoDocumentFilename').val('" . $newDoc['PoDocument']['filename'] . "');
    $('#show-edit-form #PoDocumentCompetency option[value=\'" . $newDoc['PoDocument']['competency'] . "\']').attr('selected', 'selected');
    $('#show-edit-form #PoDocumentDescription').val('" . $newDoc['PoDocument']['description'] . "');
    $('#title-show-edit').html('" . addslashes(__d('portfolio', "Create a document", true)) . "');
    $('#show-edit').slideDown('slow');
    $('#show-toggle').show();";
    else
        $initEdit = '';
    $this->Js->Buffer($initEdit . "
    $('#title-show').html('" . addslashes(__d('portfolio', "Display", true)) . "');");
    // Sortie des scripts pour affichage Ajax
    echo $this->Js->writeBuffer();
}
?>