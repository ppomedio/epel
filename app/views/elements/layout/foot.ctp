
            <p>
                <span id="backtotop-left" class="backtotop left link"></span>
                <span id="backtotop-right" class="backtotop right link"></span><?php

                /* Version
                $version = Configure::read('epel.version');
                if (!empty($version))
                    echo "ePEL v" . $version . "&nbsp;&bull; "; //*/

                // COE
                __("This model conforms to common principles and guidelines of the");
                $coeUrl = Configure::read('epel.coe.url');
                if ($coeUrl) { ?>

                <a href="<?php echo $coeUrl; ?>"><?php __("Council of Europe"); ?></a><?php
                } else {
                __("Council of Europe");
                } ?>&nbsp;(<?php __("Electronic ELP #"); ?>117.2010)<br /><?php

                // CNIL
                $cnil = Configure::read('epel.cnil.number');
                if (isset($cnil)) {
                    if (empty($cnil))
                        __("Authorisation from the French privacy and personal data national commission (Commission Nationale Informatique et Libertés) pending");
                    else
                        echo __("French privacy and personal data national commission authorisation n°", true) . $cnil;
                    echo "<br />";
                }

                // Université hôte
                $rootUniversity = Configure::read('epel.university');
                if ($rootUniversity['url']) { ?>

                <a href="<?php echo $rootUniversity['url']; ?>"><?php echo $rootUniversity['name']; ?></a><?php
                } else {
                    echo $rootUniversity['name'];
                } ?>

            </p>
            <div id="data_backtotop-left" class="tooltip-data"><?php __('Go back to top of page'); ?></div>
            <div id="data_backtotop-right" class="tooltip-data"><?php __('Go back to top of page'); ?></div>
