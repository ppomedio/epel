<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php
 *
 * This is an application wide file to load any function that is not used within a class
 * define. You can also use this to include or require any files in your application.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * App::build(array(
 *     'plugins' => array('/full/path/to/plugins/', '/next/full/path/to/plugins/'),
 *     'models' =>  array('/full/path/to/models/', '/next/full/path/to/models/'),
 *     'views' => array('/full/path/to/views/', '/next/full/path/to/views/'),
 *     'controllers' => array('/full/path/to/controllers/', '/next/full/path/to/controllers/'),
 *     'datasources' => array('/full/path/to/datasources/', '/next/full/path/to/datasources/'),
 *     'behaviors' => array('/full/path/to/behaviors/', '/next/full/path/to/behaviors/'),
 *     'components' => array('/full/path/to/components/', '/next/full/path/to/components/'),
 *     'helpers' => array('/full/path/to/helpers/', '/next/full/path/to/helpers/'),
 *     'vendors' => array('/full/path/to/vendors/', '/next/full/path/to/vendors/'),
 *     'shells' => array('/full/path/to/shells/', '/next/full/path/to/shells/'),
 *     'locales' => array('/full/path/to/locale/', '/next/full/path/to/locale/')
 * ));
 *
 */

/**
 * As of 1.3, additional rules for the inflector are added below
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */



/**
 * Configuration
 *
 */

Configure::load('config');



/**
 * Détermination de la langue de l'interface
 *
 */

// Matrice des langues installées
$languages = Configure::read('epel.locales.available');

// Langue déterminée par l'url ?
$url = env('REQUEST_URI');
if (strlen($url) > 1) {
    if ((strlen($url) > 3) && (strpos($url, '/', 3) == 3) || ((strlen($url) <= 3))) {
        $langFromUrl = substr(env('REQUEST_URI'),  1, 2);
        if (array_key_exists($langFromUrl, $languages))
            $language = $langFromUrl;
        else
            header('Location: ' . str_replace('/' . $langFromUrl, '/' . Configure::read('epel.default.language'), $url));
    }
}

// Sinon, par le navigateur ?
if (!isset($language)) {
    // Détection de la langue du navigateur
    $navLang = explode(",", env('HTTP_ACCEPT_LANGUAGE'));
    $navLang = strtolower(substr(rtrim($navLang[0]), 0, 2));
    // Langue installée dans l'interface ?
    if (array_key_exists($navLang, $languages))
        $language = $navLang;
}

// Sinon, langue par défaut…
if (!isset($language))
    $language = Configure::read('epel.default.language');

// Enregistrement
Configure::write('Config.language', $language);



/**
 * N° de version
 *
 */

Configure::write('epel.version', '2.0.12');

