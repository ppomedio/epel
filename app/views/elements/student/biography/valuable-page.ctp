<?php

$paginator->options(array(
    'update' => '#valuable'
    ));

?>

                        <div id="valuable" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Context"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="valuable-add" class="add"></span>
                                            <span id="valuable-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3">
                                             <p class="alert"><?php
                                             __d('portfolio', "There is no memorable experience recorded in your Biography at the moment.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#valuable');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="name"><?php echo $paginator->sort(__d('portfolio', "Context", true), 'PoValuable.name', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="valuable-add" class="add"></span>
                                            <span id="valuable-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoValuable'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="valuable-name_<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
                                        <td id="valuable-description_<?php echo $value['id']; ?>"><?php echo nl2br($value['description']); ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="valuable-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="valuable-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_valuable-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this experience");
                                    ?></div>
                                    <div id="data_valuable-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this experience");
                                    ?></div>
                                    <div id="valuable-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete this experience"); echo ' (' . $value['name'] . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'valuable'));
                            } ?>

                            <div id="data_sort-valuable-name" class="tooltip-data"><?php __("Sort by context"); ?></div>
                            <div id="data_valuable-add" class="tooltip-data"><?php __d('portfolio', "Add a new experience"); ?></div>
                            <div id="data_valuable-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                            <p class="info"><?php __d('portfolio', "Those experience items appears in the Passport part of your Portfolio, in the “Learning experience (detail)” table.");  ?></p>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#valuable-edit-form #PoValuableDescription').val() == ''
            && $('#valuable-edit-form #PoValuableName').val() == '') {
        if ($('#valuable-edit').is(':hidden'))
            $('#valuable-toggle').hide();
        else
            $('#valuable-add').hide();
    }
    // Id, classes et cie
    $('#valuable table thead tr th.name a').attr('id', 'sort-valuable-name').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#valuable table thead');
    tooltipAdd('#valuable table thead th.controls');
    tooltipGear('#valuable table thead th.controls');
    tooltipEdit('#valuable table tbody td.controls div.controls');
    tooltipSupp('#valuable table tbody td.controls div.controls');
    // Tips
    addTipInfo('#valuable');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#valuable-toggle').live('click', function() {
        if ($('#valuable-edit-form #PoValuableId').val() == '')
            $('#valuable-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        if ($('#valuable-edit').is(':hidden'))
            $('#valuable-edit').slideDown('slow');
        else {
            $('#valuable-edit').slideUp('slow');
            $('#valuable-add').fadeIn('slow');
            if ($('#valuable-edit-form #PoValuableDescription').val() == ''
                    && $('#valuable-edit-form #PoValuableName').val() == '')
                $('#valuable-toggle').hide();
        }
    });
    // Ajout d'une expérience
    $('#valuable-add').live('click', function() {
        $('#valuable-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#valuable-edit').is(':hidden')) {
            $('#valuable-edit').slideDown('slow');
        }
        $('#valuable-add').hide();
        $('#valuable-toggle').fadeIn('slow');
    });
    // Édition d'une expérience
    $('#valuable table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#valuable-edit > h2').text('" . addslashes(__d('portfolio', "Edit an experience", true)) . "');
        // Chargement des données dans le formulaire
        $('#valuable-edit-form #PoValuableId').val(itemId);
        $('#valuable-edit-form #PoValuableName').val($('#valuable-name_' + itemId).text());
        $('#valuable-edit-form #PoValuableDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Affichage si masqué
        if ($('#valuable-edit').is(':hidden')) {
            $('#valuable-add').hide();
            $('#valuable-edit').slideDown('slow');
            $('#valuable-add').fadeIn('slow');
            $('#valuable-toggle').fadeIn('slow');
        } else {
            if ($('#valuable-add').is(':hidden'))
                $('#valuable-add').fadeIn('slow');
        }
    });
    // Suppression d'une expérience
    $('#valuable table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#valuable-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
					var extract = new Array();
					extract = $(this).attr('id').split('_');
					var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#valuable').load('/" . $this->params['language'] . "/student/biography/delete/" . urlencode($language) . "/valuable/' + itemId, function() {
                            if ($('#valuable-edit-form #PoValuableId').val() == itemId) {
                                $('#valuable-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
                                initForm();
                                if ($('#valuable-edit').is(':hidden'))
                                    $('#valuable-toggle').hide();
                                else
                                    $('#valuable-add').hide();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-valuable').html('" . addslashes(__d('portfolio', "My most memorable language-learning experiences", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>