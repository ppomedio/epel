<?php

/**
 * {app}/models/assess_item.php
 */
class AssessItem extends AppModel {

    var $name = 'AssessItem';
    var $useDbConfig = 'epel';


    /**
     * Liste ordonnée des compétences, pour un groupe de compétences et un niveau
     *
     * @param type $competency
     * @param type $level
     * @return type array
     */
    function getAssessItems($competency, $level) {

        // Chargement des données
        $data = $this->find('all', array(
            'conditions' => array(
                'AssessItem.competency' => $competency,
                'AssessItem.level' => $level
            ),
            'fields' => array(
                'order',
                'description'
            ),
            'order' => array('AssessItem.order ASC'),
            'recursive' => -1
        ));

        // Mise en forme
        $items = array();
        foreach ($data as $item) {
            $items[$item['AssessItem']['order']] = $item['AssessItem']['description'];
        }

        // Retour
        return $items;

    }


}

