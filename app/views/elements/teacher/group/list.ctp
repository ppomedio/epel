
                    <section>
                        <h1 id="title-groups" class="spaced"><?php __("Groups and referring teachers"); ?></h1>
                        <?php echo $this->element('teacher/group/list-page'); ?>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

?>