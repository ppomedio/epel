<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#visit-page',
    'before' => $this->Js->get('#visit-page')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="visit-page" class="table"><?php
                            if (empty($user['Visit'])) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="student-name"><?php __("Student"); ?></th>
                                        <th class="visitor-name"><?php __("Visitor"); ?></th>
                                        <th class="visit-date"><?php __("Date"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3"><?php __("There has been no visit at the moment."); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="student-name"><?php echo $paginator->sort(__("Student", true), 'Student.name'); ?></th>
                                        <th class="visitor-name"><?php echo $paginator->sort(__("Visitor", true), 'Visitor.name'); ?></th>
                                        <th class="visit-date"><?php echo $paginator->sort(__("Date", true), 'Visit.date'); ?></th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user['Visit'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>
                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php
                                        echo $this->Html->link($value['Student']['name'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['Student']['id'] . '_'
                                                        . $value['Student']['firstname'] . ' '
                                                        . $value['Student']['name'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['Student']['id'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['Student']['firstname'];
                                        ?></td>
                                        <td><?php
                                        echo $this->Html->link($value['Visitor']['name'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['Visitor']['id'] . '_'
                                                        . $value['Visitor']['firstname'] . ' '
                                                        . $value['Visitor']['name'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['Visitor']['id'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['Visitor']['firstname'];
                                        ?></td>
                                        <td class="smaller"><?php echo $this->Date->Convert($value['date']); ?></td>
                                        <div id="data_contact-user_<?php echo $value['Student']['id']; ?>" class="inline tooltip-data"><?php
                                            echo __("Send an email to", true) . ' ' . $value['Student']['firstname'] . ' ' . $value['Student']['name'];
                                        ?></div>
                                        <div id="data_contact-user_<?php echo $value['Visitor']['id']; ?>" class="inline tooltip-data"><?php
                                            echo __("Send an email to", true) . ' ' . $value['Visitor']['firstname'] . ' ' . $value['Visitor']['name'];
                                        ?></div>
                                    </tr><?php
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'visit-page', 'half' => true));
                            } ?>

                            <div id="data_sort-student-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-visitor-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-visit-date" class="tooltip-data"><?php __("Sort by date"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Id, classes et cie
    $('#visit-page table thead tr th.student-name a').attr('id', 'sort-student-name').addClass('sort-help');
    $('#visit-page table thead tr th.visitor-name a').attr('id', 'sort-visitor-name').addClass('sort-help');
    $('#visit-page table thead tr th.visit-date a').attr('id', 'sort-visit-date').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#visit-page table thead tr th');
    tooltipLink('#visit-page table tbody tr td');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#visit-page').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-visit-page').html('" . addslashes(__("Visits", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>