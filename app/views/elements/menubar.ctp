
                <nav>
                    <ul><?php
                        /**
                         * Menu utilisateur et déconnexion
                         */
                        if ($this->Session->check('Auth.User')) {
                            echo $this->Menu->renderMenu($this->Session->check('Auth.User.view'));
                            ?>
                        <li class="tools log"><?php
                            echo $this->Html->link(__("Logout", true),
                                array(
                                    'controller' => 'user',
                                    'action' => 'logout'
                                )
                            );
                        ?></li><?php
                        }

                        /**
                         * Contact sauf pour l'administrateur
                         */
                        if ((!$this->Session->check('Auth.User')) || ($this->Session->check('Auth.User')
                                && ($this->Session->read('Auth.User.role') != 'admin'))) { ?>

                        <li class="tools"><?php
                            if ($this->Session->check('Auth.User.view')) {
                                // Contact étudiant depuis une visite
                                $student = $this->Session->read('Auth.User.view');
                                echo $this->Html->link(__("Contact", true),
                                        array(
                                            'controller' => 'contact',
                                            'action' => 'form',
                                            base64_encode($student['id'] . '_' . $student['firstname'] . ' ' . $student['name'])
                                        ),
                                        array(
                                            'id' => __("Send an email to", true) . ' ' . $student['firstname'] . ' ' . $student['name'],
                                            'class' => 'iframe contact link'
                                        )
                                );
                            } else {
                                // Contact par défaut
                                echo $this->Html->link(__("Contact", true),
                                    array(
                                        'controller' => 'contact',
                                        'action' => 'form'
                                    ),
                                    array('class' => 'iframe contact')
                                );
                            }
                        ?></li><?php
                        }

                        
                        /**
                         * Menu additionel des étudiants
                         */
                        if ($this->Session->check('Auth.User')
                                && ($this->Session->read('Auth.User.role') == 'student')) {

                            $this->Js->Buffer("
    // Fancybox -> soon…
    $('a.iframe.soon').fancybox({
        'width'             :   500,
        'height'            :	80,
        'padding'           :	0,
        'margin'            :	0,
        'speedIn'           :	900,
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })

    // Fancybox -> guide des listes de repérage
    $('a.iframe.self-assessment').fancybox({
        'width'             :   500,
        'height'            :	475,
        'padding'           :	0,
        'margin'            :	0,
        'speedIn'           :	900,
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })

    // Fancybox -> grille des niveaux
    $('a.iframe.levels-grid').fancybox({
        'width'             :   800,
        'height'            :	600,
        'padding'           :	0,
        'margin'            :	0,
        'speedIn'           :	900,
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })

    // Fancybox -> clavier virtuel
    $('a.iframe.vkbd').fancybox({
        'width'             :   " . Configure::read('epel.vkbd.width') . ",
        'height'            :	" . Configure::read('epel.vkbd.height') . ",
        'padding'           :	0,
        'margin'            :	0,
        'speedIn'           :	900,
        'scrolling'         :   'no',
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })
");
                        ?>

                        <li class="tools"><span><?php __('Tools') ?></span>
                            <ul>
                                <li><?php
                                echo $this->Html->link(__d('levels_grid', "Levels grid", true),
                                    array(
                                        'controller' => 'guide',
                                        'action' => 'levels-grid'
                                    ),
                                    array('class'=>'iframe levels-grid')
                                );
                                ?></li>
                                <li><?php
                                echo $this->Html->link(__("PDF export", true),
                                    array(
                                        'controller' => 'guide',
                                        'action' => 'soon'
                                    ),
                                    array('class'=>'iframe soon')
                                );
                                ?></li>
                                <li><a href="<?php
                                echo Configure::read('epel.vkbd.url');
                                ?>" class="iframe vkbd"><?php __('Virtual keyboard'); ?></a></li>
                            </ul>
                        </li><?php } ?>

                    </ul>
                </nav><?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Barre de navigation
    $('#content > nav > ul > li').hoverIntent(function() {
        $(this).children('ul').show('slow');
    }, function() {
        $(this).children('ul').hide('fast');
    });

    // Fancybox (contact)
    fancyBox();
");

?>