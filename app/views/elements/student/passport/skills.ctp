
                    <section class="half right">
                        <h2><?php __d('portfolio', "Self-assessment"); ?></h2>
                        <?php echo $this->Form->create('Portfolio', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/passport/edit/' . urlencode($language),
                            'id' => 'skills-form',
                            'class' => ''
                            ));
                        ?>
                        <p class="info"><?php  __d('portfolio', "Use the levels grid (on the right side, or in the “Tools” menu) to determine the most appropriate level for each competency.");  ?></p>
                        <fieldset>
                            <legend><?php __d('portfolio', "Understanding"); ?></legend>
                            <?php
                            echo $this->Form->input('listening', array(
                                'label' => __d('portfolio', "Listening", true),
                                'options' => $levels,
                                'empty' => array('0' => "…")
                                ));
                            echo $this->Form->input('listening_part', array(
                                'label' => array(
                                    'class' => 'part-comp-label',
                                    'text' => __d('portfolio', "Also", true)
                                ),
                                'type' => 'textarea'
                                ));
                            echo $this->Form->input('reading', array(
                                'label' => __d('portfolio', "Reading", true),
                                'options' => $levels,
                                'empty' => array('0' => "…")
                                ));
                            echo $this->Form->input('reading_part', array(
                                'label' => array(
                                    'class' => 'part-comp-label',
                                    'text' => __d('portfolio', "Also", true)
                                ),
                                'type' => 'textarea'
                                ));
                            ?>
                        </fieldset>
                        <fieldset>
                            <legend><?php __d('portfolio', "Speaking"); ?></legend>
                            <?php
                            echo $this->Form->input('speak_int', array(
                                'label' => __d('portfolio', "Spoken interaction", true),
                                'options' => $levels,
                                'empty' => array('0' => "…")
                                ));
                            echo $this->Form->input('speak_int_part', array(
                                'label' => array(
                                    'class' => 'part-comp-label',
                                    'text' => __d('portfolio', "Also", true)
                                ),
                                'type' => 'textarea'
                                ));
                            echo $this->Form->input('speak_prod', array(
                                'label' => __d('portfolio', "Spoken production", true),
                                'options' => $levels,
                                'empty' => array('0' => "…")
                                ));
                            echo $this->Form->input('speak_prod_part', array(
                                'label' => array(
                                    'class' => 'part-comp-label',
                                    'text' => __d('portfolio', "Also", true)
                                ),
                                'type' => 'textarea'
                                ));
                            ?>
                        </fieldset>
                        <fieldset>
                            <legend><?php __d('portfolio', "Writing"); ?></legend>
                            <?php
                            echo $this->Form->input('writing', array(
                                'label' => __d('portfolio', "Writing", true),
                                'options' => $levels,
                                'empty' => array('0' => "…")
                                ));
                            echo $this->Form->input('writing_part', array(
                                'label' => array(
                                    'class' => 'part-comp-label',
                                    'text' => __d('portfolio', "Also", true)
                                ),
                                'type' => 'textarea'
                                ));
                            ?>

                        </fieldset>
                        <p class="info"><?php  __d('portfolio', "You can use the “Also” text areas to indicate what you can do.");  ?></p><?php
                        echo $this->element('student/passport/skills-update');
                        echo $this->Form->end(__("Update", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ui.widget',
        'jquery.ui.mouse',
        'jquery.ui.slider'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Sliders
    var bubbleClass = {
        'PortfolioListening': '" . $levelDocs['PortfolioListening']['value'] . "',
        'PortfolioReading': '" . $levelDocs['PortfolioReading']['value'] . "',
        'PortfolioSpeakInt': '" . $levelDocs['PortfolioSpeakInt']['value'] . "',
        'PortfolioSpeakProd': '" . $levelDocs['PortfolioSpeakProd']['value'] . "',
        'PortfolioWriting': '" . $levelDocs['PortfolioWriting']['value'] . "'
    };
    var bubbleId = {
        'PortfolioListening': '" . addslashes($levelDocs['PortfolioListening']['tooltip']) . "',
        'PortfolioReading': '" . addslashes($levelDocs['PortfolioReading']['tooltip']) . "',
        'PortfolioSpeakInt': '" . addslashes($levelDocs['PortfolioSpeakInt']['tooltip']) . "',
        'PortfolioSpeakProd': '" . addslashes($levelDocs['PortfolioSpeakProd']['tooltip']) . "',
        'PortfolioWriting': '" . addslashes($levelDocs['PortfolioWriting']['tooltip']) . "'
    };
    $('#skills-form div.select select').css('width', '15%').css('float', 'right').each(function() {
        var selectId = $(this).attr('id');
        $('<span class=\"picto' + selectId + '\"></span>').insertBefore(this);
        var sliderId = 'slider' + selectId;
        var slider = $('<div id=\"' + sliderId + '\" class=\"slider\"></div>').insertBefore(this).slider({
            min: 0,
            max: 6,
            value: $(this).val(),
            slide: function(event, ui) {
                $('#' + selectId).val(ui.value);
                var text = $('#' + selectId + ' option[value=\"' + ui.value + '\"]').text();
                skillsSubmit();
            }
        });
        $('<span id=\"' + bubbleId[selectId] + '\" class=\"bubble-' + bubbleClass[selectId] + '\"></span>')
            .tooltip({
                bordercolor: '#dcdcdd',
                bgcolor: '#fefeff',
                fontcolor : '#666',
                fontsize : '12px',
                width : '210px',
                cursor : 'help'
            })
            .insertBefore(this);
        $(this).click(function() {
            slider.slider('value', $(this).val());
        });
    });
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#skills-form div.submit input[type=submit]');
    // Activation du submit
    $('#skills-form div.submit input[type=submit]').attr('disabled', 'disabled');
    function skillsSubmit(){
        if ($('#skills-form div.submit input[type=submit]').attr('disabled'))
            $('#skills-form div.submit input[type=submit]').removeAttr('disabled');
    };
    $('#skills-form select').change(function() { skillsSubmit(); });
    $('#skills-form textarea').keydown(function() { skillsSubmit(); });
    // Soumission Ajax du formulaire
    $('#skills-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/passport/ajax_save/" . urlencode($language) . "',
        target: '#skills-update',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#skills-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#skills-form div.loading').show();
            prevSpeakInt = $('#PortfolioSpeakInt').val();
            prevSpeakProd = $('#PortfolioSpeakProd').val();
        },
        success: function() {
            $('#skills-form div.loading').hide();
            //$('#skills-form div.submit input[type=submit]').removeAttr('disabled');
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Passport has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
            });
        }
    })
");

?>