<?php

/**
 * Short description for file.
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Short description for class.
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */
class AppController extends Controller {

    // Composants utilisés dans tous les contrôleurs
    var $components = array(
        'AppAuth',
        'Session',
        'Cookie',
        'Menu',
        'RequestHandler'
    );
    // Assistants utilisés dans tous les contrôleurs
    var $helpers = array(
        'Session',
        'Form',
        'Html',
        'Js' => array('Jquery'),
        'JsValidate.Validation'
    );
    // Layout par défaut
    var $layout = 'app_default';


    /**
     * Before action callback
     *
     * @return void
     * @access public
     */
    function beforeFilter() {

        /**
         * Maintenance
         */
        #$this->log($this->RequestHandler->getClientIp(), 'debug');
        if (Configure::read('epel.maintenance.state')
                && !in_array($this->RequestHandler->getClientIp(), Configure::read('epel.maintenance.access'))) {
            $this->set('maintenance', true);
            if ($this->AppAuth->user()) {
                $this->Session->destroy();
                $this->redirect($this->AppAuth->logout());
            }
        }

        /**
         * Nom des cookies
         */
        $cookieName = 'v' . str_replace('.', '', Configure::read('epel.version'));
        Configure::write('epel.cookie.name', $cookieName);
        $this->Cookie->name = $cookieName;

        /**
         * Lecture du cookie de la langue utilisée par l'interface
         */
        $language = $this->Cookie->read('language');

        /**
         * Cookie de contrôle posé en JavaScript
         */
        if (!$this->RequestHandler->isAjax()) {
            if ($this->Cookie->read('js_check'))
                Configure::write('epel.js_check', true);
        }

        /**
         * Arrivée sur le site par l'url de base -> redirection suivant la
         * langue utilisée. S'il existe un cookie "language" on en tient compte,
         * sinon on le crée en utilisant la langue par défaut
         */
        if ($this->params['url']['url'] == '/') {
            if (!$language) {
                $language = Configure::read('Config.language');
                $this->Cookie->write('language', $language, Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            }
            $this->Session->write('Config.language', $language);
            $this->redirect("/" . $language, null, true);
        }

        /**
         * Gestion de la langue : si elle existe en variable de session
         * on la met à jour si elle a changé, sinon on enregistre sa valeur
         * par défaut ou celle éventuellement fournie par le cookie "language".
         * Dans tous les cas on pose / actualise ce cookie.
         */
        if ($this->Session->check('Config.language')) {
            if ($this->Session->read('Config.language') != $this->params['language']) {
                if ($this->_isAppLanguage($this->params['language'])) {
                    $this->Session->write('Config.language', $this->params['language']);
                    Configure::write('Config.language', $this->params['language']);
                    // Rechargement du menu (tri…)
                    $this->Menu->setMenu($this->Session->check('Auth.User.view'));
                }
            }
        } else {
            if (!$language)
                $language = Configure::read('Config.language');
            $this->Session->write('Config.language', $language);
        }
        $this->Cookie->write('language', $this->params['language'], Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));


        /**
         * Configuration du composant "AppAuth"
         */
        // Redirection après identification via la fonction login (users_controller)
        $this->AppAuth->autoRedirect = false;
        // Routage
        $this->AppAuth->loginAction = array('language' => $this->Session->read('Config.language'),
            'controller' => 'users', 'action' => 'login');
        $this->AppAuth->logoutRedirect = '/' . $this->params['language'];
        // Messages d'erreur
        $this->AppAuth->loginError = __("Login failed.", true);
        $this->AppAuth->authError = __("Please authentify.", true);

        /**
         * Vérification de la cohérence entre l'url demandée et le rôle de l'utilisateur :
         * - si ok pose / maj du cookie mémorisant la dernière page ouverte,
         * - sinon déconnexion
         */
        if ($this->AppAuth->user() && !$this->RequestHandler->isAjax()) {
            $ctrlName = explode('_', $this->params['controller']);
            switch ($ctrlName[0]) {
                case 'contacts':
                case 'pages':
                case 'users':
                    break;
                case 'view':
                    if (($this->AppAuth->user('role') != 'teacher') && ($this->AppAuth->user('role') != 'visitor')) {
                        // Ce type d'utilisateur n'accède pas aux vues
                        $this->redirect(array(
                            'language' => $this->params['language'],
                            'controller' => 'users',
                            'action' => 'logout'
                        ));
                    }
                    break;
                default:
                    if ($ctrlName[0] == $this->AppAuth->user('role')) {
                        // Pose / maj du cookie de retour (sauf si url de téléchargement)
                        if (strpos($this->params['url']['url'], 'load') === false) {
                            $this->Cookie->write(
                                    $this->AppAuth->user('id') . '.lastpage',
                                    urlencode(substr($this->params['url']['url'], strpos($this->params['url']['url'], '/'))),
                                    Configure::read('epel.cookie.encrypt'),
                                    Configure::read('epel.cookie.time')
                                    );
                        }
                    } else {
                        // L'url demandée ne correspond pas au rôle -> eject
                        $this->redirect(array(
                            'language' => $this->params['language'],
                            'controller' => 'users',
                            'action' => 'logout'
                        ));
                    }
                    break;
            }
        }

    }


    /**
     * Journalisation de l'activité des utilisateurs
     *
     * @param <type> $action
     * @param <type> $extra
     */
    function logThis($action = '…', $extra = '') {

        #$this->log($this->RequestHandler->getClientIp(), 'debug');
        if (Configure::read('epel.log.on')) {
            // Préparation des données
            $logData = array(
                'user_id' => 0,
                'role' => 'unknown',
                'university_id' => 0,
                'action' => $action,
                'extra' => $extra
            );
            // id, role, université / timeout
            if ($this->AppAuth->user()) {
                $logData['user_id'] = $this->AppAuth->user('id');
                $logData['role'] = $this->Session->read('Auth.User.role');
                if ($this->AppAuth->user('id') != 1)
                    $logData['university_id'] = $this->Session->read('Auth.User.info.university_id');
            } else if ($action == 'logout')
                $logData['extra'] = 'session timeout';
            // Traçage IP
            if (Configure::read('epel.log.ip'))
                $logData['ip'] = $this->RequestHandler->getClientIp();
            // Chargement du modèle
            $this->loadModel('Activity');
            // Enregistrement
            $this->Activity->logThis($logData);
        }

    }


    /**
     * Teste si une langue existe pour l'application
     *
     * @param <string> $language
     * @return boolean
     */
    private function _isAppLanguage($language) {

        // Chargement des langues installées pour l'application
        $languages = Configure::read('epel.locales.available');
        // Retour du résultat
        return array_key_exists($language, $languages);

    }


}

