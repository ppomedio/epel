
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <section>
                        <div id="heritage-view">
                            <p><?php echo nl2br($this->Session->read('Auth.User.view.mediation')); ?></p>
                        </div>
                    </section>

                </article>
                <aside><?php
                    echo $this->element('view/student');
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>