
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <section>
                        <div id="heritage-info">
                            <p class="info"><?php
                            __d('portfolio', "Heritage languages are, for example, the languages of ethnic minorities, the languages that immigrants bring with them to a new country, and languages that are used for purposes of religious observance or in cultural festivals.");
                            ?></p>
                        </div>
                    </section>
                    <section>
                        <div id="heritage-view">
                            <p><?php echo nl2br($this->Session->read('Auth.User.view.heritage')); ?></p>
                        </div>
                    </section>

                </article>
                <aside><?php
                    echo $this->element('view/student');
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#heritage-info');
");

?>