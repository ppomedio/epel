
                    <div id="howtolearn-update">
                        <section>
                            <h2 id="howtolearn-formal-title"><?php __d('portfolio', "Formal learning activities"); ?></h2>
                            <?php
                            // Jeu de questions
                            $question = array(
                                301 => __d('portfolio', "identify key words and concepts?", true),
                                __d('portfolio', "take effective notes from texts and lectures?", true),
                                __d('portfolio', "categorise new vocabulary?", true),
                                __d('portfolio', "organise learning material by topic, theme etc.?", true),
                                __d('portfolio', "make a schematic plan of or “mind-map” an oral or written presentation?", true),
                                __d('portfolio', "analyse and edit a first written draft?", true),
                                __d('portfolio', "use corrections to advance my learning?", true),
                                __d('portfolio', "practise pronunciation and intonation with good results?", true)
                            );
                            // Formulaire
                            echo $this->Form->create('PoHowtolearn', array(
                                'url' => '/' . Configure::read('Config.langCode')
                                    . '/student/biography/howtolearn',
                                'id' => 'howtolearn-formal-form',
                                'class' => 'full table'
                                ));
                            ?>
                            <table class="howtolearn-table">
                                <thead>
                                    <tr>
                                        <?php
                                        for ($n = 1; $n < 7; $n++) {
                                            echo "<th>" . $tableHead[$n] . "</th>";
                                        } ?>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    for ($n = 301; $n < 309; $n++) {
                                        $idPart = (string)$n;
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        echo '<tr class="'. $trClass . '"><td>' . $question[$n] . '</td>';
                                        echo '<td>' . $this->Form->radio($idPart . 'answer', $options, $attributes) . '</td>';
                                        echo '<td class="master">' . $this->Form->checkbox($idPart . 'target') . '</td>';
                                        echo '<td>' . $this->Form->input($idPart . 'date', array('label' => false)) . '</td></tr>';
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->Form->hidden('part', array('default' => 3));
                            echo $this->Form->end(__("Update", true)); ?>

                        </section>
                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // iButtons
    $('#howtolearn-formal-form table tbody input[type=checkbox]').iButton({
        labelOn: '" . __('YES', true) . "',
        labelOff: '" . __('NO', true) . "',
        resizeHandle: false,
        resizeContainer: true
        })
        .change(function() {
            // Activation du submit
            if ($('#howtolearn-formal-form div.submit input[type=submit]').attr('disabled'))
                $('#howtolearn-formal-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Instructions
    if ($('#howtolearn-intro').is(':hidden'))
        $('#howtolearn-intro').slideDown('slow');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#howtolearn-formal-form div.submit input[type=submit]');
    // Activation du submit
    $('#howtolearn-formal-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#howtolearn-formal-form input[type=radio]').change(function() {
        if ($('#howtolearn-formal-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-formal-form div.submit input[type=submit]').removeAttr('disabled');
    });
    $('#howtolearn-formal-form input[type=text]').keydown(function() {
        if ($('#howtolearn-formal-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-formal-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#howtolearn-formal-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/howtolearn/3',
        target: '#howtolearn-update',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#howtolearn-formal-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#howtolearn-formal-form div.loading').show();
            $('#howtolearn-formal-title').html('" . addslashes(__("Loading…", true)) . "');
            $('#howtolearn-update').fadeTo('fast', 0.6);
        },
        success: function() {
            $('#howtolearn-formal-form div.loading').hide();
            $('#howtolearn-update').fadeTo('slow', 1);
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>