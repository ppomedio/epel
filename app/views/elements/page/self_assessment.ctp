
        <h1><?php __d('assessment', "Goal-setting and self-assessment"); ?></h1>
        <p><?php __d('assessment', "These checklists are derived from the illustrative scales in the Common European Framework of Reference for Languages (&copy; Council of Europe 2001). For the activities of listening, reading, spoken interaction, spoken production and writing they expand on the summary descriptions contained in the self-assessment grid in the language passport."); ?></p>
        <h1><?php __("User guide"); ?></h1>
        <p><?php __d('assessment', "You can use these checklists to assess your current level of proficiency, set learning goals, and monitor your progress."); ?></p>
        <p><?php __d('assessment', "You can select your goals with the “New target” cursor. Then you can choose your own evaluation criteria."); ?></p>
        <p><?php __d('assessment', "Decide what evaluation criteria you want to use, select the most appropriate to your skills and enter a date in the text field to record your progress."); ?></p>
        <p><?php __d('assessment', "Note that the checklists do not pretend to be exhaustive. For each proficiency level other items (tasks or activities) can be added."); ?></p>
        <p><?php __d('assessment', "It is not necessary to be able to perform all the tasks or activities listed in order to achieve the level in question. If, for example, you can already perform about 80% of the items on the list for A2 “Spoken interaction”, you have probably achieved that level in terms of the self-assessment grid in the language passport."); ?></p>
