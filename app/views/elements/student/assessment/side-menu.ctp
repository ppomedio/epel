<?php

$menu = $this->Session->read('SideMenu.Self-assessment');

?>

                    <div>
                        <h2><?php __d('portfolio', "Competencies"); ?></h2>
                        <nav>
                            <ul><?php
                            foreach ($menu as $key => $value) {
                                if (strpos($this->params['url']['url'], $key))
                                    $class = 'current';
                                else
                                    $class = '';
                            ?>

                                <li class="<?php echo $class; ?>"><?php
                                echo $this->Html->link(__d('portfolio', $value, true),
                                    '/' . Configure::read('Config.langCode')
                                        . '/student/self-assessment/'
                                        . '/' . $key . '/'
                                        . urlencode($language),
                                    array(
                                        'id' => '',
                                        'class' => ''
                                    )
                                );
                                ?></li>
                            <?php } ?>

                            </ul>
                        </nav>
                        <h2><?php __d('portfolio', "Levels"); ?></h2>
                        <nav id="levels-menu" class="submenu">
                            <ul><?php
                            $levels = array('A1', 'A2', 'B1', 'B2', 'C1', 'C2');
                            foreach ($levels as $value) {
                                if ($value == $level)
                                    $addClass = ' class="current"';
                                else
                                    $addClass = '';
                                ?>

                                <li<?php echo $addClass; ?>><?php
                                echo $this->Html->link($value,
                                    '/' . Configure::read('Config.langCode')
                                        . '/student/self-assessment/'
                                        . '/' . $competency
                                        . '/' . urlencode($language)
                                        . '/' . $value,
                                    array(
                                        'id' => 'sublink_' . $value,
                                        'class' => 'jump' . $addClass
                                    )
                                );
                                ?></li>
                            <?php } ?>

                            </ul>
                        </nav>
                    </div>