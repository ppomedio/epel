<?php

/**
 * {app}/controllers/admin_elements_controller.php
 *
 */
class AdminElementsController extends AppController {

    var $name = 'AdminElements';
    // Modèle
    var $uses = array('Admin');
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect('/' . $this->params['language'] . '/admin/base');

    }


    function university() {

        // Chargement du modèle
        $this->loadModel('University');
        // Traitement des données de formulaire
        if (!empty($this->data)) {
            $this->data['University']['name'] = ucfirst($this->data['University']['name']);
            $this->University->set($this->data);
            // Si validation : sauvegarde
            if ($this->University->validates()) {
                // Sauvegarde
                if ($this->University->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array(
                                'secret',
                                'name',
                                'url'
                            )
                        ))) {
                    // Message
                    $this->Session->setFlash(__("The universities list", true) . " " . addslashes(__("has been updated.", true)));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/admin/element/university');
                }
            }
        }
        // Chargement des données de pagination
        $this->set('data', $this->_getUniversities());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.University.page', $this->params['paging']['University']['options']['page']);
        $this->Session->write('Auth.User.pagination.University.order', $this->params['paging']['University']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'element' . DS . 'universities-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Universities", true));
        }

    }


    function language() {

        // Chargement du modèle
        $this->loadModel('Language');
        // Traitement des données de formulaire
        if (!empty($this->data)) {
            $this->data['Language']['name'] = ucfirst($this->data['Language']['name']);
            $this->Language->set($this->data);
            // Si validation : sauvegarde
            if ($this->Language->validates()) {
                // Sauvegarde
                if ($this->Language->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array('name')
                        ))) {
                    // Message
                    $this->Session->setFlash(__("The languages list", true) . " " . addslashes(__("has been updated.", true)));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/admin/element/language');
                }
            }
        }
        // Chargement des données de pagination
        $this->set('data', $this->_getLanguages());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Language.page', $this->params['paging']['Language']['options']['page']);
        $this->Session->write('Auth.User.pagination.Language.order', $this->params['paging']['Language']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'element' . DS . 'languages-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Languages", true));
        }

    }


    function certificate() {

        // Chargement du modèle
        $this->loadModel('Certificate');
        // Traitement des données de formulaire
        if (!empty($this->data)) {
            $this->Certificate->set($this->data);
            // Si validation : sauvegarde
            if ($this->Certificate->validates()) {
                // Sauvegarde
                if ($this->Certificate->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array(
                                'language_id',
                                'certification_id'
                            )
                        ))) {
                    // Message
                    $this->Session->setFlash(__("The certificates list", true) . " " . addslashes(__("has been updated.", true)));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/admin/element/certificate');
                }
            }
        }
        // Chargement des données de pagination
        $this->set('data', $this->_getCertificates());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Certificate.page', $this->params['paging']['Certificate']['options']['page']);
        $this->Session->write('Auth.User.pagination.Certificate.order', $this->params['paging']['Certificate']['options']['order']);
        // Certifications
        $data = $this->Certificate->Certification->find('all', array(
                    'conditions' => array('id !=' => '0001'),
                    'order' => array('Certification.name' => 'ASC'),
                    'recursive' => -1
                ));
        $certifications = array();
        $descriptions = array();
        foreach ($data as $key => $value) {
            $certifications[$value['Certification']['id']] = $value['Certification']['name'];
            $descriptions[$value['Certification']['id']] = $value['Certification']['description'];
        }
        // Contenu des listes
        $this->set('certifications', $certifications);
        $this->set('descriptions', $descriptions);
        $this->set('languages', $this->Certificate->Language->find('list'));
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'element' . DS . 'certificates-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Certificates", true));
        }

    }


    function certification() {

        // Chargement du modèle
        $this->loadModel('Certification');
        // Traitement des données de formulaire
        if (!empty($this->data)) {
            $this->data['Certification']['name'] = ucfirst($this->data['Certification']['name']);
            $this->Certification->set($this->data);
            // Si validation : sauvegarde
            if ($this->Certification->validates()) {
                // Sauvegarde
                if ($this->Certification->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array(
                                'name',
                                'description'
                            )
                        ))) {
                    // Message
                    $this->Session->setFlash(__("The certifications list", true) . " " . addslashes(__("has been updated.", true)));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/admin/element/certification');
                }
            }
        }
        // Chargement des données de pagination
        $this->set('data', $this->_getCertifications());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Certification.page', $this->params['paging']['Certification']['options']['page']);
        $this->Session->write('Auth.User.pagination.Certification.order', $this->params['paging']['Certification']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'element' . DS . 'certifications-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Certifications", true));
        }

    }


    function domain() {

        // Chargement du modèle
        $this->loadModel('Domain');
        // Traitement des données de formulaire
        if (!empty($this->data)) {
            $this->data['Domain']['name'] = ucfirst($this->data['Domain']['name']);
            $this->Domain->set($this->data);
            // Si validation : sauvegarde
            if ($this->Domain->validates()) {
                // Sauvegarde
                if ($this->Domain->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array('name')
                        ))) {
                    // Message
                    $this->Session->setFlash(__("The domains list", true) . " " . addslashes(__("has been updated.", true)));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/admin/element/domain');
                }
            }
        }
        // Chargement des données de pagination
        $this->set('data', $this->_getDomains());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Domain.page', $this->params['paging']['Domain']['options']['page']);
        $this->Session->write('Auth.User.pagination.Domain.order', $this->params['paging']['Domain']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'element' . DS . 'domains-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Fields of study", true));
        }

    }


    function country() {

        // Chargement du modèle
        $this->loadModel('Country');
        $this->Country->validationMessages();
        // Traitement des données de formulaire
        if (!empty($this->data)) {
            $this->data['Country']['name'] = ucfirst($this->data['Country']['name']);
            $this->Country->set($this->data);
            // Si validation : sauvegarde
            if ($this->Country->validates()) {
                // Sauvegarde
                if ($this->Country->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array('name')
                        ))) {
                    // Message
                    $this->Session->setFlash(__("The countries list", true) . " " . addslashes(__("has been updated.", true)));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/admin/element/country');
                }
            }
        }
        // Chargement des données de pagination
        $this->set('data', $this->_getCountries());
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Country.page', $this->params['paging']['Country']['options']['page']);
        $this->Session->write('Auth.User.pagination.Country.order', $this->params['paging']['Country']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'admin' . DS . 'element' . DS . 'countries-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Administrator", true) . ' &bull; ' . __("Countries", true));
        }

    }


    /**
     * Suppression d'un élément
     */
    function delete() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Nom de l'action / id de l'enregistrement à supprimer
            $actionToCall = (isset($this->params['pass'][0])) ? $this->params['pass'][0] : NULL;
            $idToDelete = (isset($this->params['pass'][1])) ? $this->params['pass'][1] : NULL;
            // Nom du modèle d'après celui de l'action
            $modelToUse = ucfirst($actionToCall);
            // Chargement du modèle
            $this->loadModel($modelToUse);
            // Suppression de l'enregistrement
            $this->$modelToUse->delete($idToDelete);
            // Appel de l'action pour rafraîchir la vue
            $this->setAction($actionToCall);
        }

    }


    /**
     *
     * @return <array>
     */
    private function _getUniversities() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.University')) {
            $page = $this->Session->read('Auth.User.pagination.University.page');
            $order = $this->Session->read('Auth.User.pagination.University.order');
            $this->Session->delete('Auth.User.pagination.University');
        } else {
            $page = NULL;
            $order = array('University.id' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            #'conditions' => array('id >' => 2),
            'fields' => array(
                'id',
                'secret',
                'name',
                'url'
            ),
            'page' => $page,
            'order' => $order,
            'limit' => 20,
            'recursive' => -1
        );
        if (!Configure::read('epel.admin.show_fict_univ')) {
            $this->paginate['conditions'] = array('id !=' => 2);
        }

        // Chargement des données
        $data = $this->paginate('University');

        // Suppression possible ?
        $this->loadModel('Student');
        $this->loadModel('Teacher');
        foreach ($data as $key => $value) {
            $used = (int) $this->Student->find('count', array(
                        'conditions' => array(
                            'Student.university_id' => $value['University']['id'],
                        )
                    ));
            $used += (int) $this->Teacher->find('count', array(
                        'conditions' => array(
                            'Teacher.university_id' => $value['University']['id'],
                        )
                    ));
            $data[$key]['University']['used'] = $used;
        }

        // Envoi de la pagination
        return $data;

    }


    /**
     *
     * @return <array>
     */
    private function _getLanguages() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Language')) {
            $page = $this->Session->read('Auth.User.pagination.Language.page');
            $order = $this->Session->read('Auth.User.pagination.Language.order');
            $this->Session->delete('Auth.User.pagination.Language');
        } else {
            $page = NULL;
            $order = array('Language.name' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            'conditions' => array('id !=' => '0001'),
            'fields' => array('id', 'name'),
            'page' => $page,
            'order' => $order,
            'limit' => 20,
            'recursive' => -1
        );

        // Chargement des données
        $data = $this->paginate('Language');

        // Suppression possible ?
        $this->loadModel('Portfolio');
        $this->loadModel('Certificate');
        foreach ($data as $key => $value) {
            $used = (int) $this->Portfolio->find('count', array(
                        'conditions' => array(
                            'Portfolio.language_id' => $value['Language']['id'],
                        )
                    ));
            $used += (int) $this->Certificate->find('count', array(
                        'conditions' => array(
                            'Certificate.language_id' => $value['Language']['id'],
                        )
                    ));
            $data[$key]['Language']['used'] = $used;
        }

        // Envoi de la pagination
        return $data;

    }


    /**
     *
     * @return <array>
     */
    private function _getCertificates() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Certificate')) {
            $page = $this->Session->read('Auth.User.pagination.Certificate.page');
            $order = $this->Session->read('Auth.User.pagination.Certificate.order');
            $this->Session->delete('Auth.User.pagination.Certificate');
        } else {
            $page = NULL;
            $order = array('Certificate.id' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            'conditions' => array('Certificate.id !=' => '0001'),
            'fields' => array(
                'id',
                'language_id',
                'certification_id'
            ),
            'contain' => array(
                'Language' => array(
                    'fields' => array('id', 'name')
                ),
                'Certification' => array(
                    'fields' => array('id', 'name', 'description')
                )
            ),
            'page' => $page,
            'order' => $order,
            'limit' => 20
        );

        // Chargement des données
        $data = $this->paginate('Certificate');

        // Suppression possible ?
        $this->loadModel('PoCertificate');
        foreach ($data as $key => $value) {
            $data[$key]['Certificate']['used'] = (int) $this->PoCertificate->find('count', array(
                        'conditions' => array(
                            'PoCertificate.certificate_id' => $value['Certificate']['id'],
                        )
                    ));
        }

        // Envoi de la pagination
        return $data;

    }


    /**
     *
     * @return <array>
     */
    private function _getCertifications() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Certification')) {
            $page = $this->Session->read('Auth.User.pagination.Certification.page');
            $order = $this->Session->read('Auth.User.pagination.Certification.order');
            $this->Session->delete('Auth.User.pagination.Certification');
        } else {
            $page = NULL;
            $order = array('Certification.name' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            'conditions' => array('id !=' => '0001'),
            'fields' => array(
                'id',
                'name',
                'description'
            ),
            'page' => $page,
            'order' => $order,
            'limit' => 20,
            'recursive' => -1
        );

        // Chargement des données
        $data = $this->paginate('Certification');

        // Suppression possible ?
        $this->loadModel('Certificate');
        foreach ($data as $key => $value) {
            $data[$key]['Certification']['used'] = (int) $this->Certificate->find('count', array(
                        'conditions' => array(
                            'Certificate.certification_id' => $value['Certification']['id'],
                        )
                    ));
        }

        // Envoi de la pagination
        return $data;

    }


    /**
     *
     * @return <array>
     */
    private function _getDomains() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Domain')) {
            $page = $this->Session->read('Auth.User.pagination.Domain.page');
            $order = $this->Session->read('Auth.User.pagination.Domain.order');
            $this->Session->delete('Auth.User.pagination.Domain');
        } else {
            $page = NULL;
            $order = array('Domain.name' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            'conditions' => array('id !=' => '0001'),
            'fields' => array('id', 'name'),
            'page' => $page,
            'order' => $order,
            'limit' => 20,
            'recursive' => -1
        );

        // Chargement des données
        $data = $this->paginate('Domain');

        // Suppression possible ?
        $this->loadModel('Student');
        foreach ($data as $key => $value) {
            $data[$key]['Domain']['used'] = $this->Student->find('count', array(
                        'conditions' => array(
                            'Student.domain_id' => $value['Domain']['id'],
                        )
                    ));
        }

        // Envoi de la pagination
        return $data;

    }


    /**
     *
     * @return <array>
     */
    private function _getCountries() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Country')) {
            $page = $this->Session->read('Auth.User.pagination.Country.page');
            $order = $this->Session->read('Auth.User.pagination.Country.order');
            $this->Session->delete('Auth.User.pagination.Country');
        } else {
            $page = NULL;
            $order = array('Country.name' => 'ASC');
        }

        // Chargement des données
        $this->paginate = array(
            'fields' => array('id', 'name'),
            'page' => $page,
            'order' => $order,
            'limit' => 20,
            'recursive' => -1
        );

        // Chargement des données
        $data = $this->paginate('Country');

        // Suppression possible ?
        $this->loadModel('Student');
        $this->loadModel('Visitor');
        foreach ($data as $key => $value) {
            $used = (int) $this->Student->find('count', array(
                        'conditions' => array(
                            'Student.origin_country_id' => $value['Country']['id'],
                        )
                    ));
            $used += (int) $this->Student->find('count', array(
                        'conditions' => array(
                            'Student.living_country_id' => $value['Country']['id'],
                        )
                    ));
            $used += (int) $this->Visitor->find('count', array(
                        'conditions' => array(
                            'Visitor.country_id' => $value['Country']['id'],
                        )
                    ));
            $data[$key]['Country']['used'] = $used;
        }

        // Envoi de la pagination
        return $data;

    }


}

