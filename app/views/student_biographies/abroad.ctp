
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-abroad"><?php __d('portfolio', "My stays abroad"); ?></h2>
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "Have you had any opportunities to visit a country in which you used this language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "In what circumstances did you use it?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What strategies did you use to make progress?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "What effects did your trip abroad have on your learning and mastering of the language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Was it a language learning trip, were you there to work or was it a holiday?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="abroad-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new experience"); ?></h2>
                        <?php echo $this->Form->create('PoAbroad', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/abroad/' . urlencode($language),
                            'id' => 'abroad-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoAbroad'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        $available = Configure::read('epel.locales.available');
                        $dateNum = $available[$this->params['language']]['dateNum'];
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('date', array(
                            'type' => 'date',
                            'label' => __d('portfolio', "Date", true),
                            'dateFormat' => $dateNum,
                            'minYear' => date('Y') - 40,
                            'maxYear' => date('Y'),
                            'separator' => ' ',
                            'empty' => false
                            ));
                        echo $this->Form->input('duration', array(
                            'type' => 'text',
                            'label' => __d('portfolio', "Duration", true)
                            ));
                        echo $this->Form->input('unit', array(
                            'label' => __d('portfolio', "Unit", true),
                            'options' => $durations,
                            'empty' => "…"
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <p class="info"><?php __d('portfolio', "If possible, write in the concerned language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/biography/abroad-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography'));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('student/biography/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog',
        'jquery.ui.datepicker',
        'i18n/jquery.ui.datepicker-' . $this->params['language']
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // UIDatePicker <-> Cake
    $('#PoAbroadDateMonth').after('<input class=\"datepicker\" type=\"hidden\"/>');
    setDatePicker();
    // Tips
    addTipInfo('.instruction');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#abroad-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#abroad-edit');
    // Initialisation du formulaire
    function initForm() {
        $('#abroad-edit-form #PoAbroadId').val('');
        $('#abroad-edit-form #PoAbroadDuration').val('');
        $('#abroad-edit-form #PoAbroadDescription').val('');
        $('#abroad-edit-form select :selected').removeAttr('selected');
    };
    // Soumission Ajax du formulaire
    $('#abroad-edit-form').submit(function() {
        if (!$('#abroad-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/biography/abroad/" . urlencode($language) . "',
                target: '#abroad',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#abroad-edit-form div.loading').show();
                },
                success: function() {
                    $('#abroad-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#abroad-toggle').hide();
                    $('#abroad-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#abroad-edit-form div.loading').hide();
                    $('#abroad-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>