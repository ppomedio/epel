<?php

/**
 * {app}/models/po_mediation.php
 */
class PoMediation extends AppModel {

    var $name = 'PoMediation';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'id'
        )
    );


    /**
     *
     *
     * @param <string> $studentId
     * @return <array>
     */
    function getMediation($studentId) {

        return $this->find('first', array(
            'conditions' => array('PoMediation.id' => $studentId),
            'fields' => array(
                'id',
                'description'
            ),
            'recursive' => -1
        ));

    }


}

