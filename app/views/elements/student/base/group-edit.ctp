
                    <section>
                        <h2><?php __("Group"); echo " (";  __("editing"); echo "…)"; ?></h2>
                        <?php echo $this->Form->create('Student', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/group',
                            'id' => 'group-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Student'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('group', array(
                            'label' => __("Group code", true)
                            ));
                        echo $this->Form->input('university_id', array(
                            'label' => __("College / University", true),
                            'options' => $university,
                            'disabled' => "disabled",
                            'empty' => __("Fictitious university", true)
                            )); ?>

                        <p class="info"><?php
                        __("In order to modify university-related information, please");
                        echo " " . $this->Html->link(__("contact the administrator", true),
                            array(
                                'controller' => 'contact',
                                'action' => 'form'
                            ),
                            array('class'=>'iframe contact')
                        );
                        ?>.</p>
                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#group-form');
");

?>