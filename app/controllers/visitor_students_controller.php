<?php

/**
 * {app}/controllers/visitor_students_controller.php
 *
 */
class VisitorStudentsController extends AppController {

    var $name = 'VisitorStudents';
    var $uses = array('Student');
    // Pagination
    var $paginate;
    // Composants
    var $components = array(
        'PortfolioLists',
        'L10nSort'
    );
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect('/' . $this->params['language'] . '/teacher/base' );

    }


    function search() {

        // Modification des règles de validation
        $this->Student->validateSearch();
        // Filtrage / pagination
        if (!empty($this->data['Student'])) {
            if (!empty($this->data['Student']['name'])
                    || !empty($this->data['Student']['firstname'])
                    || !empty($this->data['Student']['origin_country_id'])
                    || !empty($this->data['Student']['language_id'])
                    || !empty($this->data['Student']['mother'])
                    || !empty($this->data['Student']['usual'])
                    || !empty($this->data['Student']['files'])
                    || !empty($this->data['Student']['listening'])
                    || !empty($this->data['Student']['reading'])
                    || !empty($this->data['Student']['speak_int'])
                    || !empty($this->data['Student']['speak_prod'])
                    || !empty($this->data['Student']['writing'])
                    || !empty($this->data['Student']['university_id'])) {
                $this->Session->write('Auth.User.search', $this->data);
            } else {
                $this->Session->write('Auth.User.search', array());
            }
            $result = $this->_getStudents($this->data['Student']);
        } else {
            if ($this->Session->check('Auth.User.search.Student'))
                $result = $this->_getStudents($this->Session->read('Auth.User.search.Student'));
            else
                $result = $this->_getStudents();
        }
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Student.page', $this->params['paging']['Student']['options']['page']);
        $this->Session->write('Auth.User.pagination.Student.order', $this->params['paging']['Student']['options']['order']);
        // Données de vue communes
        $this->set('user', $result);
        if ($this->Session->read('Auth.User.real'))
            $this->set('university', $this->Student->University->getUniversityList(true));
        else
            $this->set('university', array('0002' => __("Fictitious university", true)));
        $this->set('country', $this->L10nSort->setList($this->Student->OriginCountry->find('list'), 'country'));
        $this->set('language', $this->L10nSort->setList($this->Student->Portfolio->Language->find('list'), 'language'));
        $this->set("competency", $this->PortfolioLists->getCompetenciesList());
        $this->set("level", $this->PortfolioLists->getLevelsList());
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'visitor' . DS . 'search' . DS . 'students-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Visitor", true) . ' &bull; ' . __("Search", true));
        }

    }


    function init_criteria() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Raz variable de session
            $this->Session->write('Auth.User.search', array());
            // Sortie
            return $this->setAction('search');
        }

    }


    /**
     * Filtrage paginé de la liste d'étudiants selon les critères
     *
     * @param <array> $filter
     * @return <array>
     */
    private function _getStudents($filter = array()) {

        // Type de visiteur
        $realVisitor = $this->Session->read('Auth.User.real');

        // Conditions
        $conditions = array();
        $conditions['Student.vis_hide !='] = 1;
        $conditions['Student.real'] = $realVisitor;
        if (!empty($filter['name']))
            $conditions['Student.name LIKE'] = $filter['name'] . '%';
        if (!empty($filter['firstname']))
            $conditions['Student.firstname LIKE'] = $filter['firstname'] . '%';
        if (!empty($filter['origin_country_id']))
            $conditions['Student.origin_country_id'] = $filter['origin_country_id'];
        if ($realVisitor) {
            if (!empty($filter['university_id']))
                $conditions['Student.university_id'] = $filter['university_id'];
        } else
            $conditions['Student.university_id'] = 2;

        // Jointure Portfolio
        $portfolioConditions = array();
        if (!empty($filter['language_id'])) {
            $portfolioConditions[] = 'Portfolio.language_id = ' . $filter['language_id'];
            if (!empty($filter['mother']))
                $portfolioConditions[] = 'Portfolio.mother >= ' . $filter['mother'];
            if (!empty($filter['usual']))
                $portfolioConditions[] = 'Portfolio.usual >= ' . $filter['usual'];
            if (!empty($filter['listening']))
                $portfolioConditions[] = 'Portfolio.listening >= ' . $filter['listening'];
            if (!empty($filter['reading']))
                $portfolioConditions[] = 'Portfolio.reading >= ' . $filter['reading'];
            if (!empty($filter['speak_int']))
                $portfolioConditions[] = 'Portfolio.speak_int >= ' . $filter['speak_int'];
            if (!empty($filter['speak_prod']))
                $portfolioConditions[] = 'Portfolio.speak_prod >= ' . $filter['speak_prod'];
            if (!empty($filter['writing']))
                $portfolioConditions[] = 'Portfolio.writing >= ' . $filter['writing'];
        }
        $joins = array();
        if (!empty($portfolioConditions)) {
            $portfolioConditions[] = 'Portfolio.student_id = Student.id';
            $joins[] = array(
                'table' => 'portfolios',
                'alias' => 'Portfolio',
                'type' => 'INNER',
                'foreignKey' => false,
                'conditions' => $portfolioConditions
            );
        }

        // Jointure PoDocument
        if (!empty($filter['language_id']) && !empty($filter['files'])) {
            $joins[] = array(
                'table' => 'po_documents',
                'alias' => 'PoDocument',
                'type' => 'INNER',
                'foreignKey' => false,
                'conditions' => 'PoDocument.portfolio_id = Portfolio.id'
            );
        }

        // Champs
        $fields = array(
            'DISTINCT Student.id',
            'Student.name',
            'Student.firstname',
            'Student.birth',
            'Student.origin_country_id',
            'Student.university_id',
            'Student.diploma_id',
            'Student.diploma_info',
            'Student.specialty'
        );

        // Champs des modèles associés
        $contain = array(
            'User' => array(
                'fields' => array(
                    'id',
                    'email',
                    'active',
                    'photo'
                )
            ),
            'OriginCountry' => array(
                'fields' => array('id', 'name')
            ),
            'University' => array(
                'fields' => array('id', 'name')
            ),
            'Diploma' => array(
                'fields' => array('id', 'name', 'level')
            ),
            'Portfolio' => array(
                'fields' => array(
                    'student_id',
                    'language_id',
                    'mother',
                    'usual'
                ),
                'Language' => array(
                    'fields' => array('id', 'name')
                )
            )
        );

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Student')) {
            $page = $this->Session->read('Auth.User.pagination.Student.page');
            $order = $this->Session->read('Auth.User.pagination.Student.order');
            $this->Session->delete('Auth.User.pagination.Student');
        } else {
            $page = NULL;
            $order = array('Student.name' => 'ASC');
        }

        // Paramétrage
        $this->paginate = array(
            'conditions' => $conditions,
            'joins' => $joins,
            'fields' => $fields,
            'contain' => $contain,
            'page' => $page,
            'order' => $order,
            'limit' => 12
        );

        // Chargement des données
        $data = $this->paginate('Student');

        // Post traitement
        foreach ($data as $studentKey => $studentValue) {
            // Réorganisation des langues de Portfolio
            $portfolio = array();
            foreach ($studentValue['Portfolio'] as $key => $value) {
                $portfolio[strtolower($value['Language']['name'])]['name'] = $value['Language']['name'];
                $portfolio[strtolower($value['Language']['name'])]['mother'] = $value['mother'];
                $portfolio[strtolower($value['Language']['name'])]['usual'] = $value['usual'];
            }
            unset($data[$studentKey]['Portfolio']);
            $data[$studentKey]['Portfolio'] = $this->L10nSort->sortLocalized($portfolio);
            // Traduction des noms d'université statiques
            if ((int) $data[$studentKey]['University']['id'] < 3)
                $data[$studentKey]['University']['name'] = __($data[$studentKey]['University']['name'], true);
        }

        // Envoi de la pagination
        #$this->log($data, 'debug');
        return $data;

    }


}

