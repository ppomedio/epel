
                    <div id="howtolearn-update">
                        <section>
                            <h2 id="howtolearn-other-title"><?php __d('portfolio', "Generally speaking…"); ?></h2>
                            <div class="instruction">
                                <p><?php
                                __d('portfolio', "How have I learnt the langues that I know?");
                                ?></p>
                                <p><?php
                                __d('portfolio', "What are the strategies have proved successful? For which languages?");
                                ?></p>
                                <p><?php
                                __d('portfolio', "Could they be reproduced to improve my level?");
                                ?></p>
                                <p><?php
                                __d('portfolio', "How could I apply the strategies which have allowed me to learn my mother tongue to the learning of another language?");
                                ?></p>
                                <p class="info"><?php
                                __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                                ?></p>
                            </div>
                        </section>
                        <section>
                            <?php echo $this->Form->create('PoHowtolearn', array(
                                'url' => '/' . Configure::read('Config.langCode')
                                    . '/student/biography/howtolearn',
                                'id' => 'howtolearn-other-form',
                                'class' => 'full'
                                ));
                            ?>

                            <fieldset><?php
                            echo $this->Form->input('other', array(
                                'label' => false,
                                'rows' => '25'
                                ));
                            ?>

                            </fieldset>
                            <?php
                            echo $this->Form->hidden('part', array('default' => 5));
                            echo $this->Form->end(__("Update", true)); ?>

                        </section>
                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#howtolearn-update > section > div.instruction');
    // Instructions
    if ($('#howtolearn-intro').is(':visible'))
        $('#howtolearn-intro').slideUp('slow');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#howtolearn-other-form div.submit input[type=submit]');
    // Activation du submit
    $('#howtolearn-other-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#howtolearn-other-form textarea').keydown(function() {
        if ($('#howtolearn-other-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-other-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#howtolearn-other-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/howtolearn/5',
        target: '#howtolearn-update',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#howtolearn-other-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#howtolearn-other-form div.loading').show();
            $('#howtolearn-other-title').html('" . addslashes(__("Loading…", true)) . "');
            $('#howtolearn-update').fadeTo('fast', 0.6);
        },
        success: function() {
            $('#howtolearn-other-form div.loading').hide();
            $('#howtolearn-update').fadeTo('slow', 1);
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>