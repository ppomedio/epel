<?php

/**
 * {app}/models/po_certificate.php
 */
class PoCertificate extends AppModel {

    var $name = 'PoCertificate';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array(
        'Containable',
        'CleanData'
    );
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        ),
        'Certificate' => array(
            'className' => 'Certificate',
            'foreignKey' => 'certificate_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'certificate_info' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please enter name of certificate.", true)
            ),
            'organism' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please enter the organism name.", true)
            )
        );

        parent::__construct();

    }


    function beforeValidate() {

        // Suppression des règles "non-vide" si le certificat n'est pas listé
        if ($this->data[$this->alias]['certificate_id'] != '0001')
            unset($this->validate['certificate_info']);

        // Sinon la validation échoue
        return true;

    }


}

