
<?php if (isset($forgotten)) {
    __("Your password was reset.");
} else {
    __("Your account has been created.");
} ?>

<?php echo __("Username", true) . __(":", true) . ' ' . $data['username']; ?>

<?php echo __("Password", true) . __(":", true) . ' ' . $data['password']; ?>

<?php if (isset($inactive)) {
__("Your account is currently inactive: it needs to be authorized by the administrator. You will receive another email when it will be activated.");
} ?>

<?php __("For security reasons, please change your password as soon as possible."); ?>
