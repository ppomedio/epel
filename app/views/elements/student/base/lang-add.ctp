
                    <section>
                        <h2><?php __("Add a language"); ?></h2>
                        <?php echo $this->Form->create('Portfolio', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/language/add',
                            'id' => 'language-add-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Portfolio'); // Validation jQuery
                        ?>
                        <fieldset>
                        <p class="info"><?php
                        __("Add a new language to your Portfolio by selecting one of the languages below. If the language you want to add is not on the list, please");
                        echo " " . $this->Html->link(__("contact the administrator", true),
                            array(
                                'controller' => 'contact',
                                'action' => 'form'
                            ),
                            array('class'=>'iframe contact')
                        );
                        ?>.</p>
                        <?php
                        echo $this->Form->input('language_id', array(
                            'label' => __("Select a language", true),
                            'options' => $newLanguages,
                            'empty' => "…"
                            ));
                        ?>

                        <p class="info"><?php __("Languages will appear in the Portfolio according to their alphabetical order."); ?></p>
                        </fieldset>
                        <?php echo $this->Form->end(__("Add", true)); ?>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#language-add-form');
");

?>