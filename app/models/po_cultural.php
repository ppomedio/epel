<?php

/**
 * {app}/models/po_cultural.php
 */
class PoCultural extends AppModel {

    var $name = 'PoCultural';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please define type of experience.", true)
            ),
            'description' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please describe experience.", true)
            )
        );

        parent::__construct();

    }


}

