<?php

/**
 * {app}/models/visit.php
 */
class Visit extends AppModel {

    var $name = 'Visit';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('Containable');
    // Relations
    var $belongsTo = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'student_id'
        ),
        'Visitor' => array(
            'className' => 'Visitor',
            'foreignKey' => 'visitor_id'
        )
    );


    function visitExists($visit) {

        return $this->find('first', array(
            'conditions' => array(
                'Visit.student_id' => $visit['Visit']['student_id'],
                'Visit.visitor_id' => $visit['Visit']['visitor_id'],
                'Visit.date' => $visit['Visit']['date']
            ),
            'fields' => array('id'),
            'recursive' => -1
        ));

    }


}

