<?php

/**
 * {app}/models/po_heritage.php
 */
class PoHeritage extends AppModel {

    var $name = 'PoHeritage';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'id'
        )
    );


    /**
     *
     *
     * @param <string> $studentId
     * @return <array>
     */
    function getHeritage($studentId) {

        return $this->find('first', array(
            'conditions' => array('PoHeritage.id' => $studentId),
            'fields' => array(
                'id',
                'description'
            ),
            'recursive' => -1
        ));

    }


}

