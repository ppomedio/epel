<?php

/**
 * {app}/controllers/components/app_email.php
 *
 */
App::import('Component', 'Email');

class AppEmailComponent extends EmailComponent {


    /**
     * Startup component
     *
     * @param object $controller Instantiating controller
     * @access public
     */
    function startup(&$controller) {

        /**
         * Chargement config SMTP
         */
        $smtp = Configure::read('epel.smtp');
        if (!empty($smtp)) {
            $this->delivery = 'smtp';
            // Options SMTP
            $this->smtpOptions = array(
                'port' => $smtp['port'],
                'timeout' => $smtp['timeOut'],
                'host' => $smtp['host'],
                'username' => Configure::read('smtp.username'),
                'password' => Configure::read('smtp.password'),
                'client' => Configure::read('smtp.client'),
            );
        }
        /**
         * Encodage
         */
        $this->charset = Configure::read('App.encoding');
        /**
         * Type d'envoi
         */
        $this->sendAs = 'both';
        /**
         * Adresses par défaut (optionnelles)
         */
        $this->to = Configure::read('epel.email.to');
        $this->from = Configure::read('epel.email.from');
        /**
         * Sujet
         */
        $this->subject = '[' . __("eELP", true) . '] ';

    }


}

