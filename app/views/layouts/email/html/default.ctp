<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title><?php echo $title_for_layout; ?></title>
        <style type="text/css">
            <!--
            body { color: #666666; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; }
            h3 { color: #3399FF; font-size: 12px; }
            a:link, a:visited, a:active { color: #333333; font-style: normal; text-decoration: none; }
            a:hover { color: #3399FF; }
            -->
        </style>
    </head>
    <body>
        <?php echo $content_for_layout;?>

    </body>
</html>