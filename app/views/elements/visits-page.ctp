<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#visit-page',
    'before' => $this->Js->get('#visit-page')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="visit-page" class="table"><?php
                            if (empty($user['Visit'])) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __("Visitor"); ?></th>
                                        <th><?php __("Structure / Function"); ?></th>
                                        <th><?php __("Contact"); ?></th>
                                        <th><?php __("Date"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="4"><?php __("… nobody at the moment."); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="2" class="title-name"><?php echo $paginator->sort(__("Visitor", true), 'Visitor.name'); ?></th>
                                        <th class="title-structure"><?php echo $paginator->sort(__("Structure / Function", true), 'Visitor.structure'); ?></th>
                                        <th><?php __("Contact"); ?></th>
                                        <th class="title-date"><?php echo $paginator->sort(__("Date", true), 'Visit.date'); ?></th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user['Visit'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>
                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php
                                        if (!empty($value['visitorPhoto'])) {
                                            $pathInfo = pathinfo($value['visitorPhoto']);
                                            ?><img src="/files/users/<?php echo $value['visitorId'] . '/photo/th_' . $value['visitorId'] . '.' . $pathInfo['extension']; ?>" />
                                        <?php } else {
                                            ?><img src="/img/user_thumb.png" />
                                        <?php
                                        } ?></td>
                                        <td class="visitor-name"><?php
                                        echo $this->Html->link($value['visitorName'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['visitorId'] . '_'
                                                        . $value['visitorFirstname'] . ' '
                                                        . $value['visitorName'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['visitorId'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['visitorFirstname'];
                                        ?></td>
                                        <td class="smaller"><?php
                                        $visitorInfo = '';
                                        if (!empty($value['visitorStructure']))
                                            $visitorInfo = '<span class="bolder">' . $value['visitorStructure'] . '</span>';
                                        if (!empty($value['visitorUrl'])) {
                                            if (!empty($visitorInfo))
                                                $visitorInfo .= '<br />';
                                            $visitorInfo .= '<span class="smaller"><a href="' . $value['visitorUrl'] . '" id="' . __("Visit", true) . ' ' . $value['visitorUrl'] . '" class="link">' . $value['visitorUrl'] . '</a></span>';
                                        }
                                        if (!empty($value['visitorFunction'])) {
                                            if (!empty($visitorInfo))
                                                $visitorInfo .= '<br />';
                                            $visitorInfo .= '<span class="smaller">' . $value['visitorFunction'] . '</span>';
                                        }
                                        if (empty($visitorInfo))
                                            echo '…';
                                        else
                                            echo $visitorInfo;
                                        ?></td>
                                        <td class="smaller"><?php
                                        $visitorInfo = '';
                                        if (!empty($value['visitorAddress']))
                                            $visitorInfo = nl2br($value['visitorAddress']);
                                        if (!empty($value['visitorCountry'])) {
                                            if (!empty($visitorInfo))
                                                $visitorInfo .= '<br />';
                                            $visitorInfo .= __d('country', $value['visitorCountry'], true);
                                        }
                                        if (!empty($visitorInfo))
                                            $visitorInfo = '<span class="smaller">' . $visitorInfo . '</span><br />';
                                        if (!empty($value['visitorPhone']))
                                            $visitorInfo .= '<span class="symbol-phone"></span> ' . $value['visitorPhone'];
                                        if (empty($visitorInfo))
                                            echo '…';
                                        else
                                            echo $visitorInfo;
                                        ?></td>
                                        <td class="smaller"><?php echo $this->Date->Convert($value['date']); ?></td>
                                        <div id="data_contact-user_<?php echo $value['visitorId']; ?>" class="inline tooltip-data"><?php
                                            echo __("Send an email to", true) . ' ' . $value['visitorFirstname'] . ' ' . $value['visitorName'];
                                        ?></div>
                                    </tr><?php
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'visit-page'));
                            } ?>

                            <div id="data_sort-title-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-title-structure" class="tooltip-data"><?php __("Sort by structure"); ?></div>
                            <div id="data_sort-title-date" class="tooltip-data"><?php __("Sort by date"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Id, classes et cie
    $('#visit-page table thead tr th.title-name a').attr('id', 'sort-title-name').addClass('sort-help');
    $('#visit-page table thead tr th.title-structure a').attr('id', 'sort-title-structure').addClass('sort-help');
    $('#visit-page table thead tr th.title-date a').attr('id', 'sort-title-date').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#visit-page table thead tr th');
    tooltipLink('#visit-page table tbody tr td');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#visit-page').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-visit-page').html('" . addslashes(__("This Portfolio has been visited by…", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>