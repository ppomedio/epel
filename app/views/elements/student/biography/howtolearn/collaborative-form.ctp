
                    <div id="howtolearn-update">
                        <section>
                            <h2 id="howtolearn-collaborative-title"><?php __d('portfolio', "Collaborative activities"); ?></h2>
                            <?php
                            // Jeu de questions
                            $question = array(
                                401 => __d('portfolio', "learn from working with others?", true),
                                __d('portfolio', "contribute to a working group?", true),
                                __d('portfolio', "carry out my responsibilities in a working group?", true),
                                __d('portfolio', "help to identify and assign particular roles in a working group?", true)
                            );
                            // Formulaire
                            echo $this->Form->create('PoHowtolearn', array(
                                'url' => '/' . Configure::read('Config.langCode')
                                    . '/student/biography/howtolearn',
                                'id' => 'howtolearn-collaborative-form',
                                'class' => 'full table'
                                ));
                            ?>
                            <table class="howtolearn-table">
                                <thead>
                                    <tr>
                                        <?php
                                        for ($n = 1; $n < 7; $n++) {
                                            echo "<th>" . $tableHead[$n] . "</th>";
                                        } ?>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    for ($n = 401; $n < 405; $n++) {
                                        $idPart = (string)$n;
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        echo '<tr class="'. $trClass . '"><td>' . $question[$n] . '</td>';
                                        echo '<td>' . $this->Form->radio($idPart . 'answer', $options, $attributes) . '</td>';
                                        echo '<td class="master">' . $this->Form->checkbox($idPart . 'target') . '</td>';
                                        echo '<td>' . $this->Form->input($idPart . 'date', array('label' => false)) . '</td></tr>';
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->Form->hidden('part', array('default' => 4));
                            echo $this->Form->end(__("Update", true)); ?>

                        </section>
                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // iButtons
    $('#howtolearn-collaborative-form table tbody input[type=checkbox]').iButton({
        labelOn: '" . __('YES', true) . "',
        labelOff: '" . __('NO', true) . "',
        resizeHandle: false,
        resizeContainer: true
        })
        .change(function() {
            // Activation du submit
            if ($('#howtolearn-collaborative-form div.submit input[type=submit]').attr('disabled'))
                $('#howtolearn-collaborative-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Instructions
    if ($('#howtolearn-intro').is(':hidden'))
        $('#howtolearn-intro').slideDown('slow');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#howtolearn-collaborative-form div.submit input[type=submit]');
    // Activation du submit
    $('#howtolearn-collaborative-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#howtolearn-collaborative-form input[type=radio]').change(function() {
        if ($('#howtolearn-collaborative-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-collaborative-form div.submit input[type=submit]').removeAttr('disabled');
    });
    $('#howtolearn-collaborative-form input[type=text]').keydown(function() {
        if ($('#howtolearn-collaborative-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-collaborative-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#howtolearn-collaborative-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/howtolearn/4',
        target: '#howtolearn-update',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#howtolearn-collaborative-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#howtolearn-collaborative-form div.loading').show();
            $('#howtolearn-collaborative-title').html('" . addslashes(__("Loading…", true)) . "');
            $('#howtolearn-update').fadeTo('fast', 0.6);
        },
        success: function() {
            $('#howtolearn-collaborative-form div.loading').hide();
            $('#howtolearn-update').fadeTo('slow', 1);
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>