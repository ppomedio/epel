<?php

/**
 * {app}/controllers/student_biographies_controller.php
 *
 */
class StudentBiographiesController extends AppController {

    var $name = 'StudentBiographies';
    // Modèle
    var $uses = 'Portfolio';
    // Composants
    var $components = array(
        'PortfolioLists',
        'SideMenu'
    );
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu'
    );


    function beforeFilter() {

        if (($this->action != 'heritage')
                && ($this->action != 'plurilingualism')
                && ($this->action != 'mediation')
                && ($this->action != 'howtolearn')) {

            // Chargement de la langue en cours / des ID de portfolios
            $portfolioLanguage = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
            $portfolios = $this->Session->read('Auth.User.portfolios');
            // Si portfolio vide ou langue demandée non ouverte : redirection
            if (empty($portfolios) || ($portfolioLanguage && !array_key_exists($portfolioLanguage, $portfolios)))
                $this->redirect('/' . $this->params['language'] . '/student/base');
            // Stockage de la langue active pour les autres méthodes
            Configure::write('portfolioLanguage', $portfolioLanguage);
            // Menu latéral
            $this->SideMenu->setSideMenu('Biography');
        }

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'base');

    }


    function heritage() {

        // Chargement du modèle
        $this->loadModel('PoHeritage');

        // Enregistrement Ajax
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            if (!empty($this->data)) {
                $this->data['PoHeritage']['id'] = $this->AppAuth->user('id');
                $this->PoHeritage->set($this->data);
                if ($this->PoHeritage->save($this->data, $validate = false))
                    $this->data = $this->PoHeritage->getHeritage($this->AppAuth->user('id'));
            }
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'heritage-form');
            return;
        }

        // Traitement normal
        if (!empty($this->data)) {
            // Envoi des données au modèle
            $this->data['PoHeritage']['id'] = $this->AppAuth->user('id');
            $this->PoHeritage->set($this->data);
            // Sauvegarde
            if ($this->PoHeritage->save($this->data)) {
                // Message
                $this->Session->setFlash(__d('portfolio', "Your Biography has been updated.", true));
            }
        }

        // Chargement des données
        $this->data = $this->PoHeritage->getHeritage($this->AppAuth->user('id'));

        // Copyright CercleS
        $addCopyright['link'] = "CercleS";
        $addCopyright['url'] = Configure::read('epel.cercles.url');
        $this->set("addCopyright", $addCopyright);

        // Variables de vue
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __("Heritage languages", true));

    }


    function plurilingualism() {

        // Chargement du modèle
        $this->loadModel('PoPlurilingualism');

        // Enregistrement Ajax
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            if (!empty($this->data)) {
                $this->data['PoPlurilingualism']['id'] = $this->AppAuth->user('id');
                $this->PoPlurilingualism->set($this->data);
                if ($this->PoPlurilingualism->save($this->data, $validate = false))
                    $this->data = $this->PoPlurilingualism->getPlurilingualism($this->AppAuth->user('id'));
            }
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'plurilingualism-form');
            return;
        }

        // Traitement normal
        if (!empty($this->data)) {
            // Envoi des données au modèle
            $this->data['PoPlurilingualism']['id'] = $this->AppAuth->user('id');
            $this->PoPlurilingualism->set($this->data);
            // Sauvegarde
            if ($this->PoPlurilingualism->save($this->data)) {
                // Message
                $this->Session->setFlash(__d('portfolio', "Your Biography has been updated.", true));
            }
        }

        // Chargement des données
        $this->data = $this->PoPlurilingualism->getPlurilingualism($this->AppAuth->user('id'));

        // Copyright CercleS
        $addCopyright['link'] = "CercleS";
        $addCopyright['url'] = Configure::read('epel.cercles.url');
        $this->set("addCopyright", $addCopyright);

        // Variables de vue
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __("Plurilingualism", true));

    }


    function mediation() {

        // Chargement du modèle
        $this->loadModel('PoMediation');

        // Enregistrement Ajax
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            if (!empty($this->data)) {
                $this->data['PoMediation']['id'] = $this->AppAuth->user('id');
                $this->PoMediation->set($this->data);
                if ($this->PoMediation->save($this->data, $validate = false))
                    $this->data = $this->PoMediation->getMediation($this->AppAuth->user('id'));
            }
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'mediation-form');
            return;
        }

        // Traitement normal
        if (!empty($this->data)) {
            // Envoi des données au modèle
            $this->data['PoMediation']['id'] = $this->AppAuth->user('id');
            $this->PoMediation->set($this->data);
            // Sauvegarde
            if ($this->PoMediation->save($this->data)) {
                // Message
                $this->Session->setFlash(__d('portfolio', "Your Biography has been updated.", true));
            }
        }

        // Chargement des données
        $this->data = $this->PoMediation->getMediation($this->AppAuth->user('id'));

        // Copyright CercleS
        $addCopyright['link'] = "CercleS";
        $addCopyright['url'] = Configure::read('epel.cercles.url');
        $this->set("addCopyright", $addCopyright);

        // Variables de vue
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __("Translation and mediation", true));

    }


    function valuable() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if (!empty($this->data['PoValuable']['id'])) {
                // Mise à jour
                $this->Portfolio->PoValuable->set($this->data);
            } else {
                // Création
                $this->data['PoValuable']['portfolio_id'] = $portfolios[$language]['id'];
                $this->Portfolio->PoValuable->create($this->data);
            }
            // Enregistrement
            $this->Portfolio->PoValuable->save($this->data);
        }

        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getValuableData($portfolios[$language]['id']));

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'valuable-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'valuable', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'valuable' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . $languageTitle);
        }

    }


    function contact() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if (!empty($this->data['PoContact']['id'])) {
                // Mise à jour
                $this->Portfolio->PoContact->save($this->data);
            } else {
                // Création
                $this->data['PoContact']['portfolio_id'] = $portfolios[$language]['id'];
                $this->Portfolio->PoContact->create($this->data);
            }
            // Enregistrement
            $this->Portfolio->PoContact->save($this->data);
        }

        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getContactData($portfolios[$language]['id']));

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'contact-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'contact', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'contact' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . $languageTitle);
        }

    }


    function abroad() {

        // Assistant "Date"
        $this->helpers[] = 'Date';

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if (!empty($this->data['PoAbroad']['id'])) {
                // Mise à jour
                $this->Portfolio->PoAbroad->set($this->data);
            } else {
                // Création
                $this->data['PoAbroad']['portfolio_id'] = $portfolios[$language]['id'];
                $this->Portfolio->PoAbroad->create($this->data);
            }
            // Enregistrement
            $this->Portfolio->PoAbroad->save($this->data);
        }

        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getAbroadData($portfolios[$language]['id']));
        $this->set("durations", $this->PortfolioLists->getDurationsList());

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'abroad-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'abroad', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'abroad' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . $languageTitle);
        }

    }


    function cultural() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if (!empty($this->data['PoCultural']['id'])) {
                // Mise à jour
                $this->Portfolio->PoCultural->set($this->data);
            } else {
                // Création
                $this->data['PoCultural']['portfolio_id'] = $portfolios[$language]['id'];
                $this->Portfolio->PoCultural->create($this->data);
            }
            // Enregistrement
            $this->Portfolio->PoCultural->save($this->data);
        }

        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getCulturalData($portfolios[$language]['id']));

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'cultural-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'cultural', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'cultural' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . $languageTitle);
        }

    }


    function employment() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if (!empty($this->data['PoEmployment']['id'])) {
                // Mise à jour
                $this->Portfolio->PoEmployment->set($this->data);
            } else {
                // Création
                $this->data['PoEmployment']['portfolio_id'] = $portfolios[$language]['id'];
                $this->Portfolio->PoEmployment->create($this->data);
            }
            // Enregistrement
            $this->Portfolio->PoEmployment->save($this->data);
        }

        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getEmploymentData($portfolios[$language]['id']));

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'employment-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'employment', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'employment' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . $languageTitle);
        }

    }


    function strategy() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $languages = $this->Session->read('Auth.User.languages');
        $portfolios = $this->Session->read('Auth.User.portfolios');
        $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];

        // Traitement Ajax
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Enregistrement
            $this->data['Portfolio']['id'] = $portfolios[$language]['id'];
            $this->Portfolio->set($this->data);
            $this->Portfolio->save($this->data, $validate = false);
            // Sortie
            $this->autoRender = false;
            exit();
        }

        // Maj navigation : cookie + variable de session
        $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'strategy', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
        $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'strategy' . DS . urlencode($language));

        // Entête des tableaux
        $this->set('tableHead', array(
            1 => __d('portfolio', "No", true),
            __d('portfolio', "Sometimes", true),
            __d('portfolio', "Regularly", true),
            __d('portfolio', "Systematically", true)
        ));

        // Jeu de questions
        $this->set('question', array(
            1 => __d('portfolio', "I plan out my learning on a weekly basis and stick to it.", true),
            __d('portfolio', "I visit the language centre in my university and make the most of tutoring available there.", true),
            __d('portfolio', "I can find and select the language learning resources I need (particularly, those online).", true),
            __d('portfolio', "When I am seeking to understand a language item (in grammar or pronunciation, for example), I usually start by getting and accessing the necessary materials, consulting publications and websites, doing exercises and checking the correction until I no longer make mistakes.", true),
            __d('portfolio', "I practice pronunciation by imitating native speakers (from videos or songs, for example).", true),
            __d('portfolio', "I study my teachers’ comments on my written assignments and carefully correct my errors.", true),
            __d('portfolio', "I know how to build up a team around me or a network to help me carry out language tasks.", true)
        ));

        // Init. boutons radio
        $this->set('options', array('no' => '', 'sometimes' => '', 'regularly' => '', 'systematically' => ''));
        $this->set('attributes', array(
            'label' => false,
            'legend' => false,
            #'value' => false,
            'separator' => "</td><td>"
        ));

        // Traitement normal
        if (!empty($this->data)) {
            // Envoi des données au modèle
            $this->data['Portfolio']['id'] = $portfolios[$language]['id'];
            $this->Portfolio->set($this->data);
            // Sauvegarde
            if ($this->Portfolio->save($this->data, $validate = false)) {
                // Message
                $this->Session->setFlash(__d('portfolio', "Your Biography has been updated.", true));
            }
        }

        // Chargement des données
        $this->data = $this->Portfolio->getStrategy($portfolios[$language]['id']);

        // Variables de vue
        $this->set('language', $language);
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __d('language', $languageName, true));

    }


    function objective() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if (!empty($this->data['PoObjective']['id'])) {
                // Mise à jour
                $this->Portfolio->PoObjective->set($this->data);
            } else {
                // Création
                $this->data['PoObjective']['portfolio_id'] = $portfolios[$language]['id'];
                $this->Portfolio->PoObjective->create($this->data);
            }
            // Enregistrement
            $this->Portfolio->PoObjective->save($this->data);
        }

        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getObjectiveData($portfolios[$language]['id']));

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'objective-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.biography_' . $language, 'objective', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Biography.submenu.' . $languageName . '.action', 'objective' . DS . urlencode($language));
            // Affichage normal
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __d('language', $languageName, true));
        }

    }


    function howtolearn() {

        // Entête des tableaux
        $this->set('tableHead', array(
            1 => __d('portfolio', "Can I…", true),
            __d('portfolio', "Never", true),
            __d('portfolio', "Sometimes", true),
            __d('portfolio', "Always", true),
            __d('portfolio', "New target", true),
            __d('portfolio', "Target achieved", true) . ' <span class="smaller">(' . __d('checklists', "date", true) . ')</span>'
        ));

        // Init. boutons radio
        $this->set('options', array('never' => '', 'sometimes' => '', 'always' => ''));
        $this->set('attributes', array(
            'label' => false,
            'legend' => false,
            #'value' => false,
            'separator' => "</td><td>"
        ));

        // Chargement du modèle
        $this->loadModel('PoHowtolearn');

        // Partie en cours (chapitre)
        if (isset($this->data['PoHowtolearn']['part'])) {
            $part = $this->data['PoHowtolearn']['part'];
            unset($this->data['PoHowtolearn']['part']);
        } else if (isset($this->params['pass'][0])) {
            $part = $this->params['pass'][0];
        } else if ($cookie = $this->Cookie->read($this->AppAuth->user('id') . '.biography_howtolearn')) {
            $part = $cookie;
        } else
            $part = 1;

        // Pose du cookie de navigation
        $this->Cookie->write($this->AppAuth->user('id') . '.biography_howtolearn', $part, Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));

        /*
         * Traitement Ajax
         */

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Enregistrement
            if (!empty($this->data)) {
                // Envoi des données au modèle
                $this->data['PoHowtolearn']['id'] = $this->AppAuth->user('id');
                $this->PoHowtolearn->set($this->data);
                // Sauvegarde
                $this->PoHowtolearn->save($this->data, $validate = false);
            }
            // Chargement / rechargement des données enregistrées (nettoyage…)
            $this->data = $this->PoHowtolearn->getHowtolearn($this->AppAuth->user('id', $part));
            // Rafraîchissement du formulaire concerné
            $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __("Learning how to learn", true));
            $this->set('ajax', true);
            switch ($part) {
                case 1:
                    $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'howtolearn' . DS . 'motivation-form');
                    break;
                case 2:
                    $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'howtolearn' . DS . 'general-form');
                    break;
                case 3:
                    $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'howtolearn' . DS . 'formal-form');
                    break;
                case 4:
                    $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'howtolearn' . DS . 'collaborative-form');
                    break;
                case 5:
                    $this->render(DS . 'elements' . DS . 'student' . DS . 'biography' . DS . 'howtolearn' . DS . 'other-form');
                    break;
                default:
                    break;
            }
            // Sortie
            return;
        }

        /*
         * Traitement normal
         */

        // Enregistrement
        if (!empty($this->data)) {
            // Envoi des données au modèle
            $this->data['PoHowtolearn']['id'] = $this->AppAuth->user('id');
            $this->PoHowtolearn->set($this->data);
            // Sauvegarde
            if ($this->PoHowtolearn->save($this->data, $validate = false)) {
                // Message
                $this->Session->setFlash(__d('portfolio', "Your Biography has been updated.", true));
            }
        }

        // Chargement des données
        $this->data = $this->PoHowtolearn->getHowtolearn($this->AppAuth->user('id', $part));

        // Copyright CercleS
        $addCopyright['link'] = "CercleS";
        $addCopyright['url'] = Configure::read('epel.cercles.url');
        $this->set("addCopyright", $addCopyright);

        // Variables de vue
        $this->set("part", $part);
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; " . __("Learning how to learn", true));

    }


    /**
     * Suppression d'un élément
     */
    function delete() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Nom de l'action / id de l'enregistrement à supprimer
            $actionToCall = (isset($this->params['pass'][1])) ? $this->params['pass'][1] : NULL;
            $idToDelete = (isset($this->params['pass'][2])) ? $this->params['pass'][2] : NULL;
            // Nom du modèle d'après celui de l'action
            $modelToUse = 'Po' . ucfirst($actionToCall);
            // Suppression de l'enregistrement
            $this->Portfolio->$modelToUse->delete($idToDelete);
            // Appel de l'action pour rafraîchir la vue
            $this->setAction($actionToCall);
        }

    }


    /**
     *
     * @return <array>
     */
    private function _getValuableData($portfolio) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoValuable.portfolio_id' => $portfolio
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'name',
                'description',
                'modified'
            ),
            'order' => 'PoValuable.modified DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoValuable');
        // Mise en forme
        $exp = array();
        foreach ($data as $key => $value) {
            $exp['PoValuable'][$key] = $value['PoValuable'];
        }
        // Envoi
        return $exp;

    }


    /**
     *
     * @return <array>
     */
    private function _getContactData($portfolio) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoContact.portfolio_id' => $portfolio
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'name',
                'description',
                'modified'
            ),
            'order' => 'PoContact.modified DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoContact');
        // Mise en forme
        $exp = array();
        foreach ($data as $key => $value) {
            $exp['PoContact'][$key] = $value['PoContact'];
        }
        // Envoi
        return $exp;

    }


    /**
     *
     * @return <array>
     */
    private function _getAbroadData($portfolio) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoAbroad.portfolio_id' => $portfolio
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'date',
                'duration',
                'unit',
                'description',
                'modified'
            ),
            'order' => 'PoAbroad.date DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoAbroad');
        // Mise en forme
        $exp = array();
        $date = array();
        foreach ($data as $key => $value) {
            $exp['PoAbroad'][$key] = $value['PoAbroad'];
            // explode() via Ajax fait planter Apache…
            $date = explode('-', $value['PoAbroad']['date']);
            $exp['PoAbroad'][$key]['day'] = $date[2];
            $exp['PoAbroad'][$key]['month'] = $date[1];
            $exp['PoAbroad'][$key]['year'] = $date[0];
        }
        // Envoi
        return $exp;

    }


    /**
     *
     * @return <array>
     */
    private function _getCulturalData($portfolio) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoCultural.portfolio_id' => $portfolio
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'name',
                'description',
                'modified'
            ),
            'order' => 'PoCultural.modified DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoCultural');
        // Mise en forme
        $exp = array();
        foreach ($data as $key => $value) {
            $exp['PoCultural'][$key] = $value['PoCultural'];
        }
        // Envoi
        return $exp;

    }


    /**
     *
     * @return <array>
     */
    private function _getEmploymentData($portfolio) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoEmployment.portfolio_id' => $portfolio
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'name',
                'description',
                'modified'
            ),
            'order' => 'PoEmployment.modified DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoEmployment');
        // Mise en forme
        $exp = array();
        foreach ($data as $key => $value) {
            $exp['PoEmployment'][$key] = $value['PoEmployment'];
        }
        // Envoi
        return $exp;

    }


    /**
     *
     * @return <array>
     */
    private function _getObjectiveData($portfolio) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoObjective.portfolio_id' => $portfolio
            ),
            'limit' => 5,
            'fields' => array(
                'id',
                'portfolio_id',
                'description',
                'progress',
                'modified'
            ),
            'order' => 'PoObjective.modified DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoObjective');
        // Mise en forme
        $exp = array();
        foreach ($data as $key => $value) {
            $exp['PoObjective'][$key] = $value['PoObjective'];
        }
        // Envoi
        return $exp;

    }


}

