<?php

/**
 * {app}/controllers/view_biographies_controller.php
 *
 */
class ViewBiographiesController extends AppController {

    var $name = 'ViewBiographies';
    var $uses = array('Portfolio');
    // Composants
    var $components = array(
        'PortfolioLists',
        'L10nSort'
    );
    // Assistants
    var $helpers = array(
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Pas de variable de session = déconnexion
        if(!$this->Session->check('Auth.User.view'))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        // Données de vue : affichage des langues du Portfolio
        $portfolios = $this->Session->read('Auth.User.view.portfolios');
        $languages = array();
        foreach ($portfolios as $key => $value) {
            $languages[$value['name']]['mother'] = $value['mother'];
            $languages[$value['name']]['usual'] = $value['usual'];
        }
        $this->set('languages', $this->L10nSort->sortLocalized($languages));

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // Eject
        $this->redirect(array(
            'language' => $this->params['language'],
            'controller' => 'users',
            'action' => 'logout'
        ));

    }


    function show() {

        // Chargement de la langue en cours
        $language = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
        $portfolios = $this->Session->read('Auth.User.view.portfolios');

        // Validité de l'url
        if (!array_key_exists($language, $portfolios))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        // Données de la vue
        $this->set('user', $this->Portfolio->getBiography($portfolios[$language]['id']));
        $this->set('mother', $portfolios[$language]['mother']);
        $this->set('usual', $portfolios[$language]['usual']);
        $this->set("durations", $this->PortfolioLists->getDurationsList());
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; "
                . __d('language', $portfolios[$language]['name'], true));

    }


    function heritage() {

        // Données de la vue
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; "
                . __("Heritage languages", true));

    }


    function plurilingualism() {

        // Données de la vue
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; "
                . __("Plurilingualism", true));

    }


    function mediation() {

        // Données de la vue
        $this->set('title_for_layout', __("Language Biography", true) . " &bull; "
                . __("Translation and mediation", true));

    }


}

