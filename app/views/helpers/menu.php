<?php

/**
 * {app}/views/helpers/menu.php
 *
 */
class MenuHelper extends AppHelper {

    // Assistants
    var $helpers = array('Html', 'Session');
    // Propriétés
    private $_baseCtrl;


    /**
     * Rendu de la partie du menu stockée en session
     *
     * @param <boolean> mode vue
     * @return <string> le marquage indenté pour le menu
     */
    function renderMenu($view = false) {

        // Nom du contrôleur
        $this->_baseCtrl = ($view) ? 'view' : $this->Session->read('Auth.User.role');

        // Création et envoi du marquage
        return "\n" . $this->_buildMenu($this->Session->read('Menu'), false, 5);

    }


    /**
     * Construction du marquage pour une partie de menu
     *
     * @param <array>   tableau associatif contenant le menu
     * @param <boolean> marquage encapsulé dans <ul></ul>
     * @param <int>     nombre de tabulations (groupes de 4 espaces)
     * @return <string> marquage indenté pour la partie traitée
     */
    private function _buildMenu($data, $setUl, $nTab) {

        $tab = '';
        $out = '';

        for ($n = 0; $n < $nTab; $n++) {
            $tab .= "    ";
        }

        if ($setUl)
            $out .= $tab . "<ul>\n";

        foreach ($data as $key => $item) {

            // Détection de l'option courante
            $action = isset($item['action']) ? '/' . $item['action'] : NULL;
            if (isset($item['extCtrl'])) {
                $extCtrl = true;
                $extCtrlCurrent = strpos($this->params['url']['url'], $item['extCtrl']);
                if ($action && $extCtrlCurrent)
                    $current = strpos($this->params['url']['url'], $action);
                else
                    $current = strpos(strrchr($this->params['url']['url'], '/'), $item['extCtrl']);
            } else {
                $extCtrl = false;
                $current = stripos($this->params['url']['url'], substr($key, 0, -1));
            }
            if ($current !== false)
                $li = 'li class="current"';
            else
                $li = 'li';

            // Traduction si nom de langue
            if (isset($item['lang_file'])) {
                $title = __d($item['lang_file'], $key, true);
                $class = 'language';
            } else {
                $title = __($key, true);
                $class = '';
            }

            // Assemblage suivant le type d'option de menu
            if ($extCtrl)
                $out .= $tab . "    <" . $li . ">"
                        . $this->Html->link($title,
                                '/' . Configure::read('Config.langCode')
                                . '/' . $this->_baseCtrl
                                . '/' . $item['extCtrl']
                                . $action,
                                array('class' => $class)
                        ) . "\n";
            else if (isset($item['fancy']))
                $out .= $tab . "    <" . $li . ">"
                        . $this->Html->link($title,
                                array(
                                    'controller' => 'guide',
                                    'action' => $item['fancy']
                                ),
                                array('class' => 'iframe ' . $item['fancy'])
                        ) . "\n";
            else
                $out .= $tab . "    <" . $li . "><span>" . $title . "</span>" . "\n";

            // Auto-appel si sous-menu
            if (isset($item['submenu']) && is_array($item['submenu']))
                $out .= $this->_buildMenu($item['submenu'], true, $n + 2);

            // Balise fermante
            $out .= $tab . "    </li>\n";
        }

        // Balise fermante
        if ($setUl)
            $out .= $tab . "</ul>\n";

        // Retour
        return $out;

    }


}

