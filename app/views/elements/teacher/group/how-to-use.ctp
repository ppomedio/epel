
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="ref-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="people"></dt>
                                <dd><?php __("Change the referring teacher of a group"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit group code"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Delete a group"); ?></dd>
                                <dt class="gear"></dt>
                                <dd><?php __("Shift edition panel"); ?></dd>
                            </dl>
                            <p class="info"><?php __("You can send email to the other teachers by clicking on their email address."); ?></p>
                        </div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#ref-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#ref-info');
");
*/
?>
