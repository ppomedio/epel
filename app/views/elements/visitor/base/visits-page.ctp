<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#visit-page',
    'before' => $this->Js->get('#visit-page')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="visit-page" class="table"><?php
                            if (empty($user['Visit'])) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="student-name"><?php __("Student"); ?></th>
                                        <th class="student-university"><?php __("College / University"); ?></th>
                                        <th class="visit-date"><?php __("Date"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3"><?php __("You did not visit any Portfolio at the moment."); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="student-name"><?php echo $paginator->sort(__("Student", true), 'Student.name'); ?></th>
                                        <th class="student-university"><?php echo $paginator->sort(__("College / University", true), 'Student.university_id'); ?></th>
                                        <th class="visit-date"><?php echo $paginator->sort(__("Date", true), 'Visit.date'); ?></th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user['Visit'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>
                                    <tr class="<?php echo $trClass; ?>">
                                        <td><a href="/<?php echo $this->params['language']; ?>/view/student/init/<?php
                                        echo base64_encode($value['studentId']); ?>" id="see-link_<?php echo $value['studentId']; ?>" class="bolder link"><?php
                                        echo $value['studentName']; ?></a><br /><?php echo $value['studentFirstname'];?></td>
                                        <td class="smaller"><?php echo $value['studentUniversity']; ?></td>
                                        <td class="smaller"><?php echo $this->Date->Convert($value['date']); ?></td>
                                        <div id="data_see-link_<?php echo $value['studentId']; ?>" class="inline tooltip-data"><?php
                                            __("See the Portfolio of"); echo ' ' . $value['studentFirstname'] . ' ' . $value['studentName'];
                                        ?></div>
                                    </tr><?php
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'visit-page', 'half' => true));
                            } ?>

                            <div id="data_sort-student-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-student-university" class="tooltip-data"><?php __("Sort by College / University"); ?></div>
                            <div id="data_sort-visit-date" class="tooltip-data"><?php __("Sort by date"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Id, classes et cie
    $('#visit-page table thead tr th.student-name a').attr('id', 'sort-student-name').addClass('sort-help');
    $('#visit-page table thead tr th.student-university a').attr('id', 'sort-student-university').addClass('sort-help');
    $('#visit-page table thead tr th.visit-date a').attr('id', 'sort-visit-date').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#visit-page table thead tr th');
    tooltipLink('#visit-page table tbody tr td');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#visit-page').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-visit-page').html('" . addslashes(__("Visits", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>