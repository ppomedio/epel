<?php

$paginator->options(array(
    'update' => '#work',
    ));

?>

                        <div id="work" class="table"><?php
                            if (empty($user)) { ?>

                            <table class="dossier document">
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "File name"); ?></th>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <div id="work-loading" class="loading"></div>
                                            <div id="head-control">
                                                <span id="work-add" class="add"></span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="5">
                                             <p class="alert"><?php
                                             __d('portfolio', "Your Dossier currently contains no process material for this language.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#work');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table class="dossier document">
                                <thead>
                                    <tr>
                                        <th class="filename"><?php echo $paginator->sort(__d('portfolio', "File name", true), 'PoDocument.filename', array('url' => $this->params['pass'])); ?></th>
                                        <th class="type"><?php echo $paginator->sort(__d('portfolio', "Type", true), 'PoDocument.type', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <div id="work-loading" class="loading"></div>
                                            <div id="head-control">
                                                <span id="work-add" class="add"></span>
                                                <span id="work-toggle" class="gear"></span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoDocument'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="work-filename_<?php echo $value['id']; ?>"><?php
                                        echo $value['filename'] . '<br /><p class="xsmaller">' . __d('portfolio', "Last update", true) . __(":", true) . '<br /><span class="infotext">' . $this->Date->Convert($value['modified']) . '</span></p>';
                                        ?></td>
                                        <td id="work-type_<?php echo $value['id']; ?>"><span class="picto-<?php echo $value['type']; ?>"></span></td>
                                        <td id="work-description_<?php echo $value['id']; ?>"><?php
                                        if ($value['description'])
                                            echo nl2br($value['description']);
                                        else
                                            __d('portfolio', "Description to come…");
                                        ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="file hidden"><?php
                                            echo $value['file'];
                                            ?></span>
                                            <span class="filename hidden"><?php
                                            echo $value['filename'];
                                            ?></span>
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="work-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="work-replace_<?php echo $value['id']; ?>" class="upload"></span>
                                                <span id="work-move_<?php echo $value['id']; ?>" class="move link"></span>
                                                <span id="work-download_<?php echo $value['id']; ?>" class="download link"></span>
                                                <span id="work-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_work-download_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        echo __d('portfolio', "Download", true) . " " . $value['filename'];
                                    ?></div>
                                    <div id="data_work-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this document");
                                    ?></div>
                                    <div id="data_work-move_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        echo __d('portfolio', "Move this document to the “Display” part.", true);
                                    ?></div>
                                    <div id="data_work-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this document");
                                    ?></div>
                                    <div id="work-move-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        echo __d('portfolio', "Please confirm move document to the “Display” part", true) . ' (' . $value['filename'] . ').';
                                    ?></div>
                                    <div id="work-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete document"); echo ' (' . $value['filename'] . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'work'));
                            } ?>

                            <div id="data_sort-work-filename" class="tooltip-data"><?php __("Sort by file name"); ?></div>
                            <div id="data_sort-work-type" class="tooltip-data"><?php __("Sort by type"); ?></div>
                            <div id="data_sort-work-level" class="tooltip-data"><?php __d('portfolio', "Sort by certified level"); ?></div>
                            <div id="data_work-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial panneau d'édition & bascule
    if ($('#work-edit-form #PoDocumentId').val() == '') {
        $('#work-toggle').hide();
        $('#work-edit').slideUp('slow');
    } else if ($('#work-edit').is(':hidden'))
        $('#work-toggle').hide();
    // Id, classes et cie
    $('#work table thead tr th.filename a').attr('id', 'sort-work-filename').addClass('sort-help');
    $('#work table thead tr th.type a').attr('id', 'sort-work-type').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#work table thead');
    tooltipGear('#work table thead tr th.controls');
    tooltipLink('#work table tbody tr td');
    tooltipEdit('#work table tbody tr td.controls div.controls');
    tooltipSupp('#work table tbody tr td.controls div.controls');
    // Déplacement d'un document
    $('#work table tbody td.controls span.move').unbind('click').bind('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#work-move-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    $(this).dialog('close');
                    $('#control_' + itemId).hide();
                    $('#loading_' + itemId).show();
                    if ($('#work-edit-form #PoDocumentId').val() == itemId) {
                        // Initialisation du formulaire
                        $('#work-edit-form #PoDocumentId').val('');
                        $('#work-edit-form #PoDocumentFilename').val('');
                        $('#work-edit-form #PoDocumentDescription').val('');
                    }
                    $('#work').load('/" . $this->params['language'] . "/student/dossier/move/" . urlencode($language) . "/work/show/' + itemId, function() {
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
        return false;
    });
    // Remplacement de document
    $('#work table tbody td.controls span.upload').each(function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        new AjaxUpload($(this), {
            action: '/" . $this->params['language'] . "/student/dossier/upload/" . urlencode($language) . "',
            name: 'data[PoDocument][filename]',
            autoSubmit: true,
            responseType: false,
            tooltipId: '" . addslashes(__d('portfolio', "Replace file", true)) . ' ' ."' + $('#hidden-info_' + itemId + ' .filename').text(),
            onChange: function(file, extension){},
            onSubmit: function(file, extension) {
                this.setData({kind : 'work', id : itemId, file : $('#hidden-info_' + itemId + ' .file').text()});
                $('#divToolTip').fadeOut('fast');
                $('#control_' + itemId).hide();
                $('#loading_' + itemId).show();
                $('#title-work').html('" . addslashes(__("Uploading…", true)) . "');
            },
            onComplete: function(file, response) {
                var extract = new Array();
                var resp = new Array();
                extract = response.split('|response|');
                resp = extract[1].split('=');
                if (resp[0] === 'error') {
                    $('#title-work').html('" . addslashes(__d('portfolio', "Process", true)) . "');
                    $('#loading_' + itemId).hide();
                    $('#control_' + itemId).show();
                    $('#content div#ajaxMessage')
                        .text(resp[1])
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                } else {
                    $('#work').load('/" . $this->params['language'] . "/student/dossier/work/" . urlencode($language) . "', function() {
                        if ($('#work-edit-form #PoDocumentId').val() == itemId)
                            $('#work-edit-form #PoDocumentFilename').val(resp[1]);
                        $('#loading_' + itemId).hide();
                        $('#control_' + itemId).show();
                        $('#content div#ajaxMessage')
                            .text('" . __d('portfolio', "Your Dossier has been updated.", true) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                }
            }
        });
    });
    // Téléchargement d'un fichier
    $('#work table tbody td span.download').unbind('click').bind('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $(location).attr('href', '/" . $this->params['language'] . "/user/download/' + $('#hidden-info_' + itemId + ' .file').text() + '/' + $('#hidden-info_' + itemId + ' .filename').text());
        return false;
    });
    // Téléversement d'un fichier
    new AjaxUpload('#work-add', {
        action: '/" . $this->params['language'] . "/student/dossier/upload/" . urlencode($language) . "',
        name: 'data[PoDocument][filename]',
        autoSubmit: true,
        responseType: false,
        tooltipId: '" . addslashes(__d('portfolio', "Add a new document", true)) . "',
        onChange: function(file, extension){},
        onSubmit: function(file, extension) {
            this.setData({kind : 'work'});
            $('#divToolTip').fadeOut('fast');
            $('#head-control').hide();
            $('#work-loading').show();
            $('#title-work').html('" . addslashes(__("Uploading…", true)) . "');
        },
        onComplete: function(file, response) {
            var extract = new Array();
            var resp = new Array();
            extract = response.split('|response|');
            resp = extract[1].split('=');
            if (resp[0] === 'error') {
                $('#title-work').html('" . addslashes(__d('portfolio', "Process", true)) . "');
                $('#work-loading').hide();
                $('#head-control').show();
                $('#content div#ajaxMessage')
                    .text(resp[1])
                    .show(function(){
                        $(this).delay(3600).hide('slow');
                });
            } else {
                $('#work').load('/" . $this->params['language'] . "/student/dossier/work/" . urlencode($language) . "', {newDocId : resp[1]}, function() {
                    $('#work-loading').hide();
                    $('#head-control').show();
                    $('#content div#ajaxMessage')
                        .text('" . __d('portfolio', "Your Dossier has been updated.", true) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                    });
                });
            }
        }
    });
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#work-toggle').live('click', function() {
        if ($('#work-edit').is(':hidden'))
            $('#work-edit').slideDown('slow');
        else {
            $('#work-edit').slideUp('slow');
            if ($('#work-edit-form #PoDocumentId').val() == '')
                $('#work-toggle').fadeOut('slow');
        }
    });
    // Édition d'un document
    $('#work table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        // Chargement des données dans le formulaire
        $('#work-edit-form #PoDocumentId').val(itemId);
        $('#work-edit-form #PoDocumentFilename').val($('#hidden-info_' + itemId + ' .filename').text());
        $('#work-edit-form #PoDocumentDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Titre
        $('#title-work-edit').html('" . addslashes(__d('portfolio', "Edit a document", true)) . "');
        // Affichage si masqué
        if ($('#work-edit').is(':hidden')) {
            $('#divToolTip').fadeOut('fast');
            $('#work-add').hide();
            $('#work-edit').slideDown('slow');
            $('#work-add').fadeIn('slow');
            $('#work-toggle').fadeIn('slow');
        };
        return false;
    });
    // Suppression d'un document
    $('#work table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#work-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    var file = $('#hidden-info_' + itemId + ' .file').text();
                    $(this).dialog('close');
                    $('#control_' + itemId).hide();
                    $('#loading_' + itemId).show();
                    if ($('#work-edit-form #PoDocumentId').val() == itemId) {
                        // Initialisation du formulaire
                        $('#work-edit-form #PoDocumentId').val('');
                        $('#work-edit-form #PoDocumentFilename').val('');
                        $('#work-edit-form #PoDocumentDescription').val('');
                    }
                    $('#work').load('/" . $this->params['language'] . "/student/dossier/delete_doc/" . urlencode($language) . "/work/' + itemId + '/' + file, function() {
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
        return false;
    });
");
} else {
    if (isset($newDoc))
        $initEdit = "
    // État initial formulaire
    $('#work-edit-form #PoDocumentId').val('" . $newDoc['PoDocument']['id'] . "');
    $('#work-edit-form #PoDocumentFilename').val('" . $newDoc['PoDocument']['filename'] . "');
    $('#work-edit-form #PoDocumentDescription').val('" . $newDoc['PoDocument']['description'] . "');
    $('#title-work-edit').html('" . addslashes(__d('portfolio', "Create a document", true)) . "');
    $('#work-edit').slideDown('slow');
    $('#work-toggle').show();";
    else
        $initEdit = '';
    $this->Js->Buffer($initEdit . "
    $('#title-work').html('" . addslashes(__d('portfolio', "Process", true)) . "');");
    // Sortie des scripts pour affichage Ajax
    echo $this->Js->writeBuffer();
}
?>