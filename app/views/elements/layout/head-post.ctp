
        <!--[if lt IE 9]>
            <script type="text/javascript" src="/cjs/ie/html5.js"></script>
            <script type="text/javascript" src="/cjs/ie/IE9.js"></script>
            <![endif]-->
        <?php echo $this->Html->css('html5reset', 'stylesheet', array('media'=>'all'));?>
        <?php echo $this->Html->css(
            /*array(
                'base/epel.main',
                'base/epel.forms',
                'base/epel.jquery',
                'cake.debug'
            ),*/
            array(
                'epel.base',
                'cake.debug'
            ),
            'stylesheet',
            array(
                'media'=>'screen',
                'inline' => true
            )
        ); ?>
        <!--[if lte IE 8]>
            <link rel="stylesheet" type="text/css" media="screen" href="/ccss/epel.ie8.css" />
            <![endif]-->
        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" media="screen" href="/ccss/epel.ie7.css" />
            <![endif]-->
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="icon" type="image/png" href="/img/favicon.png" /><?php

        /**
         * Sortie des scripts de base
         */
        echo $this->Html->script(
            /*array(
                'base/jquery',
                'base/jquery.ui.core',
                'base/jquery.tooltip',
                'base/jquery.fancybox',
                'base/jquery.hoverintent',
                'base/jquery.cookies',
                'base/epel.interface'
            ),*/
            array('epel.base'),
            array('inline' => true)
        ); ?>
        <script type="text/javascript">
        //<![CDATA[
        // Contrôle cookies & JavaScript
        if ($.cookies.test()) {
            $.cookies.set('<?php echo Configure::read('epel.cookie.name'); ?>[js_check]', '1');
        }
        //]]>
        </script>
    </head>