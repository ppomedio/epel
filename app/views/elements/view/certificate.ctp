
                        <h2><?php __d('portfolio', "Certificates"); ?></h2>
                        <div id="certificate" class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Date"); ?></th>
                                        <th><?php __d('portfolio', "Delivered by"); ?></th>
                                        <th><span id="certificate-level" class="help"><?php __d('portfolio', "Level(s)"); ?></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoCertificate'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>
                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php
                                        if ($value['certificate_id'] == '0001')
                                            echo '<strong>' . $value['certificate_info'] . '</strong>';
                                        else {
                                            echo '<strong>' . $value['Certificate']['Certification']['name'] . '</strong>';
                                            if ($value['Certificate']['Certification']['description'])
                                                echo "<br /><em>" .  $value['Certificate']['Certification']['description'] . "</em>";
                                        }
                                        ?></td>
                                        <td><?php echo $this->Date->Convert($value['date']); ?></td>
                                        <td><?php echo $value['organism']; ?></td>
                                        <td><?php
                                        $comp = array();
                                        foreach($competencies as $key => $competency) {
                                            if ($value[$key])
                                                $comp[] = $competency . __(":", true) . '&nbsp;<span class="bolder infotext">' . $levels[$value[$key]] . '</span>';
                                        }
                                        if (!empty($comp))
                                            echo implode("<br />", $comp);
                                        ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="data_certificate-level" class="tooltip-data"><?php
                        __d('portfolio', "CEFRL level, specified only if it appears on the original certificate.");
                        ?></div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipHelp('#certificate thead');
");

?>