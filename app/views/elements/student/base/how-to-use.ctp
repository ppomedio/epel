
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="base-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="upload"></dt>
                                <dd><?php __("Upload a new image"); ?></dd>
                                <dt class="add"></dt>
                                <dd><?php __("Add"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Delete"); ?></dd>
                                <?php
                                if ($this->Session->read('Auth.User.portfolios')) { ?>
                                <dt class="see"></dt>
                                <dd><?php __("Check what the visitors of my Portfolio can see for this language"); ?></dd>
                                <?php
                                } ?>
                            </dl>
                        </div><?php
                        if (!empty($user['Portfolio'])) { ?>

                        <h2><?php __("Visibility"); ?></h2>
                        <div id="search-info" class="information"><?php
                            if (((int)$user['University']['id'] > 1) && !empty($user['Group']['Teacher'])) { ?>

                            <p class="info"><strong><?php __("Teachers"); ?>&nbsp:</strong> <?php __("self-assessment checklists, as well as the “Learning how to learn”, “My learning strategies” and “My learning objectives” parts of the Biography, are never visible to teachers."); ?></p>
                            <?php } ?>

                            <p class="info"><strong><?php __("Visitors"); ?>&nbsp:</strong> <?php __("self-assessment checklists, as well as the “Learning how to learn”, “My learning strategies” and “My learning objectives” parts of the Biography and the Process Dossier are never visible to visitors."); ?></p>
                        </div>
                        <?php } ?>

                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#base-info');
                    addTipInfo('#search-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#base-info');
    addTipInfo('#search-info');
");
*/
?>
