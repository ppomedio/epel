
        <h3><?php __("A new account has been created."); ?></h3>
        <p><strong><?php echo __("Account type", true) . __(":", true); ?></strong> <?php __(ucfirst($data['role'])); ?><br />
        <strong><?php echo __("Name", true) . __(":", true); ?></strong> <?php echo $data['name']; ?><br />
        <strong><?php echo __("First name", true) . __(":", true); ?></strong> <?php echo $data['firstname'];
        if ($data['role'] == 'teacher') { ?>
        <br /><strong><?php echo __("University", true) . __(":", true); ?></strong> <?php echo $data['university']; ?>
        <?php
        } else if ($data['role'] == 'visitor') { ?>
        <br /><strong><?php echo __("Structure", true) . __(":", true); ?></strong> <?php echo $data['structure']; ?>
        <br /><strong><?php echo __("Function", true) . __(":", true); ?></strong> <?php echo $data['function']; ?>
        <?php
        } ?></p>
        <p><em><?php __("This account will remain inactive until you check it and decide its activation."); ?></em></p>
