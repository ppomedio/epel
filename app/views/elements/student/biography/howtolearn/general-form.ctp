
                    <div id="howtolearn-update">
                        <section>
                            <h2 id="howtolearn-general-title"><?php __d('portfolio', "General learning activities"); ?></h2>
                            <?php
                            // Jeu de questions
                            $question = array(
                                201 => __d('portfolio', "identify my strengths and weaknesses in the target language generally?", true),
                                __d('portfolio', "identify my learning strengths and weaknesses?", true),
                                __d('portfolio', "analyse the language and learning demands of a particular language assignment?", true),
                                __d('portfolio', "set short-term learning objectives?", true),
                                __d('portfolio', "identify suitable media/materials for learning?", true),
                                __d('portfolio', "plan a timescale for my learning objectives?", true),
                                __d('portfolio', "assess my progress in relation to these objectives?", true),
                                __d('portfolio', "use reference materials effectively (dictionary, grammar etc.)?", true),
                                __d('portfolio', "use effective strategies to learn and memorise new information?", true)
                            );
                            // Formulaire
                            echo $this->Form->create('PoHowtolearn', array(
                                'url' => '/' . Configure::read('Config.langCode')
                                    . '/student/biography/howtolearn',
                                'id' => 'howtolearn-general-form',
                                'class' => 'full table'
                                ));
                            ?>
                            <table class="howtolearn-table">
                                <thead>
                                    <tr>
                                        <?php
                                        for ($n = 1; $n < 7; $n++) {
                                            echo "<th>" . $tableHead[$n] . "</th>";
                                        } ?>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    for ($n = 201; $n < 210; $n++) {
                                        $idPart = (string)$n;
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        echo '<tr class="'. $trClass . '"><td>' . $question[$n] . '</td>';
                                        echo '<td>' . $this->Form->radio($idPart . 'answer', $options, $attributes) . '</td>';
                                        echo '<td class="master">' . $this->Form->checkbox($idPart . 'target') . '</td>';
                                        echo '<td>' . $this->Form->input($idPart . 'date', array('label' => false)) . '</td></tr>';
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->Form->hidden('part', array('default' => 2));
                            echo $this->Form->end(__("Update", true)); ?>

                        </section>
                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // iButtons
    $('#howtolearn-general-form table tbody input[type=checkbox]').iButton({
        labelOn: '" . __('YES', true) . "',
        labelOff: '" . __('NO', true) . "',
        resizeHandle: false,
        resizeContainer: true
        })
        .change(function() {
            // Activation du submit
            if ($('#howtolearn-general-form div.submit input[type=submit]').attr('disabled'))
                $('#howtolearn-general-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Instructions
    if ($('#howtolearn-intro').is(':hidden'))
        $('#howtolearn-intro').slideDown('slow');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#howtolearn-general-form div.submit input[type=submit]');
    // Activation du submit
    $('#howtolearn-general-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#howtolearn-general-form input[type=radio]').change(function() {
        if ($('#howtolearn-general-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-general-form div.submit input[type=submit]').removeAttr('disabled');
    });
    $('#howtolearn-general-form input[type=text]').keydown(function() {
        if ($('#howtolearn-general-form div.submit input[type=submit]').attr('disabled'))
            $('#howtolearn-general-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#howtolearn-general-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/howtolearn/2',
        target: '#howtolearn-update',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#howtolearn-general-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#howtolearn-general-form div.loading').show();
            $('#howtolearn-general-title').html('" . addslashes(__("Loading…", true)) . "');
            $('#howtolearn-update').fadeTo('fast', 0.6);
        },
        success: function() {
            $('#howtolearn-general-form div.loading').hide();
            $('#howtolearn-update').fadeTo('slow', 1);
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>