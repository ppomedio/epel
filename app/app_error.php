<?php
/**
 * Classe de gestion des erreurs
 *
 */

class AppError extends ErrorHandler {

    /**
     * Redirige les sorties d'erreur vers le layout par défaut de CakePHP
     *
     */
    function _outputMessage($template) {
        $this->controller->layout = 'default';
        return parent::_outputMessage($template);
    }

}

