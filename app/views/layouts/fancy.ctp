<!DOCTYPE html>
<html lang="<?php echo $this->Session->read('Config.language'); ?>" class="fancy">
    <head>
        <?php echo $this->Html->charset(); ?>

        <title><?php __('eELP');
            echo ' &bull; ' . $title_for_layout; ?></title>
        <!--[if lt IE 9]>
            <script type="text/javascript" src="/cjs/ie/html5.js"></script>
            <script type="text/javascript" src="/cjs/ie/IE9.js"></script>
            <![endif]-->
        <?php echo $this->Html->css('html5reset', 'stylesheet', array('media'=>'all'));
        echo $this->Html->css(
            /*array(
                'base/epel.main',
                'base/epel.forms',
                'base/epel.jquery'
            ),*/
            array('epel.base'),
            'stylesheet',
            array('media'=>'screen')
        ); ?>

        <!--[if lte IE 8]>
            <link rel="stylesheet" type="text/css" media="screen" href="/ccss/epel.ie8.css" />
            <![endif]-->

        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" media="screen" href="/ccss/epel.ie7.css" />
            <![endif]--><?php

        /**
         * Sortie des scripts de base
         */
        echo $this->Html->script(
            /*array(
                'base/jquery',
                'base/jquery.ui.core',
                'base/jquery.tooltip',
                'base/jquery.fancybox',
                'base/jquery.hoverintent',
                'base/jquery.cookies',
                'base/epel.interface'
            ),*/
            array('epel.base'),
            array('inline' => true)
        );

        /**
         * Sortie des scripts spécifiques
         */
        echo $scripts_for_layout;

        ?>

    </head>
    <body class="fancy">
        <?php

        echo $content_for_layout;

        /**
         * Sortie du buffer Js
         */
        echo "\n\t" . $this->Js->writeBuffer(array(
                'inline' => true,
                'cache' => false,
                'clear' => true,
                'onDomReady' => true,
                'safe' => true
                )
            ) . "\n";

        ?>
    </body>
</html>
