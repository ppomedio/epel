
                    <div id="group-change">
                        <h2><?php __("Add a group"); ?></h2>
                        <?php echo $this->Form->create('TeGroup', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/teacher/group/ref',
                            'id' => 'group-change-form',
                            'class' => ''
                            ));
                        echo $validation->bind('TeGroup'); // Validation jQuery
                        ?>
                        <fieldset id="group-list">
                            <?php
                            echo $this->Form->input('prevGroup', array(
                                'label' => __("Groups list", true),
                                'options' => $group,
                                'empty' => array(0 => __("New group", true) . '…')
                                ));
                            echo $this->Form->hidden('prevGroupCode');
                            ?>

                        </fieldset>
                        <fieldset id="group-code">
                            <?php
                            echo $this->Form->input('group', array(
                                'label' => __("Group code", true)
                                ));
                            ?>

                        </fieldset>
                        <fieldset>
                            <dl id="group-merge">
                                <dt id="group-merge-label" class="help"><?php echo $this->Form->label('update', __("Merge groups", true)); ?></dt>
                                <dd><?php echo $this->Form->checkbox(
                                        'group_merge',
                                        array('checked' => false)
                                        ); ?></dd>
                            </dl>
                            <dl id="ref-teacher">
                                <dt id="ref-teacher-label" class="help"><?php echo $this->Form->label('ref', __("Ref. teacher", true)); ?></dt>
                                <dd><?php echo $this->Form->checkbox(
                                        'ref_teacher',
                                        array('checked' => true)
                                        ); ?></dd>
                            </dl>
                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </div>
                    <div id="group-info">
                        <h2 id="title-group-info" class="gear"><?php __("Group management"); ?></h2>
                        <div>
                            <p class="info"><?php __("Students themselves feed the group list."); ?></p>
                            <p><?php __("When they set their group code, this group is added to the list if it does not already appear in it."); ?></p>
                            <p><?php __("As a teacher, you can manage the groups by:"); ?></p>
                            <ul>
                                <li><?php __("creating new ones,"); ?></li>
                                <li><?php __("modifying existing groups codes,"); ?></li>
                                <li><?php __("merging groups,"); ?></li>
                                <li><?php __("being or not the referring teacher of a group,"); ?></li>
                                <li><?php __("deleting a group (only if it does not contain any student)."); ?></li>
                            </ul>
                            <p class="info"><?php __("As the referring teacher of a group, your name appears in the students' “Base” section, and they can send you email through the contact form."); ?></p>
                            <div class="tail">
                                <span id="group-edit" class="gear"></span>
                            </div>
                        </div>
                        <div id="data_title-group-info" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        <div id="data_group-edit" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        <div id="data_group-merge-label" class="tooltip-data"><?php __("Set to YES to group students in one group (target), and delete the other (source). In all cases, students will be moved to the new group."); ?></div>
                        <div id="data_ref-teacher-label" class="tooltip-data"><?php __("Choose YES to be the referring teacher for the new group, NO to create a new group without a referring teacher."); ?></div>
                    </div>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ibutton'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

// Boîte d'édition visible si erreur de validation
switch ($invalid) {
    case 'add':
        // Visibilité
        $initState = "    $('#group-merge').hide();\n";
        $initState .= "    $('#ref-teacher').show();\n";
        break;
    case 'edit':
        // Visibilité
        $initState = "    $('#ref-teacher').hide();\n";
        $initState .= "    $('#group-merge').show();\n";
        // Textes
        $initState .= "    $('#group-change h2').html('" . addslashes(__("Edit group code", true)) . "');\n";
        $initState .= "    $('#group-list label').html('" . addslashes(__("Target group", true)) . "');\n";
        $initState .= "    $('#group-code label').html('" . addslashes(__("New code for this group", true)) . "');\n";
        break;
    default:
        // Visibilité
        $initState = "    $('#group-merge').hide();\n";
        $initState .= "    $('#ref-teacher').show();\n";
        $initState .= "    $('div#group-change').hide();\n";
        // Reset option par défaut
        $initState .= "    $('#TeGroupPrevGroup option[selected]').removeAttr('selected');\n";
        $initState .= "    $('#TeGroupPrevGroup option[value=\'0\']').attr('selected', 'selected');\n";

}

$this->Js->Buffer("
    // iButtons
    $('#group-change').show();
    $('#group-change-form fieldset dl dd input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        });
    // État initial
" . $initState . "
    // Bascule fonctionnelle
    $('#TeGroupPrevGroup').change(function() {
        if ($(this).val() == 0) {
            // Ajouter un groupe
            $('#group-change h2').html('" . addslashes(__("Add a group", true)) . "');
            $('#group-list label').html('" . addslashes(__("Groups list", true)) . "');
            $('#group-code label').html('" . addslashes(__("Group code", true)) . "');
            $('#TeGroupPrevGroup option[value=\'0\']')
                .text('" . addslashes(__("New group", true)) . '…' . "');
            $('#group-merge').hide('fast');
            $('#ref-teacher').show('slow');
        } else {
            // Modifier le code d'un groupe
            $('#group-merge dd input[type=checkbox]').iButton('toggle', false);
            $('#group-change h2').html('" . addslashes(__("Edit group code", true)) . "');
            $('#group-list label').html('" . addslashes(__("Target group", true)) . "');
            $('#group-code label').html('" . addslashes(__("New code for this group", true)) . "');
            $('#TeGroupPrevGroup option[value=\'0\']')
                .text('" . addslashes(__("Select to add group", true)) . '…' . "');
            $('#TeGroupPrevGroupCode').val($('#TeGroupPrevGroup option:selected').text());
            $('#ref-teacher').hide('fast');
            $('#group-merge').show('slow');
        }
    });
    // Bascule affichage boîte d'édition
    $('#title-group-info').bind('click', function() {
        $('#group-change').toggle('slow');
    });
    $('#group-edit').bind('click', function() {
        $('#group-change').toggle('slow');
    });
    // Bascule édition / fusion
    $('#group-merge dd input[type=checkbox]').change(function() {
        if ($(this).is(':checked')) {
            $('#group-change h2').html('" . addslashes(__("Merge two groups", true)) . "');
            $('#group-list label').html('" . addslashes(__("Source group", true)) . "');
            $('#group-code label').html('" . addslashes(__("Target group", true)) . "');
        } else {
            $('#group-change h2').html('" . addslashes(__("Edit group code", true)) . "');
            $('#group-list label').html('" . addslashes(__("Target group", true)) . "');
            $('#group-code label').html('" . addslashes(__("New code for this group", true)) . "');
        }
    })
    // Édition d'un groupe depuis la liste
    $('#groups table tbody span.group-code').live('click', function() {
        var extract = $(this).attr('id').split('-');
        var groupId = extract[1];
        var groupCode = extract[2];
        $('#TeGroupPrevGroup option[selected]').removeAttr('selected');
        $('#TeGroupPrevGroup option[value=\'' + groupId + '\']').attr('selected', 'selected');
        $('#TeGroupPrevGroupCode').val(groupCode);
        $('#TeGroupPrevGroup option[value=\'0\']')
            .text('" . addslashes(__("Select to add group", true)) . '…' . "');
        $('#group-change').show('slow');
        $('#group-change-form fieldset dl dd input[type=checkbox]').iButton('toggle', false);
        $('#group-change h2').html('" . addslashes(__("Edit group code", true)) . "');
        $('#group-list label').html('" . addslashes(__("Target group", true)) . "');
        $('#group-code label').html('" . addslashes(__("New code for this group", true)) . "');
        $('#ref-teacher').hide('fast');
        $('#group-merge').show('slow');
    });
    // Tooltips
    tooltipHelp('#group-change');
    tooltipGear('#group-info');
    // Tips
    addTipInfo('#group-info');
");

?>