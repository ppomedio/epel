<?php

/**
 * {app}/controllers/view_students_controller.php
 *
 */
class ViewStudentsController extends AppController {

    var $name = 'ViewStudents';
    var $uses = array('Student');
    // Composants
    var $components = array('L10nSort');
    // Assistants
    var $helpers = array(
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // Pas de variable de session = déconnexion
        if (!$this->Session->check('Auth.User.view'))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        // Données de vue : affichage des langues du Portfolio
        $portfolios = $this->Session->read('Auth.User.view.portfolios');
        $languages = array();
        foreach ($portfolios as $key => $value) {
            $languages[$value['name']]['mother'] = $value['mother'];
            $languages[$value['name']]['usual'] = $value['usual'];
        }
        $this->set('languages', $this->L10nSort->sortLocalized($languages));

    }


    function init() {

        // ID de l'étudiant
        $studentId = (isset($this->params['pass'][0])) ? base64_decode($this->params['pass'][0]) : NULL;
        // Partie fixe de l'url de retour
        $returnUrl = DS . $this->Session->read('Auth.User.role') . '/student/search';
        $this->Session->write('Auth.User.returnUrl', $returnUrl);
        // Traitement si ID non nulle
        if ($studentId) {
            // Chargement des infos de base
            $user = $this->Student->getStudentInfo($studentId);
            if (!empty($user)) {
                if (!empty($user['portfolios'])) {
                    if ($this->Session->read('Auth.User.role') == 'visitor') {
                        if ($user['vis_hide']) {
                            // Arrivée depuis la liste des visites, le portfolio est invisible : message…
                            $this->Session->setFlash(__("This student's Portfolio is no longer visible to visitors.", true));
                            // … et retour à la page d'où on vient.
                            $this->redirect(DS . $this->params['language'] . $this->Session->read('Auth.User.returnUrl'));
                        } else if ($user['vis_hide'] || (!$user['vis_passport'] && !$user['vis_biography'] && !$user['vis_dossier'])) {
                            // Arrivée depuis la liste des visites, toutes les parties du portfolio sont masquées : message…
                            $this->Session->setFlash(__("All parts of this student's Portfolio are invisible to visitors.", true));
                            // … et retour à la page d'où on vient.
                            $this->redirect(DS . $this->params['language'] . $this->Session->read('Auth.User.returnUrl'));
                        } else {
                            /**
                             * Enregistrement de la visite
                             */
                            // Préparation
                            $data['Visit']['student_id'] = $studentId;
                            $data['Visit']['visitor_id'] = $this->Session->read('Auth.User.id');
                            $data['Visit']['date'] = date('Y-m-d');
                            // Si ce visiteur n'a pas déjà vu ce portfolio aujourd'hui…
                            if (!$this->Student->Visit->visitExists($data)) {
                                // … on enregistre une nouvelle visite.
                                $this->Student->Visit->create($data);
                                $this->Student->Visit->save($data, array('callbacks' => false));
                            }
                        }
                    } else if ($this->Session->read('Auth.User.role') == 'teacher') {
                        if ($user['tea_hide']) {
                            // Arrivée depuis la liste des visites, le portfolio est invisible : message…
                            $this->Session->setFlash(__("This student's Portfolio is not visible to teachers.", true));
                            // … et retour à la page d'où on vient.
                            $this->redirect(DS . $this->params['language'] . $this->Session->read('Auth.User.returnUrl'));
                        }
                    }
                    // Stockage de l'ID de l'étudiant en session
                    $this->Session->write('Auth.User.view', $user);
                    // Remplacement du menu
                    $this->Menu->setMenu(true);
                    // Redirection
                    $this->redirect(DS . $this->params['language'] . '/view/student');
                } else {
                    // Paramètre inexploitable : message…
                    $this->Session->setFlash(__("This student's Portfolio is empty.", true));
                    // … et retour à la page d'où on vient.
                    $this->redirect(DS . $this->params['language'] . $this->Session->read('Auth.User.returnUrl'));
                }
            } else {
                // Paramètre inexploitable : message…
                $this->Session->setFlash(__("This student does not exist, or has deleted his Portfolio.", true));
                // … et retour à la page d'où on vient.
                $this->redirect(DS . $this->params['language'] . $this->Session->read('Auth.User.returnUrl'));
            }
        }
        // Paramètre absent : eject
        $this->redirect(array(
            'language' => $this->params['language'],
            'controller' => 'users',
            'action' => 'logout'
        ));

    }


    function quit() {

        // Initialisation variable de session
        $this->Session->delete('Auth.User.view');
        // Remplacement du menu
        $this->Menu->setMenu();
        // Redirection
        $this->redirect(DS . $this->params['language'] . $this->Session->read('Auth.User.returnUrl'));

    }


}

