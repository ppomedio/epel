<?php

/**
 * {app}/controllers/student_passports_controller.php
 *
 */
class StudentPassportsController extends AppController {

    var $name = 'StudentPassports';
    // Modèle
    var $uses = 'Portfolio';
    // Composants
    var $components = array(
        'PortfolioLists',
        'L10nSort'
    );
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu',
        'Date'
    );


    function beforeFilter() {

        /**
         * Si portfolio vide ou langue demandée non ouverte : redirection ;
         * sinon stockage de la langue active pour les autres méthodes.
         */
        $portfolioLanguage = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
        $portfolios = $this->Session->read('Auth.User.portfolios');
        if (empty($portfolios) || ($portfolioLanguage && !array_key_exists($portfolioLanguage, $portfolios)))
            $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'base');
        Configure::write('portfolioLanguage', $portfolioLanguage);

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'base');

    }


    function edit() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $languages = $this->Session->read('Auth.User.languages');
        $portfolios = $this->Session->read('Auth.User.portfolios');

        // Chargement des données de pagination des expériences marquantes
        $valuable = $this->_getExperienceData($portfolios[$language]['id']);

        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.PoValuable.page', $this->params['paging']['PoValuable']['options']['page']);
        $this->Session->write('Auth.User.pagination.PoValuable.order', $this->params['paging']['PoValuable']['options']['order']);

        // Pagination Ajax
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->set('user', $valuable);
            $this->set("durations", $this->PortfolioLists->getDurationsList());
            $this->render(DS . 'elements' . DS . 'student' . DS . 'passport' . DS . 'exp-detail-page');
            return;
        }

        // Retour de formulaire
        /*if (!empty($this->data)) {
            // Envoi des données au modèle
            $this->data['Portfolio']['id'] = $portfolios[$language]['id'];
            $this->Portfolio->set($this->data);
            // Sauvegarde
            if ($this->Portfolio->save($this->data)) {
                // Message
                $this->Session->setFlash(__d('portfolio', "Your Passport has been updated.", true));
            }
        }*/

        // Chargement des données
        $this->data = $this->Portfolio->getPassport($portfolios[$language]['id']);
        $user = $this->data;
        if (!empty($valuable))
            $user = $user + $valuable;

        // Mémorisation de l'évaluation en session
        $this->_setPreviousAssessment($user['Portfolio']);

        // Contenu des listes déroulantes
        $this->set('competencies', $this->PortfolioLists->getCompetenciesList());
        $this->set('levels', $this->PortfolioLists->getLevelsList());
        $this->set('durations', $this->PortfolioLists->getDurationsList());
        $this->set('schoolStudyPeriods', $this->PortfolioLists->getSchoolStudyPeriodsList());
        $this->set('otherStudyPeriods', $this->PortfolioLists->getOtherStudyPeriodsList());
        $this->set('activityPeriods', $this->PortfolioLists->getActivityPeriodsList());

        // Documents attestant un niveau
        $this->set("levelDocs", $this->Portfolio->countLevelDocuments($portfolios[$language]['id'], true));

        // Copyright EuroPass
        $addCopyright['link'] = "Europass";
        $addCopyright['url'] = Configure::read('epel.europass.url');
        $this->set('addCopyright', $addCopyright);

        // Autres variables de vue
        $this->set('language', $language);
        $languageTitle = __d('language', $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'], true);
        $this->set('languageTitle', $languageTitle);
        $this->set('title_for_layout', __("Language Passport", true) . " &bull; " . $languageTitle);
        $this->set('user', $user);

    }


    function overview() {

        // Chargement des données dans les variables de vue
        $this->set('user', $this->L10nSort->sortLocalized($this->Portfolio->getPassportOverview($this->Session->read('Auth.User.id'), $this->Session->read('Auth.User.languages'))));
        $this->set('competencies', $this->PortfolioLists->getCompetenciesList());
        $this->set('levels', $this->PortfolioLists->getLevelsList());
        $this->set('levelsDescriptions', $this->PortfolioLists->getLevelsDescriptions());
        $this->set('title_for_layout', __("Language Passport", true) . " &bull; " . __("Overview", true));

    }


    /**
     * Enregistrement Ajax
     *
     */
    function ajax_save() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Chargement de la langue
            $language = Configure::read('portfolioLanguage');
            if ($language) {
                // Mother / usual
                if (isset($this->data['Portfolio']['mother'])) {
                    // Maj session
                    $this->Session->write('Auth.User.portfolios.' . $language . '.mother', $this->data['Portfolio']['mother']);
                    $this->Session->write('Auth.User.portfolios.' . $language . '.usual', $this->data['Portfolio']['usual']);
                }
                // Évaluation
                if (isset($this->data['Portfolio']['listening'])) {
                    $update = true;
                    $prevPassport = $this->Session->read('Auth.User.prevPassport');
                    if ($prevPassport['assessment'] != $this->data['Portfolio']) {
                        $user['Portfolio']['assess_update'] = date('Y-m-d');
                        $this->data['Portfolio']['assess_update'] = $user['Portfolio']['assess_update'];
                        $this->_setPreviousAssessment($this->data['Portfolio']);
                    } else {
                        $user['Portfolio']['assess_update'] = $prevPassport['assess_update'];
                    }
                }
                // Enregistrement
                $portfolios = $this->Session->read('Auth.User.portfolios');
                $this->Portfolio->id = $portfolios[$language]['id'];
                $this->Portfolio->set($this->data);
                $this->Portfolio->save($this->data);
                // Sortie
                if (isset($update)) {
                    $this->set('user', $user);
                    $this->render(DS . 'elements' . DS . 'student' . DS . 'passport' . DS . 'skills-update');
                    return;
                } else {
                    $this->autoRender = false;
                    exit();
                }
            }
        }

    }


    /**
     *
     * @return <array>
     */
    private function _setPreviousAssessment($data) {

        $prevPassport = array();
        $prevPassport['assessment']['listening'] = $data['listening'];
        $prevPassport['assessment']['listening_part'] = $data['listening_part'];
        $prevPassport['assessment']['reading'] = $data['reading'];
        $prevPassport['assessment']['reading_part'] = $data['reading_part'];
        $prevPassport['assessment']['speak_int'] = $data['speak_int'];
        $prevPassport['assessment']['speak_int_part'] = $data['speak_int_part'];
        $prevPassport['assessment']['speak_prod'] = $data['speak_prod'];
        $prevPassport['assessment']['speak_prod_part'] = $data['speak_prod_part'];
        $prevPassport['assessment']['writing'] = $data['writing'];
        $prevPassport['assessment']['writing_part'] = $data['writing_part'];
        $prevPassport['assess_update'] = $data['assess_update'];
        $this->Session->write('Auth.User.prevPassport', $prevPassport);

    }


    /**
     *
     * @return <array>
     */
    private function _getExperienceData($portfolio) {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.PoValuable')) {
            $page = $this->Session->read('Auth.User.pagination.PoValuable.page');
            $order = $this->Session->read('Auth.User.pagination.PoValuable.order');
            $this->Session->delete('Auth.User.pagination.PoValuable');
        } else {
            $page = NULL;
            $order = array('PoValuable.name' => 'ASC');
        }
        // Paramétrage
        $this->paginate = array(
            'conditions' => array(
                'PoValuable.portfolio_id' => $portfolio
            ),
            'limit' => 5,
            'fields' => array(
                'id',
                'portfolio_id',
                'name',
                'description'
            ),
            'page' => $page,
            'order' => $order,
            'recursive' => -1
        );
        // Chargement des données
        $data = $this->paginate('PoValuable');
        $exp = array();
        foreach ($data as $key => $value) {
            $exp['PoValuable'][$key] = $value['PoValuable'];
        }
        // Envoi de la pagination
        return $exp;

    }


}

