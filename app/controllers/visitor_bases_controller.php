<?php

/**
 * {app}/controllers/visitor_bases_controller.php
 *
 */
class VisitorBasesController extends AppController {

    var $name = 'VisitorBases';
    // Modèle
    var $uses = 'Visitor';
    // Pagination
    var $paginate;
    // Composants
    var $components = array(
        'L10nSort',
        'FormatInput'
    );
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Autorisation des champs "password" vides
        if ($this->params['action'] == 'connect')
            $this->Visitor->User->allowEmptyPasswords();

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $id = $this->AppAuth->user('id');
        // Chargement des données de pagination des visites
        $visits = $this->_getVisitorVisits($id);
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visit.page', $this->params['paging']['Visit']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visit.order', $this->params['paging']['Visit']['options']['order']);
        // Traitement
        if ($this->RequestHandler->isAjax()) {
            /**
             * Pagination Ajax
             */
            Configure::write('debug', 0);
            // Variables de vue
            $this->set('ajax', true);
            $this->set('user', $visits);
            // Rendu de l'élément
            $this->render(DS . 'elements' . DS . 'visitor' . DS . 'base' . DS . 'visits-page');
            return;
        } else {
            /**
             * Affichage normal
             */
            // Chargement des données
            $user = $this->Visitor->getProfile($id) + $visits;
            // Variables de vue
            $this->set('user', $user);
            $this->set('title_for_layout', __("Visitor", true) . ' &bull; ' . __("Base", true));
        }

    }


    /**
     * Mise à jour du profil
     *
     */
    function profile() {

        $id = $this->AppAuth->user('id');
        // Chargement des données de pagination des visites
        $visits = $this->_getVisitorVisits($id);
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visit.page', $this->params['paging']['Visit']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visit.order', $this->params['paging']['Visit']['options']['order']);
        // Traitement des données de formulaire
        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->Visitor->getProfile($id);
            $user = $this->data + $visits;
        } else {
            // Formatage (ne fonctionne pas dans beforeValidate…)
            $this->data['Visitor']['name'] = $this->FormatInput->formatName($this->data['Visitor']['name']);
            $this->data['Visitor']['firstname'] = $this->FormatInput->formatName($this->data['Visitor']['firstname']);
            $this->data['Visitor']['function'] = $this->FormatInput->capFirst($this->data['Visitor']['function']);
            $this->data['Visitor']['phone'] = $this->FormatInput->formatPhone($this->data['Visitor']['phone']);
            // Envoi des données au modèle
            $this->data['Visitor']['id'] = $id;
            $this->Visitor->set($this->data);
            // Si validation : sauvegarde
            if ($this->Visitor->validates()) {
                // Préparation maj des variables de session
                $updateSession = $this->Session->read('Auth.User.info');
                $updateSession['name'] = $this->data['Visitor']['name'];
                $updateSession['firstname'] = $this->data['Visitor']['firstname'];
                // Sauvegarde
                if ($this->Visitor->save($this->data, array(
                            'validate' => false,
                            'fieldList' => array(
                                'name',
                                'firstname',
                                'function',
                                'structure',
                                'phone',
                                'url',
                                'address',
                                'country_id'
                            )
                        ))) {
                    // Maj des variables de session
                    $this->Session->write('Auth.User.info', $updateSession);
                    // Message
                    $this->Session->setFlash(__("Your profile has been updated.", true));
                    // Redirection
                    $this->redirect('/' . $this->params['language'] . '/visitor/base');
                }
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->Visitor->getProfile($id) + $visits;
        }
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            /**
             * Pagination Ajax
             */
            Configure::write('debug', 0);
            // Variables de vue
            $this->set('ajax', true);
            $this->set('user', $visits);
            // Rendu de l'élément
            $this->render(DS . 'elements' . DS . 'visitor' . DS . 'base' . DS . 'visits-page');
            return;
        } else {
            /**
             * Affichage normal
             */
            $this->set('user', $user);
            $this->set('country', $this->L10nSort->setList($this->Visitor->Country->find('list'), 'country'));
            $this->set('title_for_layout', __("Visitor", true) . ' &bull; ' . __("Profile edition", true));
        }

    }


    /**
     * Édition des infos de connexion / courriel
     *
     */
    function connect() {

        $id = $this->AppAuth->user('id');
        // Chargement des données de pagination des visites
        $visits = $this->_getVisitorVisits($id);
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visit.page', $this->params['paging']['Visit']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visit.order', $this->params['paging']['Visit']['options']['order']);
        // Traitement des données de formulaire
        if (empty($this->data)) {
            // Chargement des données
            $this->data = $this->Visitor->getProfile($id);
            $user = $this->data + $visits;
        } else {
            // Envoi des données au modèle
            $this->data['Visitor']['id'] = $id;
            $this->data['User']['id'] = $id;
            $this->Visitor->set($this->data);
            // Préparation maj des variables de session
            $updateSession = array(
                'email' => $this->data['User']['email'],
                'username' => $this->data['User']['username']
            );
            // Sauvegarde avec validation
            if ($this->Visitor->User->save($this->data, array(
                        'validate' => true,
                        'fieldList' => array(
                            'email',
                            'username',
                            'password',
                            'passwd',
                            'passwd_confirm'
                        )
                    ))) {
                // Maj des variables de session
                $this->Session->write('Auth.User.email', $updateSession['email']);
                $this->Session->write('Auth.User.username', $updateSession['username']);
                // Message
                $this->Session->setFlash(__("Your information has been updated.", true));
                // Redirection
                $this->redirect('/' . $this->params['language'] . '/visitor/base');
            }
            // Rechargement des données de vues et de formulaires
            $user = $this->Visitor->getProfile($id) + $visits;
        }
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            /**
             * Pagination Ajax
             */
            Configure::write('debug', 0);
            // Variables de vue
            $this->set('ajax', true);
            $this->set('user', $visits);
            // Rendu de l'élément
            $this->render(DS . 'elements' . DS . 'visitor' . DS . 'base' . DS . 'visits-page');
            return;
        } else {
            /**
             * Affichage normal
             */
            $this->set('user', $user);
            $this->set('title_for_layout', __("Visitor", true) . ' &bull; ' . __("Connection / Email data edition", true));
        }

    }


    /**
     * Chargement de la liste des visites d'un visiteur
     *
     * @param <string> $languageId
     * @return <array>
     */
    private function _getVisitorVisits($visitorId) {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Visit')) {
            $page = $this->Session->read('Auth.User.pagination.Visit.page');
            $order = $this->Session->read('Auth.User.pagination.Visit.order');
            $this->Session->delete('Auth.User.pagination.Visit');
        } else {
            $page = NULL;
            $order = array('Visit.date' => 'DESC');
        }
        // Paramétrage
        $this->paginate = array(
            'conditions' => array('Visit.visitor_id' => $visitorId),
            'fields' => array(
                'id',
                'student_id',
                'date'
            ),
            'contain' => array(
                'Student' => array(
                    'fields' => array(
                        'id',
                        'name',
                        'firstname',
                        'university_id'
                    ),
                    'University' => array(
                        'fields' => array('id', 'name')
                    )
                )
            ),
            'page' => $page,
            'order' => $order,
            'limit' => 12
        );
        // Chargement des données
        $data = $this->paginate('Visit');
        $visits = array();
        foreach ($data as $key => $value) {
            $visits['Visit'][$key]['date'] = $value['Visit']['date'];
            $visits['Visit'][$key]['studentId'] = $value['Student']['id'];
            $visits['Visit'][$key]['studentName'] = $value['Student']['name'];
            $visits['Visit'][$key]['studentFirstname'] = $value['Student']['firstname'];            // Traduction des noms d'université statiques
            if ((int) $value['Student']['University']['id'] < 3)
                $visits['Visit'][$key]['studentUniversity'] = __($value['Student']['University']['name'], true);
            else
                $visits['Visit'][$key]['studentUniversity'] = $value['Student']['University']['name'];
        }
        // Envoi de la pagination
        return $visits;

    }


}

