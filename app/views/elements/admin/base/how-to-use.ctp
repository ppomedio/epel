
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="admin-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="edit"></dt>
                                <dd><?php __("Edit"); ?></dd>
                            </dl><?php
                            if (!empty($user['Visit'])) { ?>

                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                            <p class="info"><?php __("You can send email to users by clicking on their name."); ?></p><?php
                            } ?>

                        </div>
                    </div><?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#admin-info');
");

?>