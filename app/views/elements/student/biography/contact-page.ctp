<?php

$paginator->options(array(
    'update' => '#contact'
    ));

?>

                        <div id="contact" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Context"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="contact-add" class="add"></span>
                                            <span id="contact-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3">
                                             <p class="alert"><?php
                                             __d('portfolio', "There is no contact recorded in your Biography at the moment.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#contact');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="name"><?php echo $paginator->sort(__d('portfolio', "Context", true), 'PoContact.name', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="contact-add" class="add"></span>
                                            <span id="contact-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoContact'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="contact-name_<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
                                        <td id="contact-description_<?php echo $value['id']; ?>"><?php echo nl2br($value['description']); ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="contact-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="contact-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_contact-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this experience");
                                    ?></div>
                                    <div id="data_contact-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this experience");
                                    ?></div>
                                    <div id="contact-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete this experience"); echo ' (' . $value['name'] . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'contact'));
                            } ?>

                            <div id="data_sort-contact-name" class="tooltip-data"><?php __("Sort by context"); ?></div>
                            <div id="data_contact-add" class="tooltip-data"><?php __d('portfolio', "Add a new experience"); ?></div>
                            <div id="data_contact-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#contact-edit-form #PoContactDescription').val() == ''
            && $('#contact-edit-form #PoContactName').val() == '') {
        if ($('#contact-edit').is(':hidden'))
            $('#contact-toggle').hide();
        else
            $('#contact-add').hide();
    }
    // Id, classes et cie
    $('#contact table thead tr th.name a').attr('id', 'sort-contact-name').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#contact table thead');
    tooltipAdd('#contact table thead th.controls');
    tooltipGear('#contact table thead th.controls');
    tooltipEdit('#contact table tbody td.controls div.controls');
    tooltipSupp('#contact table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#contact-toggle').live('click', function() {
        if ($('#contact-edit-form #PoContactId').val() == '')
            $('#contact-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        if ($('#contact-edit').is(':hidden'))
            $('#contact-edit').slideDown('slow');
        else {
            $('#contact-edit').slideUp('slow');
            $('#contact-add').fadeIn('slow');
            if ($('#contact-edit-form #PoContactDescription').val() == ''
                    && $('#contact-edit-form #PoContactName').val() == '')
                $('#contact-toggle').hide();
        }
    });
    // Ajout d'une expérience
    $('#contact-add').live('click', function() {
        $('#contact-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#contact-edit').is(':hidden')) {
            $('#contact-edit').slideDown('slow');
        }
        $('#contact-add').hide();
        $('#contact-toggle').fadeIn('slow');
    });
    // Édition d'une expérience
    $('#contact table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#contact-edit > h2').text('" . addslashes(__d('portfolio', "Edit an experience", true)) . "');
        // Chargement des données dans le formulaire
        $('#contact-edit-form #PoContactId').val(itemId);
        $('#contact-edit-form #PoContactName').val($('#contact-name_' + itemId).text());
        $('#contact-edit-form #PoContactDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Affichage si masqué
        if ($('#contact-edit').is(':hidden')) {
            $('#contact-add').hide();
            $('#contact-edit').slideDown('slow');
            $('#contact-add').fadeIn('slow');
            $('#contact-toggle').fadeIn('slow');
        } else {
            if ($('#contact-add').is(':hidden'))
                $('#contact-add').fadeIn('slow');
        }
    });
    // Suppression d'une expérience
    $('#contact table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#contact-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#contact').load('/" . $this->params['language'] . "/student/biography/delete/" . urlencode($language) . "/contact/' + itemId, function() {
                            if ($('#contact-edit-form #PoContactId').val() == itemId) {
                                $('#contact-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
                                initForm();
                                if ($('#contact-edit').is(':hidden'))
                                    $('#contact-toggle').hide();
                                else
                                    $('#contact-add').hide();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-contact').html('" . addslashes(__d('portfolio', "My contacts with speakers of the language", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>