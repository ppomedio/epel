<?php

// Initialisation des matrices coté client
$this->Js->Buffer("
    var visibility = new Array();
    var partVisibility = new Array();
    var visible = new Array();
    var partName = new Array();
");

?>

                    <section>
                        <h2><?php __("Language(s) of the Portfolio"); ?></h2>
                        <div id="language-list"><?php
                            if (empty($user['Portfolio'])) { ?>

                            <dl>
                                <dt class="alert"><?php __("Your Portfolio is empty"); echo '…'; ?></dt>
                                <dd><?php __("You need to add at least one language in order to start using it.");  ?></dd>
                            </dl>
                            <?php
                            } else { ?>

                            <dl>
                                <dt><?php __("Portfolio open for"); ?></dt>
                                <?php
                                $state = array();
                                $partVisibility = $this->Session->read('Auth.User.visibility');
                                $visible = array(
                                    0 => '<strong class="alerttext">' . __("invisible", true) . '</strong>',
                                    1 => '<strong class="infotext">' . __("visible", true) . '</strong>'
                                );
                                $symbol = false;
                                foreach ($user['Portfolio'] as $key => $value) {
                                    $key = strtolower($key);
                                    // Signalisation maternelle / usuelle
                                    $add = ' ';
                                    if ($value['mother']) {
                                        $add .= '<span class="symbol-mother"></span>';
                                        $symbol = true;
                                    }
                                    if ($value['usual']) {
                                        $add .= '<span class="symbol-usual"></span>';
                                        $symbol = true;
                                    } ?>

                                    <dd><span id="language-info_<?php echo $key; ?>" class="see"></span><span class="language-list"><?php __d('language', $value['Language']['name']); ?></span><?php
                                    if (!empty($add))
                                        echo $add;
                                    ?></dd>
                                    <div id="data_language-info_<?php echo $key; ?>" class="tooltip-data"><?php
                                    echo __("Passport", true) . __(":", true) . " ";
                                    if ($this->Session->read('Auth.User.info.vis_passport')) {
                                        echo $visible[$partVisibility[$key]['passport']];
                                        $visibility[$key]['passport'] = $partVisibility[$key]['passport'];
                                    } else {
                                        $visibility[$key]['passport'] = 0;
                                        echo $visible[0];
                                    }
                                    echo '<br />' .  __("Biography", true) . __(":", true) . " ";
                                    if ($this->Session->read('Auth.User.info.vis_biography')) {
                                        echo $visible[$partVisibility[$key]['biography']];
                                        $visibility[$key]['biography'] = $partVisibility[$key]['biography'];
                                    } else {
                                        $visibility[$key]['biography'] = 0;
                                        echo $visible[0];
                                    }
                                    echo '<br />' . __("Dossier", true) . __(":", true) . " ";
                                    if ($this->Session->read('Auth.User.info.vis_dossier')) {
                                        echo $visible[$partVisibility[$key]['dossier']];
                                        $visibility[$key]['dossier'] = $partVisibility[$key]['dossier'];
                                    } else {
                                        $visibility[$key]['dossier'] = 0;
                                        echo $visible[0];
                                    }
                                    ?></div>
                                    <?php
                                }
                                // Chargement des matrices coté client
                                $this->Js->Buffer("
    visible[0] = '" . $visible[0] . "';
    visible[1] = '" . $visible[1] . "';
    partName['passport'] = '" . addslashes(__("Passport", true) . __(":", true)) . ' ' . "';
    partName['biography'] = '" . '<br />' . addslashes(__("Biography", true) . __(":", true)) . ' ' . "';
    partName['dossier'] = '" . '<br />' . addslashes(__("Dossier", true) . __(":", true)) . ' ' . "';");
                                foreach ($visibility as $key => $value) {
                                    $this->Js->Buffer("
    visibility['" . $key . "'] = new Array();
    visibility['" . $key . "']['passport'] = " . $value['passport'] . ";
    visibility['" . $key . "']['biography'] = " . $value['biography'] . ";
    visibility['" . $key . "']['dossier'] = " . $value['dossier'] . ";");
                                }
                                foreach ($partVisibility as $key => $value) {
                                    $this->Js->Buffer("
    partVisibility['" . $key . "'] = new Array();
    partVisibility['" . $key . "']['passport'] = " . $value['passport'] . ";
    partVisibility['" . $key . "']['biography'] = " . $value['biography'] . ";
    partVisibility['" . $key . "']['dossier'] = " . $value['dossier'] . ";");
                                }
                                ?>
                            </dl><?php
                                if ($symbol) {  ?>

                            <p class="info"><?php echo '<span class="symbol-mother"></span>&nbsp;' . __d('portfolio', "Mother tongue", true) . ' / <span class="symbol-usual"></span>&nbsp;' . __d('portfolio', "Usual language", true); ?></p><?php
                                } ?>

                            <p class="visibility info"><?php __("Put the cursor of your mouse above the icon (on the left side of the name of the language) to check what visitors can see for this language."); ?></p>
                            <p class="info"><?php __("Deletion of your Portfolio can take place only if it contains no language (to be able to delete your Portfolio, first delete all the languages that it contains)."); ?></p><?php
                            } ?>

                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls"><?php
                                    if (empty($user['Portfolio'])) { ?>

                                    <span id="portfolio-delete" class="supp"></span>
                                    <?php
                                    } else { ?>

                                    <span id="language-delete" class="supp"></span><?php
                                    }
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/student/base/language/add',
                                        array(
                                            'id' => 'language-add',
                                            'class' => 'add'
                                        )
                                    ); ?>

                                </div>
                            </div>
                        </div>
                        <div id="data_language-add" class="tooltip-data"><?php __('Add a language'); ?></div>
                        <div id="data_language-delete" class="tooltip-data"><?php __('Delete a language'); ?></div>
                        <div id="data_portfolio-delete" class="tooltip-data"><?php __('Delete Portfolio'); ?></div>
                        <div id="language-delete-confirm" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php __("Do you really want to delete a language from your Portfolio?"); ?></div>
                        <div id="portfolio-delete-confirm" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php __("Do you really want to delete your Portfolio?"); ?></div>
                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipSee('#language-list dl dd');
    // Tips
    addTipInfo('#language-list');
    // Confirmation de la suppression d'une langue
    $('#language-delete').bind('click', function() {
        $(this).closest('div.tail').children().unbind('click', loading);
        $('#language-delete-confirm').dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    $(this).dialog('close');
                    $('span#language-delete').closest('div.controls').hide();
                    $('span#language-delete').closest('div.tail').children('div.loading').show();
                    $(location).attr('href', '/" . $this->params['language'] . "/student/base/language/delete');
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });

    });
    // Confirmation de la suppression du Portfolio
    $('#portfolio-delete').bind('click', function() {
        $(this).closest('div.tail').children().unbind('click', loading);
        $('#portfolio-delete-confirm').dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    $(this).dialog('close');
                    $('span#portfolio-delete').closest('div.controls').hide();
                    $('span#portfolio-delete').closest('div.tail').children('div.loading').show();
                    $(location).attr('href', '/" . $this->params['language'] . "/student/base/delete');
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });

    });
");

?>