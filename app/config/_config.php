<?php

/**
 * Configuration de l'application
 *
 * @global <type> $GLOBALS['config']
 */



/**
 * Structure hôte
 *
 * Ceci s'affichera en pied de page, sous forme de lien.
 *
 * Note : pour que les modifications soient prises en compte, vous devez
 * supprimer les fichiers « tmp/cache/views/element_xx_layout_foot »
 */

$config['epel.university'] = array(
    'name' => "Université ____________",
    'url' => "http://universite.fr/"
);



/**
 * Déclaration CNIL
 *
 * La valeur ci-dessous permet d'afficher votre n° d'autorisation CNIL
 * dans le pied de page : laissez-la vide si votre demande est en cours,
 * laissez-la commentée si vous ne souhaitez rien afficher.
 *
 * Note : pour que les modifications soient prises en compte, vous devez
 * supprimer les fichiers « tmp/cache/views/element_xx_layout_foot »
 */

#$config['epel.cnil.number'] = '';



/**
 * Ouverture de la création de compte visiteur
 *
 */

$config['epel.account_creation.visitor'] = true;



/**
 * Visibilité de l'université fictive et de ses utilisateurs dans l'interface d'administration
 *
 */

$config['epel.admin.show_fict_univ'] = true;



/**
 * Poids maxi des fichiers téléversables en Mo
 *
 * - les photos du profil sont redimensionnées et leurs originaux effacés,
 * - les documents téléversés restent tels quels dans l'espace de
 * stockage jusqu'à ce que l'utilisateur les supprime.
 *
 * Vérifier si "max_input_time" est positionnée à "-1" dans "php.ini", ".htaccess"
 * ou la configuration "VirtualHost" du serveur Apache ; ou au moins à une valeur
 * permettant le téléversement d'un fichier du poids défini.
 *
 */

$config['epel.photo.size'] = 2;      // Photo du profil
$config['epel.document.size'] = 30;  // Documents du dossier



/**
 * Envoi de courriel
 *
 */

// Paramétrage SMTP
$config['epel.smtp'] = array(
    // décommenter et renseigner pour utiliser un serveur SMTP
    // à la place de la fonction "mail" de PHP utilisée par défaut.
    #'host' => "smtp.universite.fr",
    #'port' => '25',
    #'timeOut' => '30',
    // Décommenter et renseigner si une identification est requise.
    #'username' => "smtp_username",
    #'password' => "smtp_password",
    // Décommenter et renseigner si le serveur SMTP
    // ne respecte pas totalement RFC 821 (SMTP HELO)
    #'client' => "smtp_helo_hostname"
);

// Adresses source / cible
$config['epel.email'] = array(
    // L'adresse de courriel principale utilisée par le formulaire de
    // contact est celle de l'administrateur (user n°1) par défaut,
    // mais peut-être remplacée en décommentant la ligne ci-dessous.
    #'to' => "somebody@smtp_domain.tld",
    // Si le serveur SMTP refuse d'envoyer du courrier :
    // décommenter la ligne suivante en y précisant une adresse
    // de courriel faisant partie du même domaine que lui.
    #'from' => "somebody@smtp_domain.tld",
);



/**
 * Journalisation de l'activité des utilisateurs
 *
 * Lorsque la limite du nombre d'enregistrements (en dizaines de milliers)
 * est atteinte la moitié des enregistrements les plus anciens est supprimée.
 *
 */

$config['epel.log'] = array(
    // Activation
    'on' => false,
    // Conservation des adresses IP
    'ip' => true,
    // Limite du nombre d'enregistrements
    // valeur > 10 ramenée à 10, 0 ou false = pas de limite)
    'limit' => 1
);



/**
 * Timezone
 *
 * http://fr.php.net/manual/fr/timezones.php
 *
 */

date_default_timezone_set('Europe/Paris');



/**
 * Cookies
 *
 * 'time' = valeur compréhensible par la fonction strtotime de PHP
 * 'encrypt' = booléen activant l'encryptage des valeurs
 *
 */

$config['epel.cookie'] = array(
    'time' => '6 months',
    'encrypt' => true
);



/**
 * URLs des liens externes
 *
 */

$config['epel.coe.url'] = 'http://www.coe.int/';
$config['epel.europass.url'] = 'http://europass.cedefop.europa.eu/';
$config['epel.cercles.url'] = 'http://www.cercles.org/';

// Notices des descripteurs de niveau CECRL
$config['epel.leveldesc.url'] = array(
    #'en' => 'http://ec.europa.eu/education/pub/pdf/general/eqf/leaflet_en.pdf',
    #'fr' => 'http://ec.europa.eu/education/pub/pdf/general/eqf/leaflet_fr.pdf',
    'en' => 'https://europass.cedefop.europa.eu/sites/default/files/cefr-en.pdf',
    'fr' => 'https://europass.cedefop.europa.eu/sites/default/files/cefr-fr.pdf',
);

// Page des niveaux CECRL contenant les grilles d'autoévaluation téléchargeables
#$config['epel.levelgrid.url'] = 'http://www.coe.int/T/DG4/Portfolio/?L=E&M=/main_pages/levels.html';
$config['epel.levelgrid.url'] = 'https://www.coe.int/fr/web/portfolio/self-assessment-grid';



/**
 * Langues installées
 *
 * Définies selon la norme ISO 639-1 :
 *      name        =>  le nom de la langue dans cette langue,
 *      flag        =>  le nom du fichier /img/flags/.png contenant
 *                      l'image de drapeau à afficher,
 *      dateFormat  =>  expression régulière formattant l'affichage littéral
 *                      des dates dans cette langue.
 *
 * http://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1
 *
 */

$config['epel.locales.available'] = array(
    'en' => array(
        'name' => 'english',
        'flag' => 'gb',
        'dateNum' => 'MDY',
        'dateText' => 'l, jS F Y'
    ),
    'fr' => array(
        'name' => 'français',
        'flag' => 'fr',
        'dateNum' => 'DMY',
        'dateText' => 'l j F Y'
    ),
);



/**
 * Langue par défaut
 *
 */

$config['epel.default.language'] = 'en';



/**
 * Types de fichiers autorisés en téléversement
 *
 * Choix parmi la liste native de types mime du plugin "Uploader", pour en
 * ajouter s'assurer qu'ils sont déclarés dans "../plugins/uploader/config/config.php"
 * puis inscrire les extensions de fichiers correspondantes ici.
 *
 */

$config['epel.photo.type'] = array('gif', 'jpg', 'jpeg', 'png');
$config['epel.document.type'] = array(
    'gif', 'jpg', 'jpeg', 'png',
    'txt', 'htm', 'html', 'rtf', 'rtx', 'sgm', 'sgml', 'tpl', 'xml', 'xht', 'xhtml',
    'bz2', 'gz', 'gtar', 'rar', 'rev', 'sit', 'tar', 'tgz', 'z', '7z', 'zip',
    'aif', 'aifc', 'aiff', 'au', 'flac', 'mp2', 'mp3', 'mpga', 'oga', 'ogg', 'ra', 'ram', 'rm', 'rpm', 'snd', 'wav', 'wma',
    'flv', 'fli', 'avi', 'qt', 'mov', 'movie', 'mp2', 'mpa', 'mpv2', 'mpe', 'mpeg', 'mpg', 'mp4', 'mkv', 'webm', 'wmv', '3gp', '3g2',
    'doc', 'dot', 'docx', 'dotx', 'odm', 'odp', 'odt', 'otp', 'ott', 'pdf', 'ppt', 'pps', 'sxi', 'sxw', 'xls', 'xlc'
);

// Téléchargement forcé
$config['epel.document.forcedl'] = true;



/**
 * Paramètres du clavier virtuel
 *
 */

$config['epel.vkbd.url'] = 'http://www.virtualkeyboard.ws/plugin.shtml';
$config['epel.vkbd.width'] = '420';
$config['epel.vkbd.height'] = '290';



/**
 * Maintenance "légère"
 *
 *  Plus d'authentification, sauf pour les IP non concernées.
 *
 */

// État (true / false)
#$config['epel.maintenance.state'] = true;

// IP non concernées
$config['epel.maintenance.access'] = array(
    '127.0.0.1',
    '::1'
);



/**
 * Mode dev. (décommenter pour l'utiliser)
 *
 *  1 : les messages d'avertissement et d'erreur sont affichés, les caches des
 *  modeles sont rafraîchis, les messages « Flash » ne redirigent plus.
 *  2 : comme 1, avec des messages de déboguage complets et les requêtes SQL.
 *
 */

#Configure::write('debug', 2);

