
                <article class="half">
                    <section>
                        <h2 id="title-elements-list"><?php __("Universities"); ?></h2>
                        <?php echo $this->element('admin/element/universities-page'); ?>

                    </section>
                </article>
                <article class="half">
                    <section>
                        <h2 id="title-elements-form"><?php __("Add"); ?></h2>
                        <?php echo $this->Form->create('University', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/admin/element/university',
                            'id' => 'elements-form',
                            'class' => ''
                            ));
                        echo $validation->bind('University'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __("Name", true)
                            ));
                        echo $this->Form->input('secret', array(
                            'label' => __("Teacher account creation code", true)
                            ));
                        echo $this->Form->input('url', array(
                            'label' => __("Web site", true)
                            ));
                        ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
                    <section>
                        <h2><?php __("Teacher account creation code"); ?></h2>
                        <div id="language-info">
                            <p class="info"><?php __("The “account creation code” is a phrase of your choice which you will indicate to those of the teachers of an university who do not already have an account so that they can create one."); ?></p>
                        </div>
                    </section>

                </article>
                <aside><?php
                    echo $this->element('admin/element/how-to-use');
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#language-info');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#elements-form div.submit input[type=submit]');
    // Soumission du formulaire
    $('#elements-form').submit(function() {
        if (!$('#elements-form .error').length)
            $('#elements-form div.loading').show();
        else
            return false;
    });
");

?>
