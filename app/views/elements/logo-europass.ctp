
                    <div id="logos">
                        <a href="<?php echo Configure::read('epel.europass.url'); ?>" id="<?php __("Visit the Europass website"); ?>" class="link">
                            <div class="logo" id="eplogo"></div>
                        </a>
                        <a href="<?php echo Configure::read('epel.coe.url'); ?>" id="<?php __("Visit the website of the Council of Europe"); ?>" class="link">
                            <div class="logo" id="coelogo"></div>
                        </a>
                    </div>