
                        <?php echo $this->Form->create('PoMediation', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/mediation',
                            'id' => 'mediation-edit-form',
                            'class' => 'full'
                            ));
                        ?>

                        <fieldset><?php
                        echo $this->Form->input('description', array(
                            'label' => false,
                            'rows' => '25'
                            ));
                        ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#mediation-edit-form div.submit input[type=submit]');
    // Activation du submit
    $('#mediation-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#mediation-edit-form textarea').keydown(function() {
        if ($('#mediation-edit-form div.submit input[type=submit]').attr('disabled'))
            $('#mediation-edit-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#mediation-edit-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/mediation',
        target: '#mediation-edit-form',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#mediation-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#mediation-edit-form div.loading').show();
        },
        success: function() {
            $('#mediation-edit-form div.loading').hide();
            $('#mediation-edit-form div.submit input[type=submit]').removeAttr('disabled');
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>