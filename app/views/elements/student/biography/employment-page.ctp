<?php

$paginator->options(array(
    'update' => '#employment'
    ));

?>

                        <div id="employment" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Type"); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="employment-add" class="add"></span>
                                            <span id="employment-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3">
                                             <p class="alert"><?php
                                             __d('portfolio', "There is no competency recorded in your Biography at the moment.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#employment');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="name"><?php echo $paginator->sort(__d('portfolio', "Type", true), 'PoEmployment.name', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th class="controls">
                                            <span id="employment-add" class="add"></span>
                                            <span id="employment-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoEmployment'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="employment-name_<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
                                        <td id="employment-description_<?php echo $value['id']; ?>"><?php echo nl2br($value['description']); ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="employment-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="employment-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_employment-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this experience");
                                    ?></div>
                                    <div id="data_employment-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this experience");
                                    ?></div>
                                    <div id="employment-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete this experience"); echo ' (' . $value['name'] . ').';
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'employment'));
                            } ?>

                            <div id="data_sort-employment-name" class="tooltip-data"><?php __("Sort by type"); ?></div>
                            <div id="data_employment-add" class="tooltip-data"><?php __d('portfolio', "Add a new experience"); ?></div>
                            <div id="data_employment-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#employment-edit-form #PoEmploymentDescription').val() == ''
            && $('#employment-edit-form #PoEmploymentName').val() == '') {
        if ($('#employment-edit').is(':hidden'))
            $('#employment-toggle').hide();
        else
            $('#employment-add').hide();
    }
    // Id, classes et cie
    $('#employment table thead tr th.name a').attr('id', 'sort-employment-name').addClass('sort-help');
    // Tooltips
    tooltipSortHelp('#employment table thead');
    tooltipAdd('#employment table thead th.controls');
    tooltipGear('#employment table thead th.controls');
    tooltipEdit('#employment table tbody td.controls div.controls');
    tooltipSupp('#employment table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#employment-toggle').live('click', function() {
        if ($('#employment-edit-form #PoEmploymentId').val() == '')
            $('#employment-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        if ($('#employment-edit').is(':hidden'))
            $('#employment-edit').slideDown('slow');
        else {
            $('#employment-edit').slideUp('slow');
            $('#employment-add').fadeIn('slow');
            if ($('#employment-edit-form #PoEmploymentDescription').val() == ''
                    && $('#employment-edit-form #PoEmploymentName').val() == '')
                $('#employment-toggle').hide();
        }
    });
    // Ajout d'une expérience
    $('#employment-add').live('click', function() {
        $('#employment-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#employment-edit').is(':hidden')) {
            $('#employment-edit').slideDown('slow');
        }
        $('#employment-add').hide();
        $('#employment-toggle').fadeIn('slow');
    });
    // Édition d'une expérience
    $('#employment table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#employment-edit > h2').text('" . addslashes(__d('portfolio', "Edit an experience", true)) . "');
        // Chargement des données dans le formulaire
        $('#employment-edit-form #PoEmploymentId').val(itemId);
        $('#employment-edit-form #PoEmploymentName').val($('#employment-name_' + itemId).text());
        $('#employment-edit-form #PoEmploymentDescription').val($('#hidden-info_' + itemId + ' .description').text());
        // Affichage si masqué
        if ($('#employment-edit').is(':hidden')) {
            $('#employment-add').hide();
            $('#employment-edit').slideDown('slow');
            $('#employment-add').fadeIn('slow');
            $('#employment-toggle').fadeIn('slow');
        } else {
            if ($('#employment-add').is(':hidden'))
                $('#employment-add').fadeIn('slow');
        }
    });
    // Suppression d'une expérience
    $('#employment table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#employment-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
					var extract = new Array();
					extract = $(this).attr('id').split('_');
					var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#employment').load('/" . $this->params['language'] . "/student/biography/delete/" . urlencode($language) . "/employment/' + itemId, function() {
                            if ($('#employment-edit-form #PoEmploymentId').val() == itemId) {
                                $('#employment-edit > h2').text('" . addslashes(__d('portfolio', "Add a new experience", true)) . "');
                                initForm();
                                if ($('#employment-edit').is(':hidden'))
                                    $('#employment-toggle').hide();
                                else
                                    $('#employment-add').hide();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-employment').html('" . addslashes(__d('portfolio', "Competencies which improve my employment prospects", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>