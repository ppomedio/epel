<?php

/**
 * {app}/controllers/components/menu.php
 *
 */
class MenuComponent extends Object {

    // Composants utilisés
    var $components = array(
        'L10nSort',
        'AppAuth',
        'Cookie',
        'Session'
    );


    /**
     * Création de la matrice contenant le menu des utilisateurs,
     * stockée dans une variable de session.
     *
     * @param <boolean> mode vue
     */
    function setMenu($view = false) {

        if ($this->AppAuth->user()) {

            // Nom du contrôleur
            $typeMenu = ($view) ? 'view' : $this->AppAuth->user('role');

            switch ($typeMenu) {

                /**
                 * Vues enseignant / visiteur
                 */
                case 'view':
                    $menu = array(
                        'Return' => array(
                            'extCtrl' => 'student',
                            'action' => 'quit'
                        ),
                        'Passport' => array(
                            'submenu' => array()
                        ),
                        'Biography' => array(
                            'submenu' => array()
                        ),
                        'Dossier' => array(
                            'submenu' => array()
                        )
                    );
                    /**
                     * Complétion suivant l'état du portfolio
                     */
                    $portfolios = $this->Session->read('Auth.User.view.portfolios');
                    // Création des options de menu
                    if (!empty($portfolios)) {
                        // Tri des options de langue selon la langue de l'interface
                        $localizedPortfolios = $this->L10nSort->sortLocalized($portfolios);
                        foreach ($localizedPortfolios as $key => $value) {
                            // Passeport
                            $menu['Passport']['submenu'][$value['name']]['extCtrl'] = 'passport';
                            $menu['Passport']['submenu'][$value['name']]['lang_file'] = 'language';
                            $menu['Passport']['submenu'][$value['name']]['action'] = 'show' . '/' . urlencode($key);
                            // Biographie
                            $menu['Biography']['submenu'][$value['name']]['extCtrl'] = 'biography';
                            $menu['Biography']['submenu'][$value['name']]['lang_file'] = 'language';
                            $menu['Biography']['submenu'][$value['name']]['action'] = 'show' . '/' . urlencode($key);
                            // Dossier
                            $menu['Dossier']['submenu'][$value['name']]['extCtrl'] = 'dossier';
                            $menu['Dossier']['submenu'][$value['name']]['lang_file'] = 'language';
                            $menu['Dossier']['submenu'][$value['name']]['action'] = 'show' . '/' . urlencode($key);
                        }
                    }
                    // Biographie : langues patrimoniales
                    if ($this->Session->read('Auth.User.view.heritage') != '') {
                        $menu['Biography']['submenu']['Heritage languages']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Heritage languages']['action'] = 'heritage';
                    }
                    // Biographie : plurilingualisme
                    if ($this->Session->read('Auth.User.view.plurilingualism') != '') {
                        $menu['Biography']['submenu']['Plurilingualism']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Plurilingualism']['action'] = 'plurilingualism';
                    }
                    // Biographie : médiation
                    if ($this->Session->read('Auth.User.view.mediation') != '') {
                        $menu['Biography']['submenu']['Translation and mediation']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Translation and mediation']['action'] = 'mediation';
                    }
                    /**
                     * Retrait des options masquées (visiteur)
                     */
                    if ($this->AppAuth->user('role') == 'visitor') {
                        // Passeport
                        if ($this->Session->read('Auth.User.view.vis_passport') == 1) {
                            foreach ($localizedPortfolios as $key => $value) {
                                if (!$value['vis_passport']) {
                                    unset($menu['Passport']['submenu'][$value['name']]);
                                }
                            }
                        } else
                            unset($menu['Passport']);
                        // Biographie
                        if ($this->Session->read('Auth.User.view.vis_biography') == 1) {
                            foreach ($localizedPortfolios as $key => $value) {
                                if (!$value['vis_biography']) {
                                    unset($menu['Biography']['submenu'][$value['name']]);
                                }
                            }
                        } else
                            unset($menu['Biography']);
                        // Dossier
                        if ($this->Session->read('Auth.User.view.vis_dossier') == 1) {
                            foreach ($localizedPortfolios as $key => $value) {
                                if (!$value['vis_dossier']) {
                                    unset($menu['Dossier']['submenu'][$value['name']]);
                                }
                            }
                        } else
                            unset($menu['Dossier']);
                    } else {
                        // Visites
                        $menu['Visits']['extCtrl'] = 'visit';
                        $menu['Visits']['action'] = 'show';
                    }
                    break;

                /**
                 * Administrateur
                 */
                case 'admin':
                    $menu = array(
                        'Base' => array(
                            'submenu' => array(
                                'Overview' => array(
                                    'extCtrl' => 'base'
                                ),
                                'Connection / Email' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'connect'
                                ),
                                'Activity log' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'activity'
                                )
                            )
                        ),
                        'Users' => array(
                            'submenu' => array(
                                'Students' => array(
                                    'extCtrl' => 'user',
                                    'action' => 'student'
                                ),
                                'Teachers' => array(
                                    'extCtrl' => 'user',
                                    'action' => 'teacher'
                                ),
                                'Visitors' => array(
                                    'extCtrl' => 'user',
                                    'action' => 'visitor'
                                )
                            )
                        ),
                        'Elements' => array(
                            'submenu' => array(
                                'Universities' => array(
                                    'extCtrl' => 'element',
                                    'action' => 'university'
                                ),
                                'Languages' => array(
                                    'extCtrl' => 'element',
                                    'action' => 'language'
                                ),
                                'Certificates' => array(
                                    'extCtrl' => 'element',
                                    'action' => 'certificate'
                                ),
                                'Certifications' => array(
                                    'extCtrl' => 'element',
                                    'action' => 'certification'
                                ),
                                'Domains' => array(
                                    'extCtrl' => 'element',
                                    'action' => 'domain'
                                ),
                                'Countries' => array(
                                    'extCtrl' => 'element',
                                    'action' => 'country'
                                )
                            )
                        )
                    );
                    break;

                /**
                 * Étudiant
                 */
                case 'student':
                    $menu = array(
                        'Base' => array(
                            'submenu' => array(
                                'Overview' => array(
                                    'extCtrl' => 'base'
                                ),
                                'Profile' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'profile'
                                ),
                                'Group' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'group'
                                ),
                                'Connection / Email' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'connect'
                                ),
                                'Add a language' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'language/add'
                                )
                            )
                        ),
                        'Passport' => array(
                            'submenu' => array()
                        ),
                        'Biography' => array(
                            'submenu' => array(
                                'Heritage languages' => array(
                                    'extCtrl' => 'biography',
                                    'action' => 'heritage'
                                ),
                                'Plurilingualism' => array(
                                    'extCtrl' => 'biography',
                                    'action' => 'plurilingualism'
                                ),
                                'Translation and mediation' => array(
                                    'extCtrl' => 'biography',
                                    'action' => 'mediation'
                                ),
                                'Learning how to learn' => array(
                                    'extCtrl' => 'biography',
                                    'action' => 'howtolearn'
                                )
                            )
                        ),
                        'Dossier' => array(
                            'submenu' => array()
                        )
                    );
                    /**
                     * Complétion suivant l'état du portfolio
                     */
                    $portfolios = $this->Session->read('Auth.User.portfolios');
                    $languages = $this->Session->read('Auth.User.languages');
                    // Création des options de menu
                    if (!empty($portfolios)) {
                        // Tri des options de langue selon la langue de l'interface
                        $localizedPortfolios = $this->L10nSort->sortLocalized($portfolios);
                        // Langues du portfolio
                        unset($menu['Biography']['submenu']['Learning how to learn']);
                        unset($menu['Biography']['submenu']['Translation and mediation']);
                        unset($menu['Biography']['submenu']['Plurilingualism']);
                        unset($menu['Biography']['submenu']['Heritage languages']);
                        // Guide des listes de repérage
                        $menu['Self-assessment']['submenu']['User guide']['fancy'] = 'self-assessment';
                        // Langues du portfolio
                        foreach ($localizedPortfolios as $key => $value) {
                            // Nom de la langue
                            $language = $languages[str_pad((string) $languages[$key], 4, '0', STR_PAD_LEFT)]['name'];
                            // Passeport
                            $menu['Passport']['submenu'][$language]['extCtrl'] = 'passport';
                            $menu['Passport']['submenu'][$language]['lang_file'] = 'language';
                            $menu['Passport']['submenu'][$language]['action'] = 'edit' . '/' . urlencode($key);
                            // Biographie
                            $menu['Biography']['submenu'][$language]['extCtrl'] = 'biography';
                            $menu['Biography']['submenu'][$language]['lang_file'] = 'language';
                            $cookie = $this->Cookie->read($this->AppAuth->user('id') . '.biography_' . $key);
                            if ($cookie)
                                $menu['Biography']['submenu'][$language]['action'] = $cookie . '/' . urlencode($key);
                            else
                                $menu['Biography']['submenu'][$language]['action'] = 'valuable' . '/' . urlencode($key);
                            // Dossier
                            $menu['Dossier']['submenu'][$language]['extCtrl'] = 'dossier';
                            $menu['Dossier']['submenu'][$language]['lang_file'] = 'language';
                            $cookie = $this->Cookie->read($this->AppAuth->user('id') . '.dossier_' . $key);
                            if ($cookie)
                                $menu['Dossier']['submenu'][$language]['action'] = $cookie . '/' . urlencode($key);
                            else
                                $menu['Dossier']['submenu'][$language]['action'] = 'show' . '/' . urlencode($key);
                            // Listes de repérage
                            $menu['Self-assessment']['submenu'][$language]['extCtrl'] = 'self-assessment';
                            $menu['Self-assessment']['submenu'][$language]['lang_file'] = 'language';
                            $cookie = $this->Cookie->read($this->AppAuth->user('id') . '.self-assessment_' . $key);
                            if ($cookie)
                                $menu['Self-assessment']['submenu'][$language]['action'] = $cookie . '/' . urlencode($key);
                            else
                                $menu['Self-assessment']['submenu'][$language]['action'] = 'listen' . '/' . urlencode($key);
                        }
                        // Autres options Passeport
                        $menu['Passport']['submenu']['Overview']['extCtrl'] = 'passport';
                        $menu['Passport']['submenu']['Overview']['action'] = 'overview';
                        // Autres options Biographie
                        $menu['Biography']['submenu']['Heritage languages']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Heritage languages']['action'] = 'heritage';
                        $menu['Biography']['submenu']['Plurilingualism']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Plurilingualism']['action'] = 'plurilingualism';
                        $menu['Biography']['submenu']['Translation and mediation']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Translation and mediation']['action'] = 'mediation';
                        $menu['Biography']['submenu']['Learning how to learn']['extCtrl'] = 'biography';
                        $menu['Biography']['submenu']['Learning how to learn']['action'] = 'howtolearn';
                        // Visites
                        $menu['Visits']['extCtrl'] = 'visit';
                        $menu['Visits']['action'] = 'show';
                    } else {
                        // Portfolio vide
                        unset($menu['Passport']);
                        unset($menu['Dossier']);
                    }
                    break;

                /**
                 * Enseignant
                 */
                case 'teacher':
                    $menu = array(
                        'Base' => array(
                            'submenu' => array(
                                'Overview' => array(
                                    'extCtrl' => 'base'
                                ),
                                'Profile' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'profile'
                                ),
                                'Connection / Email' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'connect'
                                ),
                                'Account creation code' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'secret'
                                )
                            )
                        ),
                        'Groups' => array(
                            'extCtrl' => 'group',
                            'action' => 'ref'
                        ),
                        'Students' => array(
                            'extCtrl' => 'student',
                            'action' => 'search'
                        )
                    );
                    break;

                /**
                 * Visiteur
                 */
                case 'visitor':
                    $menu = array(
                        'Base' => array(
                            'submenu' => array(
                                'Overview' => array(
                                    'extCtrl' => 'base'
                                ),
                                'Profile' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'profile'
                                ),
                                'Connection / Email' => array(
                                    'extCtrl' => 'base',
                                    'action' => 'connect'
                                )
                            )
                        ),
                        'Students' => array(
                            'extCtrl' => 'student',
                            'action' => 'search'
                        )
                    );
                    break;

                /**
                 * Indéfini…
                 */
                default:
                    $menu = array();
                    break;
            }
        } else
            $menu = array();

        $this->Session->write('Menu', $menu);

    }


}

