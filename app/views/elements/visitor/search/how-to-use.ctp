
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="search-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="search"></dt>
                                <dd><?php __("Filter criteria"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Reset filter criteria"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("See a student's profile"); ?></dd>
                                <dt class="see"></dt>
                                <dd><?php __("See a student's Portfolio (if not empty)"); ?></dd>
                            </dl>
                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                            <p class="info"><?php __("You can send email to students by clicking on their name."); ?></p>
                        </div>
                    </div>                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#search-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#search-info');
");
*/
?>
