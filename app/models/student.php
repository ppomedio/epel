<?php

/**
 * {app}/models/student.php
 */
class Student extends AppModel {

    var $name = 'Student';
    var $useDbConfig = 'epel';
    // Init. du tableau de validation
    var $validate = array();
    // Comportements
    var $actsAs = array(
        'Containable',
        'CleanData'
    );
    // Relations
    var $hasOne = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'id',
            'conditions' => array('User.role' => 'student')
        ),
        'PoHowtolearn' => array(
            'className' => 'PoHowtolearn',
            'foreignKey' => 'id',
            'dependent' => true
        ),
        'PoHeritage' => array(
            'className' => 'PoHeritage',
            'foreignKey' => 'id',
            'dependent' => true
        ),
        'PoPlurilingualism' => array(
            'className' => 'PoPlurilingualism',
            'foreignKey' => 'id',
            'dependent' => true
        ),
        'PoMediation' => array(
            'className' => 'PoMediation',
            'foreignKey' => 'id',
            'dependent' => true
        )
    );
    var $hasMany = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'student_id',
            'dependent' => true
        ),
        'Visit' => array(
            'className' => 'Visit',
            'foreignKey' => 'student_id',
            'dependent' => true
        )
    );
    var $belongsTo = array(
        'Diploma' => array(
            'className' => 'Diploma',
            'foreignKey' => 'diploma_id'
        ),
        'Domain' => array(
            'className' => 'Domain',
            'foreignKey' => 'domain_id'
        ),
        'LivingCountry' => array(
            'className' => 'Country',
            'foreignKey' => 'living_country_id'
        ),
        'OriginCountry' => array(
            'className' => 'Country',
            'foreignKey' => 'origin_country_id'
        ),
        'University' => array(
            'className' => 'University',
            'foreignKey' => 'university_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Please enter a name.", true)
                )
            ),
            'firstname' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your first name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                    'message' => __d('validation', "Please enter a first name.", true)
                )
            ),
            'birth' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please select your date of birth.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('date', 'ymd'),
                    'message' => __d('validation', "This date of birth is not valid.", true)
                )
            ),
            'origin_country_id' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please select your country of origin.", true)
            ),
            'living_country_id' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please select the country you live in.", true)
            ),
            'university_id' => array(
                'rule' => 'notEmpty',
                'message' => __d('validation', "Please select a university.", true)
            ),
            'diploma_info' => array(
                'rule' => array('custom', '/^[a-zA-Z0-9 \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                'allowEmpty' => true,
                'message' => __d('validation', "Some characters are not allowed…", true)
            ),
            'domain_info' => array(
                'rule' => array('custom', '/^[a-zA-Z0-9 \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                'allowEmpty' => true,
                'message' => __d('validation', "Some characters are not allowed…", true)
            ),
            'specialty' => array(
                'rule' => array('custom', '/^[a-zA-Z0-9 ,()\-\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                'allowEmpty' => true,
                'message' => __d('validation', "Some characters are not allowed…", true)
            ),
            'group' => array(
                'rule' => array('alphaNumeric'),
                'allowEmpty' => true,
                'message' => __d('validation', "Group code must only contain letters and numbers, without accentuated characters.", true)
            ),
            'secret' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter your student account creation code.", true),
                    'last' => true
                ),
                'check' => array(
                    'rule' => array('checkCode'),
                    'message' => __d('validation', "This account creation code does not exist.", true)
                )
            )
        );

        parent::__construct();
    }


    function beforeSave() {

        // Niveau d'études validé
        if (isset($this->data[$this->alias]['diploma_info']))
            $this->data[$this->alias]['diploma_info'] = ucfirst($this->data[$this->alias]['diploma_info']);

        // Domaine d'études
        if (isset($this->data[$this->alias]['domain_id'])) {
            if ($this->data[$this->alias]['domain_id'] != '0001')
                $this->data[$this->alias]['domain_info'] = '';
            else
                $this->data[$this->alias]['domain_info'] = ucfirst($this->data[$this->alias]['domain_info']);
        }

        // Création du groupe si inexistant dans la table "te_groups"
        if (!empty($this->data[$this->alias]['university_id']) && !empty($this->data[$this->alias]['group'])) {
            // Chargement du modèle
            $TeGroup = ClassRegistry::init('TeGroup');
            // Vérification de l'existence du groupe (création sinon)
            if (isset($this->data[$this->alias]['teacher_id']))
                $teacherId = $this->data[$this->alias]['teacher_id'];
            else
                $teacherId = NULL;
            $TeGroup->checkGroup($this->data[$this->alias]['university_id'], $this->data[$this->alias]['group'], $teacherId, true);
        }

        #$this->log($this->data, 'debug');
        // Sinon la sauvegarde n'a pas lieu
        return true;
    }


    /**
     * Création de compte : validation de la « phrase secrète »
     *
     */
    function checkCode() {

        if (ClassRegistry::init('Teacher')->find('count', array(
                    'conditions' => array(
                        'Teacher.secret' => $this->data['Student']['secret'],
                        'Teacher.university_id >' => 2,
                        'Teacher.real' => 1
                        ))))
            return true;
        else
            return false;
    }


    /**
     * Création de compte : enseignant responsable / université
     *
     * @return <array>
     */
    function getTeacherInfo() {

        $data = ClassRegistry::init('Teacher')->find('first', array(
            'conditions' => array(
                'Teacher.secret' => $this->data['Student']['secret'],
                'Teacher.university_id >' => 2,
                'Teacher.real' => 1
            ),
            'fields' => array(
                'id',
                'university_id'
            ),
            'recursive' => -1
        ));

        if (!empty($data))
            return $data;
        else
            return array(
                'Teacher.id' => NULL,
                'Teacher.university_id' => 1
            );
    }


    /**
     * Profil : modification des règles de validation
     *
     */
    function validateUniversity() {

        // Champ université desactivé
        unset($this->validate['university_id']);
    }


    /**
     * Recherche : modification des règles de validation
     *
     */
    function validateSearch() {

        // Suppression des règles "non-vide"
        unset($this->validate['name']['notEmpty']);
        unset($this->validate['firstname']['notEmpty']);
        unset($this->validate['origin_country_id']);
        unset($this->validate['university_id']);
        // Autorisation champs vides
        $this->validate['name']['format']['allowEmpty'] = true;
        $this->validate['firstname']['format']['allowEmpty'] = true;
    }


    /**
     * Nombre d'étudants pour une université / total
     *
     * @return <int>
     */
    function count($university) {

        $count = array('total' => NULL);
        $count['university'] = $this->find('count', array(
            'conditions' => array(
                'Student.university_id' => $university
                )));
        if (((int) $university > 2) && ($this->University->find('count') > 3))
            $count['total'] = $this->find('count', array(
                'conditions' => array(
                    'Student.university_id >' => 2,
                    'Student.real' => 1
                    )));

        return $count;
    }


    /**
     * Chargement d'un profil complet
     *
     * @param <string> $id
     * @return <array>
     */
    function getProfile($id) {

        // Chargement du profil
        $profile = $this->find('first', array(
            'conditions' => array('Student.id' => $id),
            'fields' => array(
                'id',
                'name',
                'firstname',
                'birth',
                'origin_country_id',
                'living_country_id',
                'university_id',
                'diploma_id',
                'diploma_info',
                'domain_id',
                'domain_info',
                'specialty',
                'group',
                'tea_hide',
                'vis_hide',
                'vis_passport',
                'vis_biography',
                'vis_dossier'
            ),
            'contain' => array(
                'OriginCountry',
                'LivingCountry',
                'University' => array(
                    'fields' => array(
                        'id',
                        'name'
                    )
                ),
                'Diploma' => array(
                    'fields' => array(
                        'id',
                        'name',
                        'level'
                    )
                ),
                'Domain' => array(
                    'fields' => array(
                        'id',
                        'name'
                    )
                ),
                'User' => array(
                    'fields' => array(
                        'id',
                        'email',
                        'username',
                        'photo'
                    )
                ),
                'Portfolio' => array(
                    'fields' => array(
                        'id',
                        'student_id',
                        'language_id',
                        'mother',
                        'usual'
                    ),
                    'Language' => array(
                        'fields' => array(
                            'id',
                            'name'
                        )
                    )
                )
            )
                ));

        // Traduction des noms d'université statiques
        if ((int) $profile['University']['id'] < 3)
            $profile['University']['name'] = __($profile['University']['name'], true);

        // Réorganisation [Portfolio]
        foreach ($profile['Portfolio'] as $key => $value) {
            $profile['Portfolio'][$profile['Portfolio'][$key]['Language']['name']]['id'] = $profile['Portfolio'][$key]['id'];
            $profile['Portfolio'][$profile['Portfolio'][$key]['Language']['name']]['Language']['id'] = $profile['Portfolio'][$key]['language_id'];
            $profile['Portfolio'][$profile['Portfolio'][$key]['Language']['name']]['Language']['name'] = $profile['Portfolio'][$key]['Language']['name'];
            $profile['Portfolio'][$profile['Portfolio'][$key]['Language']['name']]['mother'] = $profile['Portfolio'][$key]['mother'];
            $profile['Portfolio'][$profile['Portfolio'][$key]['Language']['name']]['usual'] = $profile['Portfolio'][$key]['usual'];
            unset($profile['Portfolio'][$key]);
        }

        // Chargement des infos de groupe
        $TeGroup = ClassRegistry::init('TeGroup');
        $profile['Group'] = $TeGroup->getGroupInfo($profile['Student']['group']);

        // Modification 'diploma_id' (ordre d'affichage)
        if ($profile['Student']['diploma_id'] == '10')
            $profile['Student']['diploma_id'] = '99';

        #$this->log($profile, 'debug');
        return $profile;
    }


    /**
     * Chargement des infos de base d'un étudiant (view)
     *
     * @param <string> $studentId
     * @return <array>
     */
    function getStudentInfo($studentId) {

        // Chargement
        $data = $this->find('first', array(
            'conditions' => array('Student.id' => $studentId),
            'fields' => array(
                'id',
                'name',
                'firstname',
                'birth',
                'origin_country_id',
                'living_country_id',
                'university_id',
                'diploma_id',
                'diploma_info',
                'domain_id',
                'domain_info',
                'specialty',
                'group',
                'tea_hide',
                'vis_hide',
                'vis_passport',
                'vis_biography',
                'vis_dossier'
            ),
            'contain' => array(
                'User' => array(
                    'fields' => array('id', 'email', 'photo')
                ),
                'OriginCountry' => array(
                    'fields' => array('id', 'name')
                ),
                'LivingCountry' => array(
                    'fields' => array('id', 'name')
                ),
                'University' => array(
                    'fields' => array('id', 'name')
                ),
                'Diploma' => array(
                    'fields' => array('id', 'name', 'level')
                ),
                'Domain' => array(
                    'fields' => array('id', 'name')
                ),
                'PoHeritage' => array(
                    'fields' => array('id', 'description')
                ),
                'PoPlurilingualism' => array(
                    'fields' => array('id', 'description')
                ),
                'PoMediation' => array(
                    'fields' => array('id', 'description')
                ),
                'Portfolio' => array(
                    'fields' => array(
                        'id',
                        'student_id',
                        'language_id',
                        'vis_passport',
                        'vis_biography',
                        'vis_dossier',
                        'mother',
                        'usual'
                    ),
                    'Language' => array(
                        'fields' => array('id', 'name')
                    )
                )
            )
                ));

        // Retour si pas de résultat
        if (empty($data))
            return false;

        // Suppression des éléments redondants
        unset($data['Student']['origin_country_id']);
        unset($data['Student']['living_country_id']);
        unset($data['Student']['university_id']);
        unset($data['Student']['diploma_id']);

        // Mise en forme
        #$this->log($data, 'debug');
        $baseInfo = $data['Student'];
        if ((int) $data['University']['id'] < 3)
            $baseInfo['university'] = __($data['University']['name'], true);
        else
            $baseInfo['university'] = $data['University']['name'];
        $baseInfo['email'] = $data['User']['email'];
        $baseInfo['photo'] = $data['User']['photo'];
        $baseInfo['origin'] = $data['OriginCountry']['name'];
        $baseInfo['living'] = $data['LivingCountry']['name'];
        $baseInfo['diploma'] = __($data['Diploma']['name'], true);
        $baseInfo['diploma_level'] = __($data['Diploma']['level'], true);
        $baseInfo['domain'] = __d('domain', $data['Domain']['name'], true);
        $baseInfo['heritage'] = $data['PoHeritage']['description'];
        $baseInfo['plurilingualism'] = $data['PoPlurilingualism']['description'];
        $baseInfo['mediation'] = $data['PoMediation']['description'];
        foreach ($data['Portfolio'] as $key => $value) {
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['id'] = $value['id'];
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['name'] = $value['Language']['name'];
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['mother'] = $value['mother'];
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['usual'] = $value['usual'];
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['vis_passport'] = $value['vis_passport'];
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['vis_biography'] = $value['vis_biography'];
            $baseInfo['portfolios'][strtolower($value['Language']['name'])]['vis_dossier'] = $value['vis_dossier'];
        }

        #$this->log($baseInfo, 'debug');
        return $baseInfo;
    }


    /**
     * Mise à jour massive du code groupe
     *
     * @param <string> $university  ID université
     * @param <string> $newGroup    nouveau code groupe
     * @param <string> $prevGroup   ancien code groupe
     */
    function massGroupSet($university, $newGroup, $prevGroup) {

        // Chargement des données
        $result = $this->find('all', array(
            'conditions' => array(
                'AND' => array(
                    'Student.university_id' => $university,
                    'Student.group' => $prevGroup
            )),
            'fields' => array('id'),
            'recursive' => -1
                ));

        if (!empty($result)) {
            // Formatage
            $students = array();
            foreach ($result as $key => $value) {
                $students['Student'][$key] = $value['Student'];
                $students['Student'][$key]['group'] = $newGroup;
            }
            // Enregistrement
            $this->saveAll($students['Student'], array(
                'callbacks' => false,
                'validate' => false,
                'atomic' => true,
                'fieldlist' => array('group')
            ));
        }
    }


}

