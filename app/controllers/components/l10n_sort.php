<?php

/**
 * {app}/controllers/components/l10n_sort.php
 *
 */
class L10nSortComponent extends Object {


    /**
     * Tri des options traduites de listes déroulantes
     * 
     * @param <array> $list
     * @param <string> $domain      nom du fichier po/mo
     * @param <bool> $dispFirst     affichage du premier élément 'Other…'
     * @return <array> $list
     */
    function setList($list = array(), $domain = 'default', $dispFirst = true) {

        // L'option 'Other…' ne fait pas partie du tri
        if (!$dispFirst) {
            if (isset($list['0001']))
                unset($list['0001']);
        }
        // Tri de la liste traduite
        $data = array();
        $i = 0;
        foreach ($list as $id => $name) {
            $data[$i]['id'] = $id;
            $data[$i]['name'] = __d($domain, $name, true);
            $data[$i]['noName'] = $this->_latinCleaner($data[$i]['name']);
            $i++;
        }
        $data = Set::sort($data, '{n}.noName', 'asc');
        $list = array();
        foreach ($data as $key => $value)
            $list[$value['id']] = $value['name'];

        return $list;

    }


    /**
     * Tri alphabétique d'une matrice de langues selon la langue de l'interface
     *
     * @param <type> $data
     * @return <array> $localizedData
     */
    function sortLocalized($data) {

        $tempSort = array();
        foreach ($data as $key => $value) {
            $tempSort[$this->_latinCleaner(__d('language', ucfirst($key), true))] = $key;
        }
        ksort($tempSort, SORT_REGULAR);
        $localizedData = array();
        foreach ($tempSort as $key => $value) {
            $localizedData[$value] = $data[$value];
        }

        return $localizedData;

    }


    private function _latinCleaner($str, $charset = 'utf-8') {

        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        $str = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $str);
        $str = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#\&[^;]+\;#', '', $str); // supprime les autres caractères

        return $str;

    }


}

