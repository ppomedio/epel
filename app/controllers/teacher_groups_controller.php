<?php

/**
 * {app}/controllers/teacher_groups_controller.php
 *
 */
class TeacherGroupsController extends AppController {

    var $name = 'TeacherGroups';
    // Modèle
    var $uses = 'TeGroup';
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Menu'
    );


    function beforeFilter() {

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect(DS . $this->params['language'] . DS . 'teacher' . DS . 'group' . DS . 'ref');

    }


    function ref() {

        // Traitement
        $invalid = false;
        if (empty($this->data)) {
            // Chargement des données
            $user = $this->_getGroupsData();
        } else {
            // Formatage
            $this->data['TeGroup']['group'] = strtoupper($this->data['TeGroup']['group']);
            // Envoi des données au modèle
            $this->TeGroup->set($this->data);
            // Modif. règles de validation si fusion
            if ($this->data['TeGroup']['group_merge']) {
                $this->TeGroup->allowExistingGroup();
            }
            // Si validation : sauvegarde
            if ($this->TeGroup->validates()) {
                // Données supplémentaires
                $this->data['TeGroup']['university_id'] = $this->Session->read('Auth.User.info.university_id');
                // Copie pour affichage
                $newGroup = $this->data['TeGroup']['group'];
                // Enregistrement
                if (!empty($this->data['TeGroup']['prevGroup'])) {
                    /**
                     * Changement de code groupe
                     */
                    // Copie pour affichage
                    $newGroup = $this->data['TeGroup']['group'];
                    // Mise à jour des étudiants du groupe
                    $this->loadModel('Student');
                    $this->Student->massGroupSet(
                            $this->data['TeGroup']['university_id'],
                            $this->data['TeGroup']['group'],
                            $this->data['TeGroup']['prevGroupCode']
                    );
                    // Fusion : si le groupe cible n'existe pas -> changement de code groupe…
                    if ($this->data['TeGroup']['group_merge']
                            && !$this->TeGroup->checkGroup($this->data['TeGroup']['university_id'], $this->data['TeGroup']['group']))
                        $this->data['TeGroup']['group_merge'] = false;
                    // Bascule fusion / édition
                    if ($this->data['TeGroup']['group_merge']) {
                        // Fusion : suppression du groupe source
                        $this->TeGroup->delete($this->data['TeGroup']['prevGroup']);
                        // Message
                        $this->Session->setFlash(__("The resulting group code is", true) . ' ' . $newGroup . '.');
                    } else {
                        // Édition : changement de code pour le groupe
                        $this->TeGroup->id = $this->data['TeGroup']['prevGroup'];
                        $this->TeGroup->saveField('group', $this->data['TeGroup']['group']);
                        // Message
                        $this->Session->setFlash(__("The group code is now", true) . ' ' . $newGroup . '.');
                    }
                } else {
                    /**
                     * Ajout d'un nouveau groupe
                     */
                    if ($this->data['TeGroup']['ref_teacher'])
                        $this->data['TeGroup']['teacher_id'] = $this->AppAuth->user('id');
                    else
                        $this->data['TeGroup']['teacher_id'] = NULL;
                    if ($this->TeGroup->save($this->data, array(
                                'validate' => false,
                                'fieldList' => array(
                                    'university_id',
                                    'teacher_id',
                                    'group'
                                )
                            ))) {
                        // Message
                        $this->Session->setFlash(__("The group", true) . ' '
                                . $newGroup . ' ' . __("has been added.", true));
                    }
                }
            } else {
                // Vue : état du panneau
                if (!empty($this->data['TeGroup']['prevGroup']))
                    $invalid = 'edit';
                else
                    $invalid = 'add';
            }
            // Rechargement des données pour le reste de la vue
            $user = $this->_getGroupsData();
        }
        // Nombre d'étudiants par groupe
        $this->loadModel('Student');
        foreach ($user as $key => $value) {
            $user[$key]['TeGroup']['count'] = $this->Student->find('count', array(
                        'conditions' => array('Student.group =' => $value['TeGroup']['group'])
                    ));
        }
        // Chargement des variables de vue
        $this->set('user', $user);
        $this->set('group', $this->TeGroup->getGroupsList($this->Session->read('Auth.User.info.university_id')));
        $this->set('moreInfo', $this->TeGroup->Teacher->read('phone', $this->AppAuth->user('id')));
        $this->set('invalid', $invalid);
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.TeGroup.page', $this->params['paging']['TeGroup']['options']['page']);
        $this->Session->write('Auth.User.pagination.TeGroup.order', $this->params['paging']['TeGroup']['options']['order']);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'teacher' . DS . 'group' . DS . 'list-page');
            return;
        } else {
            // Affichage normal
            $this->set('title_for_layout', __("Teacher", true) . ' &bull; ' . __("Group management", true));
        }

    }


    function ref_set() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Paramètres url
            $group['id'] = $this->params['pass'][0];
            $group['code'] = $this->params['pass'][1];
            // Sauvegarde
            $this->TeGroup->id = $group['id'];
            $this->TeGroup->saveField('teacher_id', $this->AppAuth->user('id'));
            // Message
            __("You are the referring teacher for the group");
            echo ' ' . $group['code'];
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    function ref_unset() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Paramètres url
            $group['id'] = $this->params['pass'][0];
            $group['code'] = $this->params['pass'][1];
            // Sauvegarde
            $this->TeGroup->id = $group['id'];
            $this->TeGroup->saveField('teacher_id', NULL);
            // Message
            __("There is no referring teacher for the group");
            echo ' ' . $group['code'];
            // Sortie
            $this->autoRender = false;
            exit();
        }

    }


    function delete() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Paramètres url
            $group['id'] = $this->params['pass'][0];
            $group['code'] = $this->params['pass'][1];
            // Suppression
            $this->TeGroup->delete($group['id']);
            // Rappel de pagination
            $this->Session->write('Pagination.set', true);
            // Appel de l'action pour rafraîchir la vue
            $this->setAction('ref');
        }

    }


    /**
     *
     * @return <array>
     */
    private function _getGroupsData() {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.TeGroup')) {
            $page = $this->Session->read('Auth.User.pagination.TeGroup.page');
            $order = $this->Session->read('Auth.User.pagination.TeGroup.order');
            $this->Session->delete('Auth.User.pagination.TeGroup');
        } else {
            $page = NULL;
            $order = array('TeGroup.group' => 'ASC');
        }

        // Chargement des paramètres
        $this->paginate = array(
            'conditions' => array(
                'TeGroup.university_id' => $this->Session->read('Auth.User.info.university_id')
            ),
            'limit' => 12,
            'page' => $page,
            'order' => $order,
            'fields' => array(
                'id',
                'teacher_id',
                'group'
            ),
            'contain' => array(
                'Teacher' => array(
                    'fields' => array(
                        'id',
                        'name',
                        'firstname',
                        'quality',
                        'specialty',
                        'phone'
                    ),
                    'User' => array(
                        'fields' => array(
                            'id',
                            'email',
                            'username',
                            'photo'
                        )
                    )
                )
            )
        );

        // Envoi de la pagination
        return $this->paginate('TeGroup');

    }


}

