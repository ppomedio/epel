
                    <section>
                        <h2><?php __("Profile"); echo " (";  __("editing"); echo "…)"; ?></h2>
                        <?php echo $this->Form->create('Student', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/student/base/profile',
                            'id' => 'profile-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Student'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        $available = Configure::read('epel.locales.available');
                        $dateNum = $available[$this->params['language']]['dateNum'];
                        echo $this->Form->input('name', array(
                            'label' => __("Name", true)
                            ));
                        echo $this->Form->input('firstname', array(
                            'label' => __("First name", true)
                            ));
                        echo $this->Form->input('birth', array(
                            'type' => 'date',
                            'label' => __("Date of birth", true),
                            'dateFormat' =>  $dateNum,
                            'minYear' => date('Y') - 76,
                            'maxYear' => date('Y') - 16,
                            'separator' => ' '
                            ));
                        echo $this->Form->input('origin_country_id', array(
                            'label' => __("Country of origin", true),
                            'options' => $originCountry,
                            'empty' => "…"
                            ));
                        echo $this->Form->input('living_country_id', array(
                            'label' => __("Country of residence", true),
                            'options' => $livingCountry,
                            'empty' => "…"
                            ));
                        echo $this->Form->input('diploma_id', array(
                            'label' => __("Level of validated studies", true),
                            'options' => $diploma,
                            'default' => '99'
                            ));
                        echo $this->Form->input('diploma_info', array(
                            'label' => __("Validated studies", true),
                            ));
                        echo $this->Form->input('domain_id', array(
                            'label' => __("Field of study", true),
                            'options' => $domain,
                            'empty' => array('0001' => __("Other…", true))
                            ));
                        echo $this->Form->input('domain_info', array(
                            'label' => __("Other field of study", true),
                            ));
                        echo $this->Form->input('specialty', array(
                            'label' => __("Specialty", true)
                            ));
                        ?>

                        </fieldset>
                        <p class="info"><?php
                        $levelDocs = Configure::read('epel.leveldesc.url');
                        if (isset($levelDocs[$this->params['language']]))
                            $levelDoc = $levelDocs[$this->params['language']];
                        else
                            $levelDoc = $levelDocs['en'];
                        __("To help you define your European Qualification Framework (EQF) level of study, you can use the");
                        ?> <a id="<?php echo __("Download the", true) . " " . __("EQF levels descriptors", true); ?>" class="link" href="<?php echo $levelDoc; ?>"><?php __("EQF levels descriptors"); ?></a>.</p>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ui.datepicker',
        'i18n/jquery.ui.datepicker-' . $this->params['language']
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

// État initial de "div input#StudentDomainInfo" -> buffer Js
if ($this->data['Student']['domain_id'] != '0001')
    $this->Js->Buffer("
        $('input#StudentDomainInfo').parent('div').css('display', 'none');"
    );

$this->Js->Buffer("
    // Tips
    addTipInfo('#profile-form');
    // Tooltips
    tooltipLink('#profile-form p.info');
    // Bascule de div input#StudentDomainInfo
    $('select#StudentDomainId').change(function() {
        if ($(this).val() == '0001')
            $('input#StudentDomainInfo').parent('div').show('slow');
        else
            $('input#StudentDomainInfo').parent('div').hide('slow');
    });
    // UIDatePicker <-> Cake
    $('#StudentBirthMonth').after('<input class=\"datepicker\" type=\"hidden\"/>');
    setDatePicker();
");

?>