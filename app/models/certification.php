<?php

/**
 * {app}/models/certification.php
 */
class Certification extends AppModel {

    var $name = 'Certification';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array(
        'Containable',
        'CleanData'
    );
    // Relations
    var $hasMany = array(
        'Certificate' => array(
            'className' => 'Certificate',
            'foreignKey' => 'certification_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => __d('validation', "Please enter a certification name.", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z0-9 ()\.\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^\^\*\?\$\|\/\\\\_<>¶@§°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷\t]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true),
                    'last' => true
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => __d('validation', "This certification name already exists.", true)
                )
            )
        );

        parent::__construct();

    }


}

