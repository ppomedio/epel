
                    <section>
                        <h2><?php __("University"); ?> / <?php __("Group"); ?></h2>
                        <div id="university-info"><?php
                            if ((int)$user['Student']['university_id'] == 1) { ?>

                            <dl>
                                <dt><?php __("College / University"); ?></dt>
                                <dd><?php __("Undefined university"); ?></dd>
                            </dl>
                            <p class="info"><?php
                            __('In order to modify university-related information, please');
                            echo " " . $this->Html->link(__("contact the administrator", true),
                                array(
                                    'controller' => 'contact',
                                    'action' => 'form'
                                ),
                                array('class'=>'iframe contact')
                            );
                            ?>.</p><?php
                            } else { ?>
                            <dl>
                                <dt><?php __("College / University"); ?></dt>
                                <dd><?php echo $user['University']['name']; ?></dd>
                                <dt><?php __("Group code"); ?></dt>
                                <?php
                                if ($user['Student']['group']) {
                                    echo '<dd>' . $user['Student']['group'] . '</dd>';
                                    if (!empty($user['Group']['Teacher']['id'])) { ?>

                                <div id="data_contact" class="tooltip-data"><?php
                                __('Send an email to');
                                echo ' ' . $user['Group']['Teacher']['firstname'] . ' ' . $user['Group']['Teacher']['name'];
                                ?></div>
                                <dt><?php __("Referring teacher"); ?></dt>
                                <dd class="bolder"><?php
                                    echo $this->Html->link($user['Group']['Teacher']['firstname'] . ' ' . $user['Group']['Teacher']['name'],
                                        array(
                                            'controller' => 'contact',
                                            'action' => 'form',
                                            base64_encode($user['Group']['Teacher']['id'] . '_' . $user['Group']['Teacher']['firstname'] . ' ' . $user['Group']['Teacher']['name'])
                                        ),
                                        array(
                                            'id' => 'contact',
                                            'class' => 'iframe contact link'
                                        )
                                    );
                                    if (!empty($user['Group']['Teacher']['User']['photo'])) { ?>
                                    <span id="teacher-photo" class="photo"></span>
                                    <div id="data_teacher-photo" class="tooltip-data"><img src="<?php
                                    echo '/files/users/' . $user['Group']['Teacher']['id'] . '/photo/' . $user['Group']['Teacher']['User']['photo'];
                                    ?>" /></div>
                                        <?php }
                                    ?></dd><?php }
                                    } else { ?>

                                    <dd><?php __("Undefined"); ?></dd>
                                <?php } ?>

                            </dl>
                            <p class="info"><?php __("You can send an email to your referring teacher by clicking on his/her name above."); ?></p>
                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls">
                                    <?php
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/student/base/group',
                                        array(
                                            'id' => 'group',
                                            'class' => 'edit'
                                        )
                                    );
                                    ?>

                                </div>
                            </div>
                            <?php } ?>

                        </div>
                        <div id="data_group" class="tooltip-data"><?php __('Edit group code'); ?></div>
                    </section>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#university-info');
    // Tooltips
    tooltipPhoto();
");

?>