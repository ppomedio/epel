-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: epel
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `user_id` mediumint(7) unsigned zerofill NOT NULL,
  `role` enum('unknown','student','teacher','visitor','admin') NOT NULL DEFAULT 'unknown',
  `university_id` smallint(4) unsigned zerofill NOT NULL DEFAULT '0000',
  `action` enum('…','login_failure','login','logout','acc_delete','lang_add','lang_delete','file_upload','file_delete','email') NOT NULL DEFAULT '…',
  `extra` text NOT NULL,
  `ip` varchar(39) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activities_datetime` (`datetime`),
  KEY `activities_user_id` (`user_id`),
  KEY `activities_role` (`role`),
  KEY `activities_university_id` (`university_id`),
  KEY `activities_action` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assess_items`
--

DROP TABLE IF EXISTS `assess_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assess_items` (
  `id` smallint(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `competency` enum('listen','read','converse','speak','write') NOT NULL,
  `level` enum('A1','A2','B1','B2','C1','C2') NOT NULL,
  `order` tinyint(2) unsigned zerofill NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `competency` (`competency`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assess_items`
--

LOCK TABLES `assess_items` WRITE;
/*!40000 ALTER TABLE `assess_items` DISABLE KEYS */;
INSERT INTO `assess_items` VALUES (0001,'listen','A1',01,'I can understand basic words and phrases about myself and my family when people speak slowly and clearly.'),(0002,'listen','A1',02,'I can understand simple instructions, directions and comments.'),(0003,'listen','A1',03,'I can understand the names of everyday objects in my immediate environment.'),(0004,'listen','A1',04,'I can understand basic greetings and routine phrases (e.g., please, thank you).'),(0005,'listen','A1',05,'I can understand simple questions about myself when people speak slowly and clearly.'),(0006,'listen','A1',06,'I can understand numbers and prices.'),(0007,'listen','A1',07,'I can understand days of the week and months of the year.'),(0008,'listen','A1',08,'I can understand times and dates.'),(0009,'listen','A2',01,'I can understand what people say to me in simple everyday conversation when they speak slowly and clearly.'),(0010,'listen','A2',02,'I can understand everyday words and phrases relating to areas of immediate personal relevance (e.g., family, student life, local environment, employment).'),(0011,'listen','A2',03,'I can understand everyday words and phrases relating to areas of personal interest (e.g., hobbies, social life, holidays, music, TV, films, travel).'),(0012,'listen','A2',04,'I can grasp the essential elements of clear simple messages and recorded announcements (e.g., on the telephone, at the railway station).'),(0013,'listen','A2',05,'I can understand simple phrases, questions and information relating to basic personal needs (e.g., shopping, eating out, going to the doctor).'),(0014,'listen','A2',06,'I can follow simple directions (e.g., how to get from X to Y) by foot or public transport.'),(0015,'listen','A2',07,'I can usually identify the topic of conversation around me when people speak slowly and clearly.'),(0016,'listen','A2',08,'I can follow changes of topic in factual TV news items and form an idea of the main content.'),(0017,'listen','A2',09,'I can identify the main point of TV news items reporting events, accidents, etc., if there is visual support.'),(0018,'listen','B1',01,'I can follow the gist of everyday conversation when people speak clearly to me in standard dialect.'),(0019,'listen','B1',02,'I can understand straightforward factual information about everyday, study- or work-related topics, identifying both general messages and specific details, provided speech is clearly articulated in a generally familiar accent.'),(0020,'listen','B1',03,'I can understand the main points of discussions on familiar topics in everyday situations when people speak clearly in standard dialect.'),(0021,'listen','B1',04,'I can follow a lecture or talk within my own academic or professional field, provided the subject matter is familiar and the presentation straightforward and clearly structured.'),(0022,'listen','B1',05,'I can catch the main elements of radio news bulletins and recorded audio material on familiar topics delivered in clear standard speech.'),(0023,'listen','B1',06,'I can follow many TV programmes on topics of personal or cultural interest broadcast in standard dialect.'),(0024,'listen','B1',07,'I can follow many films in which visuals and action carry much of the storyline, when the language is clear and straightforward.'),(0025,'listen','B1',08,'I can follow detailed directions, messages and information (e.g., travel arrangements, recorded weather forecasts, answering-machines).'),(0026,'listen','B1',09,'I can understand simple technical information, such as operating instructions for everyday equipment.'),(0027,'listen','B2',01,'I can understand standard spoken language on both familiar and unfamiliar topics in everyday situations even in a noisy environment.'),(0028,'listen','B2',02,'I can with some effort catch much of what is said around me, but may find it difficult to understand a discussion between several native speakers who do not modify their language in any way.'),(0029,'listen','B2',03,'I can understand announcements and messages on concrete and abstract topics spoken in standard dialect at normal speed.'),(0030,'listen','B2',04,'I can follow extended talks delivered in standard dialect on cultural, intercultural and social issues (e.g., customs, media, lifestyle, EU).'),(0031,'listen','B2',05,'I can follow complex lines of argument, provided these are clearly signposted and the topic is reasonably familiar.'),(0032,'listen','B2',06,'I can follow the essentials of lectures, talks and reports and other forms of academic or professional presentation in my field.'),(0033,'listen','B2',07,'I can follow most TV news programmes, documentaries, interviews, talk shows and the majority of films in standard dialect.'),(0034,'listen','B2',08,'I can follow most radio programmes and audio material delivered in standard dialect and identify the speaker’s mood, tone, etc.'),(0035,'listen','B2',09,'I am sensitive to expressions of feeling and attitudes (e.g., critical, ironic, supportive, flippant, disapproving).'),(0036,'listen','C1',01,'I can follow extended speech even when it is not clearly structured and when relationships are only implied and not signalled explicitly.'),(0037,'listen','C1',02,'I can recognize a wide range of idiomatic expressions and colloquialisms, appreciating register shifts.'),(0038,'listen','C1',03,'I can understand enough to follow extended speech on abstract and complex topics of academic or vocational relevance, though I may need to confirm occasional details, especially if the accent is unfamiliar.'),(0039,'listen','C1',04,'I can easily follow complex interactions between third parties in group discussion and debate, even on abstract and unfamiliar topics.'),(0040,'listen','C1',05,'I can follow most lectures, discussions and debates in my academic or professional field with relative ease.'),(0041,'listen','C1',06,'I can understand complex technical information, such as operating instructions and specifications for familiar products and services.'),(0042,'listen','C1',07,'I can extract specific information from poor quality, audibly distorted public announcements (e.g., in a station, sports stadium, etc.).'),(0043,'listen','C1',08,'I can understand a wide range of recorded and broadcast audio material, including some non-standard usage, and identify finer points of detail including implicit attitudes and relationships between speakers.'),(0044,'listen','C1',09,'I can follow films employing a considerable degree of slang and idiomatic usage.'),(0045,'listen','C2',01,'I have no difficulty in understanding any kind of spoken language, whether live or broadcast, delivered at fast native speed.'),(0046,'listen','C2',02,'I can follow specialised lectures and presentations employing a high degree of colloquialism, regional usage or unfamiliar terminology.'),(0047,'read','A1',01,'I can pick out familiar names, words and phrases in very short simple texts.'),(0048,'read','A1',02,'I can understand words and phrases on simple everyday signs and notices (e.g., exit, no smoking, danger, days of the week, times).'),(0049,'read','A1',03,'I can understand simple forms well enough to give basic personal details (e.g., name, address, date of birth).'),(0050,'read','A1',04,'I can understand simple written messages and comments relating to my studies (e.g., “well done”, “revise”).'),(0051,'read','A1',05,'I can understand short simple messages on greeting cards and postcards (e.g., holiday greetings, birthday greetings).'),(0052,'read','A1',06,'I can get an idea of the content of simple informational material if there is pictorial support (e.g., posters, catalogues, advertisements).'),(0053,'read','A1',07,'I can follow short simple written directions (e.g., to go from X to Y).'),(0054,'read','A2',01,'I can understand short simple messages and texts containing basic everyday vocabulary relating to areas of personal relevance or interest.'),(0055,'read','A2',02,'I can understand everyday signs and public notices (e.g., on the street, in shops, hotels, railway stations).'),(0056,'read','A2',03,'I can find specific predictable information in simple everyday material such as advertisements, timetables, menus, directories, brochures.'),(0057,'read','A2',04,'I can understand instructions when expressed in simple language (e.g., how to use a public telephone).'),(0058,'read','A2',05,'I can understand regulations when expressed in simple language (e.g., safety, attendance at lectures).'),(0059,'read','A2',06,'I can understand short simple personal letters giving or requesting information about everyday life or offering an invitation.'),(0060,'read','A2',07,'I can identify key information in short newspaper/magazine reports recounting stories or events.'),(0061,'read','A2',08,'I can understand basic information in routine letters and messages (e.g., hotel reservations, personal telephone messages).'),(0062,'read','B1',01,'I can read straightforward factual texts on subjects related to my field of interest with a reasonable level of understanding.'),(0063,'read','B1',02,'I can recognize significant points in straightforward newspaper articles on familiar subjects.'),(0064,'read','B1',03,'I can identify the main conclusions in clearly signalled argumentative texts related to my academic or professional field.'),(0065,'read','B1',04,'I can understand the description of events, feelings and wishes in personal letters and e- mails well enough to correspond with a pen friend.'),(0066,'read','B1',05,'I can find and understand relevant information in everyday material, such as standard letters, brochures and short official documents.'),(0067,'read','B1',06,'I can understand clearly written straightforward instructions (e.g., for using a piece of equipment, for answering questions in an exam).'),(0068,'read','B1',07,'I can scan longer texts in order to locate desired information, and gather information from different parts of a text, or from different texts in order to fulfil a specific task.'),(0069,'read','B1',08,'I can follow the plot of clearly structured narratives and modern literary texts.'),(0070,'read','B2',01,'I can quickly scan through long and complex texts on a variety of topics in my field to locate relevant details.'),(0071,'read','B2',02,'I can read correspondence relating to my field of interest and readily grasp the essential meaning.'),(0072,'read','B2',03,'I can obtain information, ideas and opinions from highly specialized sources within my academic or professional field.'),(0073,'read','B2',04,'I can understand articles on specialized topics using a dictionary and other appropriate reference resources.'),(0074,'read','B2',05,'I can quickly identify the content and relevance of news items, articles and reports on a wide range of professional topics, deciding whether closer study is worthwhile.'),(0075,'read','B2',06,'I can understand articles and reports concerned with contemporary problems in which the writers adopt particular stances or viewpoints.'),(0076,'read','B2',07,'I can understand lengthy complex instructions in my field, including details on conditions or warnings, provided I can reread difficult sections.'),(0077,'read','B2',08,'I can readily appreciate most narratives and modern literary texts (e.g., novels, short stories, poems, plays).'),(0078,'read','C1',01,'I can understand in detail highly specialized texts in my own academic or professional field, such as research reports and abstracts.'),(0079,'read','C1',02,'I can understand any correspondence given the occasional use of a dictionary.'),(0080,'read','C1',03,'I can read contemporary literary texts with no difficulty and with appreciation of implicit meanings and ideas.'),(0081,'read','C1',04,'I can appreciate the relevant socio-historical or political context of most literary works.'),(0082,'read','C1',05,'I can understand detailed and complex instructions for a new machine or procedure, whether or not the instructions relate to my own area of speciality, provided I can reread difficult sections.'),(0083,'read','C2',01,'I can understand a wide range of long and complex texts, appreciating subtle distinctions of style and implicit as well as explicit meaning.'),(0084,'read','C2',02,'I can understand and interpret critically virtually all forms of the written language including abstract, structurally complex, or highly colloquial literary and non-literary writings.'),(0085,'read','C2',03,'I can make effective use of complex, technical or highly specialized texts to meet my academic or professional purposes.'),(0086,'read','C2',04,'I can critically appraise classical as well as contemporary literary texts in different genres.'),(0087,'read','C2',05,'I can appreciate the finer subtleties of meaning, rhetorical effect and stylistic language use in critical or satirical forms of discourse.'),(0088,'read','C2',06,'I can understand complex factual documents such as technical manuals and legal contracts.'),(0089,'converse','A1',01,'I can say basic greetings and phrases (e.g., please, thank you), ask how someone is and say how I am.'),(0090,'converse','A1',02,'I can say who I am, ask someone’s name and introduce someone.'),(0091,'converse','A1',03,'I can say I don’t understand, ask people to repeat what they say or speak more slowly, attract attention and ask for help.'),(0092,'converse','A1',04,'I can ask how to say something in the language or what a word means.'),(0093,'converse','A1',05,'I can ask and answer simple direct questions on very familiar topics (e.g., family, student life) with help from the person I am talking to.'),(0094,'converse','A1',06,'I can ask people for things and give people things.'),(0095,'converse','A1',07,'I can handle numbers, quantities, cost and time.'),(0096,'converse','A1',08,'I can make simple purchases, using pointing and gestures to support what I say.'),(0097,'converse','A1',09,'I can reply in an interview to simple direct questions about personal details if these are spoken very slowly and clearly in standard dialect.'),(0098,'converse','A2',01,'I can handle short social exchanges and make myself understood if people help me.'),(0099,'converse','A2',02,'I can participate in short conversations in routine contexts on topics of interest.'),(0100,'converse','A2',03,'I can make and respond to invitations, suggestions, apologies and requests for permission.'),(0101,'converse','A2',04,'I can say what I like or dislike, agree or disagree with people, and make comparisons.'),(0102,'converse','A2',05,'I can express what I feel in simple terms, and express thanks.'),(0103,'converse','A2',06,'I can discuss what to do, where to go, make arrangements to meet (e.g., in the evening, at the weekend).'),(0104,'converse','A2',07,'I can ask and answer simple questions about familiar topics (e.g., weather, hobbies, social life, music, sport).'),(0105,'converse','A2',08,'I can ask and answer simple questions about things that have happened (e.g., yesterday, last week, last year).'),(0106,'converse','A2',09,'I can handle simple telephone calls (e.g., say who is calling, ask to speak to someone, give my number, take a simple message).'),(0107,'converse','A2',10,'I can make simple transactions (e.g., in shops, post offices, railway stations) and order something to eat or drink.'),(0108,'converse','A2',11,'I can get simple practical information (e.g., asking for directions, booking accommodation, going to the doctor).'),(0109,'converse','B1',01,'I can readily handle conversations on most topics that are familiar or of personal interest, with generally appropriate use of register.'),(0110,'converse','B1',02,'I can sustain an extended conversation or discussion but may sometimes need a little help in communicating my thoughts.'),(0111,'converse','B1',03,'I can take part in routine formal discussion on familiar subjects in my academic or professional field if it is conducted in clearly articulated speech in standard dialect.'),(0112,'converse','B1',04,'I can exchange, check and confirm factual information on familiar routine and non-routine matters within my field with some confidence.'),(0113,'converse','B1',05,'I can express and respond to feelings and attitudes (e.g., surprise, happiness, sadness, interest, uncertainty, indifference).'),(0114,'converse','B1',06,'I can agree and disagree politely, exchange personal opinions, negotiate decisions and ideas.'),(0115,'converse','B1',07,'I can express my thoughts about abstract or cultural topics such as music or films, and give brief comments on the views of others.'),(0116,'converse','B1',08,'I can explain why something is a problem, discuss what to do next, compare and contrast alternatives.'),(0117,'converse','B1',09,'I can obtain detailed information, messages, instructions and explanations, and can ask for and follow detailed directions.'),(0118,'converse','B1',10,'I can handle most practical tasks in everyday situations (e.g., making telephone enquiries, asking for a refund, negotiating purchase).'),(0119,'converse','B1',11,'I can provide concrete information required in an interview/consultation (e.g., describe symptoms to a doctor), but with limited precision.'),(0120,'converse','B1',12,'I can take some initiatives in an interview/consultation (e.g., bring up a new subject) but am very dependent on the interviewer to provide support.'),(0121,'converse','B1',13,'I can use a prepared questionnaire to carry out a structured interview, with some spontaneous follow-up questions.'),(0122,'converse','B2',01,'I can participate fully in conversations on general topics with a degree of fluency and naturalness, and appropriate use of register.'),(0123,'converse','B2',02,'I can participate effectively in extended discussions and debates on subjects of personal, academic or professional interest, marking clearly the relationship between ideas.'),(0124,'converse','B2',03,'I can account for and sustain my opinion in discussion by providing relevant explanations, arguments and comments.'),(0125,'converse','B2',04,'I can express, negotiate and respond sensitively to feelings, attitudes, opinions, tone, viewpoints.'),(0126,'converse','B2',05,'I can exchange detailed factual information on matters within my academic or professional field.'),(0127,'converse','B2',06,'I can help along the progress of a project by inviting others to join in, express their opinions, etc.'),(0128,'converse','B2',07,'I can cope linguistically with potentially complex problems in routine situations (e.g., complaining about goods and services).'),(0129,'converse','B2',08,'I can cope adequately with emergencies (e.g., summon medical assistance, telephone the police or breakdown service).'),(0130,'converse','B2',09,'I can handle personal interviews with ease, taking initiatives and expanding ideas with little help or prodding from an interviewer.'),(0131,'converse','B2',10,'I can carry out an effective, fluent interview, departing spontaneously from prepared questions, following up and probing interesting replies.'),(0132,'converse','C1',01,'I can express myself fluently, accurately and spontaneously on a wide range of general, academic or professional topics.'),(0133,'converse','C1',02,'I can use language flexibly and effectively for social purposes, including emotional, allusive and joking usage.'),(0134,'converse','C1',03,'I can participate effectively in extended debates on abstract and complex topics of a specialist nature in my academic or professional field.'),(0135,'converse','C1',04,'I can easily follow and contribute to complex interactions between third parties in group discussion even on abstract or less familiar topics.'),(0136,'converse','C1',05,'I can argue a formal position convincingly, responding to questions and comments and answering complex lines of counter argument fluently, spontaneously and appropriately.'),(0137,'converse','C1',06,'I can participate fully in an interview, as either interviewer or interviewee, fluently expanding and developing the point under discussion, and handling interjections well.'),(0138,'converse','C2',01,'I can understand any native speaker interlocutor, given an opportunity to adjust to a non- standard accent or dialect.'),(0139,'converse','C2',02,'I can converse comfortably and appropriately, unhampered by any linguistic limitations in conducting a full social and personal life.'),(0140,'converse','C2',03,'I can hold my own in formal discussion of complex and specialist issues in my field, putting forward and sustaining an articulate and persuasive argument, at no disadvantage to native speakers.'),(0141,'converse','C2',04,'I can keep up my side of the dialogue as interviewer or interviewee with complete confidence and fluency, structuring the talk and interacting authoritatively at no disadvantage to a native speaker.'),(0142,'speak','A1',01,'I can give basic personal information about myself (e.g., age, address, family, subjects of study).'),(0143,'speak','A1',02,'I can use simple words and phrases to describe where I live.'),(0144,'speak','A1',03,'I can use simple words and phrases to describe people I know.'),(0145,'speak','A1',04,'I can read a very short rehearsed statement (e.g., to introduce a speaker, propose a toast).'),(0146,'speak','A2',01,'I can describe myself, my family and other people I know.'),(0147,'speak','A2',02,'I can describe my home and where I live.'),(0148,'speak','A2',03,'I can say what I usually do at home, at university, in my free time.'),(0149,'speak','A2',04,'I can describe my educational background and subjects of study.'),(0150,'speak','A2',05,'I can describe plans, arrangements and alternatives.'),(0151,'speak','A2',06,'I can give short simple descriptions of events or tell a simple story.'),(0152,'speak','A2',07,'I can describe past activities and personal experiences (e.g., what I did at the weekend).'),(0153,'speak','A2',08,'I can explain what I like and don’t like about something.'),(0154,'speak','A2',09,'I can give simple descriptions of things and make comparisons.'),(0155,'speak','A2',10,'I can deliver very short rehearsed announcements of predictable learnt content.'),(0156,'speak','A2',11,'I can give a short rehearsed presentation on a familiar subject in my academic or professional field.'),(0157,'speak','B1',01,'I can give a reasonably fluent description of a subject within my academic or professional field, presenting it as a linear sequence of points.'),(0158,'speak','B1',02,'I can narrate a story or relate the plot of a film or book.'),(0159,'speak','B1',03,'I can describe personal experiences, reactions, dreams, hopes, ambitions, real, imagined or unexpected events.'),(0160,'speak','B1',04,'I can briefly give reasons and explanations for opinions, plans and actions.'),(0161,'speak','B1',05,'I can develop an argument well enough to be followed without difficulty most of the time.'),(0162,'speak','B1',06,'I can give a simple summary of short written texts.'),(0163,'speak','B1',07,'I can give detailed accounts of problems and incidents (e.g., reporting a theft, traffic accident).'),(0164,'speak','B1',08,'I can deliver short rehearsed announcements and statements on everyday matters within my field.'),(0165,'speak','B1',09,'I can give a short and straightforward prepared presentation on a chosen topic in my academic or professional field in a reasonably clear and precise manner.'),(0166,'speak','B2',01,'I can give clear detailed descriptions on a wide range of subjects relating to my field, expanding and supporting ideas with subsidiary points and relevant examples.'),(0167,'speak','B2',02,'I can explain a viewpoint on a topical issue, giving the advantages and disadvantages of various options.'),(0168,'speak','B2',03,'I can develop a clear coherent argument, linking ideas logically and expanding and supporting my points with appropriate examples.'),(0169,'speak','B2',04,'I can outline an issue or a problem clearly, speculating about causes, consequences and hypothetical situations.'),(0170,'speak','B2',05,'I can summarize short discursive or narrative material (e.g., written, radio, television).'),(0171,'speak','B2',06,'I can deliver announcements on most general topics with a degree of clarity, fluency and spontaneity which causes no strain or inconvenience to the listener.'),(0172,'speak','B2',07,'I can give a clear, systematically developed presentation on a topic in my field, with highlighting of significant points and relevant supporting detail.'),(0173,'speak','B2',08,'I can depart spontaneously from a prepared text and follow up points raised by an audience.'),(0174,'speak','C1',01,'I can give clear detailed descriptions of complex subjects in my field.'),(0175,'speak','C1',02,'I can elaborate a detailed argument or narrative, integrating sub-themes, developing particular points and rounding off with an appropriate conclusion.'),(0176,'speak','C1',03,'I can give a detailed oral summary of long and complex texts relating to my area of study.'),(0177,'speak','C1',04,'I can deliver announcements fluently, almost effortlessly, using stress and intonation to convey finer shades of meaning precisely.'),(0178,'speak','C1',05,'I can give a clear, well-structured presentation on a complex subject in my field, expanding and supporting points of view with appropriate reasons and examples.'),(0179,'speak','C2',01,'I can produce clear, smoothly-flowing well-structured speech with an effective logical structure which helps the recipient to notice and remember significant points'),(0180,'speak','C2',02,'I can give clear, fluent, elaborate and often memorable descriptions.'),(0181,'speak','C2',03,'I can summarize and synthesize information and ideas from a variety of specialized sources in my field in a clear and flexible manner.'),(0182,'speak','C2',04,'I can present a complex topic in my field confidently and articulately, and can handle difficult and even hostile questioning.'),(0183,'write','A1',01,'I can fill in a simple form or questionnaire with my personal details (e.g., date of birth, address, nationality).'),(0184,'write','A1',02,'I can write a greeting card or simple postcard.'),(0185,'write','A1',03,'I can write simple phrases and sentences about myself (e.g., where I live, how many brothers and sisters I have).'),(0186,'write','A1',04,'I can write a short simple note or message (e.g., to tell somebody where I am or where to meet).'),(0187,'write','A2',01,'I can write short simple notes and messages (e.g., saying that someone telephoned, arranging to meet someone, explaining absence).'),(0188,'write','A2',02,'I can fill in a questionnaire or write a simple curriculum vitae giving personal information.'),(0189,'write','A2',03,'I can write about aspects of my everyday life in simple linked sentences (e.g., family, college life, holidays, work experience).'),(0190,'write','A2',04,'I can write short simple imaginary biographies and stories about people.'),(0191,'write','A2',05,'I can write very short basic descriptions of events, past activities and personal experiences.'),(0192,'write','A2',06,'I can open and close a simple personal letter using appropriate phrases and greetings.'),(0193,'write','A2',07,'I can write a very simple personal letter (e.g., accepting or offering an invitation, thanking someone for something, apologizing).'),(0194,'write','A2',08,'I can open and close a simple formal letter using appropriate phrases and greetings.'),(0195,'write','A2',09,'I can write very basic formal letters requesting information (e.g., about summer jobs, hotel accommodation).'),(0196,'write','B1',01,'I can write a description of an event (e.g., a recent trip), real or imagined.'),(0197,'write','B1',02,'I can write notes conveying simple information of immediate relevance to people who feature in my everyday life, getting across comprehensibly the points I feel are important.'),(0198,'write','B1',03,'I can write personal letters giving news, describing experiences and impressions, and expressing feelings.'),(0199,'write','B1',04,'I can take down messages communicating enquiries and factual information, explaining problems.'),(0200,'write','B1',05,'I can write straightforward connected texts and simple essays on familiar subjects within my field, by linking a series of shorter discrete elements into a linear sequence, and using dictionaries and reference resources.'),(0201,'write','B1',06,'I can describe the plot of a film or book, or narrate a simple story.'),(0202,'write','B1',07,'I can write very brief reports to a standard conventionalized format, which pass on routine factual information on matters relating to my field.'),(0203,'write','B1',08,'I can summarize, report and give my opinion about accumulated factual information on familiar matters in my field with some confidence.'),(0204,'write','B1',09,'I can write standard letters giving or requesting detailed information (e.g., replying to an advertisement, applying for a job).'),(0205,'write','B2',01,'I can write clear detailed text on a wide range of subjects relating to my personal, academic or professional interests.'),(0206,'write','B2',02,'I can write letters conveying degrees of emotion and highlighting the personal significance of events and experiences, and commenting on the correspondent’s news and views.'),(0207,'write','B2',03,'I can express news, views and feelings effectively in writing, and relate to those of others.'),(0208,'write','B2',04,'I can write summaries of articles on topics of general, academic or professional interest, and summarize information from different sources and media.'),(0209,'write','B2',05,'I can write an essay or report which develops an argument, giving reasons to support or negate a point of view, weighing pros and cons.'),(0210,'write','B2',06,'I can summarize and synthesize information and arguments from a number of sources.'),(0211,'write','B2',07,'I can write a short review of a film or book.'),(0212,'write','B2',08,'I can write clear detailed descriptions of real or imaginary events and experiences in a detailed and easily readable way, marking the relationship between ideas.'),(0213,'write','B2',09,'I can write standard formal letters requesting or communicating relevant information, with appropriate use of register and conventions.'),(0214,'write','C1',01,'I can express myself fluently and accurately in writing on a wide range of personal, academic or professional topics, varying my vocabulary and style according to the context.'),(0215,'write','C1',02,'I can express myself with clarity and precision in personal correspondence, using language flexibly and effectively, including emotional, allusive and joking usage.'),(0216,'write','C1',03,'I can write clear, well-structured texts on complex subjects in my field, underlining the relevant salient issues, expanding and supporting points of view at some length with subsidiary points, reasons and relevant examples, and rounding off with an appropriate conclusion.'),(0217,'write','C1',04,'I can write clear, detailed, well-structured and developed descriptions and imaginative texts in an assured, personal, natural style appropriate to the reader in mind.'),(0218,'write','C1',05,'I can elaborate my case effectively and accurately in complex formal letters (e.g., registering a complaint, taking a stand against an issue).'),(0219,'write','C2',01,'I can write clear, smoothly-flowing, complex texts relating to my academic or professional work in an appropriate and effective style and a logical structure which helps the reader to find significant points.'),(0220,'write','C2',02,'I can write clear, smoothly-flowing, and fully engrossing stories and descriptions of experience in a style appropriate to the genre adopted.'),(0221,'write','C2',03,'I can write a well-structured critical review of a paper, project or proposal relating to my academic or professional field, giving reasons for my opinion.'),(0222,'write','C2',04,'I can produce clear, smoothly-flowing, complex reports, articles or essays which present a case or elaborate an argument.'),(0223,'write','C2',05,'I can provide an appropriate and effective logical structure which helps the reader to find significant points.'),(0224,'write','C2',06,'I can write detailed critical appraisals of cultural events or literary works.'),(0225,'write','C2',07,'I can write persuasive and well-structured complex formal letters in an appropriate style.');
/*!40000 ALTER TABLE `assess_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` smallint(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `language_id` smallint(4) unsigned zerofill NOT NULL DEFAULT '0001',
  `certification_id` tinyint(3) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`),
  KEY `certificate_language_id` (`language_id`),
  KEY `certificate_certification_id` (`certification_id`),
  CONSTRAINT `certificate_certification_id` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `certificate_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT INTO `certificates` VALUES (0001,0001,001),(0002,0006,007),(0003,0004,007),(0004,0047,007),(0005,0052,007),(0006,0006,002),(0007,0006,003),(0008,0006,004),(0009,0006,005),(0010,0006,006),(0011,0006,008),(0012,0004,008),(0013,0047,008),(0014,0093,008),(0015,0067,008),(0016,0011,008),(0017,0092,008),(0018,0055,008),(0019,0095,008),(0020,0047,009),(0021,0006,010),(0022,0052,011),(0023,0004,012),(0024,0006,012),(0025,0047,012),(0026,0052,012),(0027,0083,012),(0028,0067,012),(0029,0093,012),(0030,0095,012),(0031,0108,012),(0032,0112,012),(0033,0004,013),(0034,0006,014),(0035,0006,015),(0036,0052,016),(0037,0052,017),(0038,0006,018),(0039,0004,018),(0040,0047,018),(0041,0052,019);
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certifications`
--

DROP TABLE IF EXISTS `certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certifications` (
  `id` tinyint(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certifications`
--

LOCK TABLES `certifications` WRITE;
/*!40000 ALTER TABLE `certifications` DISABLE KEYS */;
INSERT INTO `certifications` VALUES (001,'Other…',''),(002,'Cambridge First Certificate',''),(003,'Cambridge Advanced English',''),(004,'Cambridge Proficiency',''),(005,'TOEFL','Test of English as a Foreign Language'),(006,'TOEIC','Test of English for International Communication'),(007,'BULATS','Business Language Testing Service'),(008,'CLES','Certificat de Compétences en Langues de l’Enseignement Supérieur'),(009,'DELE','Diplomas de Español como Lengua Extranjera'),(010,'Ordinate - Versant','Pearson Education'),(011,'TCF','Test de Connaissance du Français'),(012,'TELC','The European Language Certificates'),(013,'ZMP','Zentrale Mittelstufenprüfung'),(014,'CELTA','Certificate in English Language Teaching to Adults'),(015,'GCSE','General Certificate of Secondary Education'),(016,'CIEP','Centre International d’Études Pédagogiques'),(017,'DELF','Diplôme d\'Études en Langue Française'),(018,'DULS','Diplôme d\'Université en Langues de Spécialité'),(019,'DALF','Diplôme Approfondi de Langue Française');
/*!40000 ALTER TABLE `certifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` smallint(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (0001,'Afghanistan'),(0003,'Albania'),(0004,'Algeria'),(0006,'Andorra'),(0007,'Angola'),(0008,'Antigua and Barbuda'),(0010,'Argentina'),(0011,'Armenia'),(0012,'Australia'),(0013,'Austria'),(0014,'Azerbaijan'),(0015,'Bahamas'),(0016,'Bahrain'),(0017,'Bangladesh'),(0018,'Barbados'),(0024,'Belarus'),(0020,'Belgium'),(0021,'Belize'),(0022,'Benin'),(0023,'Bhutan'),(0026,'Bolivia'),(0027,'Bosnia and Herzegovina'),(0028,'Botswana'),(0029,'Brazil'),(0030,'Brunei'),(0031,'Bulgaria'),(0032,'Burkina'),(0025,'Burma'),(0033,'Burundi'),(0034,'Cambodia'),(0035,'Cameroon'),(0036,'Canada'),(0037,'Cape Verde'),(0146,'Central African Republic'),(0178,'Chad'),(0038,'Chile'),(0039,'China'),(0041,'Colombia'),(0042,'Comoros'),(0043,'Congo'),(0044,'Congo (Dem. Republic)'),(0045,'Cook Islands'),(0048,'Costa Rica'),(0050,'Croatia'),(0051,'Cuba'),(0040,'Cyprus'),(0148,'Czech Republic'),(0052,'Denmark'),(0053,'Djibouti'),(0054,'Dominica'),(0147,'Dominican Republic'),(0057,'Ecuador'),(0055,'Egypt'),(0159,'El Salvador'),(0076,'Equatorial Guinea'),(0058,'Eritrea'),(0060,'Estonia'),(0062,'Ethiopia'),(0063,'Fiji'),(0064,'Finland'),(0065,'France'),(0066,'Gabon'),(0067,'Gambia'),(0068,'Gaza Strip'),(0069,'Georgia'),(0005,'Germany'),(0070,'Ghana'),(0071,'Greece'),(0072,'Grenada'),(0196,'Guadeloupe'),(0073,'Guatemala'),(0074,'Guinea'),(0075,'Guinea-Bissau'),(0077,'Guyana'),(0078,'Haiti'),(0156,'Holy See'),(0079,'Honduras'),(0080,'Hungary'),(0086,'Iceland'),(0081,'India'),(0082,'Indonesia'),(0083,'Iran'),(0084,'Iraq'),(0085,'Ireland'),(0087,'Israel'),(0088,'Italy'),(0049,'Ivory Coast'),(0089,'Jamaica'),(0090,'Japan'),(0091,'Jordan'),(0092,'Kazakhstan'),(0093,'Kenya'),(0095,'Kiribati'),(0096,'Kuwait'),(0094,'Kyrgyzstan'),(0097,'Laos'),(0099,'Latvia'),(0100,'Lebanon'),(0098,'Lesotho'),(0101,'Liberia'),(0102,'Libya'),(0103,'Liechtenstein'),(0104,'Lithuania'),(0105,'Luxembourg'),(0106,'Macedonia'),(0107,'Madagascar'),(0109,'Malawi'),(0108,'Malaysia'),(0110,'Maldives'),(0111,'Mali'),(0112,'Malta'),(0114,'Marshall Islands'),(0198,'Martinique'),(0116,'Mauritania'),(0115,'Mauritius'),(0117,'Mexico'),(0118,'Micronesia'),(0119,'Moldova'),(0120,'Monaco'),(0121,'Mongolia'),(0122,'Montenegro'),(0113,'Morocco'),(0123,'Mozambique'),(0124,'Namibia'),(0125,'Nauru'),(0126,'Nepal'),(0140,'Netherlands'),(0197,'New Caledonia'),(0132,'New Zealand'),(0127,'Nicaragua'),(0128,'Niger'),(0129,'Nigeria'),(0130,'Niue'),(0046,'North Korea'),(0131,'Norway'),(0133,'Oman'),(0136,'Pakistan'),(0019,'Palau'),(0137,'Panama'),(0138,'Papua New Guinea'),(0139,'Paraguay'),(0141,'Peru'),(0142,'Philippines'),(0143,'Poland'),(0144,'Portugal'),(0145,'Qatar'),(0149,'Romania'),(0151,'Russia'),(0152,'Rwanda'),(0153,'Saint Kitts and Nevis'),(0154,'Saint Lucia'),(0157,'Saint Vincent and the Grenadines'),(0155,'San Marino'),(0161,'Sao Tome and Principe'),(0009,'Saudi Arabia'),(0162,'Senegal'),(0163,'Seychelles'),(0164,'Sierra Leone'),(0165,'Singapore'),(0166,'Slovakia'),(0167,'Slovenia'),(0158,'Solomon Islands'),(0168,'Somalia'),(0002,'South Africa'),(0047,'South Korea'),(0059,'Spain'),(0170,'Sri Lanka'),(0169,'Sudan'),(0173,'Suriname'),(0174,'Swaziland'),(0171,'Sweden'),(0172,'Switzerland'),(0175,'Syria'),(0199,'Taiwan'),(0176,'Tajikistan'),(0177,'Tanzania'),(0179,'Thailand'),(0180,'Togo'),(0181,'Tonga'),(0182,'Trinidad and Tobago'),(0183,'Tunisia'),(0185,'Turkey'),(0184,'Turkmenistan'),(0186,'Tuvalu'),(0134,'Uganda'),(0187,'Ukraine'),(0056,'United Arab Emirates'),(0150,'United Kingdom'),(0061,'United States'),(0188,'Uruguay'),(0135,'Uzbekistan'),(0189,'Vanuatu'),(0190,'Venezuela'),(0191,'Vietnam'),(0160,'Western Samoa'),(0192,'Yemen'),(0193,'Yougoslavia'),(0194,'Zambia'),(0195,'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diplomas`
--

DROP TABLE IF EXISTS `diplomas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diplomas` (
  `id` tinyint(2) unsigned zerofill NOT NULL,
  `name` varchar(60) NOT NULL,
  `level` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diplomas`
--

LOCK TABLES `diplomas` WRITE;
/*!40000 ALTER TABLE `diplomas` DISABLE KEYS */;
INSERT INTO `diplomas` VALUES (10,'Other degrees or diplomas','EQF levels 1 to 3'),(20,'End-of-high school degree','EQF level 4'),(30,'Higher National Diploma','EQF level 5'),(40,'Bachelor\'s degree or equivalent','EQF level 6'),(50,'Master\'s degree or equivalent','EQF level 7'),(60,'Doctorate or equivalent','EQF level 8');
/*!40000 ALTER TABLE `diplomas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `id` smallint(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domains`
--

LOCK TABLES `domains` WRITE;
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;
INSERT INTO `domains` VALUES (0012,'Accounting, Management, Economics'),(0002,'Agriculture'),(0003,'Architecture'),(0004,'Army, Police'),(0005,'Arts'),(0006,'Audio, Video, Photo'),(0007,'Banks, Insurance'),(0008,'Biology, Environment'),(0009,'Building & Public Works'),(0010,'Business & Management'),(0037,'Chemistry'),(0011,'Communication'),(0013,'Dancing, Acting, Music'),(0014,'Documentation'),(0017,'Hotel, Catering'),(0028,'Human Sciences'),(0021,'Humanities'),(0018,'ICT, Computing, Multimedia'),(0019,'Journalism'),(0020,'Languages'),(0015,'Law, Political science'),(0022,'Marketing, Advertising'),(0023,'Mathematics'),(0024,'Medicine'),(0001,'Other…'),(0025,'Paramedical'),(0026,'Physics'),(0016,'Publishing'),(0027,'Religion, Theology'),(0035,'Retail & Distribution'),(0029,'Secretarial'),(0030,'Social'),(0031,'Sport'),(0032,'Technologies'),(0033,'Tourism'),(0034,'Transport, Logistics'),(0036,'Veterinary');
/*!40000 ALTER TABLE `domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` smallint(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (0002,'Afrikaans'),(0003,'Albanian'),(0005,'Amharic'),(0007,'Arabic (Algerian)'),(0008,'Arabic (Bedawi)'),(0009,'Arabic (Egyptian)'),(0137,'Arabic (Lebanese)'),(0011,'Arabic (modern)'),(0010,'Arabic (Moroccan)'),(0012,'Arabic (Saidi)'),(0013,'Arabic (Sudanese)'),(0014,'Aramaic'),(0015,'Armenian'),(0016,'Assamais'),(0017,'Awadhi'),(0018,'Azerbaijani'),(0019,'Bahasa'),(0020,'Balochi'),(0140,'Bambara'),(0021,'Basque'),(0155,'Bassar'),(0139,'Baule'),(0152,'Bearnese'),(0025,'Belarussian'),(0022,'Bengali'),(0023,'Berber'),(0024,'Bojpuri'),(0027,'Bosnian'),(0151,'Breton'),(0028,'Bulgarian'),(0026,'Burmese'),(0029,'Catalan'),(0030,'Cherokee'),(0037,'Chinese (Bei)'),(0031,'Chinese (Cantonese)'),(0032,'Chinese (Gan)'),(0034,'Chinese (Jinyu)'),(0035,'Chinese (Mandarin)'),(0036,'Chinese (Min)'),(0038,'Chinese (Wu)'),(0039,'Chinese (xiang)'),(0040,'Chinook'),(0153,'Comorian'),(0043,'Creole'),(0044,'Croatian'),(0108,'Czech'),(0045,'Danish'),(0046,'Deccan'),(0132,'Duala'),(0083,'Dutch'),(0131,'Ebrie'),(0141,'Echira'),(0006,'English'),(0129,'Eshira'),(0048,'Estonian'),(0142,'Ewe'),(0144,'Fang'),(0049,'Farsi'),(0050,'Filipino'),(0051,'Finnish'),(0134,'Fon'),(0052,'French'),(0004,'German'),(0055,'Greek'),(0054,'Gujarati'),(0056,'Haitian'),(0058,'Haryanvi'),(0057,'Hausa'),(0059,'Hawaiian'),(0060,'Hebrew'),(0061,'Hindi'),(0062,'Hungarian'),(0064,'Icelandic'),(0063,'Indonesian'),(0065,'Inuit'),(0066,'Irish'),(0067,'Italian'),(0068,'Japanese'),(0069,'Javanese'),(0157,'Joola'),(0070,'Kannada'),(0071,'Kazakh'),(0072,'Khmer'),(0042,'Korean'),(0073,'Lao'),(0074,'Latvian'),(0154,'Lingala'),(0158,'Lithuanian'),(0135,'Luxemburgish'),(0075,'Macedonian'),(0076,'Magahi'),(0143,'Mahore'),(0077,'Maithili'),(0130,'Malagasy'),(0078,'Malay'),(0079,'Malayalam'),(0145,'Malinke'),(0121,'Mande'),(0127,'Mandingo'),(0080,'Marathi'),(0081,'Marwari'),(0082,'Mongolian'),(0123,'Moore'),(0146,'Myene'),(0084,'Nepali'),(0159,'Ngombale'),(0085,'Norwegian'),(0150,'Occitan'),(0086,'Oriya'),(0001,'Other…'),(0089,'Papuan'),(0125,'Peul'),(0092,'Polish'),(0093,'Portuguese'),(0160,'Portuguese creole'),(0090,'Punjabi (east)'),(0091,'Punjabi (west)'),(0094,'Romanian'),(0095,'Russian'),(0133,'Sango'),(0096,'Saraiki'),(0156,'Senufo'),(0097,'Serbian'),(0147,'Serere'),(0098,'Sindhi'),(0099,'Singalese'),(0100,'Slovak'),(0101,'Slovenian'),(0102,'Somali'),(0148,'Soninke'),(0047,'Spanish'),(0104,'Sunda'),(0126,'Sussu'),(0105,'Swahili'),(0103,'Swedish'),(0138,'Taiwanese'),(0106,'Tajik'),(0107,'Tamil'),(0109,'Telugu'),(0110,'Thai'),(0111,'Tibetan'),(0112,'Turkish'),(0113,'Turkmen'),(0114,'Ukrainian'),(0087,'Urdu'),(0088,'Uzbek'),(0115,'Vietnamese'),(0116,'Visayan'),(0053,'Welsh'),(0128,'Wolof'),(0117,'Yiddish'),(0118,'Yoruba'),(0136,'Yurba'),(0149,'Zarma'),(0120,'Zulu');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_abroads`
--

DROP TABLE IF EXISTS `po_abroads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_abroads` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `date` date NOT NULL,
  `duration` tinyint(3) unsigned NOT NULL,
  `unit` tinyint(2) unsigned NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_abroad_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_abroad_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_abroads`
--

LOCK TABLES `po_abroads` WRITE;
/*!40000 ALTER TABLE `po_abroads` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_abroads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_assess_answers`
--

DROP TABLE IF EXISTS `po_assess_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_assess_answers` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `competency` enum('listen','read','converse','speak','write') NOT NULL,
  `A1001` tinyint(2) NOT NULL DEFAULT '0',
  `A1101` varchar(60) NOT NULL,
  `A1201` varchar(60) NOT NULL,
  `A1301` varchar(60) NOT NULL,
  `A1401` tinyint(1) NOT NULL DEFAULT '0',
  `A1002` tinyint(2) NOT NULL DEFAULT '0',
  `A1102` varchar(60) NOT NULL,
  `A1202` varchar(60) NOT NULL,
  `A1302` varchar(60) NOT NULL,
  `A1402` tinyint(1) NOT NULL DEFAULT '0',
  `A1003` tinyint(2) NOT NULL DEFAULT '0',
  `A1103` varchar(60) NOT NULL,
  `A1203` varchar(60) NOT NULL,
  `A1303` varchar(60) NOT NULL,
  `A1403` tinyint(1) NOT NULL DEFAULT '0',
  `A1004` tinyint(2) NOT NULL DEFAULT '0',
  `A1104` varchar(60) NOT NULL,
  `A1204` varchar(60) NOT NULL,
  `A1304` varchar(60) NOT NULL,
  `A1404` tinyint(1) NOT NULL DEFAULT '0',
  `A1005` tinyint(2) NOT NULL DEFAULT '0',
  `A1105` varchar(60) NOT NULL,
  `A1205` varchar(60) NOT NULL,
  `A1305` varchar(60) NOT NULL,
  `A1405` tinyint(1) NOT NULL DEFAULT '0',
  `A1006` tinyint(2) NOT NULL DEFAULT '0',
  `A1106` varchar(60) NOT NULL,
  `A1206` varchar(60) NOT NULL,
  `A1306` varchar(60) NOT NULL,
  `A1406` tinyint(1) NOT NULL DEFAULT '0',
  `A1007` tinyint(2) NOT NULL DEFAULT '0',
  `A1107` varchar(60) NOT NULL,
  `A1207` varchar(60) NOT NULL,
  `A1307` varchar(60) NOT NULL,
  `A1407` tinyint(1) NOT NULL DEFAULT '0',
  `A1008` tinyint(2) NOT NULL DEFAULT '0',
  `A1108` varchar(60) NOT NULL,
  `A1208` varchar(60) NOT NULL,
  `A1308` varchar(60) NOT NULL,
  `A1408` tinyint(1) NOT NULL DEFAULT '0',
  `A1009` tinyint(2) NOT NULL DEFAULT '0',
  `A1109` varchar(60) NOT NULL,
  `A1209` varchar(60) NOT NULL,
  `A1309` varchar(60) NOT NULL,
  `A1409` tinyint(1) NOT NULL DEFAULT '0',
  `A1010` tinyint(2) NOT NULL DEFAULT '0',
  `A1110` varchar(60) NOT NULL,
  `A1210` varchar(60) NOT NULL,
  `A1310` varchar(60) NOT NULL,
  `A1410` tinyint(1) NOT NULL DEFAULT '0',
  `A1011` tinyint(2) NOT NULL DEFAULT '0',
  `A1111` varchar(60) NOT NULL,
  `A1211` varchar(60) NOT NULL,
  `A1311` varchar(60) NOT NULL,
  `A1411` tinyint(1) NOT NULL DEFAULT '0',
  `A1012` tinyint(2) NOT NULL DEFAULT '0',
  `A1112` varchar(60) NOT NULL,
  `A1212` varchar(60) NOT NULL,
  `A1312` varchar(60) NOT NULL,
  `A1412` tinyint(1) NOT NULL DEFAULT '0',
  `A1013` tinyint(2) NOT NULL DEFAULT '0',
  `A1113` varchar(60) NOT NULL,
  `A1213` varchar(60) NOT NULL,
  `A1313` varchar(60) NOT NULL,
  `A1413` tinyint(1) NOT NULL DEFAULT '0',
  `A2001` tinyint(2) NOT NULL DEFAULT '0',
  `A2101` varchar(60) NOT NULL,
  `A2201` varchar(60) NOT NULL,
  `A2301` varchar(60) NOT NULL,
  `A2401` tinyint(1) NOT NULL DEFAULT '0',
  `A2002` tinyint(2) NOT NULL DEFAULT '0',
  `A2102` varchar(60) NOT NULL,
  `A2202` varchar(60) NOT NULL,
  `A2302` varchar(60) NOT NULL,
  `A2402` tinyint(1) NOT NULL DEFAULT '0',
  `A2003` tinyint(2) NOT NULL DEFAULT '0',
  `A2103` varchar(60) NOT NULL,
  `A2203` varchar(60) NOT NULL,
  `A2303` varchar(60) NOT NULL,
  `A2403` tinyint(1) NOT NULL DEFAULT '0',
  `A2004` tinyint(2) NOT NULL DEFAULT '0',
  `A2104` varchar(60) NOT NULL,
  `A2204` varchar(60) NOT NULL,
  `A2304` varchar(60) NOT NULL,
  `A2404` tinyint(1) NOT NULL DEFAULT '0',
  `A2005` tinyint(2) NOT NULL DEFAULT '0',
  `A2105` varchar(60) NOT NULL,
  `A2205` varchar(60) NOT NULL,
  `A2305` varchar(60) NOT NULL,
  `A2405` tinyint(1) NOT NULL DEFAULT '0',
  `A2006` tinyint(2) NOT NULL DEFAULT '0',
  `A2106` varchar(60) NOT NULL,
  `A2206` varchar(60) NOT NULL,
  `A2306` varchar(60) NOT NULL,
  `A2406` tinyint(1) NOT NULL DEFAULT '0',
  `A2007` tinyint(2) NOT NULL DEFAULT '0',
  `A2107` varchar(60) NOT NULL,
  `A2207` varchar(60) NOT NULL,
  `A2307` varchar(60) NOT NULL,
  `A2407` tinyint(1) NOT NULL DEFAULT '0',
  `A2008` tinyint(2) NOT NULL DEFAULT '0',
  `A2108` varchar(60) NOT NULL,
  `A2208` varchar(60) NOT NULL,
  `A2308` varchar(60) NOT NULL,
  `A2408` tinyint(1) NOT NULL DEFAULT '0',
  `A2009` tinyint(2) NOT NULL DEFAULT '0',
  `A2109` varchar(60) NOT NULL,
  `A2209` varchar(60) NOT NULL,
  `A2309` varchar(60) NOT NULL,
  `A2409` tinyint(1) NOT NULL DEFAULT '0',
  `A2010` tinyint(2) NOT NULL DEFAULT '0',
  `A2110` varchar(60) NOT NULL,
  `A2210` varchar(60) NOT NULL,
  `A2310` varchar(60) NOT NULL,
  `A2410` tinyint(1) NOT NULL DEFAULT '0',
  `A2011` tinyint(2) NOT NULL DEFAULT '0',
  `A2111` varchar(60) NOT NULL,
  `A2211` varchar(60) NOT NULL,
  `A2311` varchar(60) NOT NULL,
  `A2411` tinyint(1) NOT NULL DEFAULT '0',
  `A2012` tinyint(2) NOT NULL DEFAULT '0',
  `A2112` varchar(60) NOT NULL,
  `A2212` varchar(60) NOT NULL,
  `A2312` varchar(60) NOT NULL,
  `A2412` tinyint(1) NOT NULL DEFAULT '0',
  `A2013` tinyint(2) NOT NULL DEFAULT '0',
  `A2113` varchar(60) NOT NULL,
  `A2213` varchar(60) NOT NULL,
  `A2313` varchar(60) NOT NULL,
  `A2413` tinyint(1) NOT NULL DEFAULT '0',
  `B1001` tinyint(2) NOT NULL DEFAULT '0',
  `B1101` varchar(60) NOT NULL,
  `B1201` varchar(60) NOT NULL,
  `B1301` varchar(60) NOT NULL,
  `B1401` tinyint(1) NOT NULL DEFAULT '0',
  `B1002` tinyint(2) NOT NULL DEFAULT '0',
  `B1102` varchar(60) NOT NULL,
  `B1202` varchar(60) NOT NULL,
  `B1302` varchar(60) NOT NULL,
  `B1402` tinyint(1) NOT NULL DEFAULT '0',
  `B1003` tinyint(2) NOT NULL DEFAULT '0',
  `B1103` varchar(60) NOT NULL,
  `B1203` varchar(60) NOT NULL,
  `B1303` varchar(60) NOT NULL,
  `B1403` tinyint(1) NOT NULL DEFAULT '0',
  `B1004` tinyint(2) NOT NULL DEFAULT '0',
  `B1104` varchar(60) NOT NULL,
  `B1204` varchar(60) NOT NULL,
  `B1304` varchar(60) NOT NULL,
  `B1404` tinyint(1) NOT NULL DEFAULT '0',
  `B1005` tinyint(2) NOT NULL DEFAULT '0',
  `B1105` varchar(60) NOT NULL,
  `B1205` varchar(60) NOT NULL,
  `B1305` varchar(60) NOT NULL,
  `B1405` tinyint(1) NOT NULL DEFAULT '0',
  `B1006` tinyint(2) NOT NULL DEFAULT '0',
  `B1106` varchar(60) NOT NULL,
  `B1206` varchar(60) NOT NULL,
  `B1306` varchar(60) NOT NULL,
  `B1406` tinyint(1) NOT NULL DEFAULT '0',
  `B1007` tinyint(2) NOT NULL DEFAULT '0',
  `B1107` varchar(60) NOT NULL,
  `B1207` varchar(60) NOT NULL,
  `B1307` varchar(60) NOT NULL,
  `B1407` tinyint(1) NOT NULL DEFAULT '0',
  `B1008` tinyint(2) NOT NULL DEFAULT '0',
  `B1108` varchar(60) NOT NULL,
  `B1208` varchar(60) NOT NULL,
  `B1308` varchar(60) NOT NULL,
  `B1408` tinyint(1) NOT NULL DEFAULT '0',
  `B1009` tinyint(2) NOT NULL DEFAULT '0',
  `B1109` varchar(60) NOT NULL,
  `B1209` varchar(60) NOT NULL,
  `B1309` varchar(60) NOT NULL,
  `B1409` tinyint(1) NOT NULL DEFAULT '0',
  `B1010` tinyint(2) NOT NULL DEFAULT '0',
  `B1110` varchar(60) NOT NULL,
  `B1210` varchar(60) NOT NULL,
  `B1310` varchar(60) NOT NULL,
  `B1410` tinyint(1) NOT NULL DEFAULT '0',
  `B1011` tinyint(2) NOT NULL DEFAULT '0',
  `B1111` varchar(60) NOT NULL,
  `B1211` varchar(60) NOT NULL,
  `B1311` varchar(60) NOT NULL,
  `B1411` tinyint(1) NOT NULL DEFAULT '0',
  `B1012` tinyint(2) NOT NULL DEFAULT '0',
  `B1112` varchar(60) NOT NULL,
  `B1212` varchar(60) NOT NULL,
  `B1312` varchar(60) NOT NULL,
  `B1412` tinyint(1) NOT NULL DEFAULT '0',
  `B1013` tinyint(2) NOT NULL DEFAULT '0',
  `B1113` varchar(60) NOT NULL,
  `B1213` varchar(60) NOT NULL,
  `B1313` varchar(60) NOT NULL,
  `B1413` tinyint(1) NOT NULL DEFAULT '0',
  `B2001` tinyint(2) NOT NULL DEFAULT '0',
  `B2101` varchar(60) NOT NULL,
  `B2201` varchar(60) NOT NULL,
  `B2301` varchar(60) NOT NULL,
  `B2401` tinyint(1) NOT NULL DEFAULT '0',
  `B2002` tinyint(2) NOT NULL DEFAULT '0',
  `B2102` varchar(60) NOT NULL,
  `B2202` varchar(60) NOT NULL,
  `B2302` varchar(60) NOT NULL,
  `B2402` tinyint(1) NOT NULL DEFAULT '0',
  `B2003` tinyint(2) NOT NULL DEFAULT '0',
  `B2103` varchar(60) NOT NULL,
  `B2203` varchar(60) NOT NULL,
  `B2303` varchar(60) NOT NULL,
  `B2403` tinyint(1) NOT NULL DEFAULT '0',
  `B2004` tinyint(2) NOT NULL DEFAULT '0',
  `B2104` varchar(60) NOT NULL,
  `B2204` varchar(60) NOT NULL,
  `B2304` varchar(60) NOT NULL,
  `B2404` tinyint(1) NOT NULL DEFAULT '0',
  `B2005` tinyint(2) NOT NULL DEFAULT '0',
  `B2105` varchar(60) NOT NULL,
  `B2205` varchar(60) NOT NULL,
  `B2305` varchar(60) NOT NULL,
  `B2405` tinyint(1) NOT NULL DEFAULT '0',
  `B2006` tinyint(2) NOT NULL DEFAULT '0',
  `B2106` varchar(60) NOT NULL,
  `B2206` varchar(60) NOT NULL,
  `B2306` varchar(60) NOT NULL,
  `B2406` tinyint(1) NOT NULL DEFAULT '0',
  `B2007` tinyint(2) NOT NULL DEFAULT '0',
  `B2107` varchar(60) NOT NULL,
  `B2207` varchar(60) NOT NULL,
  `B2307` varchar(60) NOT NULL,
  `B2407` tinyint(1) NOT NULL DEFAULT '0',
  `B2008` tinyint(2) NOT NULL DEFAULT '0',
  `B2108` varchar(60) NOT NULL,
  `B2208` varchar(60) NOT NULL,
  `B2308` varchar(60) NOT NULL,
  `B2408` tinyint(1) NOT NULL DEFAULT '0',
  `B2009` tinyint(2) NOT NULL DEFAULT '0',
  `B2109` varchar(60) NOT NULL,
  `B2209` varchar(60) NOT NULL,
  `B2309` varchar(60) NOT NULL,
  `B2409` tinyint(1) NOT NULL DEFAULT '0',
  `B2010` tinyint(2) NOT NULL DEFAULT '0',
  `B2110` varchar(60) NOT NULL,
  `B2210` varchar(60) NOT NULL,
  `B2310` varchar(60) NOT NULL,
  `B2410` tinyint(1) NOT NULL DEFAULT '0',
  `B2011` tinyint(2) NOT NULL DEFAULT '0',
  `B2111` varchar(60) NOT NULL,
  `B2211` varchar(60) NOT NULL,
  `B2311` varchar(60) NOT NULL,
  `B2411` tinyint(1) NOT NULL DEFAULT '0',
  `B2012` tinyint(2) NOT NULL DEFAULT '0',
  `B2112` varchar(60) NOT NULL,
  `B2212` varchar(60) NOT NULL,
  `B2312` varchar(60) NOT NULL,
  `B2412` tinyint(1) NOT NULL DEFAULT '0',
  `B2013` tinyint(2) NOT NULL DEFAULT '0',
  `B2113` varchar(60) NOT NULL,
  `B2213` varchar(60) NOT NULL,
  `B2313` varchar(60) NOT NULL,
  `B2413` tinyint(1) NOT NULL DEFAULT '0',
  `C1001` tinyint(2) NOT NULL DEFAULT '0',
  `C1101` varchar(60) NOT NULL,
  `C1201` varchar(60) NOT NULL,
  `C1301` varchar(60) NOT NULL,
  `C1401` tinyint(1) NOT NULL DEFAULT '0',
  `C1002` tinyint(2) NOT NULL DEFAULT '0',
  `C1102` varchar(60) NOT NULL,
  `C1202` varchar(60) NOT NULL,
  `C1302` varchar(60) NOT NULL,
  `C1402` tinyint(1) NOT NULL DEFAULT '0',
  `C1003` tinyint(2) NOT NULL DEFAULT '0',
  `C1103` varchar(60) NOT NULL,
  `C1203` varchar(60) NOT NULL,
  `C1303` varchar(60) NOT NULL,
  `C1403` tinyint(1) NOT NULL DEFAULT '0',
  `C1004` tinyint(2) NOT NULL DEFAULT '0',
  `C1104` varchar(60) NOT NULL,
  `C1204` varchar(60) NOT NULL,
  `C1304` varchar(60) NOT NULL,
  `C1404` tinyint(1) NOT NULL DEFAULT '0',
  `C1005` tinyint(2) NOT NULL DEFAULT '0',
  `C1105` varchar(60) NOT NULL,
  `C1205` varchar(60) NOT NULL,
  `C1305` varchar(60) NOT NULL,
  `C1405` tinyint(1) NOT NULL DEFAULT '0',
  `C1006` tinyint(2) NOT NULL DEFAULT '0',
  `C1106` varchar(60) NOT NULL,
  `C1206` varchar(60) NOT NULL,
  `C1306` varchar(60) NOT NULL,
  `C1406` tinyint(1) NOT NULL DEFAULT '0',
  `C1007` tinyint(2) NOT NULL DEFAULT '0',
  `C1107` varchar(60) NOT NULL,
  `C1207` varchar(60) NOT NULL,
  `C1307` varchar(60) NOT NULL,
  `C1407` tinyint(1) NOT NULL DEFAULT '0',
  `C1008` tinyint(2) NOT NULL DEFAULT '0',
  `C1108` varchar(60) NOT NULL,
  `C1208` varchar(60) NOT NULL,
  `C1308` varchar(60) NOT NULL,
  `C1408` tinyint(1) NOT NULL DEFAULT '0',
  `C1009` tinyint(2) NOT NULL DEFAULT '0',
  `C1109` varchar(60) NOT NULL,
  `C1209` varchar(60) NOT NULL,
  `C1309` varchar(60) NOT NULL,
  `C1409` tinyint(1) NOT NULL DEFAULT '0',
  `C1010` tinyint(2) NOT NULL DEFAULT '0',
  `C1110` varchar(60) NOT NULL,
  `C1210` varchar(60) NOT NULL,
  `C1310` varchar(60) NOT NULL,
  `C1410` tinyint(1) NOT NULL DEFAULT '0',
  `C1011` tinyint(2) NOT NULL DEFAULT '0',
  `C1111` varchar(60) NOT NULL,
  `C1211` varchar(60) NOT NULL,
  `C1311` varchar(60) NOT NULL,
  `C1411` tinyint(1) NOT NULL DEFAULT '0',
  `C1012` tinyint(2) NOT NULL DEFAULT '0',
  `C1112` varchar(60) NOT NULL,
  `C1212` varchar(60) NOT NULL,
  `C1312` varchar(60) NOT NULL,
  `C1412` tinyint(1) NOT NULL DEFAULT '0',
  `C1013` tinyint(2) NOT NULL DEFAULT '0',
  `C1113` varchar(60) NOT NULL,
  `C1213` varchar(60) NOT NULL,
  `C1313` varchar(60) NOT NULL,
  `C1413` tinyint(1) NOT NULL DEFAULT '0',
  `C2001` tinyint(2) NOT NULL DEFAULT '0',
  `C2101` varchar(60) NOT NULL,
  `C2201` varchar(60) NOT NULL,
  `C2301` varchar(60) NOT NULL,
  `C2401` tinyint(1) NOT NULL DEFAULT '0',
  `C2002` tinyint(2) NOT NULL DEFAULT '0',
  `C2102` varchar(60) NOT NULL,
  `C2202` varchar(60) NOT NULL,
  `C2302` varchar(60) NOT NULL,
  `C2402` tinyint(1) NOT NULL DEFAULT '0',
  `C2003` tinyint(2) NOT NULL DEFAULT '0',
  `C2103` varchar(60) NOT NULL,
  `C2203` varchar(60) NOT NULL,
  `C2303` varchar(60) NOT NULL,
  `C2403` tinyint(1) NOT NULL DEFAULT '0',
  `C2004` tinyint(2) NOT NULL DEFAULT '0',
  `C2104` varchar(60) NOT NULL,
  `C2204` varchar(60) NOT NULL,
  `C2304` varchar(60) NOT NULL,
  `C2404` tinyint(1) NOT NULL DEFAULT '0',
  `C2005` tinyint(2) NOT NULL DEFAULT '0',
  `C2105` varchar(60) NOT NULL,
  `C2205` varchar(60) NOT NULL,
  `C2305` varchar(60) NOT NULL,
  `C2405` tinyint(1) NOT NULL DEFAULT '0',
  `C2006` tinyint(2) NOT NULL DEFAULT '0',
  `C2106` varchar(60) NOT NULL,
  `C2206` varchar(60) NOT NULL,
  `C2306` varchar(60) NOT NULL,
  `C2406` tinyint(1) NOT NULL DEFAULT '0',
  `C2007` tinyint(2) NOT NULL DEFAULT '0',
  `C2107` varchar(60) NOT NULL,
  `C2207` varchar(60) NOT NULL,
  `C2307` varchar(60) NOT NULL,
  `C2407` tinyint(1) NOT NULL DEFAULT '0',
  `C2008` tinyint(2) NOT NULL DEFAULT '0',
  `C2108` varchar(60) NOT NULL,
  `C2208` varchar(60) NOT NULL,
  `C2308` varchar(60) NOT NULL,
  `C2408` tinyint(1) NOT NULL DEFAULT '0',
  `C2009` tinyint(2) NOT NULL DEFAULT '0',
  `C2109` varchar(60) NOT NULL,
  `C2209` varchar(60) NOT NULL,
  `C2309` varchar(60) NOT NULL,
  `C2409` tinyint(1) NOT NULL DEFAULT '0',
  `C2010` tinyint(2) NOT NULL DEFAULT '0',
  `C2110` varchar(60) NOT NULL,
  `C2210` varchar(60) NOT NULL,
  `C2310` varchar(60) NOT NULL,
  `C2410` tinyint(1) NOT NULL DEFAULT '0',
  `C2011` tinyint(2) NOT NULL DEFAULT '0',
  `C2111` varchar(60) NOT NULL,
  `C2211` varchar(60) NOT NULL,
  `C2311` varchar(60) NOT NULL,
  `C2411` tinyint(1) NOT NULL DEFAULT '0',
  `C2012` tinyint(2) NOT NULL DEFAULT '0',
  `C2112` varchar(60) NOT NULL,
  `C2212` varchar(60) NOT NULL,
  `C2312` varchar(60) NOT NULL,
  `C2412` tinyint(1) NOT NULL DEFAULT '0',
  `C2013` tinyint(2) NOT NULL DEFAULT '0',
  `C2113` varchar(60) NOT NULL,
  `C2213` varchar(60) NOT NULL,
  `C2313` varchar(60) NOT NULL,
  `C2413` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_assess_answer_portfolio_id` (`portfolio_id`),
  KEY `po_assess_answer_competency` (`competency`),
  CONSTRAINT `po_assessment_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_assess_answers`
--

LOCK TABLES `po_assess_answers` WRITE;
/*!40000 ALTER TABLE `po_assess_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_assess_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_assess_criterias`
--

DROP TABLE IF EXISTS `po_assess_criterias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_assess_criterias` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `competency` enum('listen','read','converse','speak','write') NOT NULL,
  `level` enum('A1','A2','B1','B2','C1','C2') NOT NULL,
  `criteria_set` tinyint(2) unsigned NOT NULL,
  `criteria1` tinytext NOT NULL,
  `criteria2` tinytext NOT NULL,
  `criteria3` tinytext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_assess_criteria_portfolio_id` (`portfolio_id`),
  KEY `po_assess_criteria_competency` (`competency`),
  KEY `po_assess_criteria_level` (`level`),
  CONSTRAINT `po_assess_criteria_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_assess_criterias`
--

LOCK TABLES `po_assess_criterias` WRITE;
/*!40000 ALTER TABLE `po_assess_criterias` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_assess_criterias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_assess_users`
--

DROP TABLE IF EXISTS `po_assess_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_assess_users` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `competency` enum('listen','read','converse','speak','write') NOT NULL,
  `level` enum('A1','A2','B1','B2','C1','C2') NOT NULL,
  `description` text NOT NULL,
  `evaluation` tinyint(2) NOT NULL DEFAULT '0',
  `date1` varchar(60) NOT NULL,
  `date2` varchar(60) NOT NULL,
  `date3` varchar(60) NOT NULL,
  `target` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_assess_user_portfolio_id` (`portfolio_id`),
  KEY `po_assess_user_competency` (`competency`),
  KEY `po_assess_user_level` (`level`),
  CONSTRAINT `po_assess_user_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_assess_users`
--

LOCK TABLES `po_assess_users` WRITE;
/*!40000 ALTER TABLE `po_assess_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_assess_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_certificates`
--

DROP TABLE IF EXISTS `po_certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_certificates` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `certificate_id` smallint(4) unsigned zerofill NOT NULL,
  `date` date NOT NULL,
  `organism` varchar(120) NOT NULL,
  `certificate_info` varchar(120) NOT NULL,
  `listening` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `reading` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `speak_int` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `speak_prod` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `writing` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_certificate_portfolio_id` (`portfolio_id`),
  KEY `po_certificate_certificate_id` (`certificate_id`),
  CONSTRAINT `po_certificate_certificate_id` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `po_certificate_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_certificates`
--

LOCK TABLES `po_certificates` WRITE;
/*!40000 ALTER TABLE `po_certificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_contacts`
--

DROP TABLE IF EXISTS `po_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_contacts` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_contact_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_contact_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_contacts`
--

LOCK TABLES `po_contacts` WRITE;
/*!40000 ALTER TABLE `po_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_culturals`
--

DROP TABLE IF EXISTS `po_culturals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_culturals` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_cultural_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_cultural_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_culturals`
--

LOCK TABLES `po_culturals` WRITE;
/*!40000 ALTER TABLE `po_culturals` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_culturals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_documents`
--

DROP TABLE IF EXISTS `po_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_documents` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `kind` enum('share','show','work') NOT NULL,
  `competency` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(20) NOT NULL,
  `file` varchar(30) NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `mime` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_document_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_document_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_documents`
--

LOCK TABLES `po_documents` WRITE;
/*!40000 ALTER TABLE `po_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_employments`
--

DROP TABLE IF EXISTS `po_employments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_employments` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_employment_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_employment_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_employments`
--

LOCK TABLES `po_employments` WRITE;
/*!40000 ALTER TABLE `po_employments` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_employments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_heritages`
--

DROP TABLE IF EXISTS `po_heritages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_heritages` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `po_heritage_student_id` FOREIGN KEY (`id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_heritages`
--

LOCK TABLES `po_heritages` WRITE;
/*!40000 ALTER TABLE `po_heritages` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_heritages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_howtolearns`
--

DROP TABLE IF EXISTS `po_howtolearns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_howtolearns` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `101answer` enum('','never','sometimes','always') NOT NULL,
  `101target` tinyint(1) NOT NULL,
  `101date` varchar(50) NOT NULL,
  `102answer` enum('','never','sometimes','always') NOT NULL,
  `102target` tinyint(1) NOT NULL,
  `102date` varchar(50) NOT NULL,
  `103answer` enum('','never','sometimes','always') NOT NULL,
  `103target` tinyint(1) NOT NULL,
  `103date` varchar(50) NOT NULL,
  `104answer` enum('','never','sometimes','always') NOT NULL,
  `104target` tinyint(1) NOT NULL,
  `104date` varchar(50) NOT NULL,
  `105answer` enum('','never','sometimes','always') NOT NULL,
  `105target` tinyint(1) NOT NULL,
  `105date` varchar(50) NOT NULL,
  `201answer` enum('','never','sometimes','always') NOT NULL,
  `201target` tinyint(1) NOT NULL,
  `201date` varchar(50) NOT NULL,
  `202answer` enum('','never','sometimes','always') NOT NULL,
  `202target` tinyint(1) NOT NULL,
  `202date` varchar(50) NOT NULL,
  `203answer` enum('','never','sometimes','always') NOT NULL,
  `203target` tinyint(1) NOT NULL,
  `203date` varchar(50) NOT NULL,
  `204answer` enum('','never','sometimes','always') NOT NULL,
  `204target` tinyint(1) NOT NULL,
  `204date` varchar(50) NOT NULL,
  `205answer` enum('','never','sometimes','always') NOT NULL,
  `205target` tinyint(1) NOT NULL,
  `205date` varchar(50) NOT NULL,
  `206answer` enum('','never','sometimes','always') NOT NULL,
  `206target` tinyint(1) NOT NULL,
  `206date` varchar(50) NOT NULL,
  `207answer` enum('','never','sometimes','always') NOT NULL,
  `207target` tinyint(1) NOT NULL,
  `207date` varchar(50) NOT NULL,
  `208answer` enum('','never','sometimes','always') NOT NULL,
  `208target` tinyint(1) NOT NULL,
  `208date` varchar(50) NOT NULL,
  `209answer` enum('','never','sometimes','always') NOT NULL,
  `209target` tinyint(1) NOT NULL,
  `209date` varchar(50) NOT NULL,
  `301answer` enum('','never','sometimes','always') NOT NULL,
  `301target` tinyint(1) NOT NULL,
  `301date` varchar(50) NOT NULL,
  `302answer` enum('','never','sometimes','always') NOT NULL,
  `302target` tinyint(1) NOT NULL,
  `302date` varchar(50) NOT NULL,
  `303answer` enum('','never','sometimes','always') NOT NULL,
  `303target` tinyint(1) NOT NULL,
  `303date` varchar(50) NOT NULL,
  `304answer` enum('','never','sometimes','always') NOT NULL,
  `304target` tinyint(1) NOT NULL,
  `304date` varchar(50) NOT NULL,
  `305answer` enum('','never','sometimes','always') NOT NULL,
  `305target` tinyint(1) NOT NULL,
  `305date` varchar(50) NOT NULL,
  `306answer` enum('','never','sometimes','always') NOT NULL,
  `306target` tinyint(1) NOT NULL,
  `306date` varchar(50) NOT NULL,
  `307answer` enum('','never','sometimes','always') NOT NULL,
  `307target` tinyint(1) NOT NULL,
  `307date` varchar(50) NOT NULL,
  `308answer` enum('','never','sometimes','always') NOT NULL,
  `308target` tinyint(1) NOT NULL,
  `308date` varchar(50) NOT NULL,
  `401answer` enum('','never','sometimes','always') NOT NULL,
  `401target` tinyint(1) NOT NULL,
  `401date` varchar(50) NOT NULL,
  `402answer` enum('','never','sometimes','always') NOT NULL,
  `402target` tinyint(1) NOT NULL,
  `402date` varchar(50) NOT NULL,
  `403answer` enum('','never','sometimes','always') NOT NULL,
  `403target` tinyint(1) NOT NULL,
  `403date` varchar(50) NOT NULL,
  `404answer` enum('','never','sometimes','always') NOT NULL,
  `404target` tinyint(1) NOT NULL,
  `404date` varchar(50) NOT NULL,
  `other` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `po_howtolearn_student_id` FOREIGN KEY (`id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_howtolearns`
--

LOCK TABLES `po_howtolearns` WRITE;
/*!40000 ALTER TABLE `po_howtolearns` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_howtolearns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_mediations`
--

DROP TABLE IF EXISTS `po_mediations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_mediations` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `po_mediation_student_id` FOREIGN KEY (`id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_mediations`
--

LOCK TABLES `po_mediations` WRITE;
/*!40000 ALTER TABLE `po_mediations` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_mediations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_objectives`
--

DROP TABLE IF EXISTS `po_objectives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_objectives` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `description` text NOT NULL,
  `progress` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_objective_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_objective_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_objectives`
--

LOCK TABLES `po_objectives` WRITE;
/*!40000 ALTER TABLE `po_objectives` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_objectives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_plurilingualisms`
--

DROP TABLE IF EXISTS `po_plurilingualisms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_plurilingualisms` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `po_plurilingualism_student_id` FOREIGN KEY (`id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_plurilingualisms`
--

LOCK TABLES `po_plurilingualisms` WRITE;
/*!40000 ALTER TABLE `po_plurilingualisms` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_plurilingualisms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_valuables`
--

DROP TABLE IF EXISTS `po_valuables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_valuables` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(9) unsigned zerofill NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `po_valuable_portfolio_id` (`portfolio_id`),
  CONSTRAINT `po_valuable_portfolio_id` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_valuables`
--

LOCK TABLES `po_valuables` WRITE;
/*!40000 ALTER TABLE `po_valuables` DISABLE KEYS */;
/*!40000 ALTER TABLE `po_valuables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolios`
--

DROP TABLE IF EXISTS `portfolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolios` (
  `id` int(9) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `student_id` mediumint(7) unsigned zerofill NOT NULL,
  `language_id` smallint(4) unsigned zerofill NOT NULL,
  `vis_passport` tinyint(1) NOT NULL DEFAULT '1',
  `vis_biography` tinyint(1) NOT NULL DEFAULT '1',
  `vis_dossier` tinyint(1) NOT NULL DEFAULT '1',
  `mother` tinyint(1) NOT NULL DEFAULT '0',
  `usual` tinyint(1) NOT NULL DEFAULT '0',
  `listening` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `listening_part` tinytext NOT NULL,
  `reading` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `reading_part` tinytext NOT NULL,
  `speak_int` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `speak_int_part` tinytext NOT NULL,
  `speak_prod` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `speak_prod_part` tinytext NOT NULL,
  `writing` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `writing_part` tinytext NOT NULL,
  `assess_update` date NOT NULL,
  `study_pri` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `study_sup` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `study_oth` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `study_sta` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `activ_lis` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `activ_spe` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `activ_rea` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `activ_pho` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `activ_wri` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `activ_oth` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `strategy1` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `strategy2` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `strategy3` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `strategy4` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `strategy5` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `strategy6` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `strategy7` enum('','no','sometimes','regularly','systematically') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `portfolio_student_id` (`student_id`),
  KEY `portfolio_language_id` (`language_id`),
  KEY `portfolio_mother` (`mother`),
  KEY `portfolio_listening` (`listening`),
  KEY `portfolio_reading` (`reading`),
  KEY `portfolio_speak_int` (`speak_int`),
  KEY `portfolio_speak_prod` (`speak_prod`),
  KEY `portfolio_writing` (`writing`),
  CONSTRAINT `portfolio_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `portfolio_student_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolios`
--

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;
/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `name` varchar(60) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `birth` date NOT NULL,
  `origin_country_id` smallint(4) unsigned zerofill NOT NULL,
  `living_country_id` smallint(4) unsigned zerofill NOT NULL,
  `university_id` smallint(3) unsigned zerofill NOT NULL,
  `diploma_id` tinyint(2) unsigned zerofill NOT NULL,
  `diploma_info` varchar(50) NOT NULL,
  `domain_id` smallint(4) unsigned zerofill NOT NULL,
  `domain_info` varchar(50) NOT NULL,
  `specialty` varchar(100) NOT NULL,
  `group` varchar(20) NOT NULL,
  `tea_hide` tinyint(1) NOT NULL DEFAULT '0',
  `vis_hide` tinyint(1) NOT NULL DEFAULT '1',
  `vis_passport` tinyint(1) NOT NULL DEFAULT '1',
  `vis_biography` tinyint(1) NOT NULL DEFAULT '0',
  `vis_dossier` tinyint(1) NOT NULL DEFAULT '0',
  `real` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_origin_country_id` (`origin_country_id`),
  KEY `student_living_country_id` (`living_country_id`),
  KEY `student_university_id` (`university_id`),
  KEY `student_diploma_id` (`diploma_id`),
  KEY `student_domain_id` (`domain_id`),
  KEY `student_group` (`group`),
  CONSTRAINT `student_diploma_id` FOREIGN KEY (`diploma_id`) REFERENCES `diplomas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_domain_id` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_id` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_living_country_id` FOREIGN KEY (`living_country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_origin_country_id` FOREIGN KEY (`origin_country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_university_id` FOREIGN KEY (`university_id`) REFERENCES `universities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (0000031,'Dupont','Julie','1988-05-16',0065,0065,002,40,'Licence AES',0012,'','Démographie, économie du développement','TESTGROUP',0,0,1,1,1,0,'2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000032,'Dubois','Jules','1978-09-03',0065,0065,002,30,'',0012,'','Aménagement du territoire et développement local','TESTGROUP',1,0,1,0,0,0,'2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000033,'Dulac','Martine','1986-07-09',0065,0065,002,20,'',0012,'','Négoce des affaires étrangères','',0,1,1,0,0,0,'2011-10-11 11:00:00','2011-10-11 11:00:00');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `te_groups`
--

DROP TABLE IF EXISTS `te_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `te_groups` (
  `id` mediumint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `university_id` smallint(3) unsigned zerofill NOT NULL,
  `teacher_id` mediumint(7) unsigned zerofill DEFAULT NULL,
  `group` char(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `te_group_university_id` (`university_id`),
  KEY `te_group_teacher_id` (`teacher_id`),
  KEY `te_group_group` (`group`),
  CONSTRAINT `te_group_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `te_group_university_id` FOREIGN KEY (`university_id`) REFERENCES `universities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `te_groups`
--

LOCK TABLES `te_groups` WRITE;
/*!40000 ALTER TABLE `te_groups` DISABLE KEYS */;
INSERT INTO `te_groups` VALUES (000001,002,NULL,'EMPTYGROUP','2011-10-11 11:00:00','2011-10-11 11:00:00'),(000002,002,0000011,'TESTGROUP','2011-10-11 11:00:00','2011-10-11 11:00:00');
/*!40000 ALTER TABLE `te_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `name` varchar(60) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `university_id` smallint(3) unsigned zerofill NOT NULL DEFAULT '001',
  `quality` varchar(60) NOT NULL,
  `specialty` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `secret` varchar(100) NOT NULL,
  `real` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teacher_secret_UNIQUE` (`secret`),
  KEY `teacher_university_id` (`university_id`),
  CONSTRAINT `teacher_id` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `teacher_university_id` FOREIGN KEY (`university_id`) REFERENCES `universities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (0000011,'Peel','Emma',002,'Professeur agrégé','Anglais','05 04 03 02 01','secretcode001',0,'2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000012,'Steed','John',002,'Maître de conférences','Économie','05 04 03 02 02','secretcode002',0,'2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000013,'Keel','David',002,'Chargé de recherche','','05 04 03 02 03','secretcode003',0,'2011-10-11 11:00:00','2011-10-11 11:00:00');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `universities`
--

DROP TABLE IF EXISTS `universities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `universities` (
  `id` smallint(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `secret` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `url` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`secret`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `universities`
--

LOCK TABLES `universities` WRITE;
/*!40000 ALTER TABLE `universities` DISABLE KEYS */;
INSERT INTO `universities` VALUES (001,'BLANK','Undefined university','','2011-10-11 11:00:00','2011-10-11 11:00:00'),(002,'DEMO','Fictitious university','','2011-10-11 11:00:00','2011-10-11 11:00:00');
/*!40000 ALTER TABLE `universities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` mediumint(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(40) NOT NULL,
  `role` enum('admin','student','teacher','visitor') NOT NULL DEFAULT 'student',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `real` tinyint(1) NOT NULL DEFAULT '1',
  `photo` varchar(25) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0000001,'peladmin@universite.fr','peladmin','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','admin',1,1,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000011,'ens001@universite.fr','ens001','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','teacher',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000012,'ens002@universite.fr','ens002','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','teacher',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000013,'ens003@universite.fr','ens003','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','teacher',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000021,'vis001@domain.net','vis001','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','visitor',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000022,'vis002@domain.net','vis002','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','visitor',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000023,'vis003@domain.net','vis003','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','visitor',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000031,'etu001@universite.fr','etu001','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','student',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000032,'etu002@universite.fr','etu002','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','student',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000033,'etu003@universite.fr','etu003','3930d9085fc7c764023cad15bbf2b9ff1b048ccb','student',1,0,'','2011-10-11 11:00:00','2011-10-11 11:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitors` (
  `id` mediumint(7) unsigned zerofill NOT NULL,
  `name` varchar(60) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `function` varchar(100) NOT NULL,
  `structure` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `country_id` smallint(4) unsigned zerofill NOT NULL,
  `real` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `visitor_country_id` (`country_id`),
  CONSTRAINT `visitor_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `visitor_id` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors`
--

LOCK TABLES `visitors` WRITE;
/*!40000 ALTER TABLE `visitors` DISABLE KEYS */;
INSERT INTO `visitors` VALUES (0000021,'Antoine','Marc','Responsable ressources humaines','Agfa Graphics','01 47 77 10 00','http://www.agfa.fr/','212 avenue Paul Doumer\r\n92508 Rueil-Malmaison Cedex',0065,0,'2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000022,'Bowman','David','Hiring Department','Institute of Spring Technology Ltd','+44 114 2760771','','Henry Street\r\nSheffield\r\nSouth Yorkshire S3 7EQ',0150,0,'2011-10-11 11:00:00','2011-10-11 11:00:00'),(0000023,'Russel','Helena','Internationale Beziehungen','E.ON AG','+49 211 4579 0','http://www.eon.com/','E.ON-Platz 1\r\n40479 Düsseldorf',0005,0,'2011-10-11 11:00:00','2011-10-11 11:00:00');
/*!40000 ALTER TABLE `visitors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `id` int(9) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `student_id` mediumint(7) unsigned zerofill NOT NULL,
  `visitor_id` mediumint(7) unsigned zerofill NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `visit_student_id` (`student_id`),
  KEY `visit_visitor_id` (`visitor_id`),
  KEY `visit_date` (`date`),
  CONSTRAINT `visit_student_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `visit_visitor_id` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-30 18:57:58
