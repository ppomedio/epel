<?php

// Chrono
if (Configure::read('debug') > 0)
    $startTime = microtime(true);

// Début de la page (cartouche + début en-tête)
echo $this->element('layout/head-pre', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week'))); ?>

        <title><?php echo __("eELP", true) . ' &bull; ' . $title_for_layout; ?></title><?php

// Début de la page (fin en-tête)
echo $this->element('layout/head-post', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));

// Envoi immédiat au navigateur
#flush();

?>

    <body>
        <div id="wrapper">
            <header>
                <h1><span>e</span>&nbsp;<?php __("European Language Portfolio"); ?></h1><?php
                if ($this->Session->read('Auth.User.id')) { ?>

                <div id="user-info">
                    <?php echo __("Connected as", true) . ' <span>'
                            . $this->Session->read('Auth.User.info.firstname') . ' '
                            . $this->Session->read('Auth.User.info.name') . '</span>'; ?>

                </div><?php
                } ?>

                <form id="lang" method="post" action="">
                    <select id="lang_select"><?php
                        $languages = Configure::read('epel.locales.available');
                        $page = substr($this->params['url']['url'], 2);
                        foreach($languages as $key => $value) {
                            echo "\n\t\t\t" . '<option value="' . $key . $page . '"';
                            if ($key == $this->Session->read('Config.language'))
                                    echo ' selected="selected"';
                            echo '>' . $value['name'] . '</option>';
                        }
                        ?>

                    </select>
                </form>
            </header>
            <div id="content"><?php
                if (!Configure::read('epel.js_check')) { ?>

                <div id="reqNotMet">
                    <?php __("This site uses JavaScript and cookies: please set your browser to allow them."); ?>

                </div><?php
                } else { ?>

                <noscript>
                    <div id="reqNotMet">
                        <?php __("This site uses JavaScript: please set your browser to allow it."); ?>

                    </div>
                </noscript><?php
                }

                // Affichage de la barre de menu
                echo $this->element('menubar');

                // Messages flash (remplacé par le div "appMessage" ci-dessous)
                if ($this->Session->check('Message') && $this->Session->read('Message')) {
                    if ($this->Session->check('Message.auth'))
                        $message = $this->Session->flash('auth');
                    else
                        $message = $this->Session->flash();
                ?>

                <div id="appMessage">
                    <?php echo $message; ?>

                </div>
                <?php } ?>

                <div id="ajaxMessage">
                </div><?php

                // Affichage du contenu
                echo $content_for_layout;

                // Debug
                if (Configure::read('debug') > 0) {
                    echo $this->element('_debug', array('startTime' => $startTime));
                } else { ?>

                <div class="clear"></div><?php
                } ?>

            </div>
        </div>

        <footer><?php

            /**
             * Pied de page
             */

            echo $this->element('layout/foot', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week'))); ?>

            <p><?php

                // Copyright des parties spécifiques
                if (isset($addCopyright)) {
                    echo __d('portfolio', "This part of the Portfolio is", true) . ' &copy; '; ?>

                <a href="<?php echo $addCopyright['url']; ?>"><?php echo $addCopyright['link']; ?></a><?php
                } ?>

            </p>

        </footer><?php

/**
 * Sortie des scripts de gabarit
 */

echo '        ' . $scripts_for_layout;


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Contrôle cookies & JavaScript
    if (!$.cookies.test()) {
        $('#reqNotMet').slideDown('fast');
    }

    // Sélecteur de langue
    $('#lang_select').change(function(){
        this.form.action = '/' + this.value;
        this.form.submit();
    });

    // Picto de langue active
    $('#lang').css('backgroundImage',
        'url(/img/flags/" . $languages[$this->Session->read('Config.language')]['flag'] . ".png)');

    // Tooltip (liens)
    tooltipLink();

    // Masquage des pictos (loading…)
    var loading = function() {
        $('#divToolTip').fadeOut();
        $(this).closest('div.controls').hide();
        $(this).closest('div.tail').children('div.loading').show();
    };
    $('div.tail').children().bind('click', loading);

    // Messages
    if ($('#appMessage').text()) {
        $('#appMessage').delay(3600).hide('slow');
    }

    // Retour en haut de page
    $('.backtotop').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
    });

    // Masquage des menus (chargement)
    $('nav ul li ul a').bind('click', function() {
        $('nav ul li ul').hide('fast');
    });

    // Suppression des info-bulles (déchargement)
    $(window).unload(function() {
        $('#divToolTip').fadeOut('fast');
    });

");


/**
 * Sortie du buffer Js
 */

echo "\n        " . $this->Js->writeBuffer(array(
        'inline' => true,
        'cache' => false,
        'clear' => true,
        'onDomReady' => true,
        'safe' => true
        )
    ) . "\n";

?>
    </body>
</html>
