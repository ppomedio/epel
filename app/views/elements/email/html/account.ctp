
        <h3><?php
        if (isset($forgotten)) {
            __("Your password was reset.");
        } else {
            __("Your account has been created.");
        }
        ?></h3>
        <p><strong><?php echo __("Username", true) . __(":", true); ?></strong> <?php echo $data['username']; ?><br />
        <strong><?php echo __("Password", true) . __(":", true); ?></strong> <?php echo $data['password']; ?>
        </p><?php
        if (isset($inactive)) { ?>
        <hr />
        <p><em><?php __("Your account is currently inactive: it needs to be authorized by the administrator. You will receive another email when it will be activated."); ?></em></p>
        <hr />
        <?php } ?>
        <p><em><?php __("For security reasons, please change your password as soon as possible."); ?></em></p>
