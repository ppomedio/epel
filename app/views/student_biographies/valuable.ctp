
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-valuable"><?php __d('portfolio', "My most memorable language-learning experiences"); ?></h2>
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "What has helped you to progress in the language?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Are there any language learning methods which you have found really useful, and, if so, why?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Have any language activities allowed you to become more competent in a particular domain?");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Were these part of a course or related to any particular leisure activity?");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "These questions aim at making you refect and helping you structure your thoughts. You can answer to one or several of them.");
                            ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="valuable-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new experience"); ?></h2>
                        <?php echo $this->Form->create('PoValuable', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/valuable/' . urlencode($language),
                            'id' => 'valuable-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoValuable'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __d('portfolio', "Context", true)
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <p class="info"><?php __d('portfolio', "If possible, write in the concerned language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/biography/valuable-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography'));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('student/biography/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('.instruction');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#valuable-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#valuable-edit');
    // Initialisation du formulaire
    function initForm() {
        $('#valuable-edit-form #PoValuableId').val('');
        $('#valuable-edit-form #PoValuableName').val('');
        $('#valuable-edit-form #PoValuableDescription').val('');
    };
    // Soumission Ajax du formulaire
    $('#valuable-edit-form').submit(function() {
        if (!$('#valuable-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/biography/valuable/" . urlencode($language) . "',
                target: '#valuable',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#valuable-edit-form div.loading').show();
                },
                success: function() {
                    $('#valuable-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#valuable-toggle').hide();
                    $('#valuable-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#valuable-edit-form div.loading').hide();
                    $('#valuable-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>