
                    <section id="creation-code">
                        <h2><?php __("Account creation code"); echo " (";  __("editing"); echo "…)"; ?></h2>
                        <?php echo $this->Form->create('Teacher', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/teacher/base/secret',
                            'class' => ''
                            ));
                        echo $validation->bind('Teacher'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('secret', array(
                            'label' => __("Student account creation code", true)
                            ));
                        ?>
                        <p class="info"><?php __("The “account creation code” is a phrase of your choice which you will indicate to those of your students who do not already have a Portfolio so that they can create one."); ?></p>
                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Info tips
    addTipInfo('#creation-code');
");

?>