<?php

/**
 * {app}/controllers/components/side_menu.php
 *
 */
class SideMenuComponent extends Object {

    // Composants utilisés
    var $components = array('Session');


    /**
     * Création de la matrice contenant les menus latéraux,
     * stockée dans une variable de session.
     *
     * @param <string> $part
     */
    function setSideMenu($part) {

        // Retour si déjà créée
        if ($this->Session->check('SideMenu.' . $part))
            return;

        // Création de la matrice
        switch ($part) {
            case 'Biography':
                $menu = array(
                    'valuable' => "My most memorable language-learning experiences",
                    'contact' => "My contacts with speakers of the language",
                    'abroad' => "My stays abroad",
                    'cultural' => "My experience and knowledge of the culture",
                    'employment' => "Competencies which improve my employment prospects",
                    'strategy' => "My learning strategies",
                    'objective' => "My learning objectives"
                );
                break;
            case 'Dossier':
                $menu = array(
                    'show' => "Display",
                    'certificate' => "Certificates",
                    'work' => "Process"
                );
                break;
            case 'Self-assessment':
                $menu = array(
                    'listen' => "Listening",
                    'read' => "Reading",
                    'converse' => "Spoken interaction",
                    'speak' => "Spoken production",
                    'write' => "Writing"
                );
                break;
        }
        
        // Écriture en session
        $this->Session->write('SideMenu.' . $part, $menu);

    }


}

