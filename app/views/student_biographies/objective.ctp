
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-objective"><?php __d('portfolio', "My next objective"); ?></h2>
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "Choose in the checklists(*) or levels grid what you would like to be able to do next in the foreign language or in the near future. Your choice should be realistic, and within easy grasp of your current level.");
                            ?></p>
                            <p><?php
                            __d('portfolio', "Your objective will be easier to reach if you describe it in a short, precise way (for instance, knowing how to begin and end an email addressed to a public administration service). You should set a deadline to reach your target and specify which materials or which person’s help you will need.");
                            ?></p>
                            <p class="smaller"><?php
                            __d('portfolio', "(*) “My learning strategies” in the Biography, or one from the self-assessment checklists");
                            ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="objective-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new objective"); ?></h2>
                        <?php echo $this->Form->create('PoObjective', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/objective/' . urlencode($language),
                            'id' => 'objective-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoObjective'); // Validation jQuery
                        ?>

                        <?php echo $this->Form->hidden('id'); ?>
                        <fieldset>
                            <?php echo $this->Form->input('description', array(
                                'label' => __d('portfolio', "Description", true)
                                )); ?>

                        </fieldset>
                        <fieldset>
                            <legend><?php __d('portfolio', "Summarising and reflecting on my progress"); ?></legend>
                            <p><?php  __d('portfolio', "Have you reached the objective you set? What did you learn while trying to reach it, whether about this language or about the way you learn?");  ?></p>
                            <?php echo $this->Form->input('progress', array(
                                'label' => false
                                )); ?>

                        </fieldset>
                        <p class="info"><?php __d('portfolio', "If possible, write in the concerned language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/biography/objective-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'biography', 'never' => true));
                    echo $this->element('student/biography/side-menu');
                    echo $this->element('student/biography/how-to-use_obj', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#objective-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#objective-edit');
    // Initialisation du formulaire
    function initForm() {
        $('#objective-edit-form #PoObjectiveId').val('');
        $('#objective-edit-form #PoObjectiveDescription').val('');
        $('#objective-edit-form #PoObjectiveProgress').val('');
    };
    // Soumission Ajax du formulaire
    $('#objective-edit-form').submit(function() {
        if (!$('#objective-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/biography/objective/" . urlencode($language) . "',
                target: '#objective',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#objective-edit-form div.loading').show();
                },
                success: function() {
                    $('#objective-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#objective-toggle').hide();
                    $('#objective-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#objective-edit-form div.loading').hide();
                    $('#objective-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>