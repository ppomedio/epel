
                <article>
                    <section>
                        <h1 id="title-adm-visitors" class="spaced"><?php __("Visitors' accounts"); ?></h1>
                        <?php echo $this->element('admin/user/visitors-page'); ?>

                    </section>

                </article>
                <aside>
                    <div>
                        <h2><?php __("User guide"); ?></h2>
                        <div id="search-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="search"></dt>
                                <dd><?php __("Filter criteria"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __("Reset filter criteria"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __("See a visitor's profile"); ?></dd>
                            </dl>
                            <p class="info"><?php __("You can sort the list by clicking on the titles of the columns."); ?></p>
                            <p class="info"><?php __("You can send email to visitors by clicking on their name."); ?></p>
                            <p class="info"><?php __("Inactive users cannot use their account."); ?></p>
                        </div>
                    </div>
                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#search-info');
");

?>
