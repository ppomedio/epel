<?php

/**
 * {app}/controllers/users_controller.php
 *
 */
class UsersController extends AppController {

    var $name = 'Users';
    // Composants
    var $components = array(
        'Uploader.Uploader'
    );
    // Assistants
    var $helpers = array('Menu');


    function beforeFilter() {

        // Autorisations d'accès permanentes
        $this->AppAuth->allow('login', 'logout');

        /* À SUPPRIMER ********************************************************

        // Redéfinition de la fonction de hashage (dans /components/app_auth.php)
        $this->Auth->authenticate = ClassRegistry::init('User');

        ******************************************************** À SUPPRIMER */

        // Config. upload photo
        $this->Uploader->uploadDir = 'files' . DS . 'tmp' . DS;
        $this->Uploader->maxFileSize = strval(Configure::read('epel.photo.size')) . 'M';
        $this->Uploader->maxNameLength = false;
        // Retour…
        parent::beforeFilter();

    }


    function login() {

        if (!empty($this->data)) {
            if ($this->AppAuth->user()) {
                // Variables de session supplémentaires
                $this->_initSuppVars();
                // Logout si compte inactif
                if (!$this->AppAuth->user('active')) {
                    $this->setAction('logout', true);
                }
                // Chargement du menu
                $this->Menu->setMenu();
                // Journalisation du login
                $this->logThis('login');
                // Redirection
                $this->_redirect();
            } else {
                $this->User->create($this->data);
                if ($this->data['User']['password'] && !$this->User->validates(array(
                            'fieldList' => array(
                                'username',
                                'password'
                            )
                        ))) {
                    $this->validateErrors($this->User);
                } else {
                    // Journalisation des échecs d'identification
                    $this->logThis('login_failure', $this->data['User']['username']);
                }
            }
        } else {
            // Redirection si déjà authentifié
            if ($this->AppAuth->user('role'))
                $this->_redirect();
        }

        // Élements de vue
        $this->set('title_for_layout', __("Welcome", true));

    }


    function logout($inactive = false) {

        // Journalisation
        if (!$inactive)
            $this->logThis('logout');
        else
            $this->logThis('login_failure', "Inactive account");
        // RAZ session
        $this->Session->destroy();
        // Message si compte inactif
        if ($inactive)
            $this->Session->setFlash(__("Your account is not activated.", true));
        // Eject…
        $this->redirect($this->AppAuth->logout());

    }


    /**
     * Finalisation de la suppression du compte par un utilisateur
     *
     */
    function delete() {

        // Suppression de l'utilisateur
        $this->User->delete($this->AppAuth->user('id'));
        // Purge du dossier utilisateur dans "/files/users"
        $this->User->cleanUserFiles($this->AppAuth->user('id'));
        // Supp. du cookie utilisateur
        $this->Cookie->destroy();
        // RAZ session
        $this->Session->destroy();
        // Message
        $this->Session->setFlash(__("Your Portfolio has been deleted.", true));
        // Eject…
        $this->redirect($this->AppAuth->logout());

    }


    /**
     * Téléchargement d'un document
     *
     */
    function download() {

        // Nom du dossier
        $folder = $this->params['pass'][0];
        // Nom 'de stockage' du fichier
        $file = $this->params['pass'][1];
        // Nom 'utilisateur' du fichier
        $fileName = $this->params['pass'][2];
        // Types mime
        $uploadMimeTypes = Configure::read('Uploader.mimeTypes');
        $downloadMimeTypes = array();
        foreach ($uploadMimeTypes as $groupKey => $groupValue) {
            foreach ($groupValue as $key => $value) {
                $downloadMimeTypes[$key] = $value;
            }
        }
        // Traitement
        $this->view = 'Media';
        $pathInfo = pathinfo($fileName);
        $forcedl = Configure::read('epel.document.forcedl');
        $params = array(
            'id' => $file,
            'name' => $pathInfo['filename'],
            'extension' => $pathInfo['extension'],
            'download' => $forcedl,
            'mimeType' => $downloadMimeTypes,
            'path' => WWW_ROOT . 'files' . DS . 'users' . DS . $folder . DS
        );
        $this->set($params);

    }


    /**
     * Téléversement de l'image de l'utilisateur
     *
     */
    function photo_upload() {

        Configure::write('debug', 0);
        if (!empty($this->data)) {
            $this->User->set($this->data);
            if ($this->User->validates()) {
                // Suppression de l'éventuelle ancienne photo
                $this->_deletePhoto();
                // Traitement de la nouvelle
                if ($data = $this->Uploader->upload(
                                'photo', array(
                                    'overwrite' => true,
                                    'name' => uniqid('')
                        ))) {
                    // Chargement ID
                    $this->User->id = $this->AppAuth->user('id');
                    // Redimensionnement de l'image
                    $redimPath = $this->Uploader->resize(array(
                                'width' => 244,
                                'quality' => 60
                            ));
                    // Préparation du nom du fichier
                    $photoPathInfo = pathinfo($redimPath);
                    $temp = explode('_', $photoPathInfo['filename']);
                    $temp = array_merge($temp, explode('x', $temp[1]));
                    // Création du dossier (si inexistant)
                    $this->User->createUserFolder($this->AppAuth->user('id'));
                    // Déplacement
                    $photoPath = 'files' . DS . 'users' . DS . $this->AppAuth->user('id')
                            . DS . 'photo' . DS . $temp[0] . '-' . $temp[3] . '.' . $photoPathInfo['extension'];
                    $this->Uploader->move($redimPath, $photoPath, true);
                    // Création de la vignette
                    $thumbPath = $this->Uploader->crop(array(
                                'width' => 42,
                                'height' => 42,
                                'append' => 'thumb',
                                'quality' => 30
                            ));
                    $this->Uploader->move($thumbPath, DS . 'files' . DS . 'users'
                            . DS . $this->User->id . DS . 'photo' . DS . 'th_'
                            . $this->User->id . '.' . $this->Uploader->ext($thumbPath), true);
                    // Suppression du fichier téléchargé
                    $this->Uploader->delete($data['path']);
                    // Enregistrement dans la table / maj variable de session
                    $photoName = substr(strrchr($photoPath, '/'), 1);
                    $this->User->saveField('photo', $photoName);
                    $this->Session->write('Auth.User.photo', $photoName);
                    // Succés
                    echo "|response|uploaded=" . DS . $photoPath . "|response|";
                }
            } else
                echo "|response|error=" . $this->User->validationErrors['photo'] . "|response|";
        } else
            $this->_redirect();
        $this->autoRender = false;
        exit();

    }


    /**
     * Suppression de l'image de l'utilisateur
     *
     */
    function photo_delete() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->_deletePhoto();
            $this->autoRender = false;
            exit();
        }

    }


    /**
     * Suppression d'une image utilisateur :
     *
     * - effacement du fichier correspondant s'il existe,
     * - si un userId est spécifié le champ correspondant est réinitialisé.
     *
     * @param <mixed> $fieldId
     */
    private function _deletePhoto() {

        // Initialisation
        $this->User->id = $this->AppAuth->user('id');
        // Raz du champ dans la table
        $this->User->saveField('photo', '');
        // Suppression des fichiers
        $this->User->cleanUserFiles($this->AppAuth->user('id'), 'photo');
        // Raz variable de session
        $this->Session->write('Auth.User.photo', NULL);

    }


    /**
     * Init. des variables de session supplémentaires
     *
     */
    private function _initSuppVars() {

        $model = ucfirst($this->AppAuth->user('role'));
        $this->loadModel($model);
        switch ($model) {
            case 'Student':
                $this->_writeSuppVars('Student', $this->Student->find('first', array(
                            'conditions' => array('Student.id' => $this->AppAuth->user('id')),
                            'fields' => array(
                                'Student.name',
                                'Student.firstname',
                                'Student.university_id',
                                'Student.vis_hide',
                                'Student.vis_passport',
                                'Student.vis_biography',
                                'Student.vis_dossier'
                            ),
                            'recursive' => -1
                        )));
                break;
            case 'Teacher':
                $this->_writeSuppVars('Teacher', $this->Teacher->find('first', array(
                            'conditions' => array('Teacher.id' => $this->AppAuth->user('id')),
                            'fields' => array(
                                'Teacher.name',
                                'Teacher.firstname',
                                'Teacher.quality',
                                'Teacher.specialty',
                                'Teacher.university_id'
                            ),
                            'recursive' => -1
                        )));
                break;
            case 'Visitor':
                $this->_writeSuppVars('Visitor', $this->Visitor->find('first', array(
                            'conditions' => array('Visitor.id' => $this->AppAuth->user('id')),
                            'fields' => array(
                                'Visitor.name',
                                'Visitor.firstname'
                            ),
                            'recursive' => -1
                        )));
                break;
            case 'Admin':
                // L'utilisateur "admin" est unique, le modèle n'utilise pas de table.
                $this->Session->write('Auth.User.info.name', '(' . $this->AppAuth->user('username') . ')');
                $this->Session->write('Auth.User.info.firstname', __("Administrator", true));
                break;
        }

    }


    /**
     * Création des variables de session supplémentaires
     *
     * @param <string> $model
     * @param <array> $user
     */
    private function _writeSuppVars($model, $user) {

        // Nom et prénom
        $this->Session->write('Auth.User.info.name', $user[$model]['name']);
        $this->Session->write('Auth.User.info.firstname', $user[$model]['firstname']);
        // Infos complémentaires
        switch ($model) {
            case 'Student':
                // Université / spécialité / groupe
                $this->Session->write('Auth.User.info.university_id', $user['Student']['university_id']);
                // Visibilité du portfolio
                $this->Session->write('Auth.User.info.vis_hide', $user['Student']['vis_hide']);
                // Visibilité des éléments de portfolio
                $this->Session->write('Auth.User.info.vis_passport', $user['Student']['vis_passport']);
                $this->Session->write('Auth.User.info.vis_biography', $user['Student']['vis_biography']);
                $this->Session->write('Auth.User.info.vis_dossier', $user['Student']['vis_dossier']);
                // Portfolios ouverts (langues)
                $this->loadModel('Portfolio');
                $data = $this->Portfolio->find('all', array(
                            'conditions' => array('Portfolio.student_id' => $this->AppAuth->user('id')),
                            'fields' => array(
                                'Portfolio.id',
                                'Portfolio.vis_passport',
                                'Portfolio.vis_biography',
                                'Portfolio.vis_dossier',
                                'Portfolio.mother',
                                'Portfolio.usual',
                                'Language.id',
                                'Language.name'
                            ),
                            'recursive' => 0
                        ));
                // Enregistrement en session
                if (!empty($data)) {
                    $languages = array();
                    $portfolios = array();
                    foreach ($data as $key => $value) {
                        $languages[strtolower($value['Language']['id'])]['portfolio_id'] = (int) $value['Portfolio']['id'];
                        $languages[strtolower($value['Language']['id'])]['name'] = $value['Language']['name'];
                        $languages[strtolower($value['Language']['name'])] = (int) $value['Language']['id'];
                        $portfolios[strtolower($value['Language']['name'])]['id'] = (int) $value['Portfolio']['id'];
                        if ($value['Portfolio']['mother'])
                            $portfolios[strtolower($value['Language']['name'])]['mother'] = 1;
                        if ($value['Portfolio']['usual'])
                            $portfolios[strtolower($value['Language']['name'])]['usual'] = 1;
                        $visibility[strtolower($value['Language']['name'])]['passport'] = $value['Portfolio']['vis_passport'];
                        $visibility[strtolower($value['Language']['name'])]['biography'] = $value['Portfolio']['vis_biography'];
                        $visibility[strtolower($value['Language']['name'])]['dossier'] = $value['Portfolio']['vis_dossier'];
                    }
                    $this->Session->write('Auth.User.languages', $languages);
                    $this->Session->write('Auth.User.portfolios', $portfolios);
                    $this->Session->write('Auth.User.visibility', $visibility);
                }
                break;
            case 'Teacher':
                // Qualité
                $this->Session->write('Auth.User.info.quality', $user['Teacher']['quality']);
                // Spécialité
                $this->Session->write('Auth.User.info.specialty', $user['Teacher']['specialty']);
                // Université
                $this->Session->write('Auth.User.info.university_id', $user['Teacher']['university_id']);
                break;
            default:
                break;
        }

    }


    /**
     * Redirection suivant cookie[User.id]
     *
     */
    private function _redirect() {

        $lastPage = urldecode($this->Cookie->read($this->AppAuth->user('id') . '.lastpage'));
        if (empty($lastPage))
            $this->redirect('/' . $this->params['language']
                    . '/' . $this->AppAuth->user('role')
                    . '/base'
            );
        else
            $this->redirect('/' . $this->Session->read('Config.language') . $lastPage);

    }


}

