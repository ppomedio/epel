
                    <section>
                        <h2><?php __("Profile"); ?></h2>
                        <div>
                            <dl>
                                <dt><?php __("Name"); ?></dt>
                                <dd><?php echo $user['Teacher']['name']; ?></dd>
                                <dt><?php __("First name"); ?></dt>
                                <dd><?php echo $user['Teacher']['firstname']; ?></dd>
                                <dt><?php __("College / University"); ?></dt>
                                <dd><?php echo $user['University']['name']; ?></dd>
                                <?php
                                if ($user['Teacher']['quality']) {
                                ?><dt><?php __("Quality"); ?></dt>
                                <dd><?php echo $user['Teacher']['quality']; ?></dd>
                                <?php
                                }
                                if ($user['Teacher']['specialty']) {
                                ?><dt><?php __("Specialty"); ?></dt>
                                <dd><?php echo $user['Teacher']['specialty']; ?></dd>
                                <?php
                                }
                                if ($user['Teacher']['phone']) {
                                ?><dt><?php __("Telephone"); ?></dt>
                                <dd><?php echo $user['Teacher']['phone']; ?></dd>
                                <?php } ?>

                            </dl>
                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls">
                                    <?php
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/teacher/base/profile',
                                        array(
                                            'id' => 'profile',
                                            'class' => 'edit'
                                        )
                                    );
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div id="data_profile" class="tooltip-data"><?php __('Edit profile'); ?></div>
                    </section>