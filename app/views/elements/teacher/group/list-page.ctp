<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#groups',
    'before' => $this->Js->get('#groups')->effect('fadeTo', array('buffer' => false)),
   ));

?>

                        <div id="groups" class="table"><?php
                            if (empty($user)) { ?>
                            <p class="alert"><?php __("No group has been created yet."); ?></p>
                            <p class="info"><?php __("You can start creating yours using “Group management” on the right side.");
                            $this->Js->Buffer("\n    addTipAlert('#groups');\n    addTipInfo('#groups');\n");
                            ?></p>
                            <?php } else { ?>
                            <table>
                                <thead>
                                    <tr>
                                        <th class="group-code"><?php echo $paginator->sort(__("Group code", true), 'TeGroup.group'); ?></th>
                                        <th class="teacher-name"><?php echo $paginator->sort(__("Referring teacher", true), 'Teacher.name'); ?></th>
                                        <th><?php __("Contact"); ?></th>
                                        <th><?php __("Actions"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach ($user as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>
                                        <tr id="<?php echo $value['TeGroup']['id']; ?>" class="<?php echo $trClass; ?>">
                                            <td><span id="edit1-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="group-code"><?php echo $value['TeGroup']['group']; ?></span><?php echo '<br /><span class="smaller">(' . $value['TeGroup']['count'] . ')</span>'; ?></td>
                                            <?php
                                            $userClass = 'take';
                                            $teacherPhone = '';
                                            if (!empty($value['Teacher']['id'])) {
                                                $undefinedClass = '';
                                                $itsMe = false;
                                                $email = '';
                                                if ($value['Teacher']['id'] == $this->Session->read('Auth.User.id'))
                                                    $itsMe = true;
                                                if (!empty($value['Teacher']['User']['photo'])) {
                                                    $pathInfo = pathinfo($value['Teacher']['User']['photo']);
                                                    $teacherInfo = '<img src="/files/users/' . $value['TeGroup']['teacher_id'] . '/photo/th_' . $value['TeGroup']['teacher_id'] . '.' . $pathInfo['extension'] . '" />';
                                                } else
                                                    $teacherInfo = '<img src="/img/user_thumb.png" />';
                                                if ($itsMe)
                                                    $teacherInfo .= '<span class="info">' . $value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'] . '</span><br />';
                                                else
                                                    $teacherInfo .= '<span class="bolder">' .
                                                            $this->Html->link($value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'],
                                                                array(
                                                                    'controller' => 'contact',
                                                                    'action' => 'form',
                                                                    base64_encode($value['Teacher']['id'] . '_' . $value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'])
                                                                ),
                                                                array(
                                                                    'id' => 'contact1-' . $value['Teacher']['id'],
                                                                    'class' => 'iframe contact link'
                                                                )
                                                            ) . '</span><br />';
                                                if (!empty($value['Teacher']['quality']))
                                                    $teacherInfo .= $value['Teacher']['quality'];
                                                if (!empty($value['Teacher']['specialty']))
                                                    $teacherInfo .= '<br />' . $value['Teacher']['specialty'];
                                                if (!empty($value['Teacher']['phone']))
                                                    $teacherPhone = '<br /><span class="symbol-phone"></span><span class="smaller"> ' . $value['Teacher']['phone'] . '</span>';
                                                if ($itsMe) {
                                                    $email = $value['Teacher']['User']['email'];
                                                    $userInfo = $teacherInfo;
                                                    $userClass = 'release';
                                                }
                                            } else {
                                                $undefinedClass = 'alert';
                                                $teacherInfo = '<img src="/img/user_thumb.png" /><br />' . __("Undefined", true);
                                                $email = '…';
                                            } ?>

                                            <td id="teacher-info-<?php echo $value['TeGroup']['id']; ?>" class="<?php echo $undefinedClass; ?>"><?php echo $teacherInfo; ?></td>
                                            <td id="teacher-coord-<?php echo $value['TeGroup']['id']; ?>">
                                            <?php
                                            if (!empty($email)) {
                                                echo $email;
                                            } else {
                                                echo $this->Html->link($value['Teacher']['User']['email'],
                                                        array(
                                                            'controller' => 'contact',
                                                            'action' => 'form',
                                                            base64_encode($value['Teacher']['id'] . '_' . $value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'])
                                                        ),
                                                        array(
                                                            'id' => 'contact2-' . $value['Teacher']['id'],
                                                           'class' => 'iframe contact link'
                                                        )
                                                );
                                            }
                                            if (!empty($teacherPhone))
                                                echo $teacherPhone;
                                            ?></td>
                                            <td class="controls">
                                                <div id="loading-<?php echo $value['TeGroup']['id']; ?>" class="loading"></div>
                                                <div id="control-<?php echo $value['TeGroup']['id']; ?>" class="controls">
                                                    <span id="edit2-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="group-code edit link"></span>
                                                    <span id="ref_<?php echo $userClass . '-' . $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="<?php echo $userClass . ' '; ?>people"></span>
                                                    <?php if ($value['TeGroup']['count'] == 0) { ?>
                                                    <span id="supp-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="supp"></span>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <?php if ($email != '…') { ?>
                                            <div id="data_contact1-<?php echo $value['Teacher']['id']; ?>" class="tooltip-data">
                                            <?php
                                            __('Send an email to');
                                            echo ' ' . $value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'];
                                            ?></div>
                                            <div id="data_contact2-<?php echo $value['Teacher']['id']; ?>" class="tooltip-data">
                                            <?php
                                            __('Send an email to');
                                            echo ' ' . $value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'];
                                            ?></div>
                                            <?php } ?>
                                            <div id="data_edit1-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="tooltip-data">
                                            <?php
                                            __("Edit group");
                                            echo ' ' . $value['TeGroup']['group'];
                                            ?></div>
                                            <div id="data_edit2-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="tooltip-data">
                                            <?php
                                            __("Edit group");
                                            echo ' ' . $value['TeGroup']['group'];
                                            ?></div>
                                            <div id="data_ref_take-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="tooltip-data">
                                            <?php
                                            __("Be the referring teacher for the group");
                                            echo ' ' . $value['TeGroup']['group'];
                                            ?></div>
                                            <div id="data_ref_release-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="tooltip-data">
                                            <?php
                                            __("Stop being the referring teacher for the group");
                                            echo ' ' . $value['TeGroup']['group'];
                                            ?></div>
                                            <div id="data_supp-<?php echo $value['TeGroup']['id'] . '-' . $value['TeGroup']['group']; ?>" class="tooltip-data">
                                            <?php
                                            __("Delete the group");
                                            echo ' ' . $value['TeGroup']['group'];
                                            ?></div>
                                        </tr>
                                        <div id="group-ref-teacher-confirm_<?php echo $value['TeGroup']['id'] . '_'
                                                . $value['TeGroup']['group']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php __("You will be the referring teacher for the group"); echo ' ' . $value['TeGroup']['group'] . '.'; ?></div>
                                        <div id="group-unref-teacher-confirm_<?php echo $value['TeGroup']['id'] . '_'
                                                . $value['TeGroup']['group']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php __("You will cease to be the referring teacher for the group"); echo ' ' . $value['TeGroup']['group'] . '.'; ?></div>
                                        <div id="group-delete-confirm_<?php echo $value['TeGroup']['id'] . '_'
                                                . $value['TeGroup']['group']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php __("Please confirm removal of group"); echo ' ' . $value['TeGroup']['group'] . ' '; __("from the list."); ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php echo $this->element('paginate', array('subId' => 'groups')); ?>

                            <div id="data_sort-group-code" class="tooltip-data"><?php
                                __("Sort by group code");
                                echo '<br />';
                                __("Click on a group code to edit it");
                                echo '<br />';
                                __("(in brackets: number of students in the group)"); ?></div>
                            <div id="data_sort-teacher-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <?php } ?>

                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

// Contenu de la cellule "Enseignant référent" si prise de groupe
$userInfo = '';
$photo = pathinfo($this->Session->read('Auth.User.photo'));
if (!empty($photo['extension'])) {
    $userInfo .= '<img src="/files/users/'
            . $this->Session->read('Auth.User.id') . '/photo/th_'
            . $this->Session->read('Auth.User.id'). '.'
            . $photo['extension'] . '" />';
} else
    $userInfo .= '<img src="/img/user_thumb.png" />';
$userInfo .= '<span class="info">'
        . addslashes($this->Session->read('Auth.User.info.firstname')) . ' '
        . addslashes($this->Session->read('Auth.User.info.name')) . '</span><br />';
if (($quality = addslashes($this->Session->read('Auth.User.info.quality'))))
    $userInfo .= $quality;
$userInfo .= '<br />' . addslashes($this->Session->read('Auth.User.info.specialty'));
if (!empty($moreInfo['Teacher']['phone']))
    $phone = '<br /><span class="smaller">&#x2706 ' . $moreInfo['Teacher']['phone'] . '</span>';
else
    $phone = '';

$this->Js->Buffer("
    // Id, classes et cie
    $('#groups table thead tr th.group-code a').attr('id', 'sort-group-code').addClass('help');
    $('#groups table thead tr th.teacher-name a').attr('id', 'sort-teacher-name').addClass('sort-help');
    $('#groups table tbody tr td span.group-code').addClass('link');
    // Tooltips
    tooltipHelp('#groups table thead tr th.group-code');
    tooltipSortHelp('#groups table thead tr th.teacher-name');
    tooltipPeople('#groups table tbody tr td.controls');
    tooltipSupp('#groups table tbody tr td.controls');
    // Tips
    addTipInfo('#groups table');
    // Devenir l'enseignant référent d'un groupe
    function takeGroup(groupId, groupCode) {
        $('#group-ref-teacher-confirm_' + groupId + '_' + groupCode).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var groupId = extract[1];
                    var groupCode = extract[2];
                    $(this).dialog('close');
                    $('div#control-' + groupId).hide();
                    $('div#loading-' + groupId).show();
                    $('#ajaxMessage').load('/" . $this->params['language'] . "/teacher/group/ref_set/' + groupId + '/' + groupCode, function() {
                        // Maj infos enseignant
                        $('td#teacher-coord-' + groupId).html('" . $this->Session->read('Auth.User.email') . $phone . "');
                        $('td#teacher-info-' + groupId).removeClass('alert').html('" . $userInfo . "');
                        // Maj contrôles
                        $('span#ref_take-' + groupId + '-' + groupCode)
                                .removeClass('take')
                                .addClass('release')
                                .attr('id', 'ref_release-' + groupId + '-' + groupCode);
                        // Bascule pictos
                        $('div#loading-' + groupId).hide();
                        $('div#control-' + groupId).show();
                        // Tooltips
                        tooltipPeople();
                        // Message
                        $('#content div#ajaxMessage').show(function(){
                                $(this).delay(3600).hide('slow');
                        });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                    var extract = $(this).attr('id').split('_');
                    var groupId = extract[1];
                    $('div#control-' + groupId).show();
                }
            }
        });
    };
    // Ne plus être l'enseignant référent d'un groupe
    function releaseGroup(groupId, groupCode) {
        $('#group-unref-teacher-confirm_' + groupId + '_' + groupCode).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var groupId = extract[1];
                    var groupCode = extract[2];
                    $(this).dialog('close');
                    $('div#control-' + groupId).hide();
                    $('div#loading-' + groupId).show();
                    $('#ajaxMessage').load('/" . $this->params['language'] . "/teacher/group/ref_unset/' + groupId + '/' + groupCode, function() {
                        // Maj infos enseignant
                        $('td#teacher-coord-' + groupId).html('…');
                        $('td#teacher-info-' + groupId).addClass('alert').html('" . addslashes('<img src="/img/user_thumb.png" />' . '<br />' . __("Undefined", true)) . "');
                        // Maj contrôles
                        $('span#ref_release-' + groupId + '-' + groupCode)
                                .removeClass('release')
                                .addClass('take')
                                .attr('id', 'ref_take-' + groupId + '-' + groupCode);
                        // Bascule pictos
                        $('div#loading-' + groupId).hide();
                        $('div#control-' + groupId).show();
                        // Tooltips
                        tooltipPeople();
                        // Message
                        $('#content div#ajaxMessage').show(function(){
                                $(this).delay(3600).hide('slow');
                        });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                    var extract = $(this).attr('id').split('_');
                    var groupId = extract[1];
                    $('div#control-' + groupId).show();
                }
            }
        });
    };
    // Suppression d'un groupe
    $('#groups table tbody td.controls div span.supp').unbind('click').bind('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('-');
        var groupId = extract[1];
        var groupCode = extract[2];
        $('#group-delete-confirm_' + groupId + '_' + groupCode).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    $(this).dialog('close');
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var groupId = extract[1];
                    var groupCode = extract[2];
                    $('div#control-' + groupId).hide();
                    $('div#loading-' + groupId).show();
                    $('#groups').load('/" . $this->params['language'] . "/teacher/group/delete/' + groupId + '/' + groupCode, function() {
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__("The group", true)) . " ' + groupCode + ' " . addslashes(__("has been deleted.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                    var extract = $(this).attr('id').split('_');
                    var groupId = extract[1];
                    $('div#control-' + groupId).show();
                }
            }
        });
    });
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Attachements sur clic
    $('#groups table tbody td.controls div span.take').live('click', function() {
        var extract = $(this).attr('id').split('-');
        var groupId = extract[1];
        var groupCode = extract[2];
        takeGroup(groupId, groupCode);
    });
    $('#groups table tbody td.controls div span.release').live('click', function() {
        var extract = $(this).attr('id').split('-');
        var groupId = extract[1];
        var groupCode = extract[2];
        releaseGroup(groupId, groupCode);
    });
");
} else {
    $this->Js->Buffer("
    // Tooltips
    tooltipLink('#groups');
    // Rétablissement du titre
    $('#title-groups').html('" . addslashes(__("Groups and referring teachers", true)) . "');
    // Fancybox (contact)
    fancyBox('#groups table td a.iframe');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>