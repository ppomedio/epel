
                <article>
                    <h1 id="title-search-page" class="spaced"><?php __("Students"); ?></h1>
                    <section><?php
                        echo $this->element('teacher/search/students-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('teacher/search/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>