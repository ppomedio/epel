<?php

$menu = $this->Session->read('SideMenu.Dossier');

?>

                    <div>
                        <h2><?php __d('portfolio', "Chapters"); ?></h2>
                        <nav>
                            <ul><?php
                            foreach ($menu as $key => $value) {
                                if (strpos($this->params['url']['url'], $key))
                                    $class = 'current';
                                else
                                    $class = '';
                            ?>

                                <li class="<?php echo $class; ?>"><?php
                                echo $this->Html->link(__d('portfolio', $value, true),
                                    '/' . Configure::read('Config.langCode')
                                        . '/student/dossier/'
                                        . '/' . $key . '/'
                                        . urlencode($language),
                                    array(
                                        'id' => '',
                                        'class' => ''
                                    )
                                );
                                ?></li>
                            <?php } ?>

                            </ul>
                        </nav>
                    </div>