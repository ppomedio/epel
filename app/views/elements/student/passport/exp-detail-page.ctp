<?php

$paginator->options(array(
    'update' => '#valuable',
    ));

?>

                        <div id="valuable" class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="name"><?php echo $paginator->sort(__d('portfolio', "Context", true), 'PoValuable.name', array('url' => $this->params['pass'])); ?></th>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoValuable'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo nl2br($value['description']); ?></td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <div id="data_sort-valuable-name" class="tooltip-data"><?php __("Sort by context"); ?></div>
                            <p class="info"><?php  __d('portfolio', "Adding / updating / deleting these experience items takes place in the Biography part of your Portfolio.");  ?></p>
                            <?php echo $this->element('paginate', array('subId' => 'valuable')); ?>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Id, classes et cie
    $('#valuable table thead tr th.name a').attr('id', 'sort-valuable-name').addClass('sort-help');
");

if (isset($ajax))
    $this->Js->Buffer("
    // Tooltips
    tooltipHelp('#valuable table thead tr th');
    tooltipSortHelp('#valuable table thead tr th');
    $('#title-valuable').html('" . addslashes(__d('portfolio', "Memorable language-learning experiences", true)) . "');
    $('#valuable p.info').html('<span class=\"tip-info\"></span>' + $('#valuable p.info').text());
");

// Pagination
echo $this->Js->writeBuffer();

?>