
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-certificate"><?php __d('portfolio', "Certificates"); ?></h2>
                        <div id="certificate-instruction" class="instruction">
                            <p><?php echo __d('portfolio', "This part of your Dossier is where you keep", true) . ' ' . __d('portfolio', "descriptions of certificates which you acquired.", true); ?></p>
                            <p class="info"><?php echo __d('portfolio', "If you wish it you can keep a digitized copy in the “Display” part of your dossier.", true); ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="certificate-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new certificate"); ?></h2>
                        <?php echo $this->Form->create('PoCertificate', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/dossier/certificate/' . urlencode($language),
                            'id' => 'certificate-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoCertificate'); // Validation jQuery
                        ?>

                        <fieldset id="cert-info"><?php
                            $available = Configure::read('epel.locales.available');
                            $dateNum = $available[$this->params['language']]['dateNum'];
                            echo $this->Form->hidden('id');
                            echo $this->Form->input('date', array(
                                'type' => 'date',
                                'label' => __d('portfolio', "Date", true),
                                'dateFormat' => $dateNum,
                                'minYear' => date('Y') - 40,
                                'maxYear' => date('Y'),
                                'separator' => ' ',
                                'empty' => false
                                ));
                            echo $this->Form->input('certificate_id', array(
                                'label' => __d('portfolio', "Type", true),
                                'options' => $certificates,
                                'empty' => array('0001' => __("Other…", true))
                                ));
                            echo $this->Form->input('certificate_info', array(
                                'label' => __d('portfolio', "Certificate", true)
                                ));
                            echo $this->Form->input('organism', array(
                                'label' => __d('portfolio', "Delivered by", true)
                                )); ?>

                        </fieldset>
                        <p class="info"><?php
                        echo __d('portfolio', "If your certificate is not on the list, you can", true)
                            . " " . $this->Html->link(__("contact the administrator", true),
                                array(
                                    'controller' => 'contact',
                                    'action' => 'form'
                                ),
                                array('class'=>'iframe contact')
                            ) . " " . __d('portfolio', "to have it added.", true);
                        ?></p>
                        <fieldset id="cert-comp">
                            <legend id="<?php __d('portfolio', "CEFRL level, specified only if it appears on the original certificate."); ?>" class="help"><?php __d('portfolio', "Certified level(s)"); ?></legend><?php
                            $picto = array(
                                'listening' => 'pictoPortfolioListening',
                                'reading' => 'pictoPortfolioReading',
                                'speak_int' => 'pictoPortfolioSpeakInt',
                                'speak_prod' => 'pictoPortfolioSpeakProd',
                                'writing' => 'pictoPortfolioWriting'
                            );
                            $bubble = array(
                                'listening' => __d('levels_grid', "Understanding: listening", true),
                                'reading' => __d('levels_grid', "Understanding: reading", true),
                                'speak_int' => __d('levels_grid', "Speaking: spoken interaction", true),
                                'speak_prod' => __d('levels_grid', "Speaking: spoken production", true),
                                'writing' => __d('levels_grid', "Writing", true)
                            );
                            foreach ($competencies as $key => $value) {
                                echo '<span id="' . $bubble[$key] . '" class="' . $picto[$key] . '"></span>';
                                echo $this->Form->input($key, array(
                                    'label' => false,
                                    'options' => $levels,
                                    'empty' => array('0' => "…")
                                    ));
                            }
                            ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/dossier/certificate-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'dossier'));
                    echo $this->element('student/dossier/side-menu');
                    echo $this->element('student/dossier/how-to-use_cert', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog',
        'jquery.ui.datepicker',
        'i18n/jquery.ui.datepicker-' . $this->params['language']
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // UIDatePicker <-> Cake
    $('#PoCertificateDateMonth').after('<input class=\"datepicker\" type=\"hidden\"/>');
    setDatePicker();
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#certificate-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#certificate-instruction');
    addTipInfo('#certificate-edit');
    // Tooltips
    tooltipHelp('#cert-comp');
    $('#cert-comp span').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        cursor : 'help'
    });
    // Initialisation du formulaire
    function initForm() {
        $('#PoCertificateId').val('');
        $('#PoCertificateCertificateInfo').val('');
        $('#PoCertificateOrganism').val('');
        $('#certificate-edit-form select :selected').removeAttr('selected');
        $('#PoCertificateCertificateInfo').val('').parent('div').show();
    };
    // Bascule de #PoCertificateCertificateInfo
    $('#PoCertificateCertificateId').change(function() {
            if ($(this).val() == '0001')
                $('#PoCertificateCertificateInfo').val('').parent('div').show('slow');
            else
                $('#PoCertificateCertificateInfo').parent('div').hide('slow', function() {
                    $('#PoCertificateCertificateInfo').val($('#PoCertificateCertificateId :selected').text());
                });
    });
    // Soumission Ajax du formulaire
    $('#certificate-edit-form').submit(function() {
        if (!$('#certificate-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/dossier/certificate/" . urlencode($language) . "',
                target: '#certificate',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#certificate-edit-form div.loading').show();
                },
                success: function() {
                    $('#certificate-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#certificate-toggle').hide();
                    $('#certificate-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#certificate-edit-form div.loading').hide();
                    $('#certificate-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>