<?php

/**
 * {app}/controllers/student_visits_controller.php
 *
 */
class StudentVisitsController extends AppController {

    var $name = 'StudentVisits';
    // Modèle
    var $uses = array('Visit');
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Date',
        'Menu'
    );


    function beforeFilter() {

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'visit' . DS . 'show');

    }


    function show() {

        // Chargement des données de pagination des visites
        $visits = $this->_getVisits($this->AppAuth->user('id'));
        // Mémorisation des options de pagination
        $this->Session->write('Auth.User.pagination.Visit.page', $this->params['paging']['Visit']['options']['page']);
        $this->Session->write('Auth.User.pagination.Visit.order', $this->params['paging']['Visit']['options']['order']);
        // Traitement
        if ($this->RequestHandler->isAjax()) {
            /**
             * Pagination Ajax
             */
            Configure::write('debug', 0);
            // Variables de vue
            $this->set('ajax', true);
            $this->set('user', $visits);
            // Rendu de l'élément
            $this->render(DS . 'elements' . DS . 'visits-page');
            return;
        } else {
            /**
             * Affichage normal
             */
            // Variables de vue
            $this->set('user', $visits);
            $this->set('title_for_layout', __("Visits", true));
        }

    }


    /**
     * Chargement de la liste des visites du portfolio d'un étudiant
     *
     * @param <string> $languageId
     * @return <array>
     */
    private function _getVisits($studentId) {

        // Repositionnement
        if ($this->Session->check('Auth.User.pagination.Visit')) {
            $page = $this->Session->read('Auth.User.pagination.Visit.page');
            $order = $this->Session->read('Auth.User.pagination.Visit.order');
            $this->Session->delete('Auth.User.pagination.Visit');
        } else {
            $page = NULL;
            $order = array('Visit.date' => 'DESC');
        }
        // Paramétrage
        $this->paginate = array(
            'conditions' => array('Visit.student_id' => $studentId),
            'fields' => array(
                'id',
                'visitor_id',
                'date'
            ),
            'contain' => array(
                'Visitor' => array(
                    'fields' => array(
                        'id',
                        'name',
                        'firstname',
                        'function',
                        'structure',
                        'phone',
                        'url',
                        'address',
                        'country_id'
                    ),
                    'Country' => array(
                        'fields' => array('id', 'name')
                    ),
                    'User' => array(
                        'fields' => array('id', 'photo')
                    )
                )
            ),
            'page' => $page,
            'order' => $order,
            'limit' => 10
        );
        // Chargement des données
        $data = $this->paginate('Visit');
        $visits = array();
        foreach ($data as $key => $value) {
            $visits['Visit'][$key]['date'] = $value['Visit']['date'];
            $visits['Visit'][$key]['visitorId'] = $value['Visitor']['id'];
            $visits['Visit'][$key]['visitorName'] = $value['Visitor']['name'];
            $visits['Visit'][$key]['visitorFirstname'] = $value['Visitor']['firstname'];
            $visits['Visit'][$key]['visitorFunction'] = $value['Visitor']['function'];
            $visits['Visit'][$key]['visitorStructure'] = $value['Visitor']['structure'];
            $visits['Visit'][$key]['visitorPhone'] = $value['Visitor']['phone'];
            $visits['Visit'][$key]['visitorUrl'] = $value['Visitor']['url'];
            $visits['Visit'][$key]['visitorAddress'] = $value['Visitor']['address'];
            $visits['Visit'][$key]['visitorCountry'] = $value['Visitor']['Country']['name'];
            $visits['Visit'][$key]['visitorPhoto'] = $value['Visitor']['User']['photo'];
        }
        // Envoi de la pagination
        return $visits;

    }


}