/*
 * e-PEL
 *
 * Éléments d'interface
 *
 */


// Tooltips associés aux contrôles
function tooltipAdd(param){
    var element = param ? param + ' ' : '';
    $(element + '.add').tooltip({
        bordercolor: '#39f',
        bgcolor: '#39f',
        fontcolor : '#fefeff',
        fontsize : '12px',
        cursor : 'pointer'
    });
};
function tooltipEdit(param){
    var element = param ? param + ' ' : '';
    $(element + '.edit').tooltip({
        bordercolor: '#39f',
        bgcolor: '#39f',
        fontcolor : '#fefeff',
        fontsize : '12px',
        cursor : 'pointer'
    });
};
function tooltipGear(param){
    var element = param ? param + ' ' : '';
    $(element + '.gear').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        cursor : 'pointer'
    });
};
function tooltipHelp(param){
    var element = param ? param + ' ' : '';
    $(element + '.help').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        width : '210px',
        cursor : 'help'
    });
};
function tooltipLink(param){
    var element = param ? param + ' ' : '';
    $(element + '.link').tooltip({
        bordercolor: '#39f',
        bgcolor: '#39f',
        fontcolor : '#fefeff',
        fontsize : '12px',
        cursor : 'pointer'
    });
};
function tooltipPhoto(param){
    var element = param ? param + ' ' : '';
    $(element + '.photo').css('color', '#39f').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#39f',
        fontsize : '12px',
        cursor : 'pointer'
    });
};
function tooltipPeople(param){
    var element = param ? param + ' ' : '';
    $(element + '.people').tooltip({
        bordercolor: '#39f',
        bgcolor: '#39f',
        fontcolor : '#fefeff',
        fontsize : '12px',
        cursor : 'pointer'
    });
};
function tooltipSee(param){
    var element = param ? param + ' ' : '';
    $(element + '.see').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        cursor : 'help'
    });
};
function tooltipSortHelp(param){
    var element = param ? param + ' ' : '';
    $(element + '.sort-help').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        cursor : 'help'
    });
};
function tooltipSupp(param){
    var element = param ? param + ' ' : '';
    $(element + '.supp').tooltip({
        bordercolor: '#f93',
        bgcolor: '#f93',
        fontcolor : '#fefeff',
        fontsize : '12px',
        cursor : 'pointer'
    });
};

// Tips
function addTipInfo(param){
    var element = param ? param + ' ' : '';
    $(element + 'p.info').each(function(){
        var html = '<span class=\"tip-info\"></span>' + $(this).html();
        $(this).html(html);
    });
};
function addTipAlert(param){
    var element = param ? param + ' ' : '';
    $(element + 'p.alert').each(function(){
        var html = '<span class=\"tip-alert\"></span>' + $(this).html();
        $(this).html(html);
    });
};

// Fancybox de contact
function fancyBox(eParam, wParam, hParam){
    var element = eParam ? eParam : 'a.iframe.contact';
    var width = wParam ? wParam : 450;
    var height = hParam ? hParam : 410;
    $(element).fancybox({
        'width'             :   width,
        'height'            :   height,
        'padding'           :   0,
        'margin'            :   0,
        'speedIn'           :   900,
        'scrolling'         :   'no',
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })
};

// Intégration jQuery.ui.datepicker -> contrôles de date CakePHP
function setDatePicker(){
    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        buttonImage: '',
        buttonText: '',
        buttonImageOnly: true,
        changeMonth: false,
        changeYear: false,
        duration: '',
        showOn: 'both',
        onSelect: function(sel_date) {
            var newDate = sel_date.split('-');
            $(this).siblings('select').each(function(){
                id = $(this).attr('id');
                if (id.substring(id.length-3, id.length)=='Day') $(this).val(newDate[2]);
                else if (id.substring(id.length-5, id.length)=='Month') $(this).val(newDate[1]);
                else if (id.substring(id.length-4, id.length)=='Year') $(this).val(newDate[0]);
            });
        },
        beforeShow: function() {
            var year = '';
            var month = '';
            var day = '';
            var id = '';
            $(this).siblings('select').each(function() {
                id = $(this).attr('id');
                if (id.substring(id.length-3, id.length)=='Day') day = $(this).val();
                else if (id.substring(id.length-5, id.length)=='Month') month = $(this).val();
                else if (id.substring(id.length-4, id.length)=='Year') year = $(this).val();
            });
            $(this).val(year+'-'+month+'-'+day);
            return {};
        }
    });
};

