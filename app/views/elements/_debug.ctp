
<div class="clear"><?php
echo '<pre>Page générée en ' . round(getMicrotime() - $startTime, 4) . ' sec.</pre>';
/**
 * Debug
 */
#Debug($this->Session->params);
#Debug($this->Session->read());
#Debug($this->Session->read('Menu'));
#Debug($this->Session->read('SideMenu'));
/**
 * Données de session
 */
if ($this->Session->check('Auth.User'))
    Debug($this->Session->read('Auth.User'));
else {
    if ($this->Session->check('epelAccountCreation'))
        Debug($this->Session->read('epelAccountCreation'));
}
/**
 * Données de vue
 */
if (isset($this->data))
    Debug($this->data);
else {
    if (isset($user))
        Debug($user);
    else
        Debug($this->viewVars);
}
?></div>
<?php
/**
 * Requêtes BDD
 */
echo $this->element('sql_dump');
?>

