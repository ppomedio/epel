<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#search-page',
    'before' => $this->Js->get('#search-page')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="search-page" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="student-name"><?php __("Identity"); ?></th>
                                        <th class="student-studies"><?php
                                            __("Validated studies");
                                            echo ' / ';
                                            __("Specialty"); ?></th>
                                        <th class="student-group"><?php __("Group"); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="5"><?php
                                        if ($this->Session->check('Auth.User.search'))
                                            __("No result matches criteria.");
                                        else
                                            __("No student account is created for your university.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="2" class="student-name"><?php echo $paginator->sort(__("Identity", true), 'Student.name'); ?></th>
                                        <th class="student-studies"><?php echo $paginator->sort(__("Validated studies", true) . ' / ' . __("Specialty", true), 'Student.diploma_id'); ?></th>
                                        <th class="student-group"><?php echo $paginator->sort(__("Group", true), 'Student.group'); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user as $value) {
                                        if ($trClass == 'even') $trClass = 'odd';
                                        else $trClass = 'even';
                                        $isMyGroup = false;
                                        $isVisible = false;
                                        if ($value['Group']['Teacher']['id'] == $this->Session->read('Auth.User.id')) $isMyGroup = true;
                                        if ($isMyGroup && !empty($value['Portfolio']) && empty($value['Student']['tea_hide'])) $isVisible = true;
                                        ?>

                                    <tr id="<?php echo $value['Student']['id']; ?>" class="<?php echo $trClass; ?>">
                                        <td><?php
                                        if ($isVisible) { ?>
                                        <a href="/<?php echo $this->params['language']; ?>/view/student/init/<?php echo base64_encode($value['Student']['id']); ?>" id="photo-link_<?php echo $value['Student']['id']; ?>" class="bolder link page-link"><?php
                                        }
                                        if (!empty($value['User']['photo'])) {
                                            $pathInfo = pathinfo($value['User']['photo']);
                                            ?><img src="/files/users/<?php echo $value['Student']['id'] . '/photo/th_' . $value['Student']['id'] . '.' . $pathInfo['extension']; ?>" />
                                        <?php } else {
                                            ?><img src="/img/user_thumb.png" />
                                        <?php
                                        }
                                        if ($isVisible) { ?></a><?php } ?></td>
                                        <td id="edit-name_<?php echo $value['Student']['id']; ?>"><?php
                                        echo $this->Html->link($value['Student']['name'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['Student']['id'] . '_'
                                                        . $value['Student']['firstname'] . ' '
                                                        . $value['Student']['name'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['Student']['id'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['Student']['firstname'];
                                        ?></td>
                                        <td class="smaller"><?php
                                        if (!empty($value['Student']['diploma_info']))
                                            $diploma = $value['Student']['diploma_info'];
                                        else
                                            $diploma = __($value['Diploma']['name'], true);
                                        $diploma .= ' (' . __($value['Diploma']['level'], true) . ')';
                                        echo $diploma;
                                        if (!empty($value['Student']['specialty']))
                                            echo '<br /><em>' . $value['Student']['specialty'] . '</em>';
                                        ?></td>
                                        <td id="edit-group_<?php echo $value['Student']['id']; ?>"><?php
                                        if ($isMyGroup) {
                                            if (!empty($value['Student']['tea_hide'])) {
                                                $preGroup = '<span class="alerttext bolder">';
                                            } else if (!empty($value['Portfolio'])) {
                                                $preGroup = '<span class="infotext bolder">';
                                            } else {
                                                $preGroup = '<span class="bolder">';
                                            }
                                            echo $preGroup . $value['Student']['group'] . '</span>';
                                        } else {
                                            if (!empty($value['Student']['group'])) {
                                                echo $value['Student']['group'] . '<br />';
                                                if (!empty($value['Group']['TeGroup']['teacher_id'])) {
                                                    echo $this->Html->link($value['Group']['Teacher']['firstname'] . ' ' . $value['Group']['Teacher']['name'],
                                                            array(
                                                                'controller' => 'contact',
                                                                'action' => 'form',
                                                                base64_encode($value['Group']['Teacher']['id'] . '_'
                                                                    . $value['Group']['Teacher']['firstname'] . ' '
                                                                    . $value['Group']['Teacher']['name'])
                                                            ),
                                                            array(
                                                                'id' => 'contact-teacher_' . $value['Group']['Teacher']['id'],
                                                                'class' => 'iframe contact italic link'
                                                            )
                                                    );
                                                } else
                                                    echo '<span class="smaller italic alerttext">' . __("No ref. teacher", true) . '</span>';
                                            } else
                                                echo '<span class="alerttext bolder">' . __("Undefined group", true) . '</span>';
                                        } ?></td>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['Student']['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['Student']['id']; ?>" class="controls"><?php
                                            if ($isVisible) { ?>

                                                <a href="/<?php echo $this->params['language']; ?>/view/student/init/<?php echo base64_encode($value['Student']['id']); ?>" id="see-link_<?php echo $value['Student']['id']; ?>" class="see link"></a>
                                            <?php } ?>

                                                <a href="#user-edit" id="edit-link_<?php echo $value['Student']['id']; ?>" class="edit link"></a>
                                            </div>
                                        </td>
                                        <div id="hidden-info_<?php echo $value['Student']['id']; ?>" class="hidden">
                                            <span class="id hidden"><?php
                                            echo $value['Student']['id'];
                                            ?></span>
                                            <span class="group-id hidden"><?php
                                            $groupId = array_search($value['Student']['group'], $group);
                                            if ($groupId) {
                                                echo $groupId;
                                            } ?></span>
                                            <span class="group-name hidden"><?php
                                            echo $value['Student']['group'];
                                            ?></span>
                                            <span class="name hidden"><?php
                                            echo $value['Student']['name'];
                                            ?></span>
                                            <span class="firstname hidden"><?php
                                            echo $value['Student']['firstname'];
                                            ?></span>
                                            <span class="birth hidden"><?php
                                            echo $this->Date->Convert($value['Student']['birth']);
                                            ?></span>
                                            <span class="country hidden"><?php
                                            echo __d('country', $value['OriginCountry']['name'], true);
                                            ?></span>
                                            <span class="diploma hidden"><?php
                                            echo $diploma;
                                            ?></span>
                                            <span class="specialty hidden"><?php
                                            echo $value['Student']['specialty'];
                                            ?></span>
                                            <span class="email hidden"><?php
                                            echo $value['User']['email'];
                                            ?></span>
                                            <span class="languages hidden"><?php
                                            if (!empty($value['Portfolio'])) {
                                                $languages = '';
                                                foreach ($value['Portfolio'] as $portfolio) {
                                                    $languages .= __d('language', $portfolio['name'], true) . "\n";
                                                }
                                                echo substr($languages, 0, -1);
                                            } ?></span>
                                            <span class="photo hidden"><?php
                                            echo $value['User']['photo'];
                                            ?></span>
                                        </div>
                                        <div id="data_contact-user_<?php echo $value['Student']['id']; ?>" class="tooltip-data"><?php
                                            __("Send an email to");
                                            echo ' ' . $value['Student']['firstname'] . ' ' . $value['Student']['name'];
                                        ?></div>
                                        <div id="data_contact-teacher_<?php echo $value['Group']['TeGroup']['teacher_id']; ?>" class="tooltip-data"><?php
                                            __("Send an email to");
                                            echo ' ' . $value['Group']['Teacher']['firstname'] . ' ' . $value['Group']['Teacher']['name'];
                                        ?></div>
                                        <div id="data_edit-link_<?php echo $value['Student']['id']; ?>" class="inline tooltip-data"><?php
                                            __("Change the group of"); echo ' ' . $value['Student']['firstname'] . ' ' . $value['Student']['name'];
                                        ?></div>
                                        <div id="data_photo-link_<?php echo $value['Student']['id']; ?>" class="inline tooltip-data"><?php
                                            __("See the Portfolio of"); echo ' ' . $value['Student']['firstname'] . ' ' . $value['Student']['name'];
                                        ?></div>
                                        <div id="data_see-link_<?php echo $value['Student']['id']; ?>" class="inline tooltip-data"><?php
                                            __("See the Portfolio of"); echo ' ' . $value['Student']['firstname'] . ' ' . $value['Student']['name'];
                                        ?></div>
                                    </tr><?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'search-page'));
                            } ?>

                            <div id="data_search-new" class="tooltip-data"><?php __("Filter criteria"); ?></div>
                            <div id="data_search-init" class="tooltip-data"><?php __("Reset filter criteria"); ?></div>
                            <div id="data_sort-student-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-student-firstname" class="tooltip-data"><?php __("Sort by firstname"); ?></div>
                            <div id="data_sort-student-studies" class="tooltip-data"><?php __("Sort by diplomas"); ?></div>
                            <div id="data_sort-student-group" class="tooltip-data"><?php __("Sort by group code"); ?></div>
                            <div class="hidden">
                                <div id="user-edit" class="fancy">
                                    <h2><?php __("Profile of the student"); ?></h2>
                                    <?php echo $this->Form->create('TeGroup', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/teacher/student/change_group',
                                        'id' => 'edit-form',
                                        'class' => ''
                                        ));
                                    ?>

                                    <div id="user-photo"></div>
                                    <fieldset class="half"><?php
                                    echo $this->Form->hidden('id');
                                    echo $this->Form->input('edit_name', array(
                                        'label' => __("Name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_firstname', array(
                                        'label' => __("First name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_birth', array(
                                        'label' => __("Date of birth", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_country', array(
                                        'label' => __("Country of origin", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    </fieldset>
                                    <fieldset class="clear"><?php
                                    echo $this->Form->input('edit_diploma', array(
                                        'label' => __("Validated studies", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_specialty', array(
                                        'label' => __("Specialty", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_email', array(
                                        'label' => __("eMail address", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_portfolio',array(
                                        'type'=>'textarea',
                                        'label' => __("Portfolio languages", true),
                                        'readonly'=>'readonly',
                                        'escape '=> false
                                        ));
                                    echo $this->Form->input('edit_group', array(
                                        'label' => __("Actual group", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->hidden('save_group');
                                    echo $this->Form->input('group', array(
                                        'label' => __("New group", true),
                                        'options' => $group
                                        ));
                                    ?>

                                    <div id="message">
                                        <p id="info-message"></p>
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Save", true)); ?>
                                </div>
                                <div id="user-search" class="fancy">
                                    <h2><?php __("Filter criteria"); ?></h2>
                                    <?php echo $this->Form->create('Student', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/teacher/student/search',
                                        'id' => 'search-form',
                                        'class' => ''
                                        ));
                                    echo $validation->bind('Student'); // Validation jQuery
                                    if ($this->Session->check('Auth.User.search.Student')) {
                                        $default = $this->Session->read('Auth.User.search.Student');
                                        $groupId = array_search($default['group'], $group);
                                        if ($groupId)
                                            $default['group_id'] = $groupId;
                                        else
                                            $default['group_id'] = '00000';
                                    } else
                                        $default = array(
                                            'name' => '',
                                            'firstname' => '',
                                            'origin_country_id' => '',
                                            'language_id' => '',
                                            'mother' => false,
                                            'usual' => false,
                                            'files' => false,
                                            'listening' => '',
                                            'reading' => '',
                                            'speak_int' => '',
                                            'speak_prod' => '',
                                            'writing' => '',
                                            'group_id' => '00000',
                                            'group' => ''
                                        );
                                    ?>

                                    <fieldset><?php
                                    echo $this->Form->input('name', array(
                                        'label' => __("Name", true),
                                        'default' => $default['name']
                                        ));
                                    echo $this->Form->input('firstname', array(
                                        'label' => __("First name", true),
                                        'default' => $default['firstname']
                                        ));
                                    echo $this->Form->input('origin_country_id', array(
                                        'label' => __("Country of origin", true),
                                        'options' => $country,
                                        'empty' => "…",
                                        'default' => $default['origin_country_id']
                                        ));
                                    echo $this->Form->input('language_id', array(
                                        'label' => __("Language", true),
                                        'options' => $language,
                                        'empty' => "…",
                                        'default' => $default['language_id']
                                        ));
                                    ?>

                                    </fieldset>
                                    <fieldset id="competency">
                                        <div class="checkboxes"><?php
                                        echo $this->Form->input('mother', array(
                                            'type' => 'checkbox',
                                            'label' => __d('portfolio', "Mother tongue", true),
                                            'checked' => $default['mother']
                                        ));
                                        echo $this->Form->input('usual', array(
                                            'type' => 'checkbox',
                                            'label' => __d('portfolio', "Usual language", true),
                                            'checked' => $default['usual']
                                        ));
                                        ?></div>
                                        <div class="checkboxes"><?php
                                        echo $this->Form->input('files', array(
                                            'type' => 'checkbox',
                                            'label' => __d('portfolio', "Dossier contains files", true),
                                            'checked' => $default['files']
                                        ));
                                        ?></div>

                                        <?php
                                        $picto = array(
                                            'listening' => 'pictoPortfolioListening',
                                            'reading' => 'pictoPortfolioReading',
                                            'speak_int' => 'pictoPortfolioSpeakInt',
                                            'speak_prod' => 'pictoPortfolioSpeakProd',
                                            'writing' => 'pictoPortfolioWriting'
                                        );
                                        $bubble = array(
                                            'listening' => __d('levels_grid', "Understanding: listening", true),
                                            'reading' => __d('levels_grid', "Understanding: reading", true),
                                            'speak_int' => __d('levels_grid', "Speaking: spoken interaction", true),
                                            'speak_prod' => __d('levels_grid', "Speaking: spoken production", true),
                                            'writing' => __d('levels_grid', "Writing", true)
                                        );
                                        foreach ($competency as $key => $value) {
                                            echo '<span id="' . $bubble[$key] . '" class="' . $picto[$key] . '"></span>';
                                            echo $this->Form->input($key, array(
                                                'label' => false,
                                                'options' => $level,
                                                'empty' => array('0' => "…"),
                                                'default' => $default[$key]
                                                ));
                                        } ?>

                                    </fieldset>
                                    <fieldset><?php
                                    echo $this->Form->input('group_id', array(
                                        'label' => __("Group code", true),
                                        'options' => $group,
                                        'empty' => array('00000' => "…"),
                                        'default' => $default['group_id']
                                        ));
                                    echo $this->Form->input('group', array(
                                        #'label' => __("Group code", true),
                                        'label' => false,
                                        'default' => $default['group']
                                        )); ?>

                                    <div id="message">
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Filter", true)); ?>
                                </div>
                            </div>
                        </div>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form',
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

// État initial du picto d'initialisation
$initState = '';
if ($this->Session->check('Auth.User.search.Student')) {
    $initState = "    $('#search-init').css('display', 'inline-block');\n";
}

$this->Js->Buffer("
    // Id, classes et cie
    $('#search-page table thead tr th.student-name a').attr('id', 'sort-student-name').addClass('sort-help');
    $('#search-page table thead tr th.student-studies a').attr('id', 'sort-student-studies').addClass('sort-help');
    $('#search-page table thead tr th.student-group a').attr('id', 'sort-student-group').addClass('sort-help');
    // Init. pictos
" . $initState . "
    // Tooltips
    $('#competency span').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        cursor : 'help'
    });
    $('#divToolTip').css('z-index', 2000);
    // Tips
    addTipInfo('#edit-form');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#search-form div.submit input[type=submit]');
    $('<div class=\"loading\"></div>').insertAfter('#edit-form div.submit input[type=submit]');
    // Raz critères de recherche
    $('#search-page table thead tr th.controls span#search-init').unbind('click').bind('click', function() {
        $('#divToolTip').fadeOut('fast');
        $('#title-search-page').html('" . addslashes(__("Loading…", true)) . "');
        $('#search-page').fadeTo('fast', 0.6);
        $('#search-page').load('/" . $this->Session->params['language'] . "/teacher/student/init_criteria');
    });
    // État initial de #competency
    if ($('#StudentLanguageId').val() != '')
        $('#competency').show();
    // Bascule de #competency
    $('#StudentLanguageId').unbind('change').bind('change', function() {
        if ($(this).val() == '') {
            $('#competency').fadeOut('fast');
            $('#competency .checkboxes input[type=checkbox]').removeAttr('checked');
            $('#competency select :selected').removeAttr('selected');
        } else
            $('#competency').fadeIn('slow');
    });
    // Formulaire de recherche
    $('#search-new').fancybox({
        'width'             :   450,
        'height'            :   450,
        'autoDimensions'    :   false,
        'padding'           :   0,
        'margin'            :   0,
        'speedIn'           :   900,
        'scrolling'         :   'no',
        'centerOnScroll'    :   true,
        'overlayColor'      :   '#000',
        'overlayOpacity'    :   0.3,
        'onStart'           :   function() {
                                    $('div#divToolTip').fadeOut();
                                    if ($('#StudentGroupId option:selected').val() != '00000') {
                                        $('#StudentGroup').parent('div').hide();
                                        $('#StudentGroup').val($('#StudentGroupId option:selected').text());
                                    }
                                },
        'onComplete'        :   function() {
                                    // Soumission Ajax
                                    $('#search-form').ajaxForm({
                                        target:         '#search-page',
                                        beforeSubmit:   function() {
                                            if (!$('#search-form .error').length) {
                                                $('#title-search-page').html('" . addslashes(__("Loading…", true)) . "');
                                                $('#search-page').fadeTo('fast', 0.6);
                                            } else
                                                return false;
                                        },
                                        success:    function() {
                                            $.fancybox.close();
                                        }
                                    });
                                }
    });
    // Sélection d'un groupe
    $('#StudentGroupId').change(function() {
        if ($('#StudentGroupId option:selected').val() == '00000') {
            $('#StudentGroup').val('');
            $('#StudentGroup').parent('div').show();
        } else {
            $('#StudentGroup').parent('div').hide('fast');
            $('#StudentGroup').next('div.error-message').remove();
            $('#StudentGroup').val($('#StudentGroupId option:selected').text());
        }
    });
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Retour du code groupe
    $('#search-form').submit(function() {
        if ($('#StudentGroupId option:selected').val() != '00000')
            $('#StudentGroup').val($('#StudentGroupId option:selected').text());
    });
    // Formulaire d'édition
    $('a.edit').each(function(){
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var userId = extract[1];
        $(this).fancybox({
            'width'             :   450,
            'height'            :   590,
            'autoDimensions'    :   false,
            'padding'           :   0,
            'margin'            :   0,
            'speedIn'           :   900,
            'scrolling'         :   'no',
            'centerOnScroll'    :   true,
            'overlayColor'      :   '#000',
            'overlayOpacity'    :   0.3,
            'onStart'           :   function() {
                                        $('div#divToolTip').fadeOut();
                                        // Photo
                                        var userPhoto = $('#hidden-info_' + userId + ' .photo').text();
                                        if (userPhoto) {
                                            var photoUrl = 'url(/files/users/' + userId + '/photo/' + userPhoto + ')';
                                            $('#edit-form #user-photo').css('background-image', photoUrl);
                                        }
                                        // Messages
                                        $('#edit-form p#info-message').attr('class', 'info')
                                                .html('<span class=\"tip-info\"></span>" . addslashes(__("You can only change the group to which this student belongs.", true)) . "');
                                        // Chargement ID
                                        $('#edit-form #TeGroupId').val($('#hidden-info_' + userId + ' .id').text());
                                        // Initialisation des éléments
                                        $('#edit-form #TeGroupEditName').val($('#hidden-info_' + userId + ' .name').text());
                                        $('#edit-form #TeGroupEditFirstname').val($('#hidden-info_' + userId + ' .firstname').text());
                                        $('#edit-form #TeGroupEditBirth').val($('#hidden-info_' + userId + ' .birth').text());
                                        $('#edit-form #TeGroupEditCountry').val($('#hidden-info_' + userId + ' .country').text());
                                        $('#edit-form #TeGroupEditDiploma').val($('#hidden-info_' + userId + ' .diploma').text());
                                        $('#edit-form #TeGroupEditSpecialty').val($('#hidden-info_' + userId + ' .specialty').text());
                                        $('#edit-form #TeGroupEditEmail').val($('#hidden-info_' + userId + ' .email').text());
                                        $('#edit-form #TeGroupEditPortfolio').val($('#hidden-info_' + userId + ' .languages').text());
                                        if ($('#edit-form #TeGroupEditPortfolio').val() != '') {
                                            $('#language-message').show();
                                        } else {
                                            $('#language-message').hide();
                                            $('#edit-form #TeGroupEditPortfolio').val('" . addslashes(__("This student's Portfolio is empty.", true)) . "');
                                        }
                                        // Init. du groupe
                                        $('#edit-form #TeGroupEditGroup').val($('#hidden-info_' + userId + ' .group-name').text());
                                        if ($('#hidden-info_' + userId + ' .group-id').text() != '') {
                                            $('#edit-form #TeGroupGroup option[value=\'' + $('#hidden-info_' + userId + ' .group-id').text() + '\']').attr('selected', 'selected');
                                            $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
                                        } else {
                                            $('#edit-form #TeGroupGroup :selected').removeAttr('selected');
                                            $('#edit-form div.submit input[type=submit]').removeAttr('disabled');
                                        }
                                    },
            'onClosed'          :   function() {
                                        // Raz des éléments du formulaire
                                        $('#edit-form #user-photo').css('background-image', 'url(/img/user.png)');
                                        $('#edit-form input[type=text]').val('');
                                        $('#edit-form #TeGroupEditPortfolio').val('');
                                        $('#edit-form #TeGroupGroup :selected').removeAttr('selected');
                                    }
        });
    });
    // Bascule submit
    $('#edit-form #TeGroupGroup').change(function() {
        if ($('#TeGroupGroup :selected').val() != $('#hidden-info_' + $('#edit-form #TeGroupId').val() + ' .group-id').text())
            $('#edit-form div.submit input[type=submit]').removeAttr('disabled');
        else
            $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
    });
    // Changement de groupe
    $('#edit-form').submit(function() {
        if ($('#TeGroupGroup option:selected').val()) {
            $('#TeGroupSaveGroup').val($('#edit-form #TeGroupGroup option:selected').text());
            $(this).ajaxSubmit({
                beforeSubmit: function() {
                    $('#edit-form div.loading').show();
                    $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
                },
                success: function() {
                    $('#edit-form #TeGroupEditGroup').val($('#TeGroupSaveGroup').val());
                    $('#edit-form div.loading').hide();
                    $.fancybox.close();
                    // Rechargement
                    $('#search-page').fadeTo('fast', 0.6);
                    $('#search-page').load('/" . $this->params['language'] . "/teacher/student/search/', function() {
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__("This student's group has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                    });
                }
            });
            return false;
        }
    });
    // Tooltips
    tooltipSortHelp('#search-page table thead tr th');
    tooltipLink('#search-page table');
    tooltipSupp('#search-page table thead');
    // Tips
    addTipInfo('#message');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#search-page').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-search-page').html('" . addslashes(__("Students", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>