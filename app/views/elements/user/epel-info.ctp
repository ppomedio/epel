
                    <div>
                        <h2><?php __d('information', "Browsers"); ?></h2>
                        <div>
                            <p class="info"><?php __d('information', "This application requires Javascript and cookies to be activated in your browser's preferences."); ?></p>
                            <p class="info"><?php __d('information', "Try to use the most recent version of your favourite browser."); ?></p>
                        </div>
                    </div>
                    <div>
                        <h2 class="alert"><?php __d('information', "Access"); ?></h2>
                        <div class="alert">
                            <p class="alert"><?php __d('information', "Access to portfolios is reserved to university lecturers, language researchers and recruiters. It may always be limited or forbidden by the students."); ?></p>
                            <p class="alert"><?php echo __d('information', "Any information contained in the portfolios shall remain confidential", true) . ' ' . __d('information', "and shall be used solely by authorised visitors and members of the project.", true); ?></p>
                        </div>
                    </div>
                    <div id="logos">
                        <a href="<?php echo Configure::read('epel.coe.url'); ?>" id="<?php __("Visit the website of the Council of Europe"); ?>" class="link">
                            <div class="logo" id="coelogo"></div>
                        </a>
                        <a href="<?php echo Configure::read('epel.europass.url'); ?>" id="<?php __("Visit the Europass website"); ?>" class="link">
                            <div class="logo" id="eplogo"></div>
                        </a>
                        <a href="<?php echo Configure::read('epel.cercles.url'); ?>" id="<?php __("Visit CercleS's website"); ?>" class="link">
                            <div class="logo" id="cercleslogo"></div>
                        </a>
                    </div>