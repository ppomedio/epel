
                <article>
                    <section class="half left">
                        <h2><?php __d('information', "Introduction"); ?></h2>
                        <div>
                            <p><?php __d('information', "This European Language Portfolio is a virtual space for students to keep a record of their ongoing language skills and cultural experience, and thus help them reflect on their language learning."); ?></p>
                            <p><strong><?php __d('information', "It is the property of his/her holder."); ?></strong></p>
                        </div>
                    </section>
                    <section class="half right">
                        <h2><?php __d('information', "Aims"); ?></h2>
                        <ul>
                            <li><strong><?php __d('information', "Motivate learners"); ?></strong> <?php __d('information', "by supporting their efforts to extend and diversify their language skills for different competencies at different levels."); ?></li>
                            <li><strong><?php __d('information', "Provide a statement of their language and cultural skills"); ?></strong> <?php __d('information', "they have achieved during their various experiments."); ?></li>
                        </ul>
                    </section>
                    <section>
                        <h2><?php __d('information', "Contents to which you have access"); ?></h2>
                        <ul>
                            <li><strong><?php __d('information', "A language passport"); ?></strong> <?php __d('information', "based on the"); ?> <a id="<?php __("Visit the Europass website"); ?>" class="link" href="<?php echo Configure::read('epel.europass.url'); ?>">Europass</a> <?php echo __d('information', "Language Passport (&copy; European Union and Council of Europe).", true); ?></li>
                            <li><strong><?php __d('information', "A detailed language biography"); ?></strong> <?php __d('information', "including the learner's experience in each language, to help improve planning and assessment and facilitate self-assessment or assessment by a teacher."); ?></li>
                            <li><strong><?php __d('information', "A dossier"); ?></strong> <?php __d('information', "containing personal work attesting the performance in each of the foreign languages mentioned as well as diplomas, certificates, CVs and learning material (recordings, texts, vocabulary, exercises, and homework of all kinds)."); ?></li>
                        </ul>
                    </section>
                    <section id="visitor-info">
                        <h2 class="alert"><?php __d('information', "Any information contained in the portfolios shall remain confidential"); ?></h2><?php
                        if ($this->Session->read('Auth.User.role') == 'visitor') { ?>
                        <div>
                            <p><?php __d('information', "Access to portfolios is reserved to university lecturers, language researchers and recruiters. It may always be limited or forbidden by the students."); ?></p>
                            <p class="info"><?php __("Students can modify at any time the access rights they grant to you. As the contents of their Portfolio can evolve, material you previously saw may become unavailable."); ?></p>
                        </div><?php
                        } ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('view/student');
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#visitor-info');
");

?>