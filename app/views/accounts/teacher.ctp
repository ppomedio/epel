
        <h2><?php echo __("Account creation", true) . ' &bull; ' . __("Teacher", true); ?></h2>
        <?php
        echo $this->Form->create('Teacher', array(
            'url' => '/' . Configure::read('Config.langCode')
                . '/account/teacher',
            'id' => 'profile-form',
            #'class' => ''
            ));
        echo $validation->bind('Teacher'); // Validation jQuery
        ?>

        <fieldset>
        <?php
        echo $this->Form->input('code', array(
            'label' => __("University code", true)
            ));
        echo $this->Form->input('name', array(
            'label' => __("Name", true)
            ));
        echo $this->Form->input('firstname', array(
            'label' => __("First name", true)
            ));
        echo $this->Form->input('quality', array(
            'label' => __("Quality", true)
            ));
        echo $this->Form->input('specialty', array(
            'label' => __("Specialty", true)
            ));
        ?>

        </fieldset>
        <?php echo $this->Form->end(__("Save", true)); ?>

<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Redimensionnement
    parent.$('#fancybox-inner').height(320);
    parent.$('#fancybox-wrap').height(340);
    parent.$('#fancybox-content').height(340);
    parent.$.fancybox.center();
");

?>
