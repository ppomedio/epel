<?php

/**
 * {app}/controllers/view_passports_controller.php
 *
 */
class ViewPassportsController extends AppController {

    var $name = 'ViewPassports';
    var $uses = array('Portfolio');
    // Composants
    var $components = array(
        'PortfolioLists',
        'L10nSort'
    );
    // Assistants
    var $helpers = array(
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Pas de variable de session = déconnexion
        if(!$this->Session->check('Auth.User.view'))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // Eject
        $this->redirect(array(
            'language' => $this->params['language'],
            'controller' => 'users',
            'action' => 'logout'
        ));

    }


    function show() {

        // Chargement de la langue / du portfolio en cours
        $language = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
        $portfolios = $this->Session->read('Auth.User.view.portfolios');

        // Validité de l'url
        if (!array_key_exists($language, $portfolios))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        /**
         * Chargement des données de la vue
         */

        $this->set('levelDocs', $this->Portfolio->countLevelDocuments($portfolios[$language]['id']));
        $this->set('user', $this->Portfolio->getPassport($portfolios[$language]['id'], true));

        // Droits des visiteurs
        if ($this->Session->read('Auth.User.role') == 'visitor') {
            if (!$portfolios[$language]['vis_biography'] || !$this->Session->read('Auth.User.view.vis_biography'))
                $this->set('noBiography', true);
            if (!$portfolios[$language]['vis_dossier'] || !$this->Session->read('Auth.User.view.vis_dossier'))
                $this->set('noDossier', true);
        }

        // Données de vue
        $this->set('schoolStudyPeriods', $this->PortfolioLists->getSchoolStudyPeriodsList());
        $this->set('otherStudyPeriods', $this->PortfolioLists->getOtherStudyPeriodsList());
        $this->set('activityPeriods', $this->PortfolioLists->getActivityPeriodsList());
        $this->set('competencies', $this->PortfolioLists->getCompetenciesList());
        $this->set('levels', $this->PortfolioLists->getLevelsList());
        $this->set('levelsDescriptions', $this->PortfolioLists->getLevelsDescriptions());
        $this->set('title_for_layout', __("Language Passport", true) . " &bull; "
                . __d('language', $portfolios[$language]['name'], true));

        // Données de vue : affichage des langues du Portfolio
        $portfolios = $this->Session->read('Auth.User.view.portfolios');
        $languages = array();
        foreach ($portfolios as $key => $value) {
            $languages[$value['name']]['mother'] = $value['mother'];
            $languages[$value['name']]['usual'] = $value['usual'];
        }
        $this->set('languages', $this->L10nSort->sortLocalized($languages));

    }


}

