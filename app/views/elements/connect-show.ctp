
                    <section>
                        <h2><?php __("Connection / Email"); ?></h2>
                        <div>
                            <dl>
                                <dt><?php __("Username"); ?></dt>
                                <dd><?php echo $user['User']['username']; ?></dd>
                                <dt><?php __("eMail address"); ?></dt>
                                <dd><?php echo $user['User']['email']; ?></dd>
                            </dl>
                            <div class="tail">
                                <div class="loading"></div>
                                <div class="controls">
                                    <?php
                                    echo $this->Html->link('',
                                        '/' . Configure::read('Config.langCode')
                                            . '/' . $this->Session->read('Auth.User.role')
                                            . '/base/connect',
                                        array(
                                            'id' => 'connect',
                                            'class' => 'edit'
                                        )
                                    );
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div id="data_connect" class="tooltip-data"><?php __('Edit identification data and email'); ?></div>
                    </section>