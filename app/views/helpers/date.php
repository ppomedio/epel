<?php
/**
 * {app}/views/helpers/date.php
 *
 * Utilise {app}/config/config.php et {app}/locale/…/default.po
 * 
 */

class DateHelper extends AppHelper {

    /**
     * "Littéralisation" et l10n d'un champ MySQL DATE
     *
     * @param <date> $d
     * @return <string>
     */
    function convert($d) {

        // Locales disponibles
        $available = Configure::read('epel.locales.available');
        // Locale utilisée
        $locale = substr(Configure::read('Config.language'), 0, 2);
        // Formatage
        $d = date($available[$locale]['dateText'], strtotime($d));
        if ($locale == 'en') return $d;
        // Traduction
        $d = explode(' ', $d);
        $d[0] = __($d[0], true);
        $d[2] = __($d[2], true);
        return implode(' ', $d);

    }


}

