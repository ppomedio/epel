
                <article class="half left"><?php
                    echo $this->element('admin/base/users');
                    echo $this->element('connect-edit');
                    ?>

                </article>
                <article class="half">
                    <section>
                        <h2 id="title-visit-page"><?php __("Visits"); ?></h2>
                        <?php echo $this->element('admin/base/visits-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('admin/base/how-to-use');
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tooltips
    tooltipEdit();
");

?>