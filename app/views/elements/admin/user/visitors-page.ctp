<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#adm-visitors',
    'before' => $this->Js->get('#adm-visitors')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="adm-visitors" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="visitor-id"><?php "ID"; ?></th>
                                        <th class="visitor-name"><?php __("Name"); ?></th>
                                        <th class="visitor-function"><?php __("Function"); ?></th>
                                        <th class="visitor-structure"><?php __("Structure"); ?></th>
                                        <th class="user-active"><?php __("Activated"); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="6"><?php
                                        if ($this->Session->check('Auth.User.search'))
                                            __("No result matches criteria.");
                                        else
                                            __("No visitor account has currently been created.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="visitor-id"><?php echo $paginator->sort("ID", 'Visitor.id'); ?></th>
                                        <th class="visitor-name"><?php echo $paginator->sort(__("Name", true), 'Visitor.name'); ?></th>
                                        <th class="visitor-function"><?php echo $paginator->sort(__("Function", true), 'Visitor.function'); ?></th>
                                        <th class="visitor-structure"><?php echo $paginator->sort(__("Structure", true), 'Visitor.structure'); ?></th>
                                        <th class="user-active"><?php echo $paginator->sort(__("Activated", true), 'User.active'); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>
                                    <tr id="<?php echo $value['Visitor']['id']; ?>" class="<?php echo $trClass; ?>">
                                        <td id="edit-id_<?php echo $value['Visitor']['id']; ?>" class="smaller"><?php
                                        echo $value['Visitor']['id'];
                                        ?></td>
                                        <td id="edit-name_<?php echo $value['Visitor']['id']; ?>"><?php
                                        echo $this->Html->link($value['Visitor']['name'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['Visitor']['id'] . '_'
                                                        . $value['Visitor']['firstname'] . ' '
                                                        . $value['Visitor']['name'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['Visitor']['id'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['Visitor']['firstname'];
                                        ?></td>
                                        <td id="edit-function_<?php echo $value['Visitor']['id']; ?>" class="smaller"><?php
                                        echo $value['Visitor']['function'];
                                        ?></td>
                                        <td class="smaller bolder"><?php
                                        if ($value['Visitor']['url']) { ?>
                                        <a href="<?php echo $value['Visitor']['url']; ?>" id="<?php echo __("Visit", true) . ' ' . $value['Visitor']['url']; ?>" class="link"><?php echo $value['Visitor']['structure']; ?></a>
                                        <?php
                                        } else
                                            echo $value['Visitor']['structure'];
                                        ?></td>
                                        <td class="user-active"><?php
                                        echo $this->Form->create('User', array(
                                            'url' => '/' . Configure::read('Config.langCode')
                                                . '/admin/user/toggle_active/visitor/' . $value['Visitor']['id'],
                                            'id' => 'form_' . $value['Visitor']['id'],
                                            'class' => 'user-active'
                                            ));
                                        echo $this->Form->checkbox('active', array(
                                            'id' => 'active_' . $value['Visitor']['id'],
                                            'checked' => $value['User']['active']
                                        ));
                                        echo $this->Form->end();
                                        ?></td>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['Visitor']['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['Visitor']['id']; ?>" class="controls">
                                                <a href="#user-edit" id="edit-link_<?php echo $value['Visitor']['id']; ?>" class="edit"></a>
                                            </div>
                                        </td>
                                        <div id="hidden-info_<?php echo $value['Visitor']['id']; ?>" class="hidden">
                                            <span class="name hidden"><?php
                                            echo $value['Visitor']['name'];
                                            ?></span>
                                            <span class="firstname hidden"><?php
                                            echo $value['Visitor']['firstname'];
                                            ?></span>
                                            <span class="structure hidden"><?php
                                            echo $value['Visitor']['structure'];
                                            ?></span>
                                            <span class="phone hidden"><?php
                                            echo $value['Visitor']['phone'];
                                            ?></span>
                                            <span class="url hidden"><?php
                                            echo $value['Visitor']['url'];
                                            ?></span>
                                            <span class="address hidden"><?php
                                            echo $value['Visitor']['address'];
                                            ?></span>
                                            <span class="country hidden"><?php
                                            echo __d('country', $value['Country']['name'], true);
                                            ?></span>
                                            <span class="email hidden"><?php
                                            echo $value['User']['email'];
                                            ?></span>
                                            <span class="photo hidden"><?php
                                            echo $value['User']['photo'];
                                            ?></span>
                                        </div>
                                        <div id="data_contact-user_<?php echo $value['Visitor']['id']; ?>" class="tooltip-data"><?php
                                            __("Send an email to");
                                            echo ' ' . $value['Visitor']['firstname'] . ' ' . $value['Visitor']['name'];
                                        ?></div>
                                        <div id="data_edit-link_<?php echo $value['Visitor']['id']; ?>" class="inline tooltip-data"><?php
                                            __("See profile of user"); echo ' n°' . $value['Visitor']['id'];
                                        ?></div>
                                    </tr><?php } ?>
                                </tbody>
                            </table><?php
                            echo $this->element('paginate', array('subId' => 'adm-visitors'));
                            } ?>

                            <div id="data_search-new" class="tooltip-data"><?php __("Filter criteria"); ?></div>
                            <div id="data_search-init" class="tooltip-data"><?php __("Reset filter criteria"); ?></div>
                            <div id="data_sort-visitor-id" class="tooltip-data"><?php __("Sort by ID"); ?></div>
                            <div id="data_sort-visitor-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-visitor-firstname" class="tooltip-data"><?php __("Sort by firstname"); ?></div>
                            <div id="data_sort-visitor-function" class="tooltip-data"><?php __("Sort by function"); ?></div>
                            <div id="data_sort-visitor-structure" class="tooltip-data"><?php __("Sort by structure"); ?></div>
                            <div id="data_sort-user-active" class="tooltip-data"><?php __("Sort by state"); ?></div>
                            <div class="hidden">
                                <div id="user-edit" class="fancy">
                                    <h2><?php __("User's profile"); ?></h2>
                                    <?php echo $this->Form->create('Visitor', array(
                                        'url' => '',
                                        'id' => 'edit-form',
                                        'class' => ''
                                        ));
                                    ?>

                                    <div id="user-photo"></div>
                                    <fieldset class="half"><?php
                                    echo $this->Form->hidden('id');
                                    echo $this->Form->input('edit_name', array(
                                        'label' => __("Name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_firstname', array(
                                        'label' => __("First name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_function', array(
                                        'label' => __("Function", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_structure', array(
                                        'label' => __("Structure", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    </fieldset>
                                    <fieldset class="clear"><?php
                                    echo $this->Form->input('edit_url', array(
                                        'label' => __("Web site", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_phone', array(
                                        'label' => __("Telephone", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_email', array(
                                        'label' => __("eMail address", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_address', array(
                                        'label' => __("Address", true),
                                        'type'=>'textarea',
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_country', array(
                                        'label' => __("Country", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    </fieldset>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                                <div id="user-search" class="fancy">
                                    <h2><?php __("Filter criteria"); ?></h2>
                                    <?php echo $this->Form->create('Visitor', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/admin/user/visitor',
                                        'id' => 'search-form',
                                        'class' => ''
                                        ));
                                    echo $validation->bind('Visitor'); // Validation jQuery
                                    if ($this->Session->check('Auth.User.search.Visitor'))
                                        $default = $this->Session->read('Auth.User.search.Visitor');
                                    else
                                        $default = array(
                                            'name' => '',
                                            'firstname' => '',
                                            'structure' => ''
                                        );
                                    ?>

                                    <fieldset><?php
                                    echo $this->Form->input('name', array(
                                        'label' => __("Name", true),
                                        'default' => $default['name']
                                        ));
                                    echo $this->Form->input('firstname', array(
                                        'label' => __("First name", true),
                                        'default' => $default['firstname']
                                        ));
                                    echo $this->Form->input('structure', array(
                                        'label' => __("Structure", true),
                                        'default' => $default['structure']
                                        )); ?>

                                    <div id="message">
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Filter", true)); ?>
                                </div>
                            </div>
                        </div>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ibutton',
        'jquery.form',
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

// État initial du picto d'initialisation
$initState = '';
if ($this->Session->check('Auth.User.search.Visitor')) {
    $initState = "    $('#search-init').css('display', 'inline-block');\n";
}

$this->Js->Buffer("
    // iButton
    $('form.user-active input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            var extract = new Array();
            extract = $(this).attr('id').split('_');
            var userId = extract[1];
            $(this).closest('form').ajaxSubmit({
                beforeSubmit: function() {
                    $('#control_' + userId).hide();
                    $('#loading_' + userId).show();
                },
                success: function() {
                    $('#loading_' + userId).hide();
                    $('#control_' + userId).show();
                }
            })
    });
    // Id, classes et cie
    $('#adm-visitors table thead tr th.visitor-id a').attr('id', 'sort-visitor-id').addClass('sort-help');
    $('#adm-visitors table thead tr th.visitor-name a').attr('id', 'sort-visitor-name').addClass('sort-help');
    $('#adm-visitors table thead tr th.visitor-function a').attr('id', 'sort-visitor-function').addClass('sort-help');
    $('#adm-visitors table thead tr th.visitor-structure a').attr('id', 'sort-visitor-structure').addClass('sort-help');
    $('#adm-visitors table thead tr th.user-active a').attr('id', 'sort-user-active').addClass('sort-help');
    // Init. pictos
" . $initState . "
    $('<div class=\"loading\"></div>').insertAfter('#search-form div.submit input[type=submit]');
    $('<div class=\"loading\"></div>').insertAfter('#edit-form div.submit input[type=submit]');
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Raz critères de recherche
    $('#adm-visitors table thead tr th.controls span#search-init').unbind('click').bind('click', function() {
        $('#divToolTip').fadeOut('fast');
        $('#title-adm-visitors').html('" . addslashes(__("Loading…", true)) . "');
        $('#adm-visitors').fadeTo('fast', 0.6);
        $('#adm-visitors').load('/" . $this->Session->params['language'] . "/admin/user/init_criteria/visitor');
    });
    // Formulaire de recherche
    $('#search-new').fancybox({
        'width'             :   450,
        'height'            :   270,
        'autoDimensions'    :   false,
        'padding'           :   0,
        'margin'            :   0,
        'speedIn'           :   900,
        'scrolling'         :   'no',
        'centerOnScroll'    :   true,
        'overlayColor'      :   '#000',
        'overlayOpacity'    :   0.3,
        'onStart'           :   function() {
                                    $('div#divToolTip').fadeOut();
                                },
        'onComplete'        :   function() {
                                    // Soumission Ajax
                                    $('#search-form').ajaxForm({
                                        target:         '#adm-visitors',
                                        beforeSubmit:   function() {
                                            if (!$('#search-form .error').length) {
                                                $('#title-adm-visitors').html('" . addslashes(__("Loading…", true)) . "');
                                                $('#adm-visitors').fadeTo('fast', 0.6);
                                            } else
                                                return false;
                                        },
                                        success:        function() {
                                            $.fancybox.close();
                                        }
                                    });
                                }
    });
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Formulaire d'édition
    $('a.edit').each(function(){
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var userId = extract[1];
        $(this).fancybox({
            'width'             :   450,
            'height'            :   500,
            'autoDimensions'    :   false,
            'padding'           :   0,
            'margin'            :   0,
            'speedIn'           :   900,
            'scrolling'         :   'no',
            'centerOnScroll'    :   true,
            'overlayColor'      :   '#000',
            'overlayOpacity'    :   0.3,
            'onStart'           :   function() {
                                        $('div#divToolTip').fadeOut();
                                        // Photo
                                        var userPhoto = $('#hidden-info_' + userId + ' .photo').text();
                                        if (userPhoto) {
                                            var photoUrl = 'url(/files/users/' + userId + '/photo/' + userPhoto + ')';
                                            $('#edit-form #user-photo').css('background-image', photoUrl);
                                        }
                                        // ID user
                                        $('#edit-form #VisitorId').val(userId);
                                        // Initialisation des éléments du formulaire
                                        $('#edit-form #VisitorEditName').val($('#hidden-info_' + userId + ' .name').text());
                                        $('#edit-form #VisitorEditFirstname').val($('#hidden-info_' + userId + ' .firstname').text());
                                        $('#edit-form #VisitorEditFunction').val($('#edit-function_' + userId).text());
                                        $('#edit-form #VisitorEditStructure').val($('#hidden-info_' + userId + ' .structure').text());
                                        $('#edit-form #VisitorEditPhone').val($('#hidden-info_' + userId + ' .phone').text());
                                        $('#edit-form #VisitorEditUrl').val($('#hidden-info_' + userId + ' .url').text());
                                        $('#edit-form #VisitorEditAddress').val($('#hidden-info_' + userId + ' .address').text());
                                        $('#edit-form #VisitorEditCountry').val($('#hidden-info_' + userId + ' .country').text());
                                        $('#edit-form #VisitorEditEmail').val($('#hidden-info_' + userId + ' .email').text());
                                    },
            'onClosed'          :   function() {
                                        // Raz des éléments du formulaire
                                        $('#edit-form #user-photo').css('background-image', 'url(/img/user.png)');
                                        $('#edit-form #VisitorId').val('');
                                        $('#edit-form #VisitorEditName').val('');
                                        $('#edit-form #VisitorEditFirstname').val('');
                                        $('#edit-form #VisitorEditFunction').val('');
                                        $('#edit-form #VisitorEditStructure').val('');
                                        $('#edit-form #VisitorEditPhone').val('');
                                        $('#edit-form #VisitorEditUrl').val('');
                                        $('#edit-form #VisitorEditAddress').val('');
                                        $('#edit-form #VisitorEditCountry').val('');
                                        $('#edit-form #VisitorEditEmail').val('');
                                    }
        });
    });
    // Tooltips
    tooltipSortHelp('#adm-visitors table thead tr th');
    tooltipLink('#adm-visitors table');
    tooltipSupp('#adm-visitors table thead tr th.controls');
    tooltipEdit('#adm-visitors table tbody tr td div.controls');
    // Tips
    addTipAlert('#message');
    addTipInfo('#message');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#adm-visitors').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-adm-visitors').html('" . addslashes(__("Visitors' accounts", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>