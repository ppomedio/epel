<?php

/**
 * {app}/controllers/contacts_controller.php
 */
class ContactsController extends AppController {

    var $name = 'Contacts';
    // Gabarit
    var $layout = 'fancy';
    // Composants
    var $components = array('AppEmail');


    function beforeFilter() {

        // Définition des autorisations
        $this->AppAuth->allow('*');

    }


    function form() {

        // Destinataire passé en paramètre ?
        if (isset($this->params['pass'][0])) {
            $option = (!empty($this->params['pass'][0])) ? base64_decode($this->params['pass'][0]) : 1;
            $receipt = explode('_', $option);
        } else {
            // Admin
            if ($this->AppAuth->user())
                $receipt = array(0, 2 => true);
            else
                $receipt = array(0);
        }

        // Traitement
        if (!empty($this->data)) {
            $this->Contact->set($this->data);
            if (!$this->Contact->validates())
                $this->validateErrors($this->Contact);
            else {
                // Envoi des données aux éléments html/txt
                $this->set('data', $this->data);
                /**
                 * Assemblage
                 */
                // Adresse cible
                $this->loadModel('User');
                if ($receipt[0] === 0) {
                    if (!isset($this->AppEmail->to))
                        $this->AppEmail->to = $this->User->getEmail(1);
                } else
                    $this->AppEmail->to = $this->User->getEmail($receipt[0]);
                // Adresse source
                if (!isset($this->AppEmail->from))
                    $this->AppEmail->from = $this->data['Contact']['email'];
                // Sujet
                $this->AppEmail->subject .= '<- ' . $this->data['Contact']['firstname'] . ' '
                        . $this->data['Contact']['name'];
                // Template
                $this->AppEmail->template = 'contact';
                // IP
                $this->set('ipAddress', $this->RequestHandler->getClientIp());
                /**
                 * Envoi
                 */
                if ($this->AppEmail->send()) {
                    // Envoi de la copie
                    $this->AppEmail->from = $this->AppEmail->to;
                    $this->AppEmail->to = array($this->data['Contact']['email']);
                    $this->AppEmail->subject = '[' . __("eELP", true) . '] -> '
                            . __("copy of your message", true);
                    $this->AppEmail->template = 'copy';
                    $this->AppEmail->send();
                    // Journalisation de l'envoi de courriel
                    $this->logThis('email', $this->data['Contact']);
                    // Sortie
                    return $this->setAction('success');
                } else {
                    // Journalisation de l'échec d'envoi de courriel
                    $this->logThis('email', $this->AppEmail->smtpError);
                    // Sortie
                    return $this->setAction('failure');
                }
            }
        }
        $this->set('receipt', $receipt);

    }


    function success() {

    }


    function failure() {

    }


}

