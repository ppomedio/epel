
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <div id="assessment-intro"  class="instruction">
                            <p class="bolder"><?php echo __d('assessment', "Criteria sets", true) . __(":", true);  ?></p>
                            <ul>
                                <li><input name="criteria" id="criteria1" value="1" type="radio" />
                                <label for="criteria1"><?php echo "&bull; " . __d('assessment', "with a lot of help", true) . " | &bull;&bull; " . __d('assessment', "with a little help", true) . " | &bull;&bull;&bull; " . __d('assessment', "on my own", true) . ','; ?></label></li>
                                <li><input name="criteria" id="criteria2" value="2" type="radio" />
                                <label for="criteria2"><?php echo "&bull; " . __d('assessment', "with a lot of effort", true) . " | &bull;&bull; " . __d('assessment', "under normal circumstances", true) . " | &bull;&bull;&bull; " . __d('assessment', "easily in any context", true) . ','; ?></label></li>
                                <li><input name="criteria" id="criteria3" value="3" type="radio" />
                                <label for="criteria3"><?php echo __d('assessment', "Use my own criteria", true) . '.'; ?></label></li>
                            </ul>
                            <p class="info"><?php __d('assessment', "The choosen criteria set is is specific for this level of this competency.");  ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="item-edit" class="hidden">
                        <h2><?php __d('portfolio', "Add a new item"); ?></h2>
                        <?php echo $this->Form->create('PoAssessUser', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/self-assessment/item/' . urlencode($language),
                            'id' => 'item-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoAssessUser'); // Validation jQuery
                        ?>

                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->hidden('competency');
                        echo $this->Form->hidden('level');
                        echo $this->Form->input('previous_description', array(
                            'label' => false,
                            'readonly'=>'readonly',
                            'default'=>'',
                            'class' => 'hidden'
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Formulaire d'évaluation -->
                    <?php echo $this->element('student/assessment/assessment-form'); ?>

                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'assessment', 'never' => true));
                    echo $this->element('student/assessment/side-menu');
                    echo $this->element('student/assessment/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-cercles', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#assessment-intro');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#item-edit-form div.submit input[type=submit]');
    // Activation du submit
    $('#item-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#item-edit-form textarea').keydown(function() {
        if ($('#item-edit-form div.submit input[type=submit]').attr('disabled'))
            $('#item-edit-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Saut
    $('.jump').click(function() {
        // Préparation…
        $('#assessment-title').html('" . addslashes(__("Loading…", true)) . "');
        $('#assessment-update').fadeTo('fast', 0.6);
        $('#levels-menu ul li').removeClass('current')
        // Extraction ID
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        // Maj
        $('#assessment-update').load('/" . $this->params['language'] . "/student/self-assessment/" . $competency . '/' . $language . "/' + itemId, function() {
            // Rétablissement opacité / titre / menu
            $('#assessment-update').fadeTo('slow', 1);
            $('#sublink_' + itemId).closest('li').addClass('current')
            });
        return false;
    });
    // Soumission Ajax du formulaire
    $('#item-edit-form').submit(function() {
        if (!$('#item-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/self-assessment/item/" . $language . "/' + $('#PoAssessUserCompetency').val() + '/' + $('#PoAssessUserLevel').val(),
                target: '#assessment-update',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#item-edit-form div.loading').show();
                },
                success: function() {
                    $('#item-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your self-assessment has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#item-toggle').hide();
                    $('#item-add').fadeIn('slow');
                    // Raz form
                    initForm();
                    $('#item-edit-form div.loading').hide();
                    $('#item-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
    // Choix du jeu de critères
    $('#assessment-intro input[type=radio]').bind('click', function() {
        if ($(this).val() == 1) {
            $('#PoAssessCriteriaCriteriaSet').val('1');
            $('#assessment-form thead textarea').attr('readonly', 'readonly');
            $('#PoAssessCriteriaCriteria1').val('" . addslashes(__d('assessment', "with a lot of help", true)) . "');
            $('#PoAssessCriteriaCriteria2').val('" . addslashes(__d('assessment', "with a little help", true)) . "');
            $('#PoAssessCriteriaCriteria3').val('" . addslashes(__d('assessment', "on my own", true)) . "');
        } else if ($(this).val() == 2) {
            $('#PoAssessCriteriaCriteriaSet').val('2');
            $('#assessment-form thead textarea').attr('readonly', 'readonly');
            $('#PoAssessCriteriaCriteria1').val('" . addslashes(__d('assessment', "with a lot of effort", true)) . "');
            $('#PoAssessCriteriaCriteria2').val('" . addslashes(__d('assessment', "under normal circumstances", true)) . "');
            $('#PoAssessCriteriaCriteria3').val('" . addslashes(__d('assessment', "easily in any context", true)) . "');
        } else {
            $('#PoAssessCriteriaCriteriaSet').val('3');
            $('#assessment-form thead textarea').removeAttr('readonly');
            $('#assessment-form thead textarea').val('');
        }
        $('div.submit input[type=submit]').removeAttr('disabled');
    });
");

?>