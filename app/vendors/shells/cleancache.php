<?php
/**
 * Shell used to clean the cache,
 * can use the -check to clear only files that are expired
 *
 * {app}/vendors/shells/cleancache.php
 */
class cleanCacheShell extends Shell {

    /* show the help */
    function help() {
        $this->out("cleanCache [-options]");
        $this->out("\t-check: check the files expiration date");
        $this->out("\tNote: the default cake core cache directories will be cleared");
    }

    /* clean the cache according to config */
    function main() {
        $options = array_merge(array('check' => false), $this->params);
        $this->out("cleanCache, check: " . ($options['check'] ? "true" : "false" ) );
        $z = cache::getInstance();
        foreach($z->__config as $key => $val) {
            if($key == 'default')
                clearCache();
            else
                cache::clear($options['check'], $key);
            $this->out("cache: {$key} => cleared");
        }
    }
}
?>