
                    <section id="assessment-update">
                        <h2 id="assessment-title"><?php echo $languageTitle . ' &bull; ' . __d('portfolio', $competencies[$competency], true) . ' &bull; ' . $level; ?></h2>
                        <?php
                        // Formulaire
                        echo $this->Form->create('PoAssessAnswer', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/self-assessment/' . $competency . '/' . $language,
                            'id' => 'assessment-form',
                            'class' => 'full table'
                            ));
                        // Stockage de l'ID
                        echo $this->Form->hidden('id');
                        ?>
                        <div id="hidden-info" class="hidden">
                            <span class="competency hidden"><?php
                            echo $competency;
                            ?></span>
                            <span class="level hidden"><?php
                            echo $level;
                            ?></span>
                        </div>
                        <input name="data[PoAssessCriteria][id]" value="<?php echo $this->data['PoAssessCriteria']['id']?>" id="PoAssessCriteriaId" type="hidden" />
                        <input name="data[PoAssessCriteria][criteria_set]" value="<?php echo $this->data['PoAssessCriteria']['criteria_set']?>" id="PoAssessCriteriaCriteriaSet" type="hidden" />
                        <table>
                            <thead>
                                <tr>
                                    <th><?php echo $level; ?></th>
                                    <th><?php __d('portfolio', "New target");?></th>
                                    <th><div class="input textarea"><label for="PoAssessCriteriaCriteria1">&bull;</label><textarea name="data[PoAssessCriteria][criteria1]" class="criteria-1" cols="30" rows="6" id="PoAssessCriteriaCriteria1"><?php echo $this->data['PoAssessCriteria']['criteria1']; ?></textarea></div></th>
                                    <th><div class="input textarea"><label for="PoAssessCriteriaCriteria2">&bull;&bull;</label><textarea name="data[PoAssessCriteria][criteria2]" class="criteria-2" cols="30" rows="6" id="PoAssessCriteriaCriteria2"><?php echo $this->data['PoAssessCriteria']['criteria2']; ?></textarea></div></th>
                                    <th><div class="input textarea"><label for="PoAssessCriteriaCriteria3">&bull;&bull;&bull;</label><textarea name="data[PoAssessCriteria][criteria3]" class="criteria-3" cols="30" rows="6" id="PoAssessCriteriaCriteria3"><?php echo $this->data['PoAssessCriteria']['criteria3']; ?></textarea></div></th>
                                    <th class="controls">
                                        <span id="item-add" class="add"></span>
                                        <span id="item-toggle" class="gear"></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // Items par défaut (PoAssessAnswer)
                                $trClass = 'even';
                                foreach ($items as $key => $value) {
                                    if ($trClass == 'even')
                                        $trClass = 'odd';
                                    else
                                        $trClass = 'even'; ?>
                                <tr class="<?php echo $trClass; ?>">
                                    <td><?php __d('assessment', $value); ?></td>
                                    <?php
                                    echo '<td class="master">' . $this->Form->checkbox($level . '4' . $key) . '</td>';
                                    for ($loop = 1; $loop < 4; $loop++) {
                                        $check = (string)$loop;
                                    ?>
                                    <td>
                                        <ul>
                                            <li><input name="data[PoAssessAnswer][<?php echo $level . '0' . $key; ?>]" id="PoAssessAnswer<?php echo $level . '0' . $key; ?>answer<?php echo $check; ?>" value="<?php echo $check; ?>" type="radio"<?php if (isset($this->data['PoAssessAnswer'][$level . '0' . $key]) && ($this->data['PoAssessAnswer'][$level . '0' . $key] == $check)) echo ' checked="checked"'; ?> /></li>
                                            <li><?php echo $this->Form->input($level . $check . $key, array('label' => false)); ?></li>
                                        </ul>
                                    </td>
                                    <?php
                                    }
                                    echo '<td></td></tr>';
                                }
                                // Items utilisateur (PoAssessUser)
                                if (!empty($this->data['PoAssessUser'])) {
                                    foreach ($this->data['PoAssessUser'] as $key => $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even'; ?>
                                <tr class="user-assess <?php echo $trClass; ?>">
                                    <td id="item-description_<?php echo $value['id']; ?>"><?php echo $value['description']; ?></td>
                                    <td class="master">
                                        <input name="data[Target][<?php echo $key; ?>]" id="temp_<?php echo $key; ?>_" value="0" type="hidden" /><input name="data[Target][<?php echo $key; ?>]" value="<?php echo $value['target']?>" id="temp_<?php echo $key; ?>" type="checkbox"<?php if ($value['target']) echo ' checked="checked"'; ?> />
                                        <input name="data[PoAssessUser][<?php echo $key; ?>][target]" value="<?php echo $value['target']; ?>" id="target_<?php echo $key; ?>" type="hidden" />
                                    </td>
                                        <?php
                                        for ($loop = 1; $loop < 4; $loop++) {
                                            $check = (string)$loop;
                                        ?>
                                    <td>
                                        <input name="data[PoAssessUser][<?php echo $key; ?>][id]" value="<?php echo $value['id']; ?>" id="PoAssessUser<?php echo $key; ?>Id" type="hidden" />
                                        <ul>
                                            <li><input name="data[PoAssessUser][<?php echo $key; ?>][evaluation]" id="PoAssessUser<?php echo $key; ?>Evaluation<?php echo $check ?>" value="<?php echo $check; ?>" type="radio"<?php if ($value['evaluation'] == $check) echo ' checked="checked"'; ?> /></li>
                                            <li><div class="input text"><input name="data[PoAssessUser][<?php echo $key; ?>][date][<?php echo $check; ?>]" maxlength="60" value="<?php echo $value['date' . $check] ?>" id="PoAssessUser<?php echo $key; ?>Date<?php echo $check ?>" type="text"></div></li>
                                        </ul>
                                    </td>
                                        <?php
                                        } ?>
                                    <td class="controls">
                                        <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                        <div id="control_<?php echo $value['id']; ?>" class="controls">
                                            <span id="item-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                            <span id="item-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                        </div>
                                    </td>
                                </tr>
                                <div id="data_item-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                    __d('portfolio', "Edit this item");
                                ?></div>
                                <div id="data_item-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                    __d('portfolio', "Delete this item");
                                ?></div>
                                <div id="item-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                    __d('portfolio', "Please confirm delete item"); echo ' (' . $value['description'] . ').';
                                ?></div><?php
                                    }
                                } ?>

                            </tbody>
                        </table>
                        <?php echo $this->Form->end(__("Update", true)); ?>
                        <div id="data_item-add" class="tooltip-data"><?php __d('portfolio', "Add a new item"); ?></div>
                        <div id="data_item-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                    </section>
<?php

// Initialisation du jeu de critères
if (!empty($this->data['PoAssessCriteria']['criteria_set']))
    $check = $this->data['PoAssessCriteria']['criteria_set'];
else
    $check = '3';


/**
 * Chargement des scripts utilisés
 *
 */

$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ibutton',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Fonction d'initialisation du formulaire
    function initForm() {
        $('#item-edit > h2').text('" . addslashes(__d('portfolio', "Add a new item", true)) . "');
        $('#item-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
        $('#item-edit-form #PoAssessUserId').val('');
        $('#item-edit-form #PoAssessUserCompetency').val($('#hidden-info .competency').text());
        $('#item-edit-form #PoAssessUserLevel').val($('#hidden-info .level').text());
        $('#item-edit-form #PoAssessUserDescription').val('');
        $('#item-edit-form #PoAssessUserPreviousDescription').val('');
        if ($('#item-edit-form #PoAssessUserPreviousDescription').is(':visible'))
            $('#item-edit-form #PoAssessUserPreviousDescription').slideUp('slow');
        $('#item-edit-form .error').removeClass('error');
        $('#item-edit-form .error-message').remove();
    };
    // iButtons
    $('#assessment-form table tbody input[type=checkbox]').iButton({
        labelOn: '" . __('YES', true) . "',
        labelOff: '" . __('NO', true) . "',
        resizeHandle: false,
        resizeContainer: true
        })
        .change(function() {
            var extract = new Array();
            extract = $(this).attr('id').split('_');
            var itemId = extract[1];
            if (itemId) {
                if ($('#temp_' + itemId).is(':checked')) {
                    $('#target_' + itemId).val('1');
                } else
                    $('#target_' + itemId).val('0');
                }
            // Activation du submit
            if ($('#assessment-form div.submit input[type=submit]').attr('disabled'))
                $('#assessment-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Tooltips
    tooltipAdd('#assessment-form table thead th.controls');
    tooltipGear('#assessment-form table thead th.controls');
    tooltipEdit('#assessment-form table tbody td.controls div.controls');
    tooltipSupp('#assessment-form table tbody td.controls div.controls');
    // Jeu de critère initial
    var check = '" . $check . "';
    $('#criteria' + check).attr('checked', 'checked');
    if (check != '3')
        $('#assessment-form thead textarea').attr('readonly', 'readonly');
    // Initialisation du formulaire d'édition
    initForm();
    if ($('#item-edit-form').is(':hidden')) {
        $('#item-toggle').hide();
        $('#item-add').fadeIn('slow');
    } else {
        $('#item-add').hide();
        $('#item-toggle').fadeIn('slow');
    }
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#assessment-form div.submit input[type=submit]');
    // Activation du submit
    $('#assessment-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#assessment-form input[type=radio]').click(function() {
        if ($('#assessment-form div.submit input[type=submit]').attr('disabled'))
            $('#assessment-form div.submit input[type=submit]').removeAttr('disabled');
    });
    $('#assessment-form textarea').keydown(function() {
        if ($('#assessment-form div.submit input[type=submit]').attr('disabled'))
            $('#assessment-form div.submit input[type=submit]').removeAttr('disabled');
    });
    $('#assessment-form input[type=text]').keydown(function() {
        if ($('#assessment-form div.submit input[type=submit]').attr('disabled'))
            $('#assessment-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#assessment-form').submit(function() {
        $(this).ajaxSubmit({
            url: '/" . $this->params['language'] . "/student/self-assessment/" . $competency . '/' . $language . '/' . $level . "',
            target: '#assessment-update',
            replaceTarget: true,
            beforeSubmit: function() {
                $('#assessment-form div.submit input[type=submit]').attr('disabled', 'disabled');
                $('#assessment-form div.submit div.loading').show();
                prevTitle = $('#assessment-title').html();
                $('#assessment-title').html('" . addslashes(__("Loading…", true)) . "');
                $('#assessment-update').fadeTo('fast', 0.6);
            },
            success: function() {
                $('#assessment-form div.submit div.loading').hide();
                $('#assessment-title').html(prevTitle);
                $('#assessment-update').fadeTo('slow', 1);
                $('#content div#ajaxMessage')
                    .text('" . addslashes(__d('portfolio', "Your self-assessment has been updated.", true)) . "')
                    .show(function(){
                        $(this).delay(3600).hide('slow');
                    });
            }
        });
        return false;
    });
");

// Discrimination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#item-toggle').live('click', function() {
        if ($('#item-edit-form #PoAssessUserId').val() == '')
            $('#item-edit > h2').text('" . addslashes(__d('portfolio', "Add a new item", true)) . "');
        if ($('#item-edit').is(':hidden'))
            $('#item-edit').slideDown('slow');
        else {
            $('#item-edit').slideUp('slow');
            $('#item-add').fadeIn('slow');
            if ($('#item-edit-form #PoAssessUserDescription').val() == '')
                $('#item-toggle').hide();
        }
    });
    // Ajout d'un item
    $('#item-add').live('click', function() {
        $('#item-edit > h2').text('" . addslashes(__d('portfolio', "Add a new item", true)) . "');
        // Initialisation du formulaire
        initForm();
        $('#item-edit-form #PoAssessUserPreviousDescription').hide();
        if ($('#item-edit').is(':hidden')) {
            $('#item-edit').slideDown('slow');
        }
        $('#item-add').hide();
        $('#item-toggle').fadeIn('slow');
    });
    // Édition d'un item
    $('#assessment-form table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#item-edit > h2').text('" . addslashes(__d('portfolio', "Item edition", true)) . "');
        $('#item-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
        // Chargement des données dans le formulaire
        $('#item-edit-form #PoAssessUserId').val(itemId);
        $('#item-edit-form #PoAssessUserCompetency').val($('#hidden-info .competency').text());
        $('#item-edit-form #PoAssessUserLevel').val($('#hidden-info .level').text());
        $('#item-edit-form #PoAssessUserDescription').val($('#item-description_' + itemId).text());
        $('#item-edit-form #PoAssessUserPreviousDescription').val($('#item-description_' + itemId).text());
        $('#item-edit-form #PoAssessUserPreviousDescription').slideDown('fast');
        // Affichage si masqué
        if ($('#item-edit').is(':hidden')) {
            $('#item-add').hide();
            $('#item-edit').slideDown('slow');
            $('#item-add').fadeIn('slow');
            $('#item-toggle').fadeIn('slow');
        } else {
            if ($('#item-add').is(':hidden'))
                $('#item-add').fadeIn('slow');
        }
    });
    // Suppression d'un item
    $('#assessment-form table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#item-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#assessment-update').load('/" . $this->params['language'] . "/student/self-assessment/delete/" . $language . "/' + $('#hidden-info .competency').text() + '/' + $('#hidden-info .level').text() + '/' + itemId, function() {
                        if ($('#item-edit-form #PoAssessUserId').val() == itemId) {
                            $('#item-edit > h2').text('" . addslashes(__d('portfolio', "Add a new item", true)) . "');
                            initForm();
                            if ($('#item-edit').is(':hidden'))
                                $('#item-toggle').hide();
                            else
                                $('#item-add').hide();
                        }
                        $('#content div#ajaxMessage')
                            .text('" . addslashes(__d('portfolio', "Your self-assessment has been updated.", true)) . "')
                            .show(function(){
                                $(this).delay(3600).hide('slow');
                            });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
    });
");
} else {
    // Sortie des scripts pour affichage AJAX
    echo $this->Js->writeBuffer();
}

?>