<?php echo __("Send by", true)
        . " " . $data['Contact']['name']
        . " " . $data['Contact']['firstname']
        . " (" . $data['Contact']['email'] . ")"; ?>

<?php __("Message"); ?>
<?php echo $data['Contact']['message']; ?>

<?php echo date('Y/m/d - H:i (e)') . " - " . $ipAddress; ?>