
                    <div>
                        <h2><?php
                        echo $this->Html->link(__("User guide", true),
                            array(
                                'controller' => 'guide',
                                'action' => 'self-assessment'
                            ),
                            array(
                                'id'=> __("Open the user guide", true),
                                'class'=>'iframe self-assessment link'
                                )
                        );
                        ?></h2>
                        <div id="assess-info" class="information">
                            <p class="info"><?php __("Put the cursor of your mouse above icons to see their function."); ?></p>
                            <dl>
                                <dt class="add"></dt>
                                <dd><?php __d('portfolio', "Add a new item"); ?></dd>
                                <dt class="edit"></dt>
                                <dd><?php __d('portfolio', "Edit item"); ?></dd>
                                <dt class="supp"></dt>
                                <dd><?php __d('portfolio', "Delete item"); ?></dd>
                                <dt class="gear"></dt>
                                <dd><?php __("Shift edition panel"); ?></dd>
                            </dl>
                        </div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#assess-info');
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Tips
    addTipInfo('#assess-info');
");
*/
?>
