
                        <?php echo $this->Form->create('PoPlurilingualism', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/biography/plurilingualism',
                            'id' => 'plurilingualism-edit-form',
                            'class' => 'full'
                            ));
                        ?>

                        <fieldset><?php
                        echo $this->Form->input('description', array(
                            'label' => false,
                            'rows' => '25'
                            ));
                        ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#plurilingualism-edit-form div.submit input[type=submit]');
    // Activation du submit
    $('#plurilingualism-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
    $('#plurilingualism-edit-form textarea').keydown(function() {
        if ($('#plurilingualism-edit-form div.submit input[type=submit]').attr('disabled'))
            $('#plurilingualism-edit-form div.submit input[type=submit]').removeAttr('disabled');
    });
    // Soumission Ajax du formulaire
    $('#plurilingualism-edit-form').ajaxForm({
        url: '/" . $this->params['language'] . "/student/biography/plurilingualism',
        target: '#plurilingualism-edit-form',
        replaceTarget: true,
        beforeSubmit: function() {
            $('#plurilingualism-edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
            $('#plurilingualism-edit-form div.loading').show();
        },
        success: function() {
            $('#plurilingualism-edit-form div.loading').hide();
            $('#plurilingualism-edit-form div.submit input[type=submit]').removeAttr('disabled');
            $('#content div#ajaxMessage')
                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                .show(function(){
                    $(this).delay(3600).hide('slow');
                });
        }
    })
");

// Sortie des scripts pour affichage AJAX
if (isset($ajax))
    echo $this->Js->writeBuffer();

?>