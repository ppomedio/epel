<?php

/**
 * {app}/controllers/pages_controller.php
 *
 * Contrôleur des pages statiques
 *
 */
class PagesController extends AppController {

    var $name = 'Pages';
    // Ce contrôleur n'utilise pas de modèle
    var $uses = array();


    /**
     * Before filter
     *
     * @access public
     */
    function beforeFilter() {

        // Définition des autorisations
        $this->AppAuth->allow('*');
        // Retour…
        parent::beforeFilter();

    }


    /**
     * Fonctionnalité prochainement implémentée…
     *
     * @access public
     */
    function soon() {

        $this->layout = 'fancy-help';

    }


    /**
     * Grille des niveaux
     *
     * @access public
     */
    function levels_grid() {

        $this->layout = 'fancy-help';

    }


    /**
     * Guide des listes de repérage
     *
     * @access public
     */
    function self_assessment() {

        $this->layout = 'fancy-help';

    }


    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @access public
     */
    function display() {

        $this->helpers[] = 'Menu';  // Assistant pour le menu

        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = __("Welcome", true);

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));
        $this->render(implode('/', $path));

    }


}

