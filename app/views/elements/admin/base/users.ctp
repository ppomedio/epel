
                    <section>
                        <h2><?php __('Users'); ?></h2>
                        <div id="user-stats" class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __("Students"); ?></th>
                                        <th><?php __("Teachers"); ?></th>
                                        <th><?php __("Visitors"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="infotext"><?php
                                        echo $userCount['active']['student'] . '</span> / <span class="alerttext">' . $userCount['inactive']['student'];
                                        ?></span> / <?php echo $userCount['active']['student'] + $userCount['inactive']['student']; ?></td>
                                        <td><span class="infotext"><?php
                                        echo $userCount['active']['teacher'] . '</span> / <span class="alerttext">' . $userCount['inactive']['teacher'];
                                        ?></span> / <?php echo $userCount['active']['teacher'] + $userCount['inactive']['teacher']; ?></td>
                                        <td><span class="infotext"><?php
                                        echo $userCount['active']['visitor'] . '</span> / <span class="alerttext">' . $userCount['inactive']['visitor'];
                                        ?></span> / <?php echo $userCount['active']['visitor'] + $userCount['inactive']['visitor']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="info"><?php echo __("Activated users", true); ?> / <?php echo __("Inactivated users", true); ?> / <?php echo __("Total for all universities", true); ?></p>
                        </div>
                    </section><?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#user-stats');
");

?>