<?php
/**
 * Les chaînes de traduction ne sont pas stockées directement comme valeurs pour <td id="…">
 * car cela peut empécher l'affichage des info-bulles (cas de converse / B1 en anglais).
 */
?>

                    <div id="levels-grid">
                        <h2><?php
                        echo $this->Html->link(__d('levels_grid', "Levels grid", true),
                            array(
                                'controller' => 'pages',
                                'action' => 'levels_grid'
                            ),
                            array(
                                'id'=> __d('levels_grid', "Open the levels grid in full-screen mode", true),
                                'class'=> 'iframe levels-grid link'
                            )
                        );
                        ?></h2>
                        <div class="table">
                            <table id="levels-grid-table">
                                <tr id="listen">
                                    <td id="listen-title" class="title"></td>
                                    <td id="listen-A1" class="item">A1</td>
                                    <td id="listen-A2" class="item">A2</td>
                                    <td id="listen-B1" class="item">B1</td>
                                    <td id="listen-B2" class="item">B2</td>
                                    <td id="listen-C1" class="item">C1</td>
                                    <td id="listen-C2" class="item">C2</td>
                                </tr>
                                <tr id="read">
                                    <td id="read-title" class="title"></td>
                                    <td id="read-A1" class="item">A1</td>
                                    <td id="read-A2" class="item">A2</td>
                                    <td id="read-B1" class="item">B1</td>
                                    <td id="read-B2" class="item">B2</td>
                                    <td id="read-C1" class="item">C1</td>
                                    <td id="read-C2" class="item">C2</td>
                                </tr>
                                <tr id="converse">
                                    <td id="converse-title" class="title"></td>
                                    <td id="converse-A1" class="item">A1</td>
                                    <td id="converse-A2" class="item">A2</td>
                                    <td id="converse-B1" class="item">B1</td>
                                    <td id="converse-B2" class="item">B2</td>
                                    <td id="converse-C1" class="item">C1</td>
                                    <td id="converse-C2" class="item">C2</td>
                                </tr>
                                <tr id="speak">
                                    <td id="speak-title" class="title"></td>
                                    <td id="speak-A1" class="item">A1</td>
                                    <td id="speak-A2" class="item">A2</td>
                                    <td id="speak-B1" class="item">B1</td>
                                    <td id="speak-B2" class="item">B2</td>
                                    <td id="speak-C1" class="item">C1</td>
                                    <td id="speak-C2" class="item">C2</td>
                                </tr>
                                <tr id="write">
                                    <td id="write-title" class="title"></td>
                                    <td id="write-A1" class="item">A1</td>
                                    <td id="write-A2" class="item">A2</td>
                                    <td id="write-B1" class="item">B1</td>
                                    <td id="write-B2" class="item">B2</td>
                                    <td id="write-C1" class="item">C1</td>
                                    <td id="write-C2" class="item">C2</td>
                                </tr>
                            </table>
                            <p class="info"><?php
                            __d('levels_grid', "This self-assessment grid was developed by the the Council of Europe in the context of the Common European Framework of Reference for Languages (CEFRL).");
                            $levelDocs = Configure::read('epel.levelgrid.url');
                            if (isset($levelDocs)) {
                                echo '<br />' . __d('levels_grid', "You can download a printable version at", true); ?> <a href="<?php echo $levelDocs; ?>"><?php __d('levels_grid', "the page describing CEFRL levels"); ?> </a><?php __d('levels_grid', "of the Council of Europe website.");

                            } ?></p>
                        </div>
                        <div id="data_listen-title" class="tooltip-data"><?php __d('levels_grid', "Understanding: listening"); ?></div>
                        <div id="data_listen-A1" class="tooltip-data"><?php __d('levels_grid', "I can understand familiar words and very basic phrases concerning myself, my family and immediate concrete surroundings when people speak slowly and clearly."); ?></div>
                        <div id="data_listen-A2" class="tooltip-data"><?php __d('levels_grid', "I can understand phrases and the highest frequency vocabulary related to areas of most immediate personal relevance (e.g. very basic personal and family information, shopping, local area, employment). I can catch the main point in short, clear, simple messages and announcements."); ?></div>
                        <div id="data_listen-B1" class="tooltip-data"><?php __d('levels_grid', "I can understand the main points of clear standard speech on familiar matters regularly encountered in work, school, leisure, etc. I can understand the main point of many radio or TV programmes on current affairs or topics of personal or professional interest when the delivery is relatively slow and clear."); ?></div>
                        <div id="data_listen-B2" class="tooltip-data"><?php __d('levels_grid', "I can understand extended speech and lectures and follow even complex lines of argument provided the topic is reasonably familiar. I can understand most TV news and current affairs programmes. I can understand the majority of films in standard dialect."); ?></div>
                        <div id="data_listen-C1" class="tooltip-data"><?php __d('levels_grid', "I can understand extended speech even when it is not clearly structured and when relationships are only implied and not signalled explicitly. I can understand television programmes and films without too much effort."); ?></div>
                        <div id="data_listen-C2" class="tooltip-data"><?php __d('levels_grid', "I have no difficulty in understanding any kind of spoken language, whether live or broadcast, even when delivered at fast native speed, provided I have some time to get familiar with the accent."); ?></div>
                        <div id="data_read-title" class="tooltip-data"><?php __d('levels_grid', "Understanding: reading"); ?></div>
                        <div id="data_read-A1" class="tooltip-data"><?php __d('levels_grid', "I can understand familiar names, words and very simple sentences, for example on notices and posters or in catalogues."); ?></div>
                        <div id="data_read-A2" class="tooltip-data"><?php __d('levels_grid', "I can read very short, simple texts. I can find specific, predictable information in simple everyday material such as advertisements, prospectuses, menus and timetables and I can understand short simple personal letters."); ?></div>
                        <div id="data_read-B1" class="tooltip-data"><?php __d('levels_grid', "I can understand texts that consist mainly of high frequency everyday or job-related language. I can understand the description of events, feelings and wishes in personal letters."); ?></div>
                        <div id="data_read-B2" class="tooltip-data"><?php __d('levels_grid', "I can read articles and reports concerned with contemporary problems in which the writers adopt particular attitudes or viewpoints. I can understand contemporary literary prose."); ?></div>
                        <div id="data_read-C1" class="tooltip-data"><?php __d('levels_grid', "I can understand long and complex factual and literary texts, appreciating distinctions of style. I can understand specialised articles and longer technical instructions, even when they do not relate to my field."); ?></div>
                        <div id="data_read-C2" class="tooltip-data"><?php __d('levels_grid', "I can read with ease virtually all forms of the written language, including abstract, structurally or linguistically complex texts such as manuals, specialised articles and literary works."); ?></div>
                        <div id="data_converse-title" class="tooltip-data"><?php __d('levels_grid', "Speaking: spoken interaction"); ?></div>
                        <div id="data_converse-A1" class="tooltip-data"><?php __d('levels_grid', "I can interact in a simple way provided the other person is prepared to repeat or rephrase things at a slower rate of speech and help me formulate what I’m trying to say. I can ask and answer simple questions in areas of immediate need or on very familiar topics."); ?></div>
                        <div id="data_converse-A2" class="tooltip-data"><?php __d('levels_grid', "I can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar topics and activities. I can handle very short social exchanges, even though I can’t usually understand enough to keep the conversation going myself."); ?></div>
                        <div id="data_converse-B1" class="tooltip-data"><?php __d('levels_grid', "I can deal with most situations likely to arise whilst travelling in an area where the language is spoken. I can enter unprepared into conversation on topics that are familiar, of personal interest or pertinent to everyday life (e.g. family, hobbies, work, travel and current events)."); ?></div>
                        <div id="data_converse-B2" class="tooltip-data"><?php __d('levels_grid', "I can interact with a degree of fluency and spontaneity that makes regular interaction with native speakers quite possible. I can take an active part in discussion in familiar contexts, accounting for and sustaining my views."); ?></div>
                        <div id="data_converse-C1" class="tooltip-data"><?php __d('levels_grid', "I can express myself fluently and spontaneously without much obvious searching for expressions. I can use language flexibly and effectively for social and professional purposes. I can formulate ideas and opinions with precision and relate my contribution skilfully to those of other speakers."); ?></div>
                        <div id="data_converse-C2" class="tooltip-data"><?php __d('levels_grid', "I can take part effortlessly in any conversation or discussion and have a good familiarity with idiomatic expressions and colloquialisms. I can express myself fluently and convey finer shades of meaning precisely. If I do have a problem I can backtrack and restructure around the difficulty so smoothly that other people are hardly aware of it."); ?></div>
                        <div id="data_speak-title" class="tooltip-data"><?php __d('levels_grid', "Speaking: spoken production"); ?></div>
                        <div id="data_speak-A1" class="tooltip-data"><?php __d('levels_grid', "I can use simple phrases and sentences to describe where I live and people I know."); ?></div>
                        <div id="data_speak-A2" class="tooltip-data"><?php __d('levels_grid', "I can use a series of phrases and sentences to describe in simple terms my family and other people, living conditions, my educational background and my present or most recent job."); ?></div>
                        <div id="data_speak-B1" class="tooltip-data"><?php __d('levels_grid', "I can connect phrases in a simple way in order to describe experiences and events, my dreams, hopes and ambitions. I can briefly give reasons and explanations for opinions and plans. I can narrate a story or relate the plot of a book or film and describe my reactions."); ?></div>
                        <div id="data_speak-B2" class="tooltip-data"><?php __d('levels_grid', "I can present clear, detailed descriptions on a wide range of subjects related to my field of interest. I can explain a viewpoint on a topical issue giving the advantages and disadvantages of various options."); ?></div>
                        <div id="data_speak-C1" class="tooltip-data"><?php __d('levels_grid', "I can present clear, detailed descriptions of complex subjects integrating sub-themes, developing particular points and rounding off with an appropriate conclusion."); ?></div>
                        <div id="data_speak-C2" class="tooltip-data"><?php __d('levels_grid', "I can present a clear, smoothly-flowing description or argument in a style appropriate to the context and with an effective logical structure which helps the recipient to notice and remember significant points."); ?></div>
                        <div id="data_write-title" class="tooltip-data"><?php __d('levels_grid', "Writing"); ?></div>
                        <div id="data_write-A1" class="tooltip-data"><?php __d('levels_grid', "I can write a short, simple postcard, for example sending holiday greetings. I can fill in forms with personal details, for example entering my name, nationality and address on a hotel registration form."); ?></div>
                        <div id="data_write-A2" class="tooltip-data"><?php __d('levels_grid', "I can write short, simple notes and messages. I can write a very simple personal letter, for example thanking someone for something."); ?></div>
                        <div id="data_write-B1" class="tooltip-data"><?php __d('levels_grid', "I can write simple connected text on topics which are familiar or of personal interest. I can write personal letters describing experiences and impressions."); ?></div>
                        <div id="data_write-B2" class="tooltip-data"><?php __d('levels_grid', "I can write clear, detailed text on a wide range of subjects related to my interests. I can write an essay or report, passing on information or giving reasons in support of or against a particular point of view. I can write letters highlighting the personal significance of events and experiences."); ?></div>
                        <div id="data_write-C1" class="tooltip-data"><?php __d('levels_grid', "I can express myself in clear, well structured text, expressing points of view at some length. I can write about complex subjects in a letter, an essay or a report, underlining what I consider to be the salient issues. I can select a style appropriate to the reader in mind."); ?></div>
                        <div id="data_write-C2" class="tooltip-data"><?php __d('levels_grid', "I can write clear, smoothly-flowing text in an appropriate style. I can write complex letters, reports or articles which present a case with an effective logical structure which helps the recipient to notice and remember significant points. I can write summaries and reviews of professional or literary works."); ?></div>
                    </div>
                    <!-- Js non bufferisé pour mise en cache de l'élément -->
                    <script type="text/javascript">
                    //<![CDATA[
                    addTipInfo('#levels-grid');
                    $('#levels-grid div.table table tr td.item').tooltip({
                        bordercolor: '#dcdcdd',
                        bgcolor: '#fefeff',
                        fontcolor : '#666',
                        fontsize : '12px',
                        width : '210px',
                        cursor : 'help'
                        });
                    $('#levels-grid div.table table tr td.title').tooltip({
                        bordercolor: '#39f',
                        bgcolor: '#39f',
                        fontcolor : '#fefeff',
                        fontsize : '13px',
                        cursor : 'help'
                        });
                    //]]>
                    </script>
<?php

/**
 * Ajouts au buffer Js,
 * commentés si déjà inclus dans le code (mise en cache de l'élément).
 *
 */
/*
$this->Js->Buffer("
    // Grille des niveaux
    addTipInfo('#levels-grid');
    $('#levels-grid div.table table tr td.item').tooltip({
        bordercolor: '#dcdcdd',
        bgcolor: '#fefeff',
        fontcolor : '#666',
        fontsize : '12px',
        width : '210px',
        cursor : 'help'
        });
    $('#levels-grid div.table table tr td.title').tooltip({
        bordercolor: '#39f',
        bgcolor: '#39f',
        fontcolor : '#fefeff',
        fontsize : '13px',
        cursor : 'help'
        });
");
*/
?>