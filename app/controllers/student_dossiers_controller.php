<?php

/**
 * {app}/controllers/student_dossiers_controller.php
 *
 */
class StudentDossiersController extends AppController {

    var $name = 'StudentDossiers';
    // Modèle
    var $uses = array('PoDocument');
    // Composants
    var $components = array(
        'Uploader.Uploader',
        'PortfolioLists',
        'SideMenu'
    );
    // Pagination
    var $paginate;
    // Assistants
    var $helpers = array(
        'Paginator',
        'Date',
        'Menu'
    );


    function beforeFilter() {

        // Chargement de la langue en cours / des ID de portfolios
        $portfolioLanguage = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
        $portfolios = $this->Session->read('Auth.User.portfolios');
        // Si portfolio vide ou langue demandée non ouverte : redirection
        if (empty($portfolios) || ($portfolioLanguage && !array_key_exists($portfolioLanguage, $portfolios)))
            $this->redirect('/' . $this->params['language'] . '/student/base');
        // Stockage de la langue active pour les autres méthodes
        Configure::write('portfolioLanguage', $portfolioLanguage);

        // Configuration du téléversement de documents
        $this->Uploader->uploadDir = 'files' . DS . 'users' . DS . $this->AppAuth->user('id') . DS;
        $this->Uploader->maxFileSize = strval(Configure::read('epel.document.size')) . 'M';
        $this->Uploader->maxNameLength = false;

        // Menu latéral
        $this->SideMenu->setSideMenu('Dossier');

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'base');

    }


    function show() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');
        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('competencies', $this->PortfolioLists->getCompetenciesNames());
        $this->set('user', $this->_getDocuments($portfolios[$language]['id'], 'show'));
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Ajax
            Configure::write('debug', 0);
            if (isset($_POST['newDocId']))
                $this->set('newDoc', $this->PoDocument->getThisDocument($_POST['newDocId']));
            $this->layout = 'ajax';
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'dossier' . DS . 'show-page');
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.dossier_' . $language, 'show', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Dossier.submenu.' . $languageName . '.action', 'show' . DS . $language);
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Dossier", true) . " &bull; " . $languageTitle);
        }

    }


    function work() {

        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');
        // Chargement des variables de vue
        $this->set('language', $language);
        $this->set('user', $this->_getDocuments($portfolios[$language]['id'], 'work'));
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Ajax
            Configure::write('debug', 0);
            if (isset($_POST['newDocId']))
                $this->set('newDoc', $this->PoDocument->getThisDocument($_POST['newDocId']));
            $this->layout = 'ajax';
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'dossier' . DS . 'work-page');
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.dossier_' . $language, 'work', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Dossier.submenu.' . $languageName . '.action', 'work' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Dossier", true) . " &bull; " . $languageTitle);
        }

    }


    function certificate() {

        // Chargement du modèle
        $this->loadModel('PoCertificate');
        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');
        // Traitement
        if (!empty($this->data)) {
            // Préparation
            if ($this->data['PoCertificate']['certificate_id'] != '0001')
                $this->data['PoCertificate']['certificate_info'] = '';
            if (!empty($this->data['PoCertificate']['id'])) {
                // Mise à jour
                $this->PoCertificate->set($this->data);
            } else {
                // Création
                $this->data['PoCertificate']['portfolio_id'] = $portfolios[$language]['id'];
                $this->PoCertificate->create($this->data);
            }
            // Enregistrement
            $this->PoCertificate->save($this->data, array(
                'validate' => true,
                'fieldlist' => array(
                    'id',
                    'date',
                    'certificate_id',
                    'certificate_info',
                    'organism',
                    'listening',
                    'reading',
                    'speak_int',
                    'speak_prod',
                    'writing'
                )
            ));
        }
        // Chargement des variables de vue
        $this->set('language', $language);
        $competenciesList = $this->PortfolioLists->getCompetenciesList();
        $levelsList = $this->PortfolioLists->getLevelsList();
        $this->set('user', $this->_getCertificates($portfolios[$language]['id'], $competenciesList, $levelsList));
        $this->set("certificates", $this->PoCertificate->Certificate->getCertificatesList($this->Session->read('Auth.User.languages.' . $language)));
        $this->set("competencies", $competenciesList);
        $this->set("levels", $levelsList);
        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            // Pagination Ajax
            Configure::write('debug', 0);
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'dossier' . DS . 'certificate-page');
            return;
        } else {
            $languages = $this->Session->read('Auth.User.languages');
            $languageName = $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'];
            // Maj navigation : cookie + variable de session
            $this->Cookie->write($this->AppAuth->user('id') . '.dossier_' . $language, 'certificate', Configure::read('epel.cookie.encrypt'), Configure::read('epel.cookie.time'));
            $this->Session->write('Menu.Dossier.submenu.' . $languageName . '.action', 'certificate' . DS . urlencode($language));
            // Affichage normal
            $languageTitle = __d('language', $languageName, true);
            $this->set('languageTitle', $languageTitle);
            $this->set('title_for_layout', __("Dossier", true) . " &bull; " . $languageTitle);
        }

    }


    /**
     * Téléversement / mise à jour d'un fichier
     *
     */
    function upload() {

        Configure::write('debug', 0);
        // Chargement de la langue / des ID de portfolios
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');
        // Traitement
        if (!empty($this->data)) {
            $this->PoDocument->set($this->data);
            if ($this->PoDocument->validates()) {
                if ($data = $this->Uploader->upload('filename', array('overwrite' => true))) {
                    // Chargement des données de champ
                    $pathInfo = pathinfo($data['path']);
                    $this->data['PoDocument']['filename'] = $this->PoDocument->fileNameCleaner($this->data['PoDocument']['filename']['name']);
                    $this->data['PoDocument']['type'] = $data['group'];
                    $this->data['PoDocument']['file'] = uniqid('') . '.' . $pathInfo['extension'];
                    $this->data['PoDocument']['size'] = $data['size'];
                    $this->data['PoDocument']['mime'] = $data['type'];
                    // Création du dossier si inexistant
                    $baseDir = 'files' . DS . 'users' . DS . $this->AppAuth->user('id');
                    if (!file_exists($baseDir)) {
                        mkdir($baseDir, 0777, true);
                        chmod($baseDir, 0777);
                    }
                    // Changement de nom du fichier
                    $this->Uploader->rename($data['path'], $baseDir . DS . $this->data['PoDocument']['file'], true);
                    // Journalisation de l'ajout
                    $this->logThis('file_upload', $this->data['PoDocument']['file']);
                    // Enregistrement
                    if (!isset($_POST['id'])) {
                        $this->data['PoDocument']['portfolio_id'] = $portfolios[$language]['id'];
                        $this->data['PoDocument']['kind'] = $_POST['kind'];
                        $this->PoDocument->create($this->data);
                        $this->PoDocument->save($this->data, $validate = false);
                        echo "|response|id=" . $this->PoDocument->getLastInsertID() . "|response|";
                    } else {
                        $this->Uploader->delete('files' . DS . 'users' . DS . $this->AppAuth->user('id') . DS . $_POST['file']);
                        $fileName = $this->data['PoDocument']['filename'];
                        $this->data['PoDocument']['id'] = $_POST['id'];
                        $this->PoDocument->set($this->data);
                        $this->PoDocument->save($this->data, $validate = false);
                        echo "|response|filename=" . $fileName . "|response|";
                    }
                }
            } else
                echo "|response|error=" . $this->PoDocument->validationErrors['filename'] . "|response|";
        }
        $this->autoRender = false;
        exit();

    }


    /**
     * Édition des infos relatives à un document
     *
     */
    function update() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Nom de l'action à appeler
            $actionToCall = (isset($this->params['pass'][1])) ? $this->params['pass'][1] : NULL;
            // Sauvegarde sans maj de la date de dernière modification
            $this->data['PoDocument']['modified'] = false;
            $this->PoDocument->set($this->data);
            $this->PoDocument->save($this->data, array(
                'validate' => false,
                'fieldlist' => array(
                    'level',
                    'description'
                )
            ));
            // Appel de l'action pour rafraîchir la vue
            $this->setAction($actionToCall);
        }

    }


    /**
     * Déplacement du document
     *
     */
    function move() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Nom de l'action à appeler
            $actionToCall = (isset($this->params['pass'][1])) ? $this->params['pass'][1] : NULL;
            // Type du dossier de destination
            $this->data['PoDocument']['kind'] = (isset($this->params['pass'][2])) ? $this->params['pass'][2] : NULL;
            // Id du document
            $this->data['PoDocument']['id'] = (isset($this->params['pass'][3])) ? $this->params['pass'][3] : NULL;
            // Sauvegarde sans maj de la date de dernière modification
            $this->data['PoDocument']['modified'] = false;
            $this->PoDocument->set($this->data);
            $this->PoDocument->save($this->data, array(
                'validate' => false,
                'fieldlist' => array('kind')
            ));
            // Appel de l'action pour rafraîchir la vue
            $this->setAction($actionToCall);
        }

    }


    /**
     * Suppression d'un document
     *
     */
    function delete_doc() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Nom de l'action à appeler
            $actionToCall = (isset($this->params['pass'][1])) ? $this->params['pass'][1] : NULL;
            // Id de l'enregistrement à supprimer
            $idToDelete = (isset($this->params['pass'][2])) ? $this->params['pass'][2] : NULL;
            // Nom du fichier à supprimer
            $fileToDelete = (isset($this->params['pass'][3])) ? $this->params['pass'][3] : NULL;
            // Suppression de l'enregistrement
            $this->PoDocument->delete($idToDelete);
            // Suppression du fichier
            $this->Uploader->delete('files' . DS . 'users' . DS . $this->AppAuth->user('id') . DS . $fileToDelete);
            // Journalisation de la suppression
            $this->logThis('file_delete', $fileToDelete);
            // Appel de l'action pour rafraîchir la vue
            $this->setAction($actionToCall);
        }

    }


    /**
     * Suppression d'un certificat
     *
     */
    function delete_cert() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Id de l'enregistrement à supprimer
            $idToDelete = (isset($this->params['pass'][1])) ? $this->params['pass'][1] : NULL;
            // Chargement du modèle
            $this->loadModel('PoCertificate');
            // Suppression de l'enregistrement
            $this->PoCertificate->delete($idToDelete);
            // Appel de l'action pour rafraîchir la vue
            $this->setAction('certificate');
        }

    }


    /**
     *
     * @return <array>
     */
    private function _getDocuments($portfolioId, $kind = 'show') {

        // Chargement des paramètres
        $this->paginate = array(
            'conditions' => array(
                'PoDocument.portfolio_id' => $portfolioId,
                'PoDocument.kind' => $kind
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'kind',
                'competency',
                'filename',
                'description',
                'type',
                'file',
                'modified'
            ),
            'order' => 'PoDocument.modified DESC',
            'recursive' => -1
        );
        $data = $this->paginate('PoDocument');
        // Mise en forme
        $documents = array();
        foreach ($data as $key => $value) {
            $documents['PoDocument'][$key] = $value['PoDocument'];
        }
        // Envoi de la pagination
        return $documents;

    }


    /**
     *
     * @return <array>
     */
    private function _getCertificates($portfolio, $competencies, $levels) {

        // Chargement des données
        $this->paginate = array(
            'conditions' => array(
                'PoCertificate.portfolio_id' => $portfolio
            ),
            'limit' => 10,
            'fields' => array(
                'id',
                'portfolio_id',
                'certificate_id',
                'date',
                'organism',
                'certificate_info',
                'listening',
                'reading',
                'speak_int',
                'speak_prod',
                'writing'
            ),
            'order' => 'PoCertificate.date DESC',
            'contain' => array(
                'Certificate' => array(
                    'fields' => array(
                        'id',
                        'certification_id'
                    ),
                    'Certification' => array(
                        'fields' => array(
                            'id',
                            'name',
                            'description'
                        )
                    )
                )
            )
        );
        $data = $this->paginate('PoCertificate');
        // Mise en forme
        $certificate = array();
        $date = array();
        foreach ($data as $key => $value) {
            $certificate['PoCertificate'][$key] = $value['PoCertificate'];
            // Niveaux
            $competency = array();
            foreach ($competencies as $compKey => $compValue) {
                if ($certificate['PoCertificate'][$key][$compKey])
                    $competency[] = str_replace(" ", "&nbsp;", $compValue) . __(":", true) . '&nbsp;<span class="bolder infotext">' . $levels[$certificate['PoCertificate'][$key][$compKey]] . '</span>';
            }
            if (!empty($competency))
                $certificate['PoCertificate'][$key]['levels'] = implode("<br />", $competency);
            // Décomposition de la date
            $date = explode('-', $value['PoCertificate']['date']);
            $certificate['PoCertificate'][$key]['day'] = $date[2];
            $certificate['PoCertificate'][$key]['month'] = $date[1];
            $certificate['PoCertificate'][$key]['year'] = $date[0];
            // Certifications
            $certificate['PoCertificate'][$key]['Certificate'] = $value['Certificate'];
        }
        // Envoi
        return $certificate;

    }


}

