<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#adm-teachers',
    'before' => $this->Js->get('#adm-teachers')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="adm-teachers" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="teacher-id"><?php "ID"; ?></th>
                                        <th class="teacher-name"><?php __("Name"); ?></th>
                                        <th class="teacher-quality"><?php __("Quality"); ?></th>
                                        <th class="teacher-university"><?php __("University"); ?></th>
                                        <th class="user-active"><?php __("Activated"); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="6"><?php
                                        if ($this->Session->check('Auth.User.search'))
                                            __("No result matches criteria.");
                                        else
                                            __("No teacher account has currently been created.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="teacher-id"><?php echo $paginator->sort("ID", 'Teacher.id'); ?></th>
                                        <th class="teacher-name"><?php echo $paginator->sort(__("Name", true), 'Teacher.name'); ?></th>
                                        <th class="teacher-quality"><?php echo $paginator->sort(__("Quality", true), 'Teacher.quality'); ?></th>
                                        <th class="teacher-university"><?php echo $paginator->sort(__("University", true), 'Teacher.university_id'); ?></th>
                                        <th class="user-active"><?php echo $paginator->sort(__("Activated", true), 'User.active'); ?></th>
                                        <th class="controls">
                                            <span id="search-init" class="supp"></span>
                                            <a href="#user-search" id="search-new" class="link"></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($user as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>
                                    <tr id="<?php echo $value['Teacher']['id']; ?>" class="<?php echo $trClass; ?>">
                                        <td id="edit-id_<?php echo $value['Teacher']['id']; ?>" class="smaller"><?php
                                        echo $value['Teacher']['id'];
                                        ?></td>
                                        <td id="edit-name_<?php echo $value['Teacher']['id']; ?>"><?php
                                        echo $this->Html->link($value['Teacher']['name'],
                                                array(
                                                    'controller' => 'contact',
                                                    'action' => 'form',
                                                    base64_encode($value['Teacher']['id'] . '_'
                                                        . $value['Teacher']['firstname'] . ' '
                                                        . $value['Teacher']['name'])
                                                ),
                                                array(
                                                    'id' => 'contact-user_' . $value['Teacher']['id'],
                                                    'class' => 'iframe contact link bolder'
                                                )
                                        );
                                        echo '<br />' . $value['Teacher']['firstname'];
                                        ?></td>
                                        <td id="edit-quality_<?php echo $value['Teacher']['id']; ?>" class="smaller"><?php
                                        echo $value['Teacher']['quality'];
                                        ?></td>
                                        <td id="edit-university_<?php echo $value['Teacher']['id']; ?>" class="smaller"><?php
                                        if ((int) $value['University']['id'] < 3)
                                            echo __($value['University']['name']);
                                        else
                                            echo $value['University']['name'];
                                        ?></td>
                                        <td class="user-active"><?php
                                        echo $this->Form->create('User', array(
                                            'url' => '/' . Configure::read('Config.langCode')
                                                . '/admin/user/toggle_active/teacher/' . $value['Teacher']['id'],
                                            'id' => 'form_' . $value['Teacher']['id'],
                                            'class' => 'user-active'
                                            ));
                                        echo $this->Form->checkbox('active', array(
                                            'id' => 'active_' . $value['Teacher']['id'],
                                            'checked' => $value['User']['active']
                                        ));
                                        echo $this->Form->end();
                                        ?></td>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['Teacher']['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['Teacher']['id']; ?>" class="controls">
                                                <a href="#user-edit" id="edit-link_<?php echo $value['Teacher']['id']; ?>" class="edit"></a>
                                            </div>
                                        </td>
                                        <div id="hidden-info_<?php echo $value['Teacher']['id']; ?>" class="hidden">
                                            <span class="name hidden"><?php
                                            echo $value['Teacher']['name'];
                                            ?></span>
                                            <span class="firstname hidden"><?php
                                            echo $value['Teacher']['firstname'];
                                            ?></span>
                                            <span class="university-id hidden"><?php
                                            echo $value['Teacher']['university_id'];
                                            ?></span>
                                            <span class="quality hidden"><?php
                                            echo $value['Teacher']['quality'];
                                            ?></span>
                                            <span class="specialty hidden"><?php
                                            echo $value['Teacher']['specialty'];
                                            ?></span>
                                            <span class="phone hidden"><?php
                                            echo $value['Teacher']['phone'];
                                            ?></span>
                                            <span class="email hidden"><?php
                                            echo $value['User']['email'];
                                            ?></span>
                                            <span class="photo hidden"><?php
                                            echo $value['User']['photo'];
                                            ?></span>
                                        </div>
                                        <div id="data_contact-user_<?php echo $value['Teacher']['id']; ?>" class="tooltip-data"><?php
                                            __("Send an email to");
                                            echo ' ' . $value['Teacher']['firstname'] . ' ' . $value['Teacher']['name'];
                                        ?></div>
                                        <div id="data_edit-link_<?php echo $value['Teacher']['id']; ?>" class="inline tooltip-data"><?php
                                            __("Edit user"); echo ' n°' . $value['Teacher']['id'];
                                        ?></div>
                                    </tr><?php } ?>
                                </tbody>
                            </table><?php
                            echo $this->element('paginate', array('subId' => 'adm-teachers'));
                            } ?>

                            <div id="data_search-new" class="tooltip-data"><?php __("Filter criteria"); ?></div>
                            <div id="data_search-init" class="tooltip-data"><?php __("Reset filter criteria"); ?></div>
                            <div id="data_sort-teacher-id" class="tooltip-data"><?php __("Sort by ID"); ?></div>
                            <div id="data_sort-teacher-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-teacher-firstname" class="tooltip-data"><?php __("Sort by firstname"); ?></div>
                            <div id="data_sort-teacher-quality" class="tooltip-data"><?php __("Sort by quality"); ?></div>
                            <div id="data_sort-teacher-university" class="tooltip-data"><?php __("Sort by College / University"); ?></div>
                            <div id="data_sort-user-active" class="tooltip-data"><?php __("Sort by state"); ?></div>
                            <div class="hidden">
                                <div id="user-edit" class="fancy">
                                    <h2><?php __("User's profile"); ?></h2>
                                    <?php echo $this->Form->create('Teacher', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/admin/user/change_teacher_university',
                                        'id' => 'edit-form',
                                        'class' => ''
                                        ));
                                    ?>

                                    <div id="user-photo"></div>
                                    <fieldset class="half"><?php
                                    echo $this->Form->hidden('id');
                                    echo $this->Form->input('edit_name', array(
                                        'label' => __("Name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_firstname', array(
                                        'label' => __("First name", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_quality', array(
                                        'label' => __("Quality", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_specialty', array(
                                        'label' => __("Specialty", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    </fieldset>
                                    <fieldset class="clear"><?php
                                    echo $this->Form->input('university_id', array(
                                        'label' => __("College / University", true),
                                        'options' => $university,
                                        ));
                                    echo $this->Form->input('edit_phone', array(
                                        'label' => __("Telephone", true),
                                        'readonly'=>'readonly'
                                        ));
                                    echo $this->Form->input('edit_email', array(
                                        'label' => __("eMail address", true),
                                        'readonly'=>'readonly'
                                        ));
                                    ?>

                                    <div id="message">
                                        <p id="info-message" class="info"></p>
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Save", true)); ?>
                                </div>
                                <div id="user-search" class="fancy">
                                    <h2><?php __("Filter criteria"); ?></h2>
                                    <?php echo $this->Form->create('Teacher', array(
                                        'url' => '/' . Configure::read('Config.langCode')
                                            . '/admin/user/teacher',
                                        'id' => 'search-form',
                                        'class' => ''
                                        ));
                                    echo $validation->bind('Teacher'); // Validation jQuery
                                    if ($this->Session->check('Auth.User.search.Teacher'))
                                        $default = $this->Session->read('Auth.User.search.Teacher');
                                    else
                                        $default = array(
                                            'name' => '',
                                            'firstname' => '',
                                            'university_id' => ''
                                        );
                                    ?>

                                    <fieldset><?php
                                    echo $this->Form->input('name', array(
                                        'label' => __("Name", true),
                                        'default' => $default['name']
                                        ));
                                    echo $this->Form->input('firstname', array(
                                        'label' => __("First name", true),
                                        'default' => $default['firstname']
                                        ));
                                    echo $this->Form->input('university_id', array(
                                        'label' => __("College / University", true),
                                        'options' => $university,
                                        'empty' => array(__("All universities", true)),
                                        'default' => $default['university_id']
                                        )); ?>

                                    <div id="message">
                                    </div>
                                    </fieldset>
                                    <?php echo $this->Form->end(__("Filter", true)); ?>
                                </div>
                            </div>
                        </div>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.ibutton',
        'jquery.form',
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

// État initial du picto d'initialisation
$initState = '';
if ($this->Session->check('Auth.User.search.Teacher')) {
    $initState = "    $('#search-init').css('display', 'inline-block');\n";
}

$this->Js->Buffer("
    // iButton
    $('form.user-active input[type=checkbox]')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            var extract = new Array();
            extract = $(this).attr('id').split('_');
            var userId = extract[1];
            $(this).closest('form').ajaxSubmit({
                beforeSubmit: function() {
                    $('#control_' + userId).hide();
                    $('#loading_' + userId).show();
                },
                success: function() {
                    $('#loading_' + userId).hide();
                    $('#control_' + userId).show();
                }
            })
    });
    // Id, classes et cie
    $('#adm-teachers table thead tr th.teacher-id a').attr('id', 'sort-teacher-id').addClass('sort-help');
    $('#adm-teachers table thead tr th.teacher-name a').attr('id', 'sort-teacher-name').addClass('sort-help');
    $('#adm-teachers table thead tr th.teacher-quality a').attr('id', 'sort-teacher-quality').addClass('sort-help');
    $('#adm-teachers table thead tr th.teacher-university a').attr('id', 'sort-teacher-university').addClass('sort-help');
    $('#adm-teachers table thead tr th.user-active a').attr('id', 'sort-user-active').addClass('sort-help');
    // Init. pictos
" . $initState . "
    $('<div class=\"loading\"></div>').insertAfter('#search-form div.submit input[type=submit]');
    $('<div class=\"loading\"></div>').insertAfter('#edit-form div.submit input[type=submit]');
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Raz critères de recherche
    $('#adm-teachers table thead tr th.controls span#search-init').unbind('click').bind('click', function() {
        $('#divToolTip').fadeOut('fast');
        $('#title-adm-teachers').html('" . addslashes(__("Loading…", true)) . "');
        $('#adm-teachers').fadeTo('fast', 0.6);
        $('#adm-teachers').load('/" . $this->Session->params['language'] . "/admin/user/init_criteria/teacher');
    });
    // Formulaire de recherche
    $('#search-new').fancybox({
        'width'             :   450,
        'height'            :   270,
        'autoDimensions'    :   false,
        'padding'           :   0,
        'margin'            :   0,
        'speedIn'           :   900,
        'scrolling'         :   'no',
        'centerOnScroll'    :   true,
        'overlayColor'      :   '#000',
        'overlayOpacity'    :   0.3,
        'onStart'           :   function() {
                                    $('div#divToolTip').fadeOut();
                                },
        'onComplete'        :   function() {
                                    // Soumission Ajax
                                    $('#search-form').ajaxForm({
                                        target:         '#adm-teachers',
                                        beforeSubmit:   function() {
                                            if (!$('#search-form .error').length) {
                                                $('#title-adm-teachers').html('" . addslashes(__("Loading…", true)) . "');
                                                $('#adm-teachers').fadeTo('fast', 0.6);
                                            } else
                                                return false;
                                        },
                                        success:        function() {
                                            $.fancybox.close();
                                        }
                                    });
                                }
    });
    // Sélection d'une université
    $('#edit-form #TeacherUniversityId').change(function() {
        if ($('#edit-form #TeacherUniversityId option:selected').val() != $('#hidden-info_' + $('#edit-form #TeacherId').val() + ' .university-id').text()) {
            $('p#info-message').attr('class', 'info')
                    .html('<span class=\"tip-info\"></span>"
                    . addslashes(__("You can only change this teacher's university.", true)) . "');
            $('#edit-form input[type=submit]').removeAttr('disabled');
        } else
            $('#edit-form input[type=submit]').attr('disabled', 'disabled');
    });
    // Loading…
    $('#search-form div.submit input[type=submit]').unbind('click').bind('click', function() {
        // Masquage si erreur de saisie -> jquery.validation.js lignes 77-80
        $('#search-form div.submit div.loading').show();
    });
    // Formulaire d'édition
    $('a.edit').each(function(){
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var userId = extract[1];
        $(this).fancybox({
            'width'             :   450,
            'height'            :   430,
            'autoDimensions'    :   false,
            'padding'           :   0,
            'margin'            :   0,
            'speedIn'           :   900,
            'scrolling'         :   'no',
            'centerOnScroll'    :   true,
            'overlayColor'      :   '#000',
            'overlayOpacity'    :   0.3,
            'onStart'           :   function() {
                                        $('div#divToolTip').fadeOut();
                                        // Photo
                                        var userPhoto = $('#hidden-info_' + userId + ' .photo').text();
                                        if (userPhoto) {
                                            var photoUrl = 'url(/files/users/' + userId + '/photo/' + userPhoto + ')';
                                            $('#edit-form #user-photo').css('background-image', photoUrl);
                                        }
                                        // Messages
                                        $('#edit-form p#info-message').attr('class', 'info')
                                                .html('<span class=\"tip-info\"></span>" . addslashes(__("You can only change this teacher's university.", true)) . "');
                                        // ID user
                                        $('#edit-form #TeacherId').val(userId);
                                        // Initialisation des éléments du formulaire
                                        $('#edit-form #TeacherEditName').val($('#hidden-info_' + userId + ' .name').text());
                                        $('#edit-form #TeacherEditFirstname').val($('#hidden-info_' + userId + ' .firstname').text());
                                        $('#edit-form #TeacherEditQuality').val($('#edit-quality_' + userId).text());
                                        $('#edit-form #TeacherEditSpecialty').val($('#hidden-info_' + userId + ' .specialty').text());
                                        $('#edit-form #TeacherEditPhone').val($('#hidden-info_' + userId + ' .phone').text());
                                        $('#edit-form #TeacherEditEmail').val($('#hidden-info_' + userId + ' .email').text());
                                        $('#edit-form #TeacherUniversityId option[value=\'' + $('#hidden-info_' + userId + ' .university-id').text() + '\']').attr('selected', 'selected');
                                        $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
                                    },
            'onClosed'          :   function() {
                                        // Raz des éléments du formulaire
                                        $('#edit-form #user-photo').css('background-image', 'url(/img/user.png)');
                                        $('#edit-form #TeacherId').val('');
                                        $('#edit-form #TeacherEditName').val('');
                                        $('#edit-form #TeacherEditFirstname').val('');
                                        $('#edit-form #TeacherEditQuality').val('');
                                        $('#edit-form #TeacherEditSpecialty').val('');
                                        $('#edit-form #TeacherEditPhone').val('');
                                        $('#edit-form #TeacherEditEmail').val('');
                                    }
        });
    });
    // Changement d'université (Ajax)
    $('#edit-form').submit(function() {
        $(this).ajaxSubmit({
            beforeSubmit: function() {
                $('#edit-form div.loading').show();
                $('#edit-form div.submit input[type=submit]').attr('disabled', 'disabled');
            },
            success: function() {
                var userId = $('#edit-form #TeacherId').val();
                var newUniversityId = $('#edit-form #TeacherUniversityId option:selected').val();
                // Maj DOM
                $('#adm-teachers #edit-university_' + userId).text($('#edit-form #TeacherUniversityId option:selected').text());
                $('#hidden-info_' + userId + ' .university-id').text(newUniversityId);
                // Affichage
                $('#edit-form p#info-message').attr('class', 'alert')
                    .html('" . addslashes(__("This user's university has been updated.", true)) . "');
                $('#edit-form div.loading').hide();
            }
        });
        return false;
    });
    // Tooltips
    tooltipSortHelp('#adm-teachers table thead tr th');
    tooltipLink('#adm-teachers table');
    tooltipSupp('#adm-teachers table thead tr th.controls');
    tooltipEdit('#adm-teachers table tbody tr td div.controls');
    // Tips
    addTipAlert('#message');
    addTipInfo('#message');
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    if (empty($user))
        $this->Js->Buffer("
    // Rétablissement de l'opacité
    $('#adm-teachers').fadeTo('slow', 1);
");
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-adm-teachers').html('" . addslashes(__("Teachers' accounts", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>