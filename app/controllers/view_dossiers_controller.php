<?php

/**
 * {app}/controllers/view_dossiers_controller.php
 *
 */
class ViewDossiersController extends AppController {

    var $name = 'ViewDossiers';
    var $uses = array('Portfolio');
    // Composants
    var $components = array(
        'PortfolioLists',
        'L10nSort'
    );
    // Assistants
    var $helpers = array(
        'Menu',
        'Date'
    );


    function beforeFilter() {

        // Pas de variable de session = déconnexion
        if(!$this->Session->check('Auth.User.view'))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // Eject
        $this->redirect(array(
            'language' => $this->params['language'],
            'controller' => 'users',
            'action' => 'logout'
        ));

    }


    function show() {

        // Chargement de la langue en cours
        $language = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
        $portfolios = $this->Session->read('Auth.User.view.portfolios');

        // Validité de l'url
        if (!array_key_exists($language, $portfolios))
            $this->redirect(array(
                'language' => $this->params['language'],
                'controller' => 'users',
                'action' => 'logout'
            ));

        // Données de vue
        $this->set('user', $this->Portfolio->getDossier($portfolios[$language]['id'],
                ($this->Session->read('Auth.User.role') == 'visitor')));
        $this->set('compNames', $this->PortfolioLists->getCompetenciesNames());
        $this->set('competencies', $this->PortfolioLists->getCompetenciesList());
        $this->set('levels', $this->PortfolioLists->getLevelsList());
        $this->set('mother', $portfolios[$language]['mother']);
        $this->set('usual', $portfolios[$language]['usual']);
        $this->set('title_for_layout', __("Dossier", true) . " &bull; "
                . __d('language', $portfolios[$language]['name'], true));

        // Données de vue : affichage des langues du Portfolio
        $portfolios = $this->Session->read('Auth.User.view.portfolios');
        $languages = array();
        foreach ($portfolios as $key => $value) {
            $languages[$value['name']]['mother'] = $value['mother'];
            $languages[$value['name']]['usual'] = $value['usual'];
        }
        $this->set('languages', $this->L10nSort->sortLocalized($languages));

    }


}

