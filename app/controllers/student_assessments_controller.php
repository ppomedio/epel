<?php

/**
 * {app}/controllers/student_assessments_controller.php
 *
 */
class StudentAssessmentsController extends AppController {

    var $name = 'StudentAssessments';
    // Modèle
    var $uses = array(
        'AssessItem',
        'PoAssessAnswer',
        'PoAssessCriteria',
        'PoAssessUser'
    );
    // Composants
    var $components = array(
        'SideMenu'
    );
    // Assistants
    var $helpers = array(
        'Menu'
    );


    function beforeFilter() {

        // Chargement de la langue en cours
        $portfolioLanguage = (isset($this->params['pass'][0])) ? urldecode($this->params['pass'][0]) : NULL;
        Configure::write('portfolioLanguage', $portfolioLanguage);

        // Contrôle de cohérence / menu latéral (hors Ajax)
        if (!$this->RequestHandler->isAjax()) {
            // Si portfolio vide ou langue demandée non ouverte : redirection
            $portfolios = $this->Session->read('Auth.User.portfolios');
            if (empty($portfolios) || empty($portfolioLanguage) || ($portfolioLanguage && !array_key_exists($portfolioLanguage, $portfolios)))
                $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'base');
            // Chargement du niveau / retrait de sa mention dans l'url
            $level = (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL;
            if ($level) {
                $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'self-assessment' . DS . $this->params['action'] . DS . $portfolioLanguage);
            }
            // Menu latéral
            $this->SideMenu->setSideMenu('Self-assessment');
        }

        // Retour…
        parent::beforeFilter();

    }


    function index() {

        // URL incomplète en …/student/self-assessment
        $this->redirect(DS . $this->params['language'] . DS . 'student' . DS . 'base');

    }


    function listen() {

        $this->setAction('assessment', 'listen', (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL);

    }


    function read() {

        $this->setAction('assessment', 'read', (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL);

    }


    function converse() {

        $this->setAction('assessment', 'converse', (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL);

    }


    function speak() {

        $this->setAction('assessment', 'speak', (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL);

    }


    function write() {

        $this->setAction('assessment', 'write', (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL);

    }


    function assessment($competency, $level) {

        // Ajax
        if ($this->RequestHandler->isAjax())
            Configure::write('debug', 0);

        // Chargement des ID de portfolios, de la langue et du niveau demandé
        $language = Configure::read('portfolioLanguage');
        $portfolios = $this->Session->read('Auth.User.portfolios');
        if (empty($level)) {
            $cookie = $this->Cookie->read($this->AppAuth->user('id') . '.self-assessment_' . $language . '_' . $competency);
            if ($cookie) {
                $level = $cookie;
            } else
                $level = 'A1';
        }

        // Maj navigation : cookies + maj menu (variable de session)
        $encrypt = Configure::read('epel.cookie.encrypt');
        $time = Configure::read('epel.cookie.time');
        $this->Cookie->write($this->AppAuth->user('id') . '.self-assessment_' . $language, $competency, $encrypt, $time);
        $this->Cookie->write($this->AppAuth->user('id') . '.self-assessment_' . $language . '_' . $competency, $level, $encrypt, $time);
        $this->Cookie->write($this->AppAuth->user('id') . '.lastpage', urlencode('/student/self-assessment/' . $competency . '/' . $language . '/' . $level), $encrypt, $time);
        $this->Session->write('Menu.Self-assessment.submenu.' . ucfirst($language) . '.action', $competency . DS . urlencode($language));

        // Chargement des variables de vue
        $this->set('language', $language);
        $languages = $this->Session->read('Auth.User.languages');
        $languageTitle = __d('language', $languages[str_pad((string) $languages[$language], 4, '0', STR_PAD_LEFT)]['name'], true);
        $this->set('languageTitle', $languageTitle);
        $this->set('competencies', array(
            'listen' => "Listening",
            'read' => "Reading",
            'converse' => "Spoken interaction",
            'speak' => "Spoken production",
            'write' => "Writing"
        ));
        $this->set('competency', $competency);
        $this->set('level', $level);
        $this->set('items', $this->AssessItem->getAssessItems($competency, $level));

        // Traitement
        if (!empty($this->data)) {
            // PoAssessAnswer
            if (!empty($this->data['PoAssessAnswer'])) {
                // Préparation
                $this->data['PoAssessAnswer']['portfolio_id'] = $portfolios[$language]['id'];
                $this->data['PoAssessAnswer']['competency'] = $competency;
                if (!empty($this->data['PoAssessAnswer']['id'])) {
                    // Mise à jour
                    $this->PoAssessAnswer->set($this->data);
                } else {
                    // Création
                    $this->PoAssessAnswer->create($this->data);
                }
                // Enregistrement
                $this->PoAssessAnswer->save($this->data, $validate = false);
            }
            // PoAssessCriteria
            if (!empty($this->data['PoAssessCriteria'])) {
                // Préparation
                $this->data['PoAssessCriteria']['portfolio_id'] = $portfolios[$language]['id'];
                $this->data['PoAssessCriteria']['competency'] = $competency;
                $this->data['PoAssessCriteria']['level'] = $level;
                if ($this->data['PoAssessCriteria']['criteria_set'] != '3') {
                    $this->data['PoAssessCriteria']['criteria1'] = '';
                    $this->data['PoAssessCriteria']['criteria2'] = '';
                    $this->data['PoAssessCriteria']['criteria3'] = '';
                }
                if (!empty($this->data['PoAssessCriteria']['id'])) {
                    // Mise à jour
                    $this->PoAssessCriteria->set($this->data);
                } else {
                    // Création
                    $this->PoAssessCriteria->create($this->data);
                }
                // Enregistrement
                $this->PoAssessCriteria->save($this->data, $validate = false);
            }
            // PoAssessUser
            if (!empty($this->data['PoAssessUser'])) {
                // Préparation
                foreach ($this->data['PoAssessUser'] as $key => $value) {
                    foreach ($value['date'] as $subKey => $subValue) {
                        $this->data['PoAssessUser'][$key]['date'. $subKey] = $subValue;
                    }
                    unset($this->data['PoAssessUser'][$key]['date']);
                }
                // Enregistrement
                $this->PoAssessUser->saveAll($this->data['PoAssessUser']);
            }
        }

        // Chargement / rechargement des données
        $assessAnswer = $this->PoAssessAnswer->getAssessAnswers($portfolios[$language]['id'], $competency, $level);
        $assessUser = $this->PoAssessUser->getAssessUserData($portfolios[$language]['id'], $competency, $level);
        $assessCriteria = $this->PoAssessCriteria->getAssessCriteria($portfolios[$language]['id'], $competency, $level);
        if (!empty($assessAnswer) && !empty($assessUser))
            $this->data = array_merge($assessAnswer, $assessUser);
        else if (!empty($assessAnswer))
            $this->data = $assessAnswer;
        else if (!empty($assessUser))
            $this->data = $assessUser;
        if (!empty($assessCriteria))
            $this->data = array_merge($this->data, $assessCriteria);

        // Bascule suivant le type de rendu
        if ($this->RequestHandler->isAjax()) {
            $this->set('ajax', true);
            $this->render(DS . 'elements' . DS . 'student' . DS . 'assessment' . DS . 'assessment-form');
            return;
        } else {
            // Copyright CercleS
            $addCopyright['link'] = "CercleS";
            $addCopyright['url'] = Configure::read('epel.cercles.url');
            $this->set("addCopyright", $addCopyright);
            // Titre de la page
            $this->set('title_for_layout', __("Self-assessment", true) . " &bull; " . __d('language', $languageTitle, true));
        }

    }


    /**
     * Création / édition d'un item
     *
     */
    function item() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            if (!empty($this->data)) {
                // Chargement des ID de portfolios / langue / compétence / niveau
                $portfolios = $this->Session->read('Auth.User.portfolios');
                $language = Configure::read('portfolioLanguage');
                $competency = (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL;
                $level = (isset($this->params['pass'][2])) ? urldecode($this->params['pass'][2]) : NULL;
                // Préparation
                $this->data['PoAssessUser']['portfolio_id'] = $portfolios[$language]['id'];
                // Création / mise à jour
                if (!empty($this->data['PoAssessUser']['id']))
                    $this->PoAssessUser->set($this->data);
                else
                    $this->PoAssessUser->create($this->data);
                // Enregistrement
                if ($this->PoAssessUser->save($this->data, $validate = false)) {
                    // Init.
                    $this->data = array();
                    // Sortie
                    return $this->setAction('assessment', $competency, $level);
                } else {
                    // Init.
                    $this->data = array();
                }
            }
        }

    }


    /**
     * Suppression d'un item
     *
     */
    function delete() {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            // Chargement compétence / niveau / ID item
            $competency = (isset($this->params['pass'][1])) ? urldecode($this->params['pass'][1]) : NULL;
            $level = (isset($this->params['pass'][2])) ? urldecode($this->params['pass'][2]) : NULL;
            $userAssessID = (isset($this->params['pass'][3])) ? urldecode($this->params['pass'][3]) : NULL;
            // Suppression de l'enregistrement
            $this->PoAssessUser->delete($userAssessID);
            // Appel de l'action pour rafraîchir la vue
            return $this->setAction('assessment', $competency, $level);
        }

    }


}

