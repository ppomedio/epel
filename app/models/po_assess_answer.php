<?php

/**
 * {app}/models/po_assess_answer.php
 */
class PoAssessAnswer extends AppModel {

    var $name = 'PoAssessAnswer';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    function getAssessAnswers($portfolioId, $competency, $level = false) {

        // Champs
        switch ($level) {
            case 'A1':
                $fields = array(
                    'id',
                    'A1001', 'A1101', 'A1201', 'A1301', 'A1401',
                    'A1002', 'A1102', 'A1202', 'A1302', 'A1402',
                    'A1003', 'A1103', 'A1203', 'A1303', 'A1403',
                    'A1004', 'A1104', 'A1204', 'A1304', 'A1404',
                    'A1005', 'A1105', 'A1205', 'A1305', 'A1405',
                    'A1006', 'A1106', 'A1206', 'A1306', 'A1406',
                    'A1007', 'A1107', 'A1207', 'A1307', 'A1407',
                    'A1008', 'A1108', 'A1208', 'A1308', 'A1408',
                    'A1009', 'A1109', 'A1209', 'A1309', 'A1409',
                    'A1010', 'A1110', 'A1210', 'A1310', 'A1410',
                    'A1011', 'A1111', 'A1211', 'A1311', 'A1411',
                    'A1012', 'A1112', 'A1212', 'A1312', 'A1412',
                    'A1013', 'A1113', 'A1213', 'A1313', 'A1413',
                );
                break;
            case 'A2':
                $fields = array(
                    'id',
                    'A2001', 'A2101', 'A2201', 'A2301', 'A2401',
                    'A2002', 'A2102', 'A2202', 'A2302', 'A2402',
                    'A2003', 'A2103', 'A2203', 'A2303', 'A2403',
                    'A2004', 'A2104', 'A2204', 'A2304', 'A2404',
                    'A2005', 'A2105', 'A2205', 'A2305', 'A2405',
                    'A2006', 'A2106', 'A2206', 'A2306', 'A2406',
                    'A2007', 'A2107', 'A2207', 'A2307', 'A2407',
                    'A2008', 'A2108', 'A2208', 'A2308', 'A2408',
                    'A2009', 'A2109', 'A2209', 'A2309', 'A2409',
                    'A2010', 'A2110', 'A2210', 'A2310', 'A2410',
                    'A2011', 'A2111', 'A2211', 'A2311', 'A2411',
                    'A2012', 'A2112', 'A2212', 'A2312', 'A2412',
                    'A2013', 'A2113', 'A2213', 'A2313', 'A2413',
                );
                break;
            case 'B1':
                $fields = array(
                    'id',
                    'B1001', 'B1101', 'B1201', 'B1301', 'B1401',
                    'B1002', 'B1102', 'B1202', 'B1302', 'B1402',
                    'B1003', 'B1103', 'B1203', 'B1303', 'B1403',
                    'B1004', 'B1104', 'B1204', 'B1304', 'B1404',
                    'B1005', 'B1105', 'B1205', 'B1305', 'B1405',
                    'B1006', 'B1106', 'B1206', 'B1306', 'B1406',
                    'B1007', 'B1107', 'B1207', 'B1307', 'B1407',
                    'B1008', 'B1108', 'B1208', 'B1308', 'B1408',
                    'B1009', 'B1109', 'B1209', 'B1309', 'B1409',
                    'B1010', 'B1110', 'B1210', 'B1310', 'B1410',
                    'B1011', 'B1111', 'B1211', 'B1311', 'B1411',
                    'B1012', 'B1112', 'B1212', 'B1312', 'B1412',
                    'B1013', 'B1113', 'B1213', 'B1313', 'B1413',
                );
                break;
            case 'B2':
                $fields = array(
                    'id',
                    'B2001', 'B2101', 'B2201', 'B2301', 'B2401',
                    'B2002', 'B2102', 'B2202', 'B2302', 'B2402',
                    'B2003', 'B2103', 'B2203', 'B2303', 'B2403',
                    'B2004', 'B2104', 'B2204', 'B2304', 'B2404',
                    'B2005', 'B2105', 'B2205', 'B2305', 'B2405',
                    'B2006', 'B2106', 'B2206', 'B2306', 'B2406',
                    'B2007', 'B2107', 'B2207', 'B2307', 'B2407',
                    'B2008', 'B2108', 'B2208', 'B2308', 'B2408',
                    'B2009', 'B2109', 'B2209', 'B2309', 'B2409',
                    'B2010', 'B2110', 'B2210', 'B2310', 'B2410',
                    'B2011', 'B2111', 'B2211', 'B2311', 'B2411',
                    'B2012', 'B2112', 'B2212', 'B2312', 'B2412',
                    'B2013', 'B2113', 'B2213', 'B2313', 'B2413',
                );
                break;
            case 'C1':
                $fields = array(
                    'id',
                    'C1001', 'C1101', 'C1201', 'C1301', 'C1401',
                    'C1002', 'C1102', 'C1202', 'C1302', 'C1402',
                    'C1003', 'C1103', 'C1203', 'C1303', 'C1403',
                    'C1004', 'C1104', 'C1204', 'C1304', 'C1404',
                    'C1005', 'C1105', 'C1205', 'C1305', 'C1405',
                    'C1006', 'C1106', 'C1206', 'C1306', 'C1406',
                    'C1007', 'C1107', 'C1207', 'C1307', 'C1407',
                    'C1008', 'C1108', 'C1208', 'C1308', 'C1408',
                    'C1009', 'C1109', 'C1209', 'C1309', 'C1409',
                    'C1010', 'C1110', 'C1210', 'C1310', 'C1410',
                    'C1011', 'C1111', 'C1211', 'C1311', 'C1411',
                    'C1012', 'C1112', 'C1212', 'C1312', 'C1412',
                    'C1013', 'C1113', 'C1213', 'C1313', 'C1413',
                );
                break;
            case 'C2':
                $fields = array(
                    'id',
                    'C2001', 'C2101', 'C2201', 'C2301', 'C2401',
                    'C2002', 'C2102', 'C2202', 'C2302', 'C2402',
                    'C2003', 'C2103', 'C2203', 'C2303', 'C2403',
                    'C2004', 'C2104', 'C2204', 'C2304', 'C2404',
                    'C2005', 'C2105', 'C2205', 'C2305', 'C2405',
                    'C2006', 'C2106', 'C2206', 'C2306', 'C2406',
                    'C2007', 'C2107', 'C2207', 'C2307', 'C2407',
                    'C2008', 'C2108', 'C2208', 'C2308', 'C2408',
                    'C2009', 'C2109', 'C2209', 'C2309', 'C2409',
                    'C2010', 'C2110', 'C2210', 'C2310', 'C2410',
                    'C2011', 'C2111', 'C2211', 'C2311', 'C2411',
                    'C2012', 'C2112', 'C2212', 'C2312', 'C2412',
                    'C2013', 'C2113', 'C2213', 'C2313', 'C2413',
                );
                break;
            default:
                $fields = array();
        }

        // Chargement / retour des données
        return $this->find('first', array(
            'conditions' => array(
                'PoAssessAnswer.portfolio_id' => $portfolioId,
                'PoAssessAnswer.competency' => $competency
            ),
            'fields' => $fields,
            'recursive' => -1
        ));

    }


}

