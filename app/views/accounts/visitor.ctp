
        <h2><?php echo __("Account creation", true) . ' &bull; ' . __("Visitor", true); ?></h2>
        <?php
        echo $this->Form->create('Visitor', array(
            'url' => '/' . Configure::read('Config.langCode')
                . '/account/visitor',
            'id' => 'profile-form',
            #'class' => ''
            ));
        echo $validation->bind('Visitor'); // Validation jQuery
        ?>

        <fieldset>
        <?php
        echo $this->Form->input('name', array(
            'label' => __("Name", true)
            ));
        echo $this->Form->input('firstname', array(
            'label' => __("First name", true)
            ));
        echo $this->Form->input('function', array(
            'label' => __("Function", true)
            ));
        echo $this->Form->input('structure', array(
            'label' => __("Structure", true)
            ));
        echo $this->Form->input('phone', array(
            'label' => __("Telephone", true)
            ));
        echo $this->Form->input('address', array(
            'label' => __("Address", true)
            ));
        echo $this->Form->input('country_id', array(
            'label' => __("Country", true),
            'options' => $country,
            'empty' => "…"
            ));
        ?>

        </fieldset>
        <?php echo $this->Form->end(__("Save", true)); ?>

<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Redimensionnement
    parent.$('#fancybox-inner').height(550);
    parent.$('#fancybox-wrap').height(570);
    parent.$('#fancybox-content').height(570);
    parent.$.fancybox.center();
    // Tips
    addTipInfo('#profile-form');
");

?>
