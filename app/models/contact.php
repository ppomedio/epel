<?php

/**
 * {app}/models/contact.php
 */
class Contact extends AppModel {

    var $name = 'Contact';
    // Ce modèle n'utilise pas de table dans la base
    #var $useDbConfig = 'epel';
    var $useTable = false;
    // Init. du tableau de validation
    var $validate = array();
    // Structure de données utilisée à la place
    var $_schema = array(
        'name' => array(
            'type' => 'string',
            'length' => 60
        ),
        'firstname' => array(
            'type' => 'string',
            'length' => 40
        ),
        'email' => array(
            'type' => 'string',
            'length' => 128
        ),
        'message' => array(
            'type' => 'text'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                'required' => true,
                'allowEmpty' => false,
                'message' => __d('validation', "Please enter your name.", true)
            ),
            'firstname' => array(
                'rule' => array('custom', '/^[a-zA-Z \'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                #'rule' => array('custom', '/^[^0-9\[\]\{\}\(\)\^\+\*\?\$\|\/\.\\\\_<>&¶#@§•°~¨%‰‱!¡¿¥£€"“”«»=≈≠±∞÷,;:…\t]+$/'),
                'required' => true,
                'allowEmpty' => false,
                'message' => __d('validation', "Please enter your first name.", true)
            ),
            'email' => array(
                'rule' => 'email',
                'required' => true,
                'allowEmpty' => false,
                'message' => __d('validation', "This email adress is not valid.", true)
            ),
            'message' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'required' => true,
                    'message' => __d('validation', "Why not type a message?", true),
                    'last' => true
                ),
                'format' => array(
                    'rule' => array('custom', '/^[a-zA-Z0-9 \t\r\n\/(),;\:\.…"\?\!\'\-àáâãäåāąăæçćčĉċďđèéêëēęěĕėƒĝğġģĥħìíîïīĩĭįıĳĵķĸłľĺļŀñńňņŉŋòóôõöøōőŏœŕřŗśšşŝșťţŧțùúûüūůűŭũųŵýÿŷžżźÀÁÂÃÄÅĀĄĂÆÇĆČĈĊĎĐÈÉÊËĒĘĚĔĖĜĞĠĢĤĦÌÍÎÏĪĨĬĮİĲĴĶŁĽĹĻĿÑŃŇŅŊÒÓÔÕÖØŌŐŎŒŔŘŖŚŠŞŜȘŤŢŦȚÙÚÛÜŪŮŰŬŨŲŴÝŶŸŹŽŻ]+$/'),
                    #'rule' => array('custom', '/^[^<>]+$/'),
                    'message' => __d('validation', "Some characters are not allowed…", true)
                )
            )
        );

        parent::__construct();

    }


}

