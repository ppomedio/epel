
                <article>
                    <h1 id="title-search-page" class="spaced"><?php __("Students"); ?></h1>
                    <section><?php
                        echo $this->element('visitor/search/students-page'); ?>

                    </section>

                </article>
                <aside><?php
                    echo $this->element('visitor/search/how-to-use', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>