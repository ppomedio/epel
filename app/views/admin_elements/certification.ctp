
                <article class="half">
                    <section>
                        <h2 id="title-elements-list"><?php __("Certifications"); ?></h2>
                        <?php echo $this->element('admin/element/certifications-page'); ?>

                    </section>
                </article>
                <article class="half">
                    <section>
                        <h2 id="title-elements-form"><?php __("Add"); ?></h2>
                        <?php echo $this->Form->create('Certification', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/admin/element/certification',
                            'id' => 'elements-form',
                            'class' => ''
                            ));
                        echo $validation->bind('Certification'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('name', array(
                            'label' => __("Name", true)
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __("Description", true)
                            ));
                        ?>

                        </fieldset>
                        <p class="info"><?php __("Descriptions of certifications are not translated. If the certification applies to only one language, you may write the description in this language."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('admin/element/how-to-use');
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#elements-form');
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#elements-form div.submit input[type=submit]');
    // Soumission du formulaire
    $('#elements-form').submit(function() {
        if (!$('#elements-form .error').length)
            $('#elements-form div.loading').show();
        else
            return false;
    });
");

?>
