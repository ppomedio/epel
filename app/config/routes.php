<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */


/**
 * Index
 */
Router::connect('/', array('language' => Configure::read('Config.langage'), 'controller' => 'users', 'action' => 'login'), array('language'=>'[a-z]{2}'));
Router::connect('/:language', array('controller' => 'users', 'action' => 'login'), array('language'=>'[a-z]{2}'));

/**
 * Utilisateurs
 */
Router::connect('/:language/user/:action/*', array('controller' => 'users'), array('language'=>'[a-z]{2}'));
// Étudiants
Router::connect('/:language/student/base/language/add', array('controller' => 'student_bases', 'action' => 'language_add'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/base/language/delete', array('controller' => 'student_bases', 'action' => 'language_delete'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/base/:action/*', array('controller' => 'student_bases'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/base/*', array('controller' => 'student_bases', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/passport/:action/*', array('controller' => 'student_passports'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/passport/*', array('controller' => 'student_passports', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/biography/:action/*', array('controller' => 'student_biographies'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/biography/*', array('controller' => 'student_biographies', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/dossier/:action/*', array('controller' => 'student_dossiers'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/dossier/*', array('controller' => 'student_dossiers', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/self-assessment/:action/*', array('controller' => 'student_assessments'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/self-assessment/*', array('controller' => 'student_assessments', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/visit/:action/*', array('controller' => 'student_visits'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/student/visit/*', array('controller' => 'student_visits', 'action' => 'index'), array('language'=>'[a-z]{2}'));
// Enseignants
Router::connect('/:language/teacher/base/:action/*', array('controller' => 'teacher_bases'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/teacher/base/*', array('controller' => 'teacher_bases', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/teacher/group/:action/*', array('controller' => 'teacher_groups'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/teacher/group/*', array('controller' => 'teacher_groups', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/teacher/student/:action/*', array('controller' => 'teacher_students'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/teacher/student/*', array('controller' => 'teacher_students', 'action' => 'index'), array('language'=>'[a-z]{2}'));
// Visiteurs
Router::connect('/:language/visitor/base/:action/*', array('controller' => 'visitor_bases'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/visitor/base/*', array('controller' => 'visitor_bases', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/visitor/student/:action/*', array('controller' => 'visitor_students'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/visitor/student/*', array('controller' => 'visitor_students', 'action' => 'index'), array('language'=>'[a-z]{2}'));
// Admin
Router::connect('/:language/admin/base/:action/*', array('controller' => 'admin_bases'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/admin/base/*', array('controller' => 'admin_bases', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/admin/user/:action/*', array('controller' => 'admin_users'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/admin/user/*', array('controller' => 'admin_users', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/admin/element/:action/*', array('controller' => 'admin_elements'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/admin/element/*', array('controller' => 'admin_elements', 'action' => 'index'), array('language'=>'[a-z]{2}'));

/**
 * Vues du Portfolio (enseignants / visiteurs)
 */
Router::connect('/:language/view/student/:action/*', array('controller' => 'view_students'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/student/*', array('controller' => 'view_students', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/passport/:action/*', array('controller' => 'view_passports'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/passport/*', array('controller' => 'view_passports', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/biography/:action/*', array('controller' => 'view_biographies'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/biography/*', array('controller' => 'view_biographies', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/dossier/:action/*', array('controller' => 'view_dossiers'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/dossier/*', array('controller' => 'view_dossiers', 'action' => 'index'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/visit/:action/*', array('controller' => 'view_visits'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/view/visit/*', array('controller' => 'view_visits', 'action' => 'index'), array('language'=>'[a-z]{2}'));

/**
 * Comptes
 */
Router::connect('/:language/account/:action/*', array('controller' => 'accounts'), array('language'=>'[a-z]{2}'));

/**
 * Contact
 */
Router::connect('/:language/contact/:action/*', array('controller' => 'contacts'), array('language'=>'[a-z]{2}'));

/**
 * Pages statiques (contrôleur "Pages")
 */
Router::connect('/:language/guide/soon', array('controller' => 'pages', 'action' => 'soon'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/guide/levels-grid', array('controller' => 'pages', 'action' => 'levels_grid'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/guide/self-assessment', array('controller' => 'pages', 'action' => 'self_assessment'), array('language'=>'[a-z]{2}'));
Router::connect('/:language/pages/*', array('controller' => 'pages', 'action' => 'display'), array('language'=>'[a-z]{2}'));

/**
 * Défaut
 */
Router::connect('/:language/:controller/:action/*', array(), array('language'=>'[a-z]{2}'));

