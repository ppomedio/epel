
                <article><?php
                    echo $this->element('user/epel-intro', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week'))); ?>

                </article>
                <aside><?php
                    if (isset($maintenance)) { ?>

                    <div>
                        <h2 class="alert"><?php __d('information', "Maintenance"); ?></h2>
                        <div class="alert">
                            <p><?php __d('information', "Some maintenance operations are being performed."); ?></p>
                            <p><?php __d('information', "Please come back later."); ?></p>
                        </div>
                    </div><?php
                    } else { ?>

                    <div id="login">
                        <h2><?php __("Login") ?></h2>
                        <?php
                        echo $this->Form->create('User', array(
                            'action' => 'login',
                            'class' => ''
                            ));
                        echo $validation->bind('User'); // Validation jQuery
                        ?>

                        <fieldset>
                            <?php
                            echo $this->Form->input('username',
                                    array(
                                        'label' => __("Username", true),
                                        'maxLength' => 20
                                        )
                                    );
                            echo $this->Form->input('password',
                                    array(
                                        'label' => __("Password", true),
                                        'default'=> '',
                                        'type' => 'password'
                                        )
                                    );
                            ?>

                        </fieldset>
                        <div id="forgotten"><?php
                            echo $this->Html->link(__("Forgotten password", true),
                                    array(
                                        'controller' => 'accounts',
                                        'action' => 'forgotten'
                                    ),
                                    array(
                                        'class' => 'iframe forgotten'
                                    )
                            );
                            echo '<br />' . $this->Html->link(__("Account creation", true),
                                    array(
                                        'controller' => 'accounts',
                                        'action' => 'create'
                                    ),
                                    array(
                                        'class' => 'iframe create'
                                    )
                            ); ?>

                        </div>

                        <?php echo $this->Form->end(__("Login", true)); ?>

                    </div><?php
                    }
                    echo $this->element('user/epel-info', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week'))); ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.dpassword',
        'jquery.crossfade'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Affichage du formulaire de login
    if ($.cookies.test())
        $('#login').fadeIn('fast');

    // Tips
    addTipInfo('aside');
    addTipAlert('aside');

    // Champs type:password
    $('input[type=password]').dPassword({
        duration    : 700,
        replacement : '%u25CF'
    });

    // Fancybox nouveau mot de passe
    $('a.iframe.forgotten').fancybox({
        'width'             :   450,
        'height'            :	195,
        'padding'           :	0,
        'margin'            :	0,
        'speedIn'           :	900,
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })

    // Fancybox création de compte
    $('a.iframe.create').fancybox({
        'width'             :   450,
        'height'            :	260,
        'padding'           :	0,
        'margin'            :	0,
        'speedIn'           :	900,
        'centerOnScroll'    :   true,
        'overlayColor'      :	'#000',
        'overlayOpacity'    :	0.3
    })

");

?>