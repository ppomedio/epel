<?php

/**
 * {app}/models/activity.php
 */
class Activity extends AppModel {

    var $name = 'Activity';
    var $useDbConfig = 'epel';


    /**
     * Enregistrement d'une action
     *
     * (méthode appelée depuis logThis dans app_controller.php)
     * @param <array> $data
     */
    function logThis($data) {

        // Sérialisation des infos supplémentaires
        if (is_array($data['extra']))
            $data['extra'] = serialize($data['extra']);

        // Ajout de la date et de l'heure
        $data['datetime'] = date('Y-m-d H:i:s');

        // Enregistrement
        $this->set($data);
        $this->save();

        // Limitation du nombre d'enregistrements
        if (($limit = Configure::read('epel.log.limit'))) {
            if ($limit > 10)
                $limit = 100000;
            else
                $limit = $limit * 10000;
            $lastID = $this->getLastInsertID();
            if ($lastID > $limit - 1) {
                $this->cleanLog($lastID - ($limit / 2));
            }
        }

    }


    /**
     * Maintien de la table à moins du nombre d'enregistrements défini dans
     * le fichier de configuration par réindexation de la moitié de ce
     * nombre parmi les plus récents et suppression des autres.
     *
     * @param <int> $keep   Nombre d'enregistrements à conserver
     */
    function cleanLog($keep) {

        // Suppression de la limite mémoire PHP
        $phpMemLimit = ini_get('memory_limit');
        ini_set('memory_limit', '-1');

        // Chargement en mémoire des tuples à conserver
        $result = $this->find('all', array('conditions' => array(
                        'id >' => $keep
                        )));

        // Raz de la table
        #$dataBase = & ConnectionManager::getDataSource($this->useDbConfig);
        $dataBase = & ConnectionManager::getDataSource('default');
        $dataBase->truncate('activities');

        // Formatage
        $logs = array();
        $count = 0;
        foreach ($result as $key => $value) {
            $logs['Activity'][$key] = $value['Activity'];
            $logs['Activity'][$key]['id'] = $count++;
        }

        // Enregistrement
        $this->saveAll($logs['Activity'], array(
            'callbacks' => false,
            'validate' => false,
            'atomic' => true,
            'fieldlist' => array(
                'datetime',
                'user_id',
                'role',
                'university_id',
                'action',
                'extra',
                'ip'
            )
        ));

        // Restauration de la limite mémoire
        ini_set('memory_limit', $phpMemLimit);

    }


}

