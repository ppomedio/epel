<?php

$paginator->options(array(
    'update' => '#objective'
    ));

?>

                        <div id="objective" class="table"><?php
                            if (empty($user)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th><?php __d('portfolio', "Summarising and reflecting on my progress"); ?></th>
                                        <th class="controls">
                                            <span id="objective-add" class="add"></span>
                                            <span id="objective-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3">
                                             <p class="alert"><?php
                                             __d('portfolio', "There is no objective recorded in your Biography at the moment.");
                                             // Alert Tips -> Buffer Js
                                             $this->Js->Buffer("addTipAlert('#objective');");
                                             ?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __d('portfolio', "Description"); ?></th>
                                        <th><span id="objective-progress" class="help"><?php __d('portfolio', "Summarising and reflecting on my progress"); ?></span></th>
                                        <th class="controls">
                                            <span id="objective-add" class="add"></span>
                                            <span id="objective-toggle" class="gear"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $trClass = 'even';
                                    foreach($user['PoObjective'] as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                    ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td id="objective-description_<?php echo $value['id']; ?>"><?php echo nl2br($value['description']); ?></td>
                                        <td id="objective-progress_<?php echo $value['id']; ?>"><?php echo nl2br($value['progress']); ?></td>
                                        <div id="hidden-info_<?php echo $value['id']; ?>" class="hidden">
                                            <span class="description hidden"><?php
                                            echo $value['description'];
                                            ?></span>
                                            <span class="progress hidden"><?php
                                            echo $value['progress'];
                                            ?></span>
                                        </div>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['id']; ?>" class="controls">
                                                <span id="objective-edit_<?php echo $value['id']; ?>" class="edit"></span>
                                                <span id="objective-supp_<?php echo $value['id']; ?>" class="supp"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="data_objective-edit_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Edit this objective");
                                    ?></div>
                                    <div id="data_objective-supp_<?php echo $value['id']; ?>" class="inline tooltip-data"><?php
                                        __d('portfolio', "Delete this objective");
                                    ?></div>
                                    <div id="objective-supp-confirm_<?php echo $value['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                        __d('portfolio', "Please confirm delete this objective.");
                                    ?></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'objective'));
                            } ?>

                            <div id="data_objective-progress" class="tooltip-data"><?php  __d('portfolio', "Have you reached the objective you set? What did you learn while trying to reach it, whether about this language or about the way you learn?");  ?></div>
                            <div id="data_objective-add" class="tooltip-data"><?php __d('portfolio', "Add a new objective"); ?></div>
                            <div id="data_objective-toggle" class="tooltip-data"><?php __("Shift edition panel"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // État initial pictos thead
    if ($('#objective-edit-form #PoObjectiveProgress').val() == ''
            && $('#objective-edit-form #PoObjectiveDescription').val() == '') {
        if ($('#objective-edit').is(':hidden'))
            $('#objective-toggle').hide();
        else
            $('#objective-add').hide();
    }
    // Tooltips
    tooltipHelp('#objective table thead');
    tooltipAdd('#objective table thead th.controls');
    tooltipGear('#objective table thead th.controls');
    tooltipEdit('#objective table tbody td.controls div.controls');
    tooltipSupp('#objective table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Bascule panneau d'édition
    $('#objective-toggle').live('click', function() {
        if ($('#objective-edit-form #PoObjectiveId').val() == '')
            $('#objective-edit > h2').text('" . addslashes(__d('portfolio', "Add a new objective", true)) . "');
        if ($('#objective-edit').is(':hidden'))
            $('#objective-edit').slideDown('slow');
        else {
            $('#objective-edit').slideUp('slow');
            $('#objective-add').fadeIn('slow');
            if ($('#objective-edit-form #PoObjectiveProgress').val() == ''
                    && $('#objective-edit-form #PoObjectiveDescription').val() == '')
                $('#objective-toggle').hide();
        }
    });
    // Ajout d'un objectif
    $('#objective-add').live('click', function() {
        $('#objective-edit > h2').text('" . addslashes(__d('portfolio', "Add a new objective", true)) . "');
        // Initialisation du formulaire
        initForm();
        if ($('#objective-edit').is(':hidden')) {
            $('#objective-edit').slideDown('slow');
        }
        $('#objective-add').hide();
        $('#objective-toggle').fadeIn('slow');
    });
    // Édition d'un objectif
    $('#objective table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#objective-edit > h2').text('" . addslashes(__d('portfolio', "Edit an objective", true)) . "');
        // Chargement des données dans le formulaire
        $('#objective-edit-form #PoObjectiveId').val(itemId);
        $('#objective-edit-form #PoObjectiveDescription').val($('#hidden-info_' + itemId + ' .description').text());
        $('#objective-edit-form #PoObjectiveProgress').val($('#hidden-info_' + itemId + ' .progress').text());
        // Affichage si masqué
        if ($('#objective-edit').is(':hidden')) {
            $('#objective-add').hide();
            $('#objective-edit').slideDown('slow');
            $('#objective-add').fadeIn('slow');
            $('#objective-toggle').fadeIn('slow');
        } else {
            if ($('#objective-add').is(':hidden'))
                $('#objective-add').fadeIn('slow');
        }
    });
    // Suppression d'un objectif
    $('#objective table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#objective-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
					var extract = new Array();
					extract = $(this).attr('id').split('_');
					var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#objective').load('/" . $this->params['language'] . "/student/biography/delete/" . urlencode($language) . "/objective/' + itemId, function() {
                            if ($('#objective-edit-form #PoObjectiveId').val() == itemId) {
                                $('#objective-edit > h2').text('" . addslashes(__d('portfolio', "Add a new objective", true)) . "');
                                initForm();
                                if ($('#objective-edit').is(':hidden'))
                                    $('#objective-toggle').hide();
                                else
                                    $('#objective-add').hide();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__d('portfolio', "Your Biography has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });


    });
");
} else {
    $this->Js->Buffer("
    // Supplément Ajax
    $('#title-objective').html('" . addslashes(__d('portfolio', "My next objective", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}
?>