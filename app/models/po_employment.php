<?php

/**
 * {app}/models/po_employment.php
 */
class PoEmployment extends AppModel {

    var $name = 'PoEmployment';
    var $useDbConfig = 'epel';
    // Comportements
    var $actsAs = array('CleanData');
    // Relations
    var $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );


    /**
     * Règles de validation définies dans le constructeur
     * sinon pas de traduction des messages avec js_validate
     * (traduits dans les fichiers validation.po)
     *
     * @return nothing
     * @access public
     */
    public function __construct() {

        $this->validate = array(
            'name' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please define type of competency.", true)
            ),
            'description' => array(
                'rule' => array('notEmpty'),
                'message' => __d('validation', "Please describe competency.", true)
            )
        );

        parent::__construct();

    }


}

