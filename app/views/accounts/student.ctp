
        <h2><?php echo __("Account creation", true) . ' &bull; ' . __("Student", true); ?></h2>
        <?php
        echo $this->Form->create('Student', array(
            'url' => '/' . Configure::read('Config.langCode')
                . '/account/student',
            'id' => 'profile-form',
            #'class' => ''
            ));
        echo $validation->bind('Student'); // Validation jQuery
        ?>

        <fieldset>
        <?php
        $available = Configure::read('epel.locales.available');
        $dateNum = $available[$this->params['language']]['dateNum'];
        echo $this->Form->input('secret', array(
            'label' => __("Account creation code", true)
            )); ?>

        <p class="info"><?php __("This code was given to you by your referring teacher."); ?></p>
        <?php
        echo $this->Form->input('group', array(
            'label' => __("Group code", true)
            ));
        echo $this->Form->input('name', array(
            'label' => __("Name", true)
            ));
        echo $this->Form->input('firstname', array(
            'label' => __("First name", true)
            ));
        echo $this->Form->input('birth', array(
            'type' => 'date',
            'label' => __("Date of birth", true),
            'dateFormat' => $dateNum,
            'minYear' => date('Y') - 76,
            'maxYear' => date('Y') - 16,
            'empty' => true,
            'separator' => ' '
            ));
        echo $this->Form->input('origin_country_id', array(
            'label' => __("Country of origin", true),
            'options' => $originCountry,
            'empty' => "…"
            ));
        echo $this->Form->input('living_country_id', array(
            'label' => __("Country of residence", true),
            'options' => $livingCountry,
            'empty' => "…"
            ));
        echo $this->Form->input('diploma_id', array(
            'label' => __("Level of validated studies", true),
            'options' => $diploma,
            'default' => '99'
            )); ?>
        <p class="info"><?php
        $levelDocs = Configure::read('epel.leveldesc.url');
        if (isset($levelDocs[$this->params['language']]))
            $levelDoc = $levelDocs[$this->params['language']];
        else
            $levelDoc = $levelDocs['en'];
        __("To help you define your European Qualification Framework (EQF) level of study, you can use the");
        ?> <a id="<?php echo __("Download the", true) . " " . __("EQF levels descriptors", true); ?>" class="link" href="<?php echo $levelDoc; ?>"><?php __("EQF levels descriptors"); ?></a>.</p>
        <?php
        echo $this->Form->input('domain_id', array(
            'label' => __("Field of study", true),
            'options' => $domain,
            'empty' => array('0001' => __("Other…", true))
            ));
        ?>

        </fieldset>
        <?php echo $this->Form->end(__("Save", true)); ?>

<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Redimensionnement
    parent.$('#fancybox-inner').height(565);
    parent.$('#fancybox-wrap').height(585);
    parent.$('#fancybox-content').height(585);
    parent.$.fancybox.center();
    //parent.$.fancybox.close();
    // Tips
    addTipInfo('#profile-form');
    // Tooltips
    tooltipLink('#profile-form p.info');
");

?>
