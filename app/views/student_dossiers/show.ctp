
                <article id="presentation">
                    <h1><?php echo $title_for_layout; ?></h1>
                    <!-- Instructions -->
                    <section>
                        <h2 id="title-show"><?php __d('portfolio', "Display"); ?></h2>
                        <div class="instruction">
                            <p><?php echo __d('portfolio', "This part of your Dossier is where you keep", true) . __(":", true); ?></p>
                            <ul>
                                <li><?php __d('portfolio', "examples of what you can do in the second/foreign languages you know,"); ?></li>
                                <li><?php __d('portfolio', "material that supports your current language level,"); ?></li>
                                <li><?php __d('portfolio', "any material that you would like to show to people willing to know about your language skills."); ?></li>
                            </ul>
                            <p><?php __d('portfolio', "Regularly review the content of the “Display” part of your dossier, removing items that no longer represent your proficiency level."); ?></p>
                            <p class="info"><?php echo __d('portfolio', "You can upload most of the common types of files (audio, video etc.) whose size does not exceed", true)
                                    . ' <span class="alerttext">' . strval(Configure::read('epel.document.size')) . '</span> ' . __d('portfolio', "MB. “Archive” file formats are allowed.", true)
                                    . ' ' . __d('portfolio', "Uploading may take time, especially for a large file and depending on the quality of your connection. Please wait until your new file appears on the list below.", true); ?></p>
                        </div>
                    </section>
                    <!-- Formulaire d'édition -->
                    <section id="show-edit" class="hidden">
                        <h2 id="title-show-edit"><?php __d('portfolio', "Edit a document"); ?></h2>
                        <?php echo $this->Form->create('PoDocument', array(
                            'url' => '/' . Configure::read('Config.langCode')
                                . '/student/dossier/show/' . urlencode($language),
                            'id' => 'show-edit-form',
                            'class' => 'full'
                            ));
                        echo $validation->bind('PoDocument'); // Validation jQuery
                        ?>

                        <p class="info"><?php
                        __d('portfolio', "If you wish, you can specify which language competency of your Passport this document certifies the level.");
                        ?></p>
                        <fieldset><?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->input('filename', array(
                            'label' => __d('portfolio', "File name", true),
                            'readonly'=>'readonly'
                            ));
                        echo $this->Form->input('competency', array(
                            'label' => __d('portfolio', "Competency certified by this document", true),
                            'options' => $competencies,
                            'empty' => array(0 => "…")
                            ));
                        echo $this->Form->input('description', array(
                            'label' => __d('portfolio', "Description", true)
                            )); ?>

                        </fieldset>
                        <?php echo $this->Form->end(__("Save", true)); ?>
                    </section>
                    <!-- Liste -->
                    <section><?php
                        echo $this->element('student/dossier/show-page'); ?>

                    </section>
                </article>
                <aside><?php
                    echo $this->element('student/part-access', array('part' => 'dossier'));
                    echo $this->element('student/dossier/side-menu');
                    echo $this->element('student/dossier/how-to-use_show', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
        'jquery.form',
        'jquery.ajaxupload',
        'jquery.ui.widget',
        'jquery.ui.position',
        'jquery.ui.dialog'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Loading…
    $('<div class=\"loading\"></div>').insertAfter('#show-edit-form div.submit input[type=submit]');
    // Tips
    addTipInfo('#presentation');
    // Soumission Ajax du formulaire
    $('#show-edit-form').submit(function() {
        if (!$('#show-edit-form .error').length) {
            $(this).ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/dossier/update/" . urlencode($language) . "/show',
                target: '#show',
                replaceTarget: true,
                beforeSubmit: function() {
                    $('#show-edit-form div.loading').show();
                },
                success: function() {
                    $('#show-edit').slideUp('slow');
                    $('#content div#ajaxMessage')
                        .text('" . addslashes(__d('portfolio', "Your Dossier has been updated.", true)) . "')
                        .show(function(){
                            $(this).delay(3600).hide('slow');
                        });
                    // Pictos thead
                    $('#show-toggle').hide();
                    $('#show-add').fadeIn('slow');
                    // Initialisation du formulaire
                    $('#show-edit-form #PoDocumentId').val('');
                    $('#show-edit-form #PoDocumentFilename').val('');
                    $('#show-edit-form #PoDocumentCompetency :selected').removeAttr('selected');
                    $('#show-edit-form #PoDocumentDescription').val('');
                    $('#show-edit-form div.loading').hide();
                    $('#show-edit-form div.submit input[type=submit]').removeAttr('disabled');
                }
            });
            return false;
        }
    });
");

?>