
                    <section>
                        <h2><?php __("Profile"); echo " (";  __("editing"); echo "…)"; ?></h2>
                        <?php echo $this->Form->create('Visitor', array(
                            'url' => '/' . Configure::read('Config.langCode') . '/visitor/base/profile',
                            'id' => 'visitor-profile-edit',
                            'class' => ''
                            ));
                        echo $validation->bind('Visitor'); // Validation jQuery
                        ?>
                        <fieldset>
                        <?php
                        echo $this->Form->input('name', array(
                            'label' => __("Name", true)
                            ));
                        echo $this->Form->input('firstname', array(
                            'label' => __("First name", true)
                            ));
                        echo $this->Form->input('function', array(
                            'label' => __("Function", true)
                            ));
                        echo $this->Form->input('structure', array(
                            'label' => __("Structure", true)
                            ));
                        echo $this->Form->input('phone', array(
                            'label' => __("Telephone", true)
                            ));
                        echo $this->Form->input('url', array(
                            'label' => __("Web site", true)
                            ));
                        echo $this->Form->input('address', array(
                            'label' => __("Address", true)
                            ));
                        echo $this->Form->input('country_id', array(
                            'label' => __("Country", true),
                            'options' => $country,
                            'empty' => "…"
                            ));
                        ?>
                        </fieldset>
                        <p class="info"><?php __("This information is indispensable so that students can contact you."); ?></p>
                        <?php echo $this->Form->end(__("Save", true)); ?>

                    </section>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.validation',
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#visitor-profile-edit');
");

?>