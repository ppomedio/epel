
                    <div id="part-access">
                        <h2><?php __("Visitors"); ?></h2><?php
                        if ($this->Session->read('Auth.User.info.vis_hide')) { ?>
                        <div>
                            <p class="alert"><?php echo __("Your Portfolio is invisible to visitors.", true); ?></p>
                            <p class="info"><?php __("You can change this setting through the “Visitors” panel of your “Base” pages."); ?></p>
                        </div><?php
                        } else if (isset($never)) { ?>
                        <div>
                            <p class="info"><?php
                            switch ($part) {
                                case 'biography':
                                    __("This part of your Biography is never visible to visitors.");
                                    break;
                                case 'dossier':
                                    __("This part of your Dossier is never visible to visitors.");
                                    break;
                                case 'assessment':
                                    __("This part of your Portfolio is never visible to visitors.");
                                    break;
                            }
                            ?></p>
                        </div><?php
                        } else if ($this->Session->read('Auth.User.info.vis_' . $part)) { ?>
                        <div><?php
                            if (isset($biography)) { ?>
                            <p><?php __("This part of your Biography is visible to visitors."); ?></p>
                            <p class="info"><?php __("You can change this setting through the “Visitors” panel of your “Base” pages."); ?></p>
                            <?php } else { ?>
                            <form id="part-access" class="white" method="POST" action="/<?php echo $this->params['language']; ?>/student/base/visitor_part_access" accept-charset="utf-8"><?php
                                    $initState = $this->Session->read('Auth.User.visibility.' . $language . '.' . $part);
                                    switch ($part) {
                                        case 'passport':
                                            echo '<p>' . __("Do you want your Passport for this language to be visible to visitors?", true) . '</p>';
                                            break;
                                        case 'biography':
                                            echo '<p class="info">' . __("The “Learning how to learn” part of the Biography is never visible to visitors.", true) . '</p>';
                                            echo '<p>' . __("Do you want your Biography for this language to be visible to visitors?", true) . '</p>';
                                            break;
                                        case 'dossier':
                                            echo '<p class="info">' . __("The Process Dossier is never visible to visitors.", true) . '</p>';
                                            echo '<p>' . __("Do you want your Dossier for this language to be visible to visitors?", true) . '</p>';
                                            break;
                                    } ?>

                                <fieldset>
                                    <dl>
                                        <dt><label for="vis_<?php echo $part; ?>"><?php echo $languageTitle; ?></label></dt>
                                        <dd><input name="vis_<?php echo $part; ?>" value="<?php echo $initState; ?>" id="vis_<?php echo $part; ?>" type="checkbox"<?php if ($initState) echo ' checked="checked"'; ?>/></dd>
                                    </dl>
                                </fieldset>
                                <div class="tail"><div class="loading"></div></div>
                            </form><?php
                            } ?></div>
                        <?php } else { ?>

                        <div>
                            <p class="alert"><?php
                            switch ($part) {
                                case 'passport':
                                    __("Your Passport is invisible to visitors.");
                                    break;
                                case 'biography':
                                    __("Your Biography is invisible to visitors.");
                                    break;
                                case 'dossier':
                                    __("Your Dossier is invisible to visitors.");
                                    break;
                            }
                            ?></p>
                            <p class="info"><?php __("You can change this setting through the “Visitors” panel of your “Base” pages."); ?></p>
                        </div>
                        <?php } ?>

                    </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipAlert('#part-access');
    addTipInfo('#part-access');
");


if (isset($initState)) {
    // Utilisation iButton & formulaire
    $this->Html->script(
        array(
            'jquery.form',
            'jquery.ibutton'
        ),
        array(
            'inline' => false,
            'once' => true
        )
    );
    $this->Js->Buffer("
    //$('#part-access fieldset dl dd input[type=checkbox]')
    $('#vis_" . $part . "')
        .iButton({
            labelOn: '" . __('YES', true) . "',
            labelOff: '" . __('NO', true) . "',
            resizeHandle: false,
            resizeContainer: true
        })
        .change(function() {
            if ($(this).is(':checked'))
                var checked = 1;
            else
                var checked = 0;
            $('#part-access').ajaxSubmit({
                url: '/" . $this->params['language'] . "/student/base/visitor_part_access',
                data: {
                    field: '" . $part . "',
                    state: checked,
                    language: '" . $language . "'
                },
                beforeSubmit: function() {
                    $('#part-access div.tail div.loading').show();
                },
                success: function() {
                    $('#part-access div.tail div.loading').hide();
                }
            });
        })
");
}

?>