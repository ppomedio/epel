
                <article>
                    <h1><?php echo $title_for_layout; ?></h1>
                    <section id="heritage-edit">
                        <div class="instruction">
                            <p><?php
                            __d('portfolio', "This page allows you to record any heritage language(s) that you know, how you learnt it/them, and how you use it/them.");
                            ?></p>
                            <p class="info"><?php
                            __d('portfolio', "Heritage languages are, for example, the languages of ethnic minorities, the languages that immigrants bring with them to a new country, and languages that are used for purposes of religious observance or in cultural festivals.");
                            ?></p>
                        </div>
                    </section>
                    <section><?php
                        echo $this->element('student/biography/heritage-form'); ?>

                    </section>
                </article>
                <aside><?php
                    if ($this->Session->read('Auth.User.portfolios'))
                        echo $this->element('student/part-access', array('part' => 'biography', 'biography' => true));
                    echo $this->element('levels-grid', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    echo $this->element('logo-coe', array('cache' => array('key' => $this->params['language'], 'time' => '+1 week')));
                    ?>

                </aside>
<?php

// Chargement des scripts utilisés
$this->Html->script(
    array(
        'jquery.form'
    ),
    array(
        'inline' => false,
        'once' => true
    )
);


/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#heritage-edit');
");

?>