��    &      L  5   |      P  !   Q     s          �     �     �     �     �     �     �  	                  5     C     S  
   b     m  
   �  	   �     �     �     �     �     �     �     �  
   �               1     =     D     J     W     _  
   t  �    !   .     P     \     i     x     }     �     �     �     �     �     �     �               -     ?     G     a     m     u     �     �  	   �     �     �     �     �     �     �     	     	     "	     (	     5	     >	     T	         
                    #      "         %                    $                                      	                     &         !                                              Accounting, Management, Economics Agriculture Architecture Army, Police Arts Audio, Video, Photo Banks, Insurance Biology, Environment Building & Public Works Business & Management Chemistry Communication Dancing, Acting, Music Documentation Hotel, Catering Human Sciences Humanities ICT, Computing, Multimedia Journalism Languages Law, Political science Marketing, Advertising Mathematics Medicine Other… Paramedical Physics Publishing Religion, Theology Retail & Distribution Secretarial Social Sport Technologies Tourism Transport, Logistics Veterinary Project-Id-Version: e-PEL
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Philippe Pomédio <philippe.pomedio@u-bordeaux4.fr>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Poedit-Language: French
X-Poedit-Country: FRANCE
X-Poedit-SourceCharset: utf-8
Plural-Forms: nplurals=2; plural=(n > 1);
 Comptabilité, Gestion, Économie Agriculture Architecture Armée, Police Arts Audio, Vidéo, Photo Banques, Assurances Biologie, Environnement BTP Commerce, Management Chimie Communication Danse, Théâtre, Musique Documentation Hôtellerie-restauration Sciences humaines Lettres Informatique, Multimédia Journalisme Langues Droit, Science politique Marketing, Publicité Mathématiques Médecine Autre… Paramédical Physique Édition Religion, Théologie Vente, Distribution Secrétariat Social Sport Technologies Tourisme Transport, Logistique Vétérinaire 