<?php

// Événements
$event = array(
    '…' => '…',
    'login_failure' => __("Login failure", true),
    'login' => __("Login", true),
    'logout' => __("Logout", true),
    'acc_create' => __("Account creation", true),
    'acc_delete' => __("Account deletion", true),
    'lang_add' => __("Language addition", true),
    'lang_delete' => __("Language deletion", true),
    'file_upload' => __("File upload", true),
    'file_delete' => __("File deletion", true),
    'email' => __("Sending of email", true)
);

// Pagination AJAX
$paginator->options(array(
    'update' => '#activity',
    'before' => $this->Js->get('#activity')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="activity" class="table"><?php
                            if (empty($activity) || !Configure::read('epel.log.on')) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __("Date"); ?></th>
                                        <th><?php __("Event"); ?></th>
                                        <th><?php __("Identity"); ?></th>
                                        <th><?php __("University"); ?></th>
                                        <th><?php echo "IP"; ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="5"><?php
                                        __("The activity log is empty or deactivated.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="date"><?php echo $paginator->sort(__("Date", true), 'Activity.datetime', array('url' => $this->params['pass'])); ?></th>
                                        <th class="event"><?php echo $paginator->sort(__("Event", true), 'Activity.action', array('url' => $this->params['pass'])); ?></th>
                                        <th class="identity"><?php echo $paginator->sort(__("Identity", true), 'Activity.user_id', array('url' => $this->params['pass'])); ?></th>
                                        <th class="university"><?php echo $paginator->sort(__("University", true), 'Activity.university_id', array('url' => $this->params['pass'])); ?></th>
                                        <th class="ip"><?php echo $paginator->sort("IP", 'Activity.ip', array('url' => $this->params['pass'])); ?></th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($activity as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>

                                    <tr class="<?php echo $trClass; ?>">
                                        <td class="smaller"><?php
                                        echo $this->Date->Convert($value['Activity']['date']) . '<br />' . $value['Activity']['time'] . date(' (e)');
                                        ?></td>
                                        <td class="smaller"><?php
                                        echo $event[$value['Activity']['action']];
                                        if (($value['Activity']['action'] == 'login_failure'))
                                            echo '<br /><span class="smaller">(' . __($value['Activity']['extra'], true) . ')</span>';
                                        else if (($value['Activity']['action'] == 'file_upload') || ($value['Activity']['action'] == 'file_delete'))
                                            echo '<br /><span class="smaller">(' . $value['Activity']['extra'] . ')</span>';
                                        else if (($value['Activity']['action'] == 'lang_add') || ($value['Activity']['action'] == 'lang_delete'))
                                            echo '<br /><span class="smaller">(' . __d('language', $value['Activity']['extra'], true) . ')</span>';
                                        ?></td>
                                        <td><?php
                                        if ($value['Activity']['user_id'] > 1) {
                                            if (!empty($value['Activity']['user_info'])) { ?>
                                            <div id="data_contact-user_<?php echo $value['Activity']['user_id'];?>" class="tooltip-data"><?php
                                            __("Send an email to");
                                            echo ' ' . $value['Activity']['user_info']['firstname']
                                                    . ' ' . $value['Activity']['user_info']['name'];
                                            ?></div><?php
                                            echo $this->Html->link($value['Activity']['user_info']['firstname']
                                                . ' ' . $value['Activity']['user_info']['name'],
                                                    array(
                                                        'controller' => 'contact',
                                                        'action' => 'form',
                                                        base64_encode($value['Activity']['user_id'] . '_'
                                                            . $value['Activity']['user_info']['firstname'] . ' '
                                                            . $value['Activity']['user_info']['name'])
                                                    ),
                                                    array(
                                                        'id' => 'contact-user_' . $value['Activity']['user_id'],
                                                        'class' => 'iframe contact link bolder'
                                                    )
                                            );
                                            } else {
                                                __("Deleted");
                                            }
                                            echo '<br /><span class="smaller">'
                                                . __(ucfirst($value['Activity']['role']), true) . ' (' . $value['Activity']['user_id'] . ')</span>';
                                        } else if ($value['Activity']['user_id'] == 1)
                                            echo __("Administrator");
                                        else
                                            echo '…';
                                        ?></td>
                                        <td class="smaller"><?php
                                        if ($value['Activity']['university_id'] != 0)
                                            echo $value['Activity']['university_info']['name'];
                                        else
                                            echo '…';
                                        ?></td>
                                        <td><?php echo $value['Activity']['ip']; ?></td>
                                    </tr><?php } ?>
                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'activity'));
                            } ?>

                            <div id="data_sort-date" class="tooltip-data"><?php __("Sort by date"); ?></div>
                            <div id="data_sort-event" class="tooltip-data"><?php __("Sort by event"); ?></div>
                            <div id="data_sort-identity" class="tooltip-data"><?php __("Sort by name"); ?></div>
                            <div id="data_sort-university" class="tooltip-data"><?php __("Sort by College / University"); ?></div>
                            <div id="data_sort-ip" class="tooltip-data"><?php __("Sort by IP address"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

// Systématiques
$this->Js->Buffer("
    // Id, classes et cie
    $('#activity table thead tr th.date a').attr('id', 'sort-date').addClass('sort-help');
    $('#activity table thead tr th.event a').attr('id', 'sort-event').addClass('sort-help');
    $('#activity table thead tr th.identity a').attr('id', 'sort-identity').addClass('sort-help');
    $('#activity table thead tr th.university a').attr('id', 'sort-university').addClass('sort-help');
    $('#activity table thead tr th.ip a').attr('id', 'sort-ip').addClass('sort-help');
    // Tooltips
    tooltipLink('#activity table tbody tr td:nth-child(3)');
    tooltipSortHelp('#activity');
    // Loading…
    $('#activity thead a').click(function() {
        $('#title-activity').html('" . __("Loading…", true) . "');
    });
");

// Pagination AJAX uniquement
if (isset($ajax)) {
    $this->Js->Buffer("
    // Rétablissement du titre
    $('#title-activity').html('" . addslashes(__("Activity log", true)) . "');
    // Fancybox (contact)
    fancyBox();
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>
