<?php

// Pagination AJAX
$paginator->options(array(
    'update' => '#elements-list',
    'before' => $this->Js->get('#elements-list')->effect('fadeTo', array('buffer' => false)),
    ));

?>

                        <div id="elements-list" class="table"><?php
                            if (empty($data)) { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __("ID"); ?></th>
                                        <th><?php __("Name"); ?></th>
                                        <th class="controls">
                                            <span id="element-add" class="add"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="alert" colspan="3"><?php
                                        __("The list is empty.");
                                        ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } else { ?>

                            <table>
                                <thead>
                                    <tr>
                                        <th class="id"><?php echo $paginator->sort(__("ID", true), 'Certification.id', array('url' => $this->params['pass'])); ?></th>
                                        <th class="name"><?php echo $paginator->sort(__("Name", true), 'Certification.name', array('url' => $this->params['pass'])); ?></th>
                                        <th class="controls">
                                            <span id="element-add" class="add"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                    $trClass = 'even';
                                    foreach ($data as $value) {
                                        if ($trClass == 'even')
                                            $trClass = 'odd';
                                        else
                                            $trClass = 'even';
                                        ?>

                                    <tr class="<?php echo $trClass; ?>"><?php
                                        if ($value['Certification']['used']) { ?>

                                        <td class="alerttext"><?php
                                        } else { ?>

                                        <td><?php
                                        }
                                        echo $value['Certification']['id']; ?></td>
                                        <td id="edit-name_<?php echo $value['Certification']['id']; ?>"><?php echo $value['Certification']['name']; ?></td>
                                        <td class="controls">
                                            <div id="loading_<?php echo $value['Certification']['id']; ?>" class="loading"></div>
                                            <div id="control_<?php echo $value['Certification']['id']; ?>" class="controls">
                                                <span id="element-edit_<?php echo $value['Certification']['id']; ?>" class="edit"></span><?php
                                                if (!$value['Certification']['used']) { ?>

                                                <span id="element-supp_<?php echo $value['Certification']['id']; ?>" class="supp"></span><?php
                                                } ?>

                                            </div>
                                        </td>
                                    </tr>
                                     <div id="hidden-info_<?php echo $value['Certification']['id']; ?>" class="hidden">
                                        <span class="description hidden"><?php
                                        echo $value['Certification']['description'];
                                        ?></span>
                                    </div>
                                    <div id="data_element-edit_<?php echo $value['Certification']['id']; ?>" class="inline tooltip-data"><?php
                                    echo __("Edit", true) . ' “' . $value['Certification']['name'] . '”';
                                    ?></div>
                                    <div id="data_element-supp_<?php echo $value['Certification']['id']; ?>" class="inline tooltip-data"><?php
                                    echo __("Delete", true) . ' “' . $value['Certification']['name'] . '”';
                                    ?></div>
                                    <div id="element-supp-confirm_<?php echo $value['Certification']['id']; ?>" class="dialog-content" title="<?php __("Confirmation"); ?>"><?php
                                    echo __("Please confirm delete", true) . ' “' . $value['Certification']['name'] . '”.';
                                    ?></div><?php
                                    } ?>

                                </tbody>
                            </table>
                            <?php
                            echo $this->element('paginate', array('subId' => 'elements-list', 'half' => true));
                            } ?>

                            <div id="data_element-add" class="tooltip-data"><?php __("Erase form (add)"); ?></div>
                            <div id="data_sort-id" class="tooltip-data"><?php __("Sort by ID"); ?></div>
                            <div id="data_sort-name" class="tooltip-data"><?php __("Sort by name"); ?></div>
                        </div>
<?php

/**
 * Ajouts au buffer Js
 *
 */

// Systématiques
$this->Js->Buffer("
    // Id, classes et cie
    $('#elements-list table thead tr th.id a').attr('id', 'sort-id').addClass('sort-help');
    $('#elements-list table thead tr th.name a').attr('id', 'sort-name').addClass('sort-help');
    // Loading…
    $('#elements-list thead a').click(function() {
        $('#title-elements-list').html('" . __("Loading…", true) . "');
    });
    // Tooltips
    tooltipSortHelp('#elements-list table thead tr th');
    tooltipAdd('#elements-list table thead th.controls');
    tooltipEdit('#elements-list table tbody td.controls div.controls');
    tooltipSupp('#elements-list table tbody td.controls div.controls');
");

// Affichage normal / pagination AJAX
if (!isset($ajax)) {
    $this->Js->Buffer("
    // Initialisation du formulaire
    function initForm() {
        $('#CertificationId').val('');
        $('#CertificationName').val('');
        $('#CertificationDescription').val('');
    };
    // Ajout d'un élément
    $('#element-add').live('click', function() {
        // Titre
        $('#title-elements-form').text('" . addslashes(__("Add", true)) . "');
        // Réinitialisation du formulaire
        $('#elements-form div.error-message').remove();
        initForm();
    });
    // Édition d'un élément
    $('#elements-list table tbody td.controls div span.edit').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        // Titre
        $('#title-elements-form').text('" . addslashes(__("Edit", true)) . "');
        // Initialisation du formulaire
        $('#elements-form div.error-message').remove();
        $('#CertificationId').val(itemId);
        $('#CertificationName').val($('#edit-name_' + itemId).text());
        $('#CertificationDescription').val($('#hidden-info_' + itemId + ' .description').text());
    });
    // Suppression d'un élément
    $('#elements-list table tbody td.controls div span.supp').live('click', function() {
        var extract = new Array();
        extract = $(this).attr('id').split('_');
        var itemId = extract[1];
        $('#divToolTip').hide();
        $('#element-supp-confirm_' + itemId).dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                '" . __('Yes', true) . "': function() {
                    var extract = new Array();
                    extract = $(this).attr('id').split('_');
                    var itemId = extract[1];
                    $(this).dialog('close');
                    $('div#control_' + itemId).hide();
                    $('div#loading_' + itemId).show();
                    $('#elements-list').load('/" . $this->params['language'] . "/admin/element/delete/certification/' + itemId, function() {
                            if ($('#elements-form #CertificationId').val() == itemId) {
                                $('#title-elements-form').text('" . addslashes(__("Add", true)) . "');
                                initForm();
                            }
                            $('#content div#ajaxMessage')
                                .text('" . addslashes(__("The certifications list", true)) . " " . addslashes(__("has been updated.", true)) . "')
                                .show(function(){
                                    $(this).delay(3600).hide('slow');
                                });
                        });
                },
                '" . __('No', true) . "': function() {
                    $(this).dialog('close');
                }
            }
        });
    });
");
} else {
    $this->Js->Buffer("
    // Tooltips
    tooltipSortHelp('#elements-list');
    // Rétablissement du titre
    $('#title-elements-list').html('" . addslashes(__("Certifications", true)) . "');
");
    // Sortie des scripts pour pagination AJAX
    echo $this->Js->writeBuffer();
}

?>