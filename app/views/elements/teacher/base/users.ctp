
                    <section>
                        <h2><?php __('Users'); ?></h2>
                        <div id="user-stats" class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php __("Students"); ?></th>
                                        <th><?php __("Teachers"); ?></th>
                                        <th><?php __("Visitors"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php
                                        echo $userCount['student']['university'];
                                        if ($userCount['student']['total'])
                                                echo ' / ' . $userCount['student']['total'];
                                        ?></td>
                                        <td><?php
                                        echo $userCount['teacher']['university'];
                                        if ($userCount['teacher']['total'])
                                                echo ' / ' . $userCount['teacher']['total'];
                                        ?></td>
                                        <td><?php echo $userCount['visitor']; ?></td>
                                    </tr>
                                </tbody>
                            </table><?php
                            if (($userCount['student']['total']) || ($userCount['teacher']['total'])) { ?>
                            <p class="info"><?php echo __("Members of the same university as you", true); ?> / <?php echo __("Total for all universities", true); ?></p><?php
                            } ?>

                        </div>
                   </section>
<?php

/**
 * Ajouts au buffer Js
 *
 */

$this->Js->Buffer("
    // Tips
    addTipInfo('#user-stats');
");

?>